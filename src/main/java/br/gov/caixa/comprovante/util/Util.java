package br.gov.caixa.comprovante.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.caixa.api.common.resource.util.ResourceUtil;
import br.gov.caixa.comprovante.constant.SinbmApiConstant;
import br.gov.caixa.comprovante.holder.DataRetorno;
import br.gov.caixa.comprovante.service.builder.ConsultaComprovanteTransferenciaBuilder;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

/**
 * 
 * @author Capgemini SP
 *
 */
public final class Util {

	protected static final Logger logger = LoggerFactory.getLogger(ConsultaComprovanteTransferenciaBuilder.class);
	
	/**
	 * 
	 */
	private Util() {
	}

	/**
	 * 
	 * @param num
	 * @return
	 */
	public static String unidadeStrNum(short num) {

		final StringBuffer strBuffer = new StringBuffer();

		if (num < 10) {
			strBuffer.append(0);
			strBuffer.append(num);
		} else {
			strBuffer.append(num);
		}

		return strBuffer.toString();
	}

	/**
	 * 
	 * @param num
	 * @return
	 */
	public static String unidadeStrNum(int num) {

		final StringBuffer strBuffer = new StringBuffer();

		if (num < 10) {
			strBuffer.append(0);
			strBuffer.append(num);
		} else {
			strBuffer.append(num);
		}

		return strBuffer.toString();
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Date stringToJavaDate(String source, String datePattern) {
		Date dateOb = null;

		try {
			dateOb = new SimpleDateFormat(datePattern, Locale.ENGLISH).parse(source);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return dateOb;
	}

	/**
	 * 
	 * @param xmlRefer
	 * @return
	 */
	public static String xmlGregToDateFormatDefBR(XMLGregorianCalendar xmlRefer) {

		SimpleDateFormat sDateFormat = new SimpleDateFormat(SinbmApiConstant.DATE_FORMAT_BR_DEFAULT);
		Date dateOb = xmlRefer.toGregorianCalendar().getTime();
		String returnStr = sDateFormat.format(dateOb);

		return returnStr;
	}

	public static void sortOrderList_Data(List<DataRetorno> lList) {

		Collections.sort(lList, new Comparator<DataRetorno>() {
			public int compare(DataRetorno o1, DataRetorno o2) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

				Date d2 = null;
				Date d1 = null;
				try {
					d1 = sdf.parse(o1.getData());
					d2 = sdf.parse(o2.getData());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return d2.compareTo(d1);
			}
		});

	}
	
	
	
	  public static XMLGregorianCalendar dateTimeToXMLGregorianCalendar(DateTime dateTime) {
		    XMLGregorianCalendar xmlGregorianCalendar = null;
		    try {
		      DatatypeFactory dataTypeFactory = DatatypeFactory.newInstance();
		      xmlGregorianCalendar = dataTypeFactory.newXMLGregorianCalendar(dateTime.toGregorianCalendar());
		    }
		    catch (DatatypeConfigurationException e) {
		      System.out.println("Exception in conversion of DateTime to XMLGregorianCalendar" + e);
		    }
		    return xmlGregorianCalendar;
	  }
		  
	  public static DateTime xmlGregorianCalendarToDateTime(XMLGregorianCalendar xmlGregorianCalendar) {
		    return new DateTime(xmlGregorianCalendar.toGregorianCalendar().getTimeInMillis());
	  }	
	
	
	  public static void ordenaListaPorDataHora(List<TransacaoInt> listaLancamento) {
	    	
	        Collections.sort(listaLancamento, new Comparator<TransacaoInt>() {
	        	
	            public int compare(TransacaoInt o1, TransacaoInt o2) {

	                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	                try {
	                    Date date1 = format.parse(o1.getData());
	                    Date date2 = format.parse(o2.getData());

	                    int retorno2 = (date2).compareTo(date1);
	                    if (retorno2 != 0) {
	                        return retorno2;
	                    }

	                    return retorno2;

	                } catch (ParseException ex) {
	                    logger.error("Erro ao ordenar a lista por data e numero do documento "+ex.getMessage(), ex);
	                    ResourceUtil.getInstance().imprimeFormatoJson(ex, "ERRO = ");
	                    
	                }

	                return 0;
	                
	            }
	        });

	    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

