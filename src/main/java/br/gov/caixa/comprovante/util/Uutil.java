package br.gov.caixa.comprovante.util;

import java.io.*;
import java.security.*;
import java.text.*;
import java.util.*;

/**
 * Classes utilitárias
 * @date 18-11-2002
 * @author FSW - 7COMm
 * @version 1.0
 */
public class Uutil {
    //-- variáveis de classe
    /**
     * Tabela invertida do código Base-64. Quando o código
     * for inválido, retorna -1.
     */
    private final static int tbInvBase64[] = {
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
        52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, 0, -1, -1,
        -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
        15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
        -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
        41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    };     
    /**
     * Tabela do código Base-64 ('A' = 0, 'B' = 1, ...)
     */
    private final static char tbBase64[] = {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
        'w', 'x', 'y', 'z', '0', '1', '2', '3',
        '4', '5', '6', '7', '8', '9', '+', '/',
    };
    /**
    * Tabela dos caracteres hexadecimais ('0' = 0, ..., 'A' = 0xA, ...)
    */
    private final static char hexDigits[] = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };

    private final static char[] tblBase32 = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'C', 'E', 'F', 'G', 'H',
        'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    };

    /**
     * Converte um array de bytes para Base-32.
     *
     * Definiremos Base-32 como sendo a conversão para base 32,
     * usando os seguintes símbolos:
     * <table border="1">
     * <tr><td>Símbolo</td><td>Valor</td></tr>
     * <tr><td>0</td><td>0</td></tr>
     * <tr><td>1</td><td>1</td></tr>
     * <tr><td>2</td><td>2</td></tr>
     * <tr><td>3</td><td>3</td></tr>
     * <tr><td>4</td><td>4</td></tr>
     * <tr><td>5</td><td>5</td></tr>
     * <tr><td>6</td><td>6</td></tr>
     * <tr><td>7</td><td>7</td></tr>
     * <tr><td>8</td><td>8</td></tr>
     * <tr><td>9</td><td>9</td></tr>
     * <tr><td>A</td><td>10</td></tr>
     * <tr><td>C</td><td>11</td></tr>
     * <tr><td>E</td><td>12</td></tr>
     * <tr><td>F</td><td>13</td></tr>
     * <tr><td>G</td><td>14</td></tr>
     * <tr><td>H</td><td>15</td></tr>
     * <tr><td>J</td><td>16</td></tr>
     * <tr><td>K</td><td>17</td></tr>
     * <tr><td>L</td><td>18</td></tr>
     * <tr><td>M</td><td>19</td></tr>
     * <tr><td>N</td><td>20</td></tr>
     * <tr><td>P</td><td>21</td></tr>
     * <tr><td>Q</td><td>22</td></tr>
     * <tr><td>R</td><td>23</td></tr>
     * <tr><td>S</td><td>24</td></tr>
     * <tr><td>T</td><td>25</td></tr>
     * <tr><td>U</td><td>26</td></tr>
     * <tr><td>V</td><td>27</td></tr>
     * <tr><td>W</td><td>28</td></tr>
     * <tr><td>X</td><td>29</td></tr>
     * <tr><td>Y</td><td>30</td></tr>
     * <tr><td>Z</td><td>31</td></tr>
     * </table><br>
     * Note que são usadas apenas letras maiúsculas, e as letras O, I, B e D foram
     * omitidas porque é muito fácil confundi-las com os dígitos 0, 1, 8 e 0. Pela
     * falta de letras, ainda pode haver a confusão 2 com Z, e 5 com S.<br>
     *
     * Cada byte da entrada contém 8 bits, e cada caracter da saída contém 5
     * bits da entrada.<br>
     *
     * Não usaremos caracteres de pad; supomos sempre que são completados com
     * bits 0.<br>
     *
     * Exemplos:
     * vazio -> vazio
     * 00 -> 0
     * bytes(hexa) 31, 41, 59, 26, 53, 62, CA, FE, BA, BE -> 650PL9LMEC5HXGPY
     *
     * @param b Array de bytes a ser convertido para Base-32.
     * @return A string convertida
     */
    public static String paraBase32(byte b[]) {
        return paraBase32 (b, b.length);
    }

    /**
     * Converte um array de bytes para Base-32.
     * @param b Array de bytes a ser convertido para Base-32.
     * @param tam O tamanho do array de bytes que deve ser convertido.
     * @return A string convertida
     */
    public static String paraBase32(byte b[], int tam) {
        int i; //-- Contador para os bytes em b
        int j; //-- Contador para os caracteres de sb
        int nBits;
        int w; //-- Temporário que contém os bits
        //-- Um pouco maior que a saída, isso não tem problema
        int outLength = ((tam + 4) / 5);
        StringBuffer out = new StringBuffer(outLength);
        for (i = j = 0, w = 0, nBits = 0; i < tam; ) {
            //-- Pegar um byte da entrada - isso adiciona 8 bits ao temporário
            if (nBits < 8) {
                w = (w << 8) | (0xFF & (i < b.length ? b[i++] : 0));
                nBits += 8;
            }
            //-- Pegar cada 5 bits e codificar um caracter
            for (; nBits >= 5; nBits -= 5) {
                out.append(tblBase32[(int)(w >> (nBits - 5)) & 0x1F]);
            }
        }
        //-- Tratamento do último grupo de bits (completando com bits 0 à direita)
        if (nBits > 0) {
            out.append(tblBase32[(int)(w << (5 - nBits)) & 0x1F]);
        }
        return out.toString();
    }

    /**
     * Converte de Base-32(string) para bytes.
     *
     * Para evitar problemas, não iremos rejeitar os caracteres não-alfabéticos,
     * e iremos converter todos os caracteres para maiúscula, e os caracteres
     * 'I', 'O', 'B' e 'D' serão entendidos como 1, 0, 8 e 0.
     * Isso será feito para que possamos usar caracteres de formatação tais como
     * ' ', '.', '-' etc.
     *
     * Exemplos:
     * 650P-L9LM-EC5H-XGPY -> bytes(hexa) 31, 41, 59, 26, 53, 62, CA, FE, BA, BE
     *
     * @param s A string Base-32 a ser convertida para Base-64
     * @return Os bytes
     */
    public static byte[] deBase32(String s) {
        int i;
        int n = 0;
        boolean isValid;
        int nValidos; //-- contagem de caracteres válidos
        char ch;

//        s = s.toUpperCase();
        for (i = 0, nValidos = 0; i < s.length(); ++i)
        {
            ch = s.charAt(i);
            if ('0' <= ch && ch <= '9' || 'A' <= ch && ch <= 'Z' || 'a' <= ch && ch <= 'z') {
                nValidos++;
            }
        }
        //-- Alocando os bytes. Note que o número de bytes do resultado
        //-- é o número de caracteres válidos, dividido por 8, multiplicado
        //-- por 5, e mais 1 byte se o resto for 2, 2 bytes se o resto
        //-- for 4, 3 bytes se o resto for 5 e 4 bytes se o resto for 7.
        int nBytes = (nValidos / 8) * 5 + (nValidos % 8 + 1) / 2; //(Vamos determinar o número correto depois)
        byte []b = new byte[nBytes];
        int w = 0;
        int nBits = 0;
        int j = 0;
        for (i = 0; i < s.length(); ++i) {
            isValid = true;
            switch ((ch = s.charAt(i))) {
                case '0': n = 0; break;
                case '1': n = 1; break;
                case '2': n = 2; break;
                case '3': n = 3; break;
                case '4': n = 4; break;
                case '5': n = 5; break;
                case '6': n = 6; break;
                case '7': n = 7; break;
                case '8': n = 8; break;
                case '9': n = 9; break;
                case 'A': case 'a': n = 10; break;
                case 'B': case 'b': n = 8; break; // Confusão com 8
                case 'C': case 'c': n = 11; break;
                case 'D': case 'd': n = 0; break; // Confusão com 0
                case 'E': case 'e': n = 12; break;
                case 'F': case 'f': n = 13; break;
                case 'G': case 'g': n = 14; break;
                case 'H': case 'h': n = 15; break;
                case 'I': case 'i': n = 1; break; // Confusão com 1
                case 'J': case 'j': n = 16; break;
                case 'K': case 'k': n = 17; break;
                case 'L': case 'l': n = 18; break;
                case 'M': case 'm': n = 19; break;
                case 'N': case 'n': n = 20; break;
                case 'O': case 'o': n = 0; break; // Confusão com 0
                case 'P': case 'p': n = 21; break;
                case 'Q': case 'q': n = 22; break;
                case 'R': case 'r': n = 23; break;
                case 'S': case 's': n = 24; break;
                case 'T': case 't': n = 25; break;
                case 'U': case 'u': n = 26; break;
                case 'V': case 'v': n = 27; break;
                case 'W': case 'w': n = 28; break;
                case 'X': case 'x': n = 29; break;
                case 'Y': case 'y': n = 30; break;
                case 'Z': case 'z': n = 31; break;
                default: isValid = false; break;
            }
            if (isValid) {
                nBits += 5; //-- cada letra/dígito junta mais 5 bits
                w = (w << 5) | n; // copiando os 5 bits para w
                if (nBits >= 8) { //-- i.e., se puder puxar um byte
                    //-- Pegamos os 8 bits mais significativos...
                    b[j] = (byte)((w >>> (nBits - 8)) & 0xFF);
                    j++;
                    //-- Limpo os bits que pegamos...
                    w &= ~(0xFF << (nBits - 8));
                    nBits -= 8;
                }
            }
        }
        return b;
    }

    public static byte[] deBase32(String s, int tam) {
        byte[] tmp = deBase32 (s);
        if (tmp.length == tam) return tmp;
        byte[] ret = new byte[tam];
        if (tmp.length < tam)
            System.arraycopy (tmp, 0, ret, 0, tmp.length);
        else
            System.arraycopy (tmp, 0, ret, 0, tam);
        return ret;
    }

    /**
    * Fuso Horário: UTC (GMT)
    */
    private static TimeZone utcTimeZone = TimeZone.getTimeZone ("GMT");
    /**
     * Formato de um ASN.1 GENERALIZED TIME
     */
    private static SimpleDateFormat generalizedTimeFormat =
            new SimpleDateFormat ("yyyyMMddHHmmss'Z'");
    /**
     * Formato de data e hora a guardar no arquivo .properties
     */
    private static SimpleDateFormat dataFormat =
            new SimpleDateFormat ("yyyyMMddHHmmss");
    //-- variáveis de instância
    //-- public
    //-- protected
    //-- private
    //-- construtores
    /**
     * Por ser esta uma classe utilitária, não tem métodos ou variáveis
     * que não sejam static. Portanto declaramos seu construtor como protected.
     * (Não declaramos como private porque isto iria tornar esta class "final"
     * o que não é o efeito desejado.)
     */
    protected Uutil() {}
    //-- métodos
    /**
     * Converte um ASN.1 Generalized Time para um java.util.Date.
     * Leva em conta o fuso horário.
     * @param generalizedTime Tempo, formato "YYYYMMDDHHMMSSZ"
     * @return java.util.Date
     */
    public static Date generalizedTimeToDate(String generalizedTime) {
        generalizedTimeFormat.setTimeZone (utcTimeZone);
        try {
            return generalizedTimeFormat.parse (generalizedTime);
        } catch (ParseException ex) {
            return null;
        }
    }
    /**
     * Converte um array de bytes em uma string Base-64
     * @param bytes O array de bytes a ser convertido
     * @return O array convertido para base-64
     * @see <a href="ftp://ftp.rfc-editor.org/in-notes/rfc1521.txt">RFC 1521</a>
     */
    public static String paraBase64(byte[] bytes) {
        int compBase64 = (bytes.length + 2) / 3 * 4;
        StringBuffer sbBase64 = new StringBuffer(compBase64);
        char[] blocoCodificado = new char[4];
        for (int pos = 0; pos < bytes.length; pos += 3) {
            codificaBloco(bytes, pos, blocoCodificado);
            sbBase64.append(blocoCodificado);
        }
        return sbBase64.toString();
    }

    /**
     * Codifica um arquivo para uma string Base-64.
     *
     * @param arq O arquivo.
     * @return O arquivo convertido para Base-64.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String paraBase64(File arq)
            throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream (arq);
        byte[] bytes = new byte[(int)arq.length()];
        fis.read(bytes);
        fis.close();
        return paraBase64(bytes);
    }
    /**
     * Codifica um bloco de até 3 bytes para um char array de 4 caracteres.
     * @param bytes Até 3 bytes a serem codificados (os restantes serão ignorados)
     * @param pos Posição no array de byets
     * @param blocoCodificado Bloco de caracteres codificado em Base-64.
     */
    private static void codificaBloco(byte[] bytes, int pos,
                                      char[] blocoCodificado) {
        int bloco24Bits;
        int i;
        int j;
        int bytesRestantes = bytes.length - pos;
        int nBytesACodificar = (bytesRestantes > 3) ? 3 : bytesRestantes;

        bloco24Bits = (bytes[pos] << 16) & 0x00FF0000;
        if (nBytesACodificar > 1)
            bloco24Bits |= (bytes[pos+1] << 8) & 0x0000FF00;
        if (nBytesACodificar > 2)
            bloco24Bits |= bytes[pos+2] & 0x000000FF;

        for (i = 0, j = 18; i < 4; ++i, j -= 6) {
            int bits6 = (bloco24Bits >>> j) & 0x3f;
            blocoCodificado[i] = codifica6Bits(bits6);
        }
        //-- Padding
        if (nBytesACodificar <= 1)
            blocoCodificado[2] = '=';
        if (nBytesACodificar <= 2)
            blocoCodificado[3] = '=';
    }

    /**
     * Codifica 6 bits segundo Base-64.
     * @param n 6 bits a serem codificados segundo a tabela tbBase64.
     * @return O valor codificado, ou '?' se não encontrado na tabela acima.
     */
    private static char codifica6Bits(int n) {
        return  (n >= 0 && n <= 63) ? tbBase64[n] : '?';
    }

    /**
     * Converte uma string em base-64 para um array de bytes <br>
     * Atenção: não valida a string. Se houver caracteres inválidos,
     * estes são simplesmente pulados.
     * @param s A string a ser convertida - veja RFC
     * @return O array de bytes.
     * @date 2003-01-16 EEW - Alterado para efetuar a conversão, no
     * máximo, até a primeira seqüência "^~^". Por algum motivo,
     * o iWS manda em certas condições esse dado duplicado no
     * header CLIENT_CERT. Para não ter problemas simplesmente
     * tratamos o caso aqui.
     */
    public static byte[] deBase64(String s) {
        int len;
        int i;
        int pos;
        int k;
        int nCaracValidos;
        int nBytes;
        int nPadChars;
        int bloco24Bits;
        byte[] bytes;
        int[] caracValidos;
        
        pos = s.indexOf ("^~^"); //-- Por que é que o iWS manda assim? ;-P
        len = (pos >= 0 ? pos : s.length());
        caracValidos = new int[len];

        //-- Aqui decodificamos e contamos os caracteres válidos...
        for (i = 0, nCaracValidos = 0; i < len; ++i) {
/*            
            char ch = s.charAt(i);
            int carac = decodificaCharBase64 (ch);
            if (carac >= 0) {
                caracValidos[nCaracValidos++] = carac;
            }
*/
            int carac;
            
            if ((carac = tbInvBase64[s.charAt(i) & 0xFF]) >= 0) {
                caracValidos[nCaracValidos++] = carac;
            }
        }
        //-- Note que o número de caracteres válidos DEVE ser múltiplo de 4.
        //-- Se não o for, iremos truncar para evitar problemas
        nCaracValidos &= ~0x03; //-- Isto faz com que o número seja múltiplo de 4
        
        //-- Contando os caracteres de pad
        for (i = len - 1, nPadChars = 0;
             i >= 0 && s.charAt(i) == '=';
             --i) {
            ++nPadChars;
        }
        //-- Cálculo do tamanho da saída
        nBytes = nCaracValidos * 3 / 4 - nPadChars;
        bytes = new byte[nBytes];

        //-- Convertendo blocos de 4 caracteres em blocos de 3 bytes
        //-- Para efetuar a conversão mais rapidamente iremos quebrar em 2
/*        
        for (i = 0, pos = 0; i < nCaracValidos; i += 4, pos += 3) {
            bloco24Bits = (caracValidos[i] << 18) | (caracValidos[i + 1] << 12)
               | (caracValidos[i + 2] << 6) | caracValidos[i + 3];
            if (pos < nBytes)
                bytes[pos]     = (byte) ((bloco24Bits >> 16) & 0xFF);
            if (pos + 1 < nBytes)
                bytes[pos + 1] = (byte) ((bloco24Bits >> 8) & 0xFF);
            if (pos + 2 < nBytes)
                bytes[pos + 2] = (byte) (bloco24Bits & 0xFF);
        }
*/
        for (i = 0, pos = 0; i < nCaracValidos - 4; i += 4, pos += 3) {
            bloco24Bits = (caracValidos[i] << 18) | (caracValidos[i + 1] << 12)
               | (caracValidos[i + 2] << 6) | caracValidos[i + 3];
            bytes[pos]     = (byte) ((bloco24Bits >> 16) & 0xFF);
            bytes[pos + 1] = (byte) ((bloco24Bits >> 8) & 0xFF);
            bytes[pos + 2] = (byte) (bloco24Bits & 0xFF);
        }
        if (i < nCaracValidos) {
            bloco24Bits = (caracValidos[i] << 18) | (caracValidos[i + 1] << 12)
               | (caracValidos[i + 2] << 6) | caracValidos[i + 3];
            if (pos < nBytes)
                bytes[pos]     = (byte) ((bloco24Bits >> 16) & 0xFF);
            if (pos + 1 < nBytes)
                bytes[pos + 1] = (byte) ((bloco24Bits >> 8) & 0xFF);
            if (pos + 2 < nBytes)
                bytes[pos + 2] = (byte) (bloco24Bits & 0xFF);
        }
            

        return bytes;
    }

    /**
     * Decodifica um caracter de uma string Base-64, retornando 6 bits
     * @param c O caracter
     * @return Os 6 bits correspondentes, ou -1 se não achar.
     *         O pad ('=') é também contado como zero
     */
    private static int decodificaCharBase64(char c) {
        return tbInvBase64[c & 0xFF];
    }
/*     
    private static int decodificaCharBase64(char c) {
        if (c >= 'A' && c <= 'Z')
            return c - 'A';
        else if (c >= 'a' && c <= 'z')
            return c - 'a' + '\032';
        else if (c >= '0' && c <= '9')
            return c - '0' + '4';
        else if (c == '+' || c == '*')
            return 62;
        else if (c == '/' || c == '|')
            return 63;
        else if (c == '=' || c == '-')
            return 0;
        return -1;
    }
*/
    /**
     * Driver de teste
     * @param args Argumentos do teste (nao usados)
     */
    public static void main(String[] args) {
        int i, j;
        boolean ok;
        byte[] b;
        byte[] b2;
        java.security.SecureRandom sr;
        try {
            sr = java.security.SecureRandom.getInstance("SHA1PRNG");
        }
        catch (NoSuchAlgorithmException ex) {
            sr = null;
        }
        //-- Primeiro teste - ver se deBase64 é o inverso de paraBase64
        ok = true;
        teste1:
        for (i = 0; i < 1000; ++i)
        {
            b = new byte[i];
            //-- testando contra a seqüencia 0, 1, 2, ... 255, 0, 1, 2, ...
            for (j = 0; j < i; ++j) b[j] = (byte)(j & 0xFF);
            b2 = Uutil.deBase64(Uutil.paraBase64 (b));
            for (j = 0; j < i; ++j) if (b2[j] != b[j]) {
                ok = false;
                break teste1;
            }
            //-- seqüência de zeros
            for (j = 0; j < i; ++j) b[j] = 0;
            b2 = Uutil.deBase64(Uutil.paraBase64 (b));
            for (j = 0; j < i; ++j) if (b2[j] != 0) {
                ok = false;
                break teste1;
            }
            //-- seqüência aleatória
            try {
                sr.nextBytes(b);
            } catch (NullPointerException ex) {
            }
            b2 = Uutil.deBase64(Uutil.paraBase64 (b));
            for (j = 0; j < i; ++j) if (b2[j] != b[j]) {
                ok = false;
                break teste1;
            }
        }
        System.out.println ("teste 1 " + (ok ? "ok" : "falhou"));

        //-- Teste de getDateProp, getIntProp, setDateProp, setIntProp
        Properties prop = new Properties();
        prop.setProperty("chave1", Integer.toString(0x12345678));
        ok = (0x12345678 == Uutil.getIntProp (prop, "chave1"));
        System.out.println ("teste 2 " + (ok ? "ok" : "falhou"));
        ok = (0x12345678 == Uutil.getIntProp (prop, "chave3", 0x12345678));
        System.out.println ("teste 3 " + (ok ? "ok" : "falhou"));
        //-- Note aqui que a data contém um valor
        Date d1 = new Date();
        Uutil.removeMs (d1);
        Uutil.setDateProp(prop, "chave2", d1);
        Date d2 = Uutil.getDateProp(prop, "chave2");
        ok = d1.equals(d2);
        System.out.println ("teste 4 " + (ok ? "ok" : "falhou"));
    }

    /**
     * Remove os milissegundos de um java.util.Date.
     * @param data Objeto java.util.Date que deve ter seus milissegundos zerados.
     */
    public static void removeMs (Date data) {
        long t = data.getTime();
        t -= t % 1000; //-- isto remove os milissegundos
        data.setTime(t);
    }
    /**
     * Retorna o valor inteiro de uma property.
     * @param prop Um objeto java.util.Properties.
     * @param chave A chave da entrada.
     * @return O valor, ou 0 se não encontrado ou o formato for inválido.
     */
    public static int getIntProp(Properties prop, String chave) {
        try {
            return Integer.parseInt(prop.getProperty(chave));
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    /**
     * Retorna o valor inteiro de uma property.
     * @param prop Um objeto java.util.Properties.
     * @param chave A chave da entrada.
     * @param valorDefault Valor default.
     * @return O valor, ou valorDefault se não encontrado,
     * ou 0 se o formato for inválido.
     */
    public static int getIntProp(Properties prop, String chave, int valorDefault) {
        try {
            return Integer.parseInt(prop.getProperty(chave,
                    Integer.toString(valorDefault)));
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    /**
     * Guardamos um objeto java.util.Date em formato YYYYMMDDHHMMSS, GMT em
     * um objeto Properties.
     * @param prop O objeto java.util.Properties
     * @param chave A chave da entrada
     * @param data O objeto java.util.Date
     */
    public static void setDateProp(Properties prop, String chave, Date data) {
        dataFormat.setTimeZone (utcTimeZone);
        prop.setProperty(chave, dataFormat.format(data));
    }

    /**
     * Recuperamos um objeto java.util.Date de um objeto Properties
     * que tem uma string em formato YYYYMMDDHHMMSS (GMT).
     * @param prop O objeto Properties.
     * @param chave Uma chave, cujo valor deve ser uma string no formato acima.
     * @return O valor, ou null se problemas.
     */
    public static Date getDateProp(Properties prop, String chave) {
        try {
            return dataFormat.parse(prop.getProperty(chave));
        } catch (ParseException ex) {
            return null;
        }
    }

    /**
     * Se receber null, retorna a string vazia, senão a própria string.
     * @param s Uma string, ou null.
     * @return s ou a string vazia, se s for null.
     */
    public static String ifNull(String s) {
        return (s == null) ? "" : s;
    }
    /**
     * Se receber null, retorna a string vazia, senão a própria string.
     * @param s Uma string, ou null.
     * @return s ou a string vazia, se s for null.
     */
    public static String ifNull(Object s) {
        return (s == null) ? "" : s.toString();
    }
    /**
     * Compara a versão (útil para determinar se alguma versão do Java)
     * Ex. comparaVersao ("1.3.1_02", "1.2") &gt; 0
     * @param ver1 Número de versão 1
     * @param ver2 Número de versão 2
     * @return 0 se iguais, um valor &lt; 0 se ver1 &lt; ver2,
     * um valor &gt; 0 se ver1 &gt; ver2
     * @todo Comparar corretamente cada subcomponente, em vez de comparar
     *       todos concatenados.
     */
    public static int comparaVersao (String ver1, String ver2) {
        return ver1.compareTo(ver2);
    }

    /**
     * Le um arquivo Properties
     * @param dir Um diretório
     * @param arq Nome do arquivo
     * @return Um objeto do tipo Properties (pode ser vazio)
     */
    public static Properties leProperties (File dir, String arq) {
        Properties prop = new Properties();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream (new File (dir, arq));
            prop.load (fis);
        } catch (IOException ex) {
        } finally {
            try { if (fis != null) fis.close(); } catch (IOException ex2) {}
        }
        return prop;
    }


    /**
     * Converte um byte array pequeno para uma string formatada em hexadecimal
     * (separada de 2 em 2 caracteres para facilitar).
     * @param bytes O byte array
     * @return O byte array, formatado como "12.34.56.78.90.AB.CD.EF"
     */
    public static String bytesToHex (byte[] bytes) {
        StringBuffer ret = new StringBuffer ();
        ret.setLength ((bytes.length > 0) ? 3 * bytes.length - 1 : 0);
        for (int i = 0, j = 0; i < bytes.length; ++i) {
            if (i > 0)
                ret.setCharAt(j++, '.');
            ret.setCharAt(j++, hexDigits[(bytes[i]>>4) & 0xF]);
            ret.setCharAt(j++, hexDigits[bytes[i] & 0xF]);
        }
        return ret.toString();
    }

    /**
     * Converte um byte array pequeno para uma string formatada em hexadecimal
     * @param bytes O byte array
     * @return O byte array, formatado como "1234567890ABCDEF"
     */
    public static String paraHexa (byte[] bytes) {
        return paraHexa (bytes, bytes.length);
    }

    /**
     * Converte um byte array pequeno para uma string formatada em hexadecimal
     * @param bytes O byte array
     * @return O byte array, formatado como "1234567890ABCDEF"
     */
    public static String paraHexa (byte[] bytes, int tam) {
        if (bytes == null) return "";
        StringBuffer ret = new StringBuffer ();
        ret.setLength (2 * tam);
        for (int i = 0, j = 0; i < tam; ++i) {
            int b = (i < bytes.length ? bytes[i] : 0);
            ret.setCharAt(j++, hexDigits[(b>>4) & 0xF]);
            ret.setCharAt(j++, hexDigits[b & 0xF]);
        }
        return ret.toString();
    }

    /**
     * Converte uma string hexadecimal em um array de bytes.
     * @param hex String hexadecimal. 
     * @param tam O tamanho em bytes.
     * Exemplo: "b617318655057264e28bc0b6fb378c8ef146be00"
     * @return O array convertido.
     */
    public static byte[] deHexa (String hex, int tam) {
        byte[] b = new byte[tam];

        for (int i = 0; i < tam; ++i) {
            if (i < hex.length() / 2)
                b[i] = (byte)(Integer.parseInt(hex.substring(i * 2, i * 2 + 2), 16) & 0xFF);
            else
                b[i] = 0;
        }
        return b;
    }
    
    /**
     * Converte uma string hexadecimal em um array de bytes.
     * @param hex String hexadecimal. 
     * Exemplo: "b617318655057264e28bc0b6fb378c8ef146be00"
     * @return O array convertido.
     */
    public static byte[] deHexa (String hex) {
        return deHexa (hex, hex.length() / 2);
    }


    /**
     * Auxiliar para podermos obter substrings, sem ocorrer
     * erros como "IndexOutOfBoundsException"
     * @param s A string
     * @param start A posição inicial (começa por zero)
     * @param length O comprimento da substring
     * @return A substring, ou "" se a string s não for grande suficiente.
     */
    public static String mid (String s, int start, int length) {
        if (s == null || start < 0 || start >= s.length()) return new String();
        if (start + length > s.length()) return s.substring(start);
        return new String (s.toCharArray(), start, length);
    }

    /**
     * Auxiliar para podermos obter substrings, sem ocorrer
     * erros como "IndexOutOfBoundsException"
     * @param s A string
     * @param start A posição inicial (começa por zero)
     * @param length O comprimento da substring
     * @param predef O valor predefinido, se o resultado for vazio
     * @return A substring, ou predef se a string s não for grande suficiente.
     */
    public static String mid (String s, int start, int length, String predef) {
        if (s == null || length == 0 || start < 0 || start + 1 >= s.length())
            return predef;
        if (start + length > s.length())
            return s.substring(start);
        return new String (s.toCharArray(), start, length);
    }

    /**
     * Se um valor já está presente para uma chave
     * em um map, efetua um "append" do valor (intercalando um sinal de " + ")
     * <p>Exemplo: <pre>
     * Map map = new HashMap (); putMap (map, "OU", "Caixa Economica Federal");
     * putMap (map, "OU", "AC CAIXA PF"); </pre>
     * <code>map.get ("OU")</code> irá retornar: <code>"Caixa Economica Federal + AC CAIXA PF"</code>
     * @param map O mapa.
     * @param chave A chave.
     * @param valor O valor.
     */
    public static void putMap (Map map, String chave, String valor) {
        if (map.containsKey (chave)) {
            map.put(chave, map.get(chave) + " + " + valor);
        } else {
            map.put(chave, valor);
        }
    }

    /**
     * Auxiliar para substituirmos uma string por outra
     * <p>Atenção - pode ser otimizada...
     * @param s
     * @param oldString
     * @param newString
     * @return
     */
    public static String replace (String s, String oldString, String newString) {
        if (oldString.length() == 0)
            return s;

        StringBuffer sb = new StringBuffer (s); //-- Retorno
        int posS, posS2; //-- Posição em s
        int posSb; //-- Posição em sb
        int sLength = s.length();
        int oldStringLength = oldString.length();
        int newStringLength = newString.length();

        posS = 0;
        posSb = 0;

        while (posS >= 0 && posS < sLength) {
            posS2 = s.indexOf (oldString, posS);
            if (posS2 >= 0) {
                posSb += posS2 - posS;
                sb.replace (posSb, posSb + oldStringLength, newString);
                posS2 += oldStringLength;
                posSb += newStringLength;
            }
            posS = posS2;
        }
        return sb.toString();
    }

    /**
     * Converte um dado java.util.Date em um java.sql.Timestamp.
     * @param dt O dado a ser convertido.
     * @return O dado convertido, ou null se dt==null.
     */
    public static java.sql.Timestamp toTimestamp (java.util.Date dt) {
        return (dt == null) ? null : new java.sql.Timestamp(dt.getTime());
    }
    
	/**
	 * Escreve um int (4 bytes) em uma OutputStream em network byte order
	 * @param baos OutputStream
	 * @param i O valor a escrever
	 * @throws IOException Não conseguiu escrever
	 */
	public static void writeInt(OutputStream os, int i) throws IOException
	{
		//-- Escrevendo em network byte order
		byte[] b4 = 
		{
			(byte) ((i >>> 24) & 0xFF), 
			(byte) ((i >>> 16) & 0xFF), 
			(byte) ((i >>> 8) & 0xFF), 
			(byte) (i & 0xFF)
		};
		os.write (b4, 0, 4);
	}

	/**
	 * Escreve um short (2 bytes) em uma OutputStream em network byte order
	 * @param os OutputStream
	 * @param s O valor a escrever
	 * @throws IOException Não conseguiu escrever
	 */
	public static void writeShort(OutputStream os, short s) throws IOException
	{
		//-- Escrevendo em network byte order
		byte[] b2 = {(byte) (s >>> 8), (byte) (s & 0xFF)};
		os.write (b2, 0, 2);
	}

	/**
	 * Lê um int (4 bytes) de uma InputStream em network byte order
	 * @param is InputStream
	 * @return O valor a ler
	 * @throws IOException Não conseguiu escrever
	 */
	public static int readInt(InputStream is) throws IOException
	{
		int res;
		byte[] b4 = new byte[4];
		is.read (b4);
		res = ((b4[0]&0xFF) << 24) | ((b4[1]&0xFF) << 16) | ((b4[2]&0xFF) << 8) | (b4[3] & 0xFF);
		return res;
	}

	/**
	 * Lê um short (2 bytes) de uma InputStream em network byte order
	 * @param is InputStream
	 * @return O valor a ler
	 * @throws IOException Não conseguiu escrever
	 */
	public static short readShort(InputStream is) throws IOException
	{
		int res;
		res = (is.read ()) & 0xFF;
		res = ((res&0xFF)<<8) | (is.read() & 0xFF);
		return (short) res;
	}
	
}

