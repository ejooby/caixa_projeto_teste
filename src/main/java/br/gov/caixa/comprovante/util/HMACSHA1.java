package br.gov.caixa.comprovante.util;

public class HMACSHA1
{
	// H (K XOR opad, H (K XOR ipad, text))
	private final static int BLOCK_LENGTH = 64;
	private final static byte IPAD = (byte) 0x36;
	private final static byte OPAD = (byte) 0x5C;
	private byte[] outputPad;
	private SHA1 digest;

	public static void main (String args[])
	{
		byte[] key = new byte[80];
		int i;
		for (i = 0; i < 80; ++i) key[i] = (byte) 0xAA;
		byte[] msg = "Test Using Larger Than Block-Size Key - Hash Key First".getBytes();
		byte[] hsh = new byte[20];
		HMACSHA1 h = new HMACSHA1 ();
		h.init (key);
		h.update (msg, 0, msg.length);
		h.doFinal (hsh, 0);
		for (i = 0; i < 20; ++i) System.out.println (Integer.toHexString (hsh[i] & 0xFF));
	}
	
	public HMACSHA1 ()
	{
		digest = new SHA1();
		
	}
	public void init (byte[] key)
	{
		byte[] inputPad = new byte [BLOCK_LENGTH];
		int i;
		if (key.length > BLOCK_LENGTH)
		{
			digest.update (key, 0, key.length);
			key = new byte[20];
			digest.doFinal (inputPad, 0);
		}
		else
		{
			System.arraycopy (key, 0, inputPad, 0, key.length);
		}
		outputPad = new byte [inputPad.length];
		System.arraycopy (inputPad, 0, outputPad, 0, inputPad.length);
		for (i = 0; i < inputPad.length; ++i)
		{
			inputPad[i] ^= IPAD;
		}
		for (i = 0; i < outputPad.length; ++i)
		{
			outputPad[i] ^= OPAD;
		}
		digest.update (inputPad, 0, inputPad.length); // H (K XOR ipad...
	}
	public void update (byte in) 
	{
		digest.update (in);
	}
	public void update (byte[] in, int inOff, int len) 
	{
		digest.update (in, inOff, len); // H ( ... text 
	}
	public int doFinal (byte[] out, int outOff)
	{
		byte[] tmp = new byte[20];
		digest.doFinal (tmp, 0);  // H ( text ... )
		digest.update (outputPad, 0, outputPad.length); // H ( K XOR opad, ...
		digest.update (tmp, 0, tmp.length);  // ... H (K XOR ipad, text )

		return digest.doFinal (out, outOff); // Resultado desejado
	}
	public void reset ()
	{
		if (outputPad != null) 
			for (int i = 0; i < outputPad.length; ++i) 
				outputPad [i] = 0;
		digest.reset();
	}
}