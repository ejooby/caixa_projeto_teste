package br.gov.caixa.comprovante.util;

import java.io.*;
import java.util.*;
import java.text.*;

/**
 * Gerador e verificador de códigos de autenticação.
 * @date 2003-02-03 EEW
 * @author FSW
 * @version 1.0
 */
public class Autenticador {
    /**
     * Gera um código de autenticação, usando HMAC-SHA1.
     * @param dados Dados a serem autenticados.
     * @param chave A chave usada para autenticação.
     * @param tam O tamanho do código de autenticação, em bits (64 a 160)
     * @throws java.lang.IllegalArgumentException
     *       - Tamanho inválido para o código de autenticação.
     * @return o código, em base-32 se tam for positivo, 
     *         e em hexadecimal, se negativo.
     */
    public static String autentica(String dados, String chave, int tam)
            throws java.lang.IllegalArgumentException {
        try {                
            return autentica(dados.getBytes("UTF8"), chave.getBytes("UTF8"), tam);                
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Gera um código de autenticação, usando HMAC-SHA1.
     * @param dados Dados a serem autenticados.
     * @param chave A chave usada para autenticação.
     * @param tam O tamanho do código de autenticação, em bits (64 a 160)
     * @throws java.lang.IllegalArgumentException
     *       - Tamanho inválido para o código de autenticação.
     * @return o código, em base-32 se tam for positivo, 
     *         e em hexadecimal, se negativo.
     */
    public static String autentica(String dados, byte[] chave, int tam)
            throws java.lang.IllegalArgumentException {
        try {                
            return autentica(dados.getBytes("UTF8"), chave, tam);
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalArgumentException();
        }
    }
    /**
     * Gera um código de autenticação, usando HMAC-SHA1.
     * @param dados Dados a serem autenticados (byte array)
     * @param chave A chave usada para autenticação (byte array)
     * @param tam O tamanho do código de autenticação, em bits (64 a 160)
     * @throws java.lang.IllegalArgumentException
     *       - Tamanho inválido para o código de autenticação.
     * @return o código, em base-32 se tam for positivo, 
     *         e em hexadecimal, se negativo.
     */
    public static String autentica(byte[] dados, byte[] chave, int tam)
            throws java.lang.IllegalArgumentException {
        //-- 64 <= tam <= 160 ou -160 <= tam <= -64
        if (tam > 160 || tam < -160 || (tam < 64 && tam > -64))
            throw new IllegalArgumentException ("|tam| deve estar entre 64 e 160");
        HMACSHA1 hmac = new HMACSHA1();
        byte[] hash = new byte[20];
        hmac.init(chave);
        hmac.update(dados, 0, dados.length);
        hmac.doFinal(hash, 0);
        if (tam > 0) 
            return Uutil.paraBase32 (hash, tam / 8);
        else {
            tam = -tam;
            return Uutil.paraHexa (hash, tam / 8);            
        }
    }
           
    /** 
     * Verifica um código de autenticação gerado pelo método autentica().
     * @param dados Dados que devem ser verificados.
     * @param chave A chave usada para autenticação.
     * @param autent O código que deve ser verificado (em base-32 se 
     *        tam for positivo, e em hexadecimal, se tam for negativo.
     * @param tam O tamanho em bits do código de autenticação (64 a 160)
     * @throws java.lang.IllegalArgumentException
     *       - Tamanho inválido para o código de autenticação.
     * @return OK, ERRO_FORMATO ou ERRO_AUTENT.
     */
    public static int verifica(String dados, String chave, String autent, int tam)
            throws java.lang.IllegalArgumentException {
        try {                
            return verifica(dados.getBytes("UTF8"), chave.getBytes("UTF8"), autent, tam);
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalArgumentException();
        }
    }

    /** 
     * Verifica um código de autenticação gerado pelo método autentica().
     * @param dados Dados que devem ser verificados.
     * @param chave A chave usada para autenticação.
     * @param autent O código que deve ser verificado (em base-32 se 
     *        tam for positivo, e em hexadecimal, se tam for negativo.
     * @param tam O tamanho em bits do código de autenticação (64 a 160)
     * @throws java.lang.IllegalArgumentException
     *       - Tamanho inválido para o código de autenticação.
     * @return OK, ERRO_FORMATO ou ERRO_AUTENT.
     */
    public static int verifica(String dados, byte[] chave, String autent, int tam)
            throws java.lang.IllegalArgumentException {
        try {                
            return verifica(dados.getBytes("UTF8"), chave, autent, tam);
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalArgumentException();
        }
    }

    /** 
     * Verifica um código de autenticação gerado pelo método autentica().
     * @param dados Dados que devem ser verificados (byte array).
     * @param chave A chave usada para autenticação (byte array).
     * @param autent O código que deve ser verificado (em base-32 se 
     *        tam for positivo, e em hexadecimal, se tam for negativo.)
     * @param tam O tamanho em bits do código de autenticação (64 a 160)
     * @throws java.lang.IllegalArgumentException
     *       - Tamanho inválido para o código de autenticação.
     * @return OK, ERRO_FORMATO ou ERRO_AUTENT.
     */
    public static int verifica(byte[] dados, byte[] chave, 
                               String autent, int tam)
            throws java.lang.IllegalArgumentException {
        //-- 64 <= tam <= 160 ou -160 <= tam <= -64
        if (tam > 160 || tam < -160 || (tam < 64 && tam > -64))
            throw new IllegalArgumentException ("|tam| deve estar entre 64 e 160");

        HMACSHA1 hmac = new HMACSHA1();
        byte[] hash1 = new byte[20];
        byte[] hash2;
        
        tam /= 8; //-- Dividindo por 8
        if (tam > 0)
            hash2 = Uutil.deBase32 (autent, tam);
        else {
            tam = -tam;
            hash2 = Uutil.deHexa (autent, tam);
        }

        int i;
        
        hmac.init(chave);
        hmac.update(dados, 0, dados.length);
        hmac.doFinal(hash1, 0);
        
        if (hash1.length < tam || hash2.length < tam)
            return ERRO_FORMATO;

        for (i = 0; i < tam; ++i)
            if (hash1[i] != hash2[i])
                return ERRO_AUTENT;

        return OK;                
    }  
    /** OK */
    public static final int OK = 0;
    /** A autenticação não bateu. */
    public static final int ERRO_AUTENT = 1;
    /** Erro no formato do dado - não está em base-32 */
    public static final int ERRO_FORMATO = 2;

/* 
    //-- Remova o comentário para efetuar o teste.
    
    private static byte[] conv(int val, int n) {
        byte[] b = new byte[n];
        Arrays.fill (b, (byte)val);
        return b;
    }

    private static String formata(int i) {
        //--           012 456
        //-- Exemplo. "999.999", "      0"
        char[] buf = new char[7];
        buf [6] = (char)('0' + (i % 10)); i /= 10;
        if (i != 0) { buf[5] = (char)('0' + (i % 10)); i /= 10; } else buf[5] = ' ';
        if (i != 0) { buf[4] = (char)('0' + (i % 10)); i /= 10; } else buf[4] = ' ';
        if (i != 0) { buf[3] = '.'; buf[2] = (char)('0' + (i % 10)); i /= 10; } else { buf[3] = ' '; buf[2] = ' '; }
        if (i != 0) { buf[1] = (char)('0' + (i % 10)); i /= 10; } else buf[1] = ' ';
        if (i != 0) { buf[0] = (char)('0' + (i % 10)); i /= 10; } else buf[0] = ' ';
        
        return String.valueOf(buf);
    }

    public static void main(String[] args) throws Exception {
        //-- RFC 2202 Test Cases for HMAC-MD5 and HMAC-SHA-1
        System.out.println ("----- Testes do RFC 2202 -----");
        System.out.println (autentica ("Hi There".getBytes("UTF8"), conv(0x0B, 20), 160));
        System.out.println (verifica ("Hi There".getBytes("UTF8"), conv(0x0B, 20), Util.paraBase32(Util.deHexa("b617318655057264e28bc0b6fb378c8ef146be00")), 160));
        
        System.out.println (autentica ("what do ya want for nothing?", "Jefe", 160));
        System.out.println (verifica ("what do ya want for nothing?", "Jefe", Util.paraBase32(Util.deHexa("effcdf6ae5eb2fa2d27416d5f184df9c259a7c79")), 160));
        
        System.out.println (autentica (conv(0xDD, 50), conv (0xAA, 20), 160));
        System.out.println (verifica (conv(0xDD, 50), conv (0xAA, 20), Util.paraBase32(Util.deHexa("125d7342b9ac11cd91a39af48aa17b4f63f175d3")), 160));

        System.out.println (autentica (conv(0xCD, 50), Util.deHexa("0102030405060708090a0b0c0d0e0f10111213141516171819"), 160));
        System.out.println (verifica (conv(0xCD, 50), Util.deHexa("0102030405060708090a0b0c0d0e0f10111213141516171819"), Util.paraBase32(Util.deHexa("4c9007f4026250c6bc8414f9bf50c86c2d7235da")), 160));

        System.out.println (autentica ("Test With Truncation".getBytes("UTF8"), conv (0x0C, 20), 160));
        System.out.println (verifica ("Test With Truncation".getBytes("UTF8"), conv (0x0C, 20), Util.paraBase32(Util.deHexa("4c1a03424b55e07fe7f27be1d58bb9324a9a5a04")), 160));

        System.out.println (autentica ("Test Using Larger Than Block-Size Key - Hash Key First".getBytes("UTF8"), conv(0xAA, 80), 160));
        System.out.println (verifica ("Test Using Larger Than Block-Size Key - Hash Key First".getBytes("UTF8"), conv(0xAA, 80), Util.paraBase32(Util.deHexa("aa4ae5e15272d00e95705637ce8a3b55ed402112")), 160));

        System.out.println (autentica ("Test Using Larger Than Block-Size Key and Larger Than One Block-Size Data".getBytes("UTF8"), conv(0xAA, 80), 160));
        System.out.println (verifica ("Test Using Larger Than Block-Size Key and Larger Than One Block-Size Data".getBytes("UTF8"), conv(0xAA, 80), Util.paraBase32(Util.deHexa("e8e99d0f45237d786d6bbaa7965c7808bbff1a91")), 160));

        System.out.println (autentica ("Test Using Larger Than Block-Size Key - Hash Key First".getBytes("UTF8"), conv(0xAA, 80), 160));
        System.out.println (verifica ("Test Using Larger Than Block-Size Key - Hash Key First".getBytes("UTF8"), conv(0xAA, 80), Util.paraBase32(Util.deHexa("aa4ae5e15272d00e95705637ce8a3b55ed402112")), 160));
        
        //-- Alguns testes mais palpáveis
        System.out.println ("--------- Teste -----------");
        String chave = Cifra.decifra("fA0AwNfm+pJRNcwh9Pr0N5wKLCWBzq5LFwwAavDjoXLHkGNeQJfyIvgIh/FaZbeAfWT41vMnnKCVzdSMxDf4JzNOr6xX3CeSWLxi1jEX8p7d52ycg0iJe8lMg+1k+i52+lYW0vMgM+8oALS16xxep0+QMMI0edK/fJ81t7XuLDSMvSJ++tCTNB7kaoCK0OfMKIg7zNJbKWFNIq7Jl44onMcc5wt5L9C06QhqFemLRX+Kr10nlSGc5wev/hzi6yRD");
        System.out.println ("A chave é:");
        System.out.println (chave);
        System.out.println ("Código de autenticação");
        System.out.println ("Hexadecimal: " + autentica ("Saldo atual: R$ 123.456,00||20031201", chave, -160));
        System.out.println ("Base-32: " + autentica ("Saldo atual: R$ 123.456,00||20031201", chave, 160));
        System.out.println ("Base-32: " + autentica ("Saldo atual: R$ 123.456,00||20031201", chave, 80));
        System.out.println ("Base-32: " + autentica ("Saldo atual: R$ 123.456,00||20031201", chave, 64));
        System.out.println ("Verificando...");
        System.out.println (verifica ("Saldo atual: R$ 123.456,00||20031201", chave, "KSMMCZML1SFEM5MWXRA99TP2", 120));
        System.out.println ("Verificando...");
        System.out.println (verifica ("Saldo atual: R$ 123.456,00||20031201", chave, "KSMMCZML1SFEM5MWXRA99TP2", 120));
        System.out.println (verifica ("Saldo atual: R$ 123.456,00||20031201", chave, "8E2735FE720E1AC9967CEDD494E6A2750AE19BC4", -120));
        System.out.println ("Texto alterado (valor diferente):");
        System.out.println (verifica ("Saldo atual: R$ 123.456,00||20031201", chave, "KSMMCZML1SFEM5MWXRA99TP2", 120));
        System.out.println ("Texto alterado (data diferente):");
        System.out.println (verifica ("Saldo atual: R$ 123.456,00||20031202", chave, "KSMMCZML1SFEM5MWXRA99TP2", 120));
        System.out.println ("Código de autenticação incorreto (letra trocada):");
        System.out.println (verifica ("Saldo atual: R$ 123.456,00||20031201", chave, "KSMMCZML1SFEM5MWXRA99TPZ", 120));
        System.out.println ("Código de autenticação incorreto (truncado):");
        System.out.println (verifica ("Saldo atual: R$ 123.456,00||20031201", chave, "KSMMCZML1SFEM5MWXRA99T", 120));
        System.out.println ("Código de autenticação correto, mas formatado");
        System.out.println (verifica ("Saldo atual: R$ 123.456,00||20031201", chave, "ksmm.czmL.1sfe.m5mw-xra9-9tp2", 120));
        System.out.println ("Tentando localizar um código tal que possamos alterar o saldo com o mesmo valor de autenticção - 64 bits");
        long t = System.currentTimeMillis();
        for (int i = 0; i <= 999999; ++i) {
            if (i % 1000 == 0) System.out.print (".");            
            String saldo = "Saldo atual: R$ " + formata(i) + ",00||20031201";
            if (verifica (saldo, chave, "ksmmc-zmL1s-fem5m-wxra9-9tp2g-n5g36-y4", 160) == OK) {
                System.out.println ("\n" + saldo);
            }
        }
        System.out.println ((System.currentTimeMillis() - t) + "ms");
    }
*/    
}