package br.gov.caixa.comprovante.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Cifra e decifra dados com uma chave fixa.
 * @date 2003-02-03 EEW
 * @author FSW
 * @version 1.0
 */
public class Cifrador {
    /**
     * Este array deve ter pelo menos 256/8 = 32 bytes.
     * Deve ser um valor repetido (mas não zeros), para ser
     * facilmente substituído por um programa qualquer.
     * Obs. 0xA5 = -91
     */
    private static final byte[] bytes = {
        -91, -91, -91, -91, -91, -91, -91, -91, 
        -91, -91, -91, -91, -91, -91, -91, -91, 
        -91, -91, -91, -91, -91, -91, -91, -91, 
        -91, -91, -91, -91, -91, -91, -91, -91
    };
    
    /** Número de bits usado no AES */
    private static final int AES_KEY_BITLEN = 256;
    
    /** Número mágico nas mensagens */
    private static final short MAGIC_NUMBER = (short)0x7C0D;
    
    
    /**
     * Cifra dados. Usa o algoritmo AES com modo OCB. <br>
     * Note que como cifrar dados, neste caso, é muito
     * mais delicado e infreqüente que decifrá-los, então
     * esta operação será muito mais lenta. <br>
     * Formato dos dados em binário:
     * <table border='1'>
     * <tr><td>Posição<td>Comprimento<td>Descrição<td>Conteúdo</tr>
     * <tr><td>0<td>2<td>Número mágico<td>7C 0D</tr>
     * <tr><td>2<td>2<td>Tamanho total da mensagem<td>28 + tamanho dos dados cifrados</tr>
     * <tr><td>4<td>8<td><i>salt</i><td>Valor aleatório em claro</tr>
     * <tr><td>12<td>n<td>Dados cifrados<td>Dados cifrados em AES-256</tr>
     * <tr><td>12+n<td>16<td><i>tag</i><td>Dados de verificação do modo OCB</tr>
     * </table>
     * @param dados Dados a serem cifrados.
     * @return Dados cifrados, em base-64.
     * @throws java.security.GeneralSecurityException 
     * @see <a href="http://www.cs.ucdavis.edu/~rogaway">OCB</a>
     */
    public static String cifra(String dados) 
            throws java.security.GeneralSecurityException {
                
        SecureRandom srand = SecureRandom.getInstance ("SHA1PRNG");
        byte[] salt = new byte[8];      //-- salt
        byte[] ciphertext = null;       //-- saída (texto cifrado)
        byte[] nonce = new byte[16];    //-- nonce
        byte[] tag = new byte[16];      //-- tag
        byte[] key = new byte[32];      //-- 256 bits para AES
        byte[] plaintext = null;        //-- Dados como um array de bytes
        OcbPmac keys = new OcbPmac();   //-- Cifra
        ByteArrayOutputStream baos;     //-- Para montar os dados de saída
        String ret;            
        
        try {
            plaintext = dados.getBytes("UTF8");
        } catch (UnsupportedEncodingException ex) {
            plaintext = new byte[0];
        }
        
        try {
            //-- Geração do salt
            srand.nextBytes(salt);
//            System.out.println ("salt=" + Util.paraHexa (salt));
//            byte[] bt = "7COMm".getBytes();
//            System.out.println ("bytes=" + Util.paraHexa (bt));
            //-- Geração do nonce
            getKeyNonce(bytes, salt, key, nonce);
//            getKeyNonce(bt, salt, key, nonce);
//            System.out.println ("nonce=" + Util.paraHexa (nonce));
//            System.out.println ("key=" + Util.paraHexa (key));
            //-- Cifrando os dados com AES/OCB
            keys.init(key, AES_KEY_BITLEN);
            ciphertext = keys.encrypt(plaintext, 0, plaintext.length, 
                nonce, 0, tag, 0);
//            System.out.println ("ciphertext=" + Util.paraHexa(ciphertext));                
            //-- Alocando o buffer de baos de uma só vez...
            baos = new ByteArrayOutputStream (28 + ciphertext.length);            
            //-- Agora devemos criar um array de bytes no formato adequado.
            //-- Número mágico
            Uutil.writeShort(baos, MAGIC_NUMBER);
            //-- Comprimento
            Uutil.writeShort(baos, (short)(28 + ciphertext.length));
            //-- Salt
            baos.write(salt);
            //-- Dados cifrados
            baos.write(ciphertext);
            //-- Tag
            baos.write(tag);
            //-- Convertendo para base-64...
//            System.out.println ("message=" + Util.paraHexa(baos.toByteArray()));
            ret = Uutil.paraBase64(baos.toByteArray());
            //-- Para efeitos criptográficos é interessante limpar os dados...
            baos.reset();
            baos.write(new byte[28+ciphertext.length]);
        } catch (IOException ex) {
            throw new SecurityException ("0 - problema ao cifrar os dados");
        } finally {
            Arrays.fill(salt, (byte)0);
            if (ciphertext != null) Arrays.fill(ciphertext, (byte)0);
            if (plaintext != null) Arrays.fill(plaintext, (byte)0);
            Arrays.fill(nonce, (byte)0);
            Arrays.fill(tag, (byte)0);        
            Arrays.fill(key, (byte)0);
            //-- Vamos escrever 28+n bytes em baos (não devemos escrever mais
            //-- que isto, porque o array será realocado...)
            keys.finalize();
        }
                
        return ret;
    }

    /**
     * Decifra dados. <br>
     * Formato dos dados em binário:
     * <table>
     * <tr>Posição<td>Comprimento<td>Descrição<td>Conteúdo</tr>
     * <tr>0<td>2<td>Número mágico<td>7C 0D</tr>
     * <tr>2<td>2<td>Tamanho total da mensagem<td>28 + tamanho dos dados cifrados</tr>
     * <tr>4<td>8<td><i>salt</i><td>Valor aleatório em claro</tr>
     * <tr>12<td>n<td>Dados cifrados<td>Dados cifrados em AES-256</tr>
     * <tr>12+n<td>16<td><i>tag</i><td>Dados de verificação do modo OCB</tr>
     * </table>
     * @param dados Dados a serem decifrados, em base-64.
     * @return Dados decifrados.
     * @throws java.security.GeneralSecurityException
     */
    public static String decifra(String dados)
            throws java.security.GeneralSecurityException {
        String ret;
        try {
            ret = new String(decifra2(dados), "UTF8");
        } catch (UnsupportedEncodingException ex) {
            throw new GeneralSecurityException(
                "5 - Não conseguiu converter os bytes para uma string formato UTF-8");
        }
        return ret;
    }

    /**
     * Decifra dados. <br>
     * Formato dos dados em binário:
     * <table border='1'>
     * <tr><td>Posição<td>Comprimento<td>Descrição<td>Conteúdo</tr>
     * <tr><td>0<td>2<td>Número mágico<td>7C 0D</tr>
     * <tr><td>2<td>2<td>Tamanho total da mensagem<td>28 + tamanho dos dados cifrados</tr>
     * <tr><td>4<td>8<td><i>salt</i><td>Valor aleatório em claro</tr>
     * <tr><td>12<td>n<td>Dados cifrados<td>Dados cifrados em AES-256</tr>
     * <tr>v12+n<td>16<td><i>tag</i><td>Dados de verificação do modo OCB</tr>
     * </table>
     * @param dados - Dados a serem decifrados, em base-64.
     * @return Dados decifrados, em binário.
     * @throws java.security.GeneralSecurityException
     */
    public static byte[] decifra2 (String dados)
            throws java.security.GeneralSecurityException {

        byte[] salt = new byte[8];      //-- salt
        byte[] ciphertext = null;       //-- saída (texto cifrado)
        byte[] nonce = new byte[16];    //-- nonce
        byte[] tag = new byte[16];      //-- tag
        byte[] key = new byte[32];      //-- 256 bits para AES
        byte[] plaintext = null;        //-- Dados como um array de bytes
        int len;                        //-- Comprimento total da mensagem
        OcbPmac keys = new OcbPmac();   //-- Cifra
        ByteArrayInputStream bais;
        byte[] message = null;
        
        try {
            message = Uutil.deBase64(dados);
            bais = new ByteArrayInputStream(message);
            if (Uutil.readShort(bais) != MAGIC_NUMBER)
                throw new GeneralSecurityException(
                    "1 - String nao foi cifrada: numero magico nao encontrado");
            len = Uutil.readShort(bais);
            if (len != message.length || len < 28)
                throw new GeneralSecurityException(
                    "2 - String nao foi cifrada: comprimento invalido");
            bais.read(salt);
            ciphertext = new byte[message.length - 28];
            bais.read(ciphertext);
            bais.read(tag);
            getKeyNonce(bytes, salt, key, nonce);
            keys.init(key, AES_KEY_BITLEN);
            try {
                plaintext = keys.decrypt(
                    ciphertext, 0, ciphertext.length, nonce, 0, tag, 0);
            } catch(NullPointerException ex) {
                throw new GeneralSecurityException(
                    "3 - Erro na decifracao da string");
            }
            if (plaintext == null)
                throw new GeneralSecurityException(
                    "3 - Erro na decifracao da string");
        } catch (IOException ex) {
            throw new GeneralSecurityException(
                "4 - Erro de formato da string cifrada");
        } finally {            
            //-- Como de costume devemos limpar tudo...
            if (message != null) Arrays.fill(message, (byte)0);
            Arrays.fill(salt, (byte)0);
            if (ciphertext != null) Arrays.fill(ciphertext, (byte)0);
            Arrays.fill(nonce, (byte)0);
            Arrays.fill(tag, (byte)0);        
            Arrays.fill(key, (byte)0);
            keys.finalize();
        }
        return plaintext;
    }
    
	/**
     * A partir do password, gerar a chave e o nonce (= IV) <br>
     * A chave é um array de 32 bytes (256 bits) 
     * e o nonce é um array de 16 bytes (128 bits). <br>
     * Precisamos então gerar 48 bytes. <br>
     * Para gerar os primeiros 20 bytes, devemos calcular 
     * H1 = HMAC-SHA1 (pass, salt) <br>
     * Para gerar mais 12 bytes, calculamos 
     * H2 = HMAC-SHA1 (pass, H1) e usamos os primeiros 12 bytes de H2 <br>
     * Para gerar mais 16 bytes, calculamos 
     * H3 = HMAC-SHA1 (pass, H2) e usamos os primeiros 16 bytes de H3 <br>
     * @param pass [in] A senha.
     * @param salt [in] O "salt" (gerado por um gerador de números aleatórios, 
     *        deve ter 8 bytes)
     * @param key [out] A chave de 256 bits gerada; 
     *        (deve ser um array de 32 bytes)
     * @param nonce [out] O "nonce" (deve ser um array de 16 bytes).
	 */
	private static void getKeyNonce (
	    byte[] pass, byte[] salt, byte[] key, byte[] nonce)
	{
		HMACSHA1 hm = new HMACSHA1 ();
		byte[] h1 = new byte[20];
		byte[] h2 = new byte[20];
		byte[] h3 = new byte[20];
		//-- Calculando H1
		hm.init (pass);
		hm.update (salt, 0, salt.length);
		hm.doFinal (h1, 0);
		//-- Calculando H2
		hm.init (pass);
		hm.update (h1, 0, h1.length);
		hm.doFinal (h2, 0);
		//-- Calculando H3
		hm.init (pass);
		hm.update (h2, 0, h2.length);
		hm.doFinal (h3, 0);
//		System.out.println ("h1=" + Util.paraHexa (h1));
//		System.out.println ("h2=" + Util.paraHexa (h2));
//		System.out.println ("h3=" + Util.paraHexa (h3));
		//-- key = h1, h2[0..11]
		System.arraycopy (h1, 0, key, 0, 20);
		System.arraycopy (h2, 0, key, 20, 12);
		//-- nonce = h3[0..16]
		System.arraycopy (h3, 0, nonce, 0, 16);
	}	

    // -- Remova os comentários para efetuar um teste.
/*    
	public static void main(String args[]) throws Exception {
final String[] ciphers = {
"fA0AlsqcNVn11xdG0RoCMne3Uq3gppwVCboQUQwv8ojIn8WXYu8iQGqLYfhsfPdT/sxpU6YbKIDijGC2VAbY9e9gYxYgz6MW377F+ga2ogHJeyK7MQohduGA2UgTbCzOGE3sLqzWxDgqCsyXX14Bc/Tx0WvjJglcVAW78Kj5AEUvN3aKC7jFhEauXjLMV4UfvRXX+1XT", 
"fA0AlhEPVw/4x+NdfzMWl+2g96+oBOFiuwkNJ39QrtMihRYpT2IFUJuKHQ8NsJaOPU4xYlu5mKvIYC1ryjhiETZpspRpYnSyniMsTzLSf2wGoRgFDy/KjZ3IZffgp7EQlseXFblXIMAXKjxqsgWtehZb1WlDRHlx8vJvSNCcpk/IGQFpZShaj1qIlO5AYa7izXFgGDTf", 
"fA0AlreksGWvQwdr6BGfcMe/Y9MQg0CByRpm+CGRkFQyvrcYumdHXEjYjdhQ+KUQ9HbRSM4R7GrOK+dZbmhK9PgP38UCfMMib05Axx6cbqMyJZ9Kib//+al0pvrU3OZi2qRd4oQ+5ho2WxEVGSy/AKpT9YugE3NR2DeULr51lqdU/NZwb6Uz40FCz+wECVkJX3hwJ2wj", 
"fA0Alr14H2nXSt8R+jimzLitqrvBluZ7HEshhboXyVrSRPDYnhjzSBTmTtyrzP+lwmeKBnPGxzIszNQe6ujWeKp12Dh4ICc5GOpVsqJ5PHKU+6u/ibBypRKxTBAPj+9cpN5Ng+4NpweNLIDWY00GGrKQliRnAn1LlcRvSTGtuABV/shAJMP/w45YFwDHL+/WUle35AW6", 
"fA0AlpEqGjFDyKmQyDnXotoHGRscWpOxucTY+JiZ+Kqe/PP1PJ1OowR2Q4/WsS1vsm4ivK7h/xeMO0Rmxe2CJgULSn1Fj8ir3hyC622YfIWhGlE1LPKdL1u/SG7lBhmSqXPvtl0fZOZIVFRY9IOEwcpFSLdgGMJrpfC2a9TFKvmMyljqhgK/SmXYU2ScxlR5kplWWlez", 
"fA0AlpWDWjYOUOIAgFPlyvIzydMXVyEicp4zOaEotOzTWfasfLZzVHsCpWcoYPBQ3BPSISl7HTYyRfk5+FNhKA9o8kXAy8cdAkEX5BeN2+2QkTUBszYSqFjDMB0T/R4Sej7creGiJ6kQ7MpsouCenUGzoEGWZVrUaUShtpDLpcLQ7naqmwZ2tN50uPWm5NcGFPk2IRwB", 
"fA0AlrhQCvpxt7epDbjKh/zedgkzMSSp8wxvM+DXQBPVMBGYso/X2Z36Tvz0GWNaVrRTX5twc0PoVP69aUK29S7qKDuywH4+qd9LyHBQiGp+U00MdK7sXcVPdKgRWxj0qOT6WHJFVLn2WxHcBK6VT2cf2N4XhMrVBr19bsUvQG3UKPR3lmuoBPe+oV41tsPEWTiTJNr9", 
"fA0AlnLHnkwKdWmzjMU8XOw0vxEmEJ8oTiBHufKYVc6WjUxpZdxVl9WYEWQDN2aV4sOmIf2xd/tVFnwPb7/wYCP5/doEw41oPRTvBz+3GnWSZg06vd52mxtXA+Gn6djLMiYAgB4l/uo5jGu8/1B6vpuFQOhFKEwpn/hNX7sj04Qzev3dwuEyWBgYr/ShmImo+M80b5ZD", 
"fA0AlkAAROg+syCH6uRcwB+hqw5yHCgVT3NzORJvZMHtoni2rLHNhUoVaIZFI2iVkxCSWUciBHpDXP3Tr0L1bWSs3h5wZCWpqATzHwYEY3wDnmTYpM3DITiajRHka8Eg1O7P/QF4slc41afw3oUBDg6V3/LfoSVbTnZRxP2RY5f5/iqgATYS8n0+Si7DRGwBmTbB0UIK", 
"fA0AlncoL9l8GaiRAmLDko3GSwacqlpTgsrXoQ5z5QmISplRTziDr8Xw7PKN3DdWw2ltQ2nDDi/CnYzfYnHULAqFvwhWOIK46ySJLeY5PFNhGmqCDvxDxqn/8razy2lZj2oUB9EJbOyrM2rGmdePYsl2FxKRk6Dhmm+wbUNMUtIUVRWeIt++fu6ftj33BL5RM9xRTfF2", 
"fA0AliwWJLML275oC9Bz7j5wUyzKcN766cTkl4/iwbYGOnZGxPeu6JDWOVgnpsLHBDUmCvL9mp1wdy6eukhermtS6zgBxyzOPch/17EFtq1iI4c2z57IrzHuwmrEF5MJ3mwIBk+8UjDCwwoVBTG6pTulJX0G6BmWNHIF8hQIavGPsM07Mbt+6l1iaGE7ecEkh9vBKPeA", 
"fA0AlvjEbO7RLym2ul0Klnesl06THQej7nrYglykCyv6wzduUcIhu7w7xN6tB4+Epc3y/3FrZYAX18izYmn+F0EdSbIC3mmPz+E5uB8DAFyU08PvKzpW02d9LqeDQCt7nAj4j0RDyEfMRBsaeUudd1grsbTqNSdbB7NkVGgPA9DaK6eSicIC5j2Zy4e4jGUD6Z3KMU8U", 
"fA0AliStReQky5rE/J2fOY/JUTQZwgfy0J49cCGm/4CluUNPYd5XwS9GUn89nYIT97lfBLIB/NxAXANeS1r1c25vAzZU8xK/95Xq16bgCasvKu8G7nnSS52l3mHgZwBu/38UKIJCXrVRYji5W86Cn2D+7E6BiOnjjFVVqKhTVruE3dboFMPnRTdUXHzgAearB84fDCT4", 
"fA0AlgC6+jVz9qRQhUW2FtFZlHsI+g+Jq0nHWKiGY6jNG62ceJ42EA1MPy3iToq3hIDI9sjAONz1GmoE7cqAkhwVcEqdngtTp/OHmdTRaSrq4ud4O3MVKgNpIc73G+FnMzef+ozOOOgdxJLPLJBKZY9j2/rwY8jJuedd8Hc86K0UBAotpPimmdPKbVBtfkU3RPGx0NRF", 
"fA0AlnlR7vvvlWdR0Yhf88R7hftua8EPOn8Bies2HvijTdu773NRH6t0oHJvvHbNd1+AODFhmJOTiiMQs4jAvyfkpY6wQYabV1I4jsbLd8UKryjp64TeHRcfIGyMJKESvQlfa3YgUpO+1ViSd49JPdoJL3KcekJbL/kfWMwonjDf6Pv0SSH1KA4xzbor1hKHq88nCOCK", 
"fA0AlrRNthpvH2DtXpMOKqkjT/6PJcxCc1YwC3wDiXw8e/xdXAsy5FMtiAIpu8uxVFIRJXr2pxTI9KrUJ+Wv6/0RH7gkuWdcczoMPzQwpjHnbROkAe7mOSjDMElat+GakYt8UMGQA5EwxdKiaC+9OqhnWh8GGi6VmNRcC0uCCdylBVcMEiGKbUGByEF5Mwd94vCpERDc", 
"fA0AlrZzUj28tQfiWEcHLxy2a/dIP1YuCrQ6l5ubijnRQzpWSVhbzMJIK3t/edPuq9ihYrRC68uPkMJLaj/f2xI1GBF8Hjz/XSFDnu4WkZpj4G7c2/QxUZ93Z/fGClQQ92CsSUYYlpCf3kIdEaXv0+9uK17AwRuYAqPpUzDZj6sB5mlIL5OUm7D4MNvIPNL0EuW4Jh/Z", 
"fA0AlrNH/ysPv6i4yImhVVEa9MFzXgvg5jEP7xDdcEAhfkdXA24No0AltUHIWFPLdFW3gd0m77IxxdvDPSSwQEhvuAr2rbchQnMXB77RYroTyLP3u7PWRdtWBweSDoLlb6/frxrIAXvgDsDe0o6ypxxyeKbMIxnSwiEb06DDnURVLRG/BvkqMW7t+kYxafDN3DXoeBgd", 
"fA0AlsIj6CK7L3al6Kj2iiVd7TVCaO0wfOJx4fCglxVcVy9XwV5Nj+balbKAi/Thts+a4cyM1R/xsGGVXfdqFixUloxfApVoV5nVg9wtj0J2O+paQf89So7ODxi2xrrjASo9mWhTWbQFZX+Kea1RfL8Bf620RZjq6srKS+vryZiDxw1TmaBU9SFs2AHya5V07x7BA1Cf", 
"fA0AlvGklgKQ2bICxVL366gadEUOfKMl+3q5k2li8xE7eJrVNl1IcMlr4i8O5wQEsZSBfNJwUUeD7nNjf96ENd9reekbtj65K/1L/3YMa8H3ojI7rfZ4/+T1iorbiwiNDcTRjeK9bBnz2V2y0R9HgKUpIMdSc3H3iAO5bNGsH6rtNtgKvw029lUdvDGKDVv/jIB9eNIH", 
"fA0AlsK1xqzbnBiJ7zWqsBHyNsRW4SnhL5QgBBUPyIsCpr+U7zlDWoQIHTCxtuYfku0dYEnM6HhLN2QePmgvHA0hJhZSVZ1kQagFG/oqAZ09i8Dlqx5W9KHrMi/ojs5NgEpHuX2ueG85V+1gOgVFJV8ERtRrU00QmCwiSlbkO0TdXL0OTCsCHuPdfH7/jXZUZiaVO/OZ", 
"fA0Almk+bBOCcQitRsoSqzGpE0s78QeXNt9fGj3B0r9w2rivtMVL3AN2Ws/DfdPqlbjnMFhaOfT5sjCylBHrXh7U13LYGl1XDVMa8oZHbVjs4XZHgQkxvRYK271kKEbJ/6K30V6v6QrQL4Pj1x1Jo+J8HceaIY9hI3htP2xSSW49HO43+FbbYGEffhyWBPP8WRSCV7BL", 
"fA0AlpXD6CelxThh+CjXfK4JT+WE/AvqdXryb2lSIlapyrq29zDNwqsGftHOqRX0DkaOjxLWROuyTg+xraTdmDtPgGAi04sZMFtW9IONvlm8S6dwupFynbViD4b/HU8Z03EV8AVskiipoEFNOSqtiEyZqa7jbGci7FjwH1C6dPcPfcSsrixv7eNwolekJqSaK891xuvU", 
"fA0AliN+FiE0ZtI8JlZlP849bREhBZk9JEmkyuD0+okYjOQsOFB5k/rrXIWFXJRjW/37BLuNJtLoWCqzOnhjCtDPwE+Vpr5zfdt5NFxlF6QJ/BpUa6H7mSUKlNT4fjI9xAzBMGETEt3+8qNX9qV/lE+expk6dxvdcn/W15gbXCAqpmRNUTfPGRHKzcNsieTonRtJShv0", 
"fA0AliMdkqVlkxQfw61Ja9kZ9Ua4Rm801spPEcxNdPXxDH0nTkHOhOUWi0QJ5yGTVd7Vk/FFmQMxloVWgbJJJe8BM57y0RLeV7s2fdQ3K8+SXgkJuNnBPKfN1ylzLs3vLEsdzA+qhV0r5Oaj7gzS5tiTaLZML7qb0wIwQvMBnEKqJqqWLi2FhXBBuLThyuGRTerFgUkL", 
"fA0AloBaBqQS8geuxiUU3eptbDJIdJPjciM7LNx8DLsgjeKM7ehOQsHGeVFzvbDwaNp5b9C3pN1y2Tism2ARnXd4w+atFuNcfpk20tpRQ7VAKuOqhceCyoteeHp8VekGAjM1tPBx3yryJpcnbIBK92xEj/hLGkWbA/topjCXLur0Psw/SikEOs5LbBMwYWS27de1/IIa", 
"fA0AlskP7io8LPgYtd7mIdkrF3H/PaFrBOFf6cd5IDNGxUNaPlSSgbGA7F/QSHErwDV9uvqdiOeVxz6x9o3J4NK34XixA9ipkZCbkSplJhHNYLrXF1b1CjXqdCcOiqOVwsWzPaR/T8rKgQQT6N0VRn/gLWpcPqjZXbxLCHJrT1Jrd9VVMKkf9XoF2sHiJCMmWqlwmH47", 
"fA0AlmmF7pcPWUJEh6gUxPav8+8Bpe6YXvFpkVlB2DReX0EZWfG2aW+mQlR3wbPVYzystE4foZZ6pKXqBvlebz3YYTV2VWirznOcIL1aeUDwpwug+Xhm4QwXLMTgyolTvWZQw4azt9vLBspHvOyzVF16cuRSeXOaFSY9lPltHYAo2Sv4yACT+TZumLWD99Kh9Sd+n0yF", 
"fA0AljL/KA+SJLVFzbcgU/0A5In+1onoHFL62r6M5/Gg3fzo3LtZEtYMzNtmKSZ+1IwViCbc7D11i8mlefDzG/a/c82TPJmI2ym8/YUXaMtqjhcCbUSHd2izwqp9NjH3/gygAc3YzUYBlVgzW7ZJzvCdyWqSYBQ4MPCAkgRrXWy3hd99ei/0/bk+5q1ibc4A9HRD8bjv", 
"fA0Alp/CoW/1oSnISDx/zJASXVyIUaRogrCtYUcG/RQ2erwCH51C5TEv6i4JowHL35YSOTK6I7f+QTHNkCHE4RzQW9IgwJXZvynkU32yyBk6nvBU8IEE5FDG4+J8pCOT4BXE98wjrkdkippLGhWrbugFPGkfUwGW5PW3SiLmDqQ2JHewAXtwbiwHNs5HE9ooHiKCprEy", 
"fA0AluTvcUOM19l90yKR2DCIK12Y4g29Yj58Ttd8nMQ9tPCUJZ7qjmfMTJGRljbdQO907/p/JuDk/3Dgn5vcsNxNMnOlmhzgeL4BRVidpfJPEtDWkLsKtLgmXo1u7QDyE3Uc4QbHpiKt1QQthATtIDDk5zdR7lBu0hev8+36tMCdUhllPgDVUzM70g7AYIXYAwQ3iJaB", 
"fA0AlpLc2YkLXfVuwKBKN70iowVbjdkV5RjhuQLUdolNdF7QQYahs3JrBoAehwr+3//VaCPpuGQ8hT4b3pl50albRLAl0IVrfVDndigNI/+rQgbR01tRaD0eC4m41nP5fi/SeIDtdrNsTtjWHlnyDcj3nlMq9gswKm0UPQ0w9nfHRBZ/dYwfUPk0CetGOkHWrHqUWMeU", 
"fA0Alu6jJPhV95XUSbePkeFsNvzxqVnnyDhg7PO10eCmGbH1tIK2JTS99vAVpUokQRQgB1o/3gtiuL+KtNX0CJjaoC3zSnCJwWo5qJ95Zcmklu0z2+yHlu6F9D2zrpc+DD+wj1rmXNHuwFyy8Eg2JFt0mR3263/MjIHo24GPR6R3OE2kc/3530Im0SLpivye/UGIUzcl", 
"fA0Alk+rf8K+xiaUd5f9RUrUjoVYIi+34G38YaOQRjoGhYR1noJCjx/MXpxxarv4j4VUxYH1UK8wqrCvzIo7xW7Nt1YbdbnV/ik9ggI+CbYrpR1Ndv+hsCwGxXBw8rk2rvsmQRp7a7WYU/uprCKJJw7KjOTdqe24ivMWW+raGJQN2f6XveQYil2VLa0isDV8U3j6CJmF", 
"fA0Alu3YWkvDASIbyIpC1NKEXXY2MEnA8EIgLFc4EmyGpzaHkqdXW0UDmeBDSKhEhDmomvkzGxmXODzHfYGgIj/aPLohoDXYfPNpBebV7USzFSf0HxfWZQuItv9bRe5uX5kHKx1OMXE53qj1sExu91bOgVQnGeNYJW9tzm5FVY2ndrG6UK8BzNIxg1HzBsruNtt2B2XM", 
"fA0AltRUFhFiF3kebNbuJIYVVhlVFJdt/Pxxef3IPkgiq+kZnhkrZlakLagNJ2ktPvX4z2SdCw/P19SzI3TBLMblOpZi+9/oj9mR5XyrMt9gpXqCh4xVVgAKDa9qJOJT5KNqMS+sUs6qQpKaPAJenlmeabHnweAsu/q4dXAJJNJoTwR8v2NIDTen5Iq2WORkn4ufcJoL", 
"fA0All5ei5LP0+35Iqh5XLtHyQfmH7HwFXfB5CWmXfuQvMS4vcqjJ9qaj8O/AT8w8leORcOKi9tHLQ51L6J8NgKY9JlM2kx8lYX4xWNNq6EFWu6af0BUfq9S3nAF6V4WuwmjyJ196hzgYVJNoV3tfvQh11uFaXq02+u0oY2f0fdd3jarwgMyZDn6zPNuALbzJgPAGWpu", 
"fA0AlkDtxmUZnqmHT1BeJHWi5quVIGRIgABpQDc89eUvcDTGmC1QKArRU2+kSkTFDTS9M3GBcqVEkDmwbRAEGq8P75Ha5kria7gZG5F1EeTJV0JubJP49ERmYDLJ1pcGVWZjWGocRitYYVdiANDD/zRNNib6bonZOxRD3y6jTc7FF/mQgjkjq31khAiXw3W8zY2wLOkG", 
"fA0AlmCpWG5Z5M8HfMW7Bo5Ej/v+J0tZtgZjaiLk75IUM4Z/C2ow4pGIA741As3cv3z/S6LvtIVu/7IxCraSEy2e9/kbCeA1bKi0xYjjHXAR0e4uLiI1AHsf5iQjcT7EwqrXn5iBCCqIGPZ8kH68NlqMSH1YFdYUUfhdFn1Wat28QbgZmmg7CCVt0uDLGZmb1bMYEnsx", 
"fA0AlnAaEibR0eShoOS6fApjiWEMPzFztdNrzwXFgElIfsYoBPCCIsnLCy7lQQyelMk1lQKAKR+whPWwKtkrV8UGB8fJnEa7wtR2kWEzVeAbjLsWFdqy/xloBN1JVrMx2PoC6/D5R7v5FNkXy+bz8LRcXbOZ5E40W91q2F4g4OzRIj3B92Imujl2YeYgSsKmCy8hvUFM", 
"fA0AlltXJh2sBSApzqQGrLXCQe/gtiT7zrKblkWBMx9FTAoleRU1zOuWpgDziFj3/gn1jyAuwSKY2ba+SvVvwjN1PHScHzOXQ/EktmkkeJjChyEWSFM6WBGciYBidSxB7VOU4wksaclXmHBoV5kpwfksWg+0En+JwYQsS8xR18kYHfNhhykW7qEC2+VqcAAp8CLcMNXh", 
"fA0Alkitslvw+oomLsTNhjTeMVmhWpV4YV50A6z42jQV5wTIHxT+JLfOCFvW8qxQksH+ICyWmXusmDbt2DQGlUwlcst39EGXx2gDmb9jN4rpji3X9qwqi5BgkiDT5+3R+Qw2RCBoX/61bT3tCQ+NpcyiqZAim2hTV0ax45D3nDcPRyWMHXPjrSYuRV+Nkiio6kfV6MQn", 
"fA0Aliy4z8GsAng55UA8zRe6lWvP1afTUysa2jSei27cWKSTVSxiWGzJFUg1/egHR/99T5S8n3rduwC+V6hLQ7HdrCzDDvbxP59OWFcoy/Z7DLfRUKaYFqavlxEvcYuJa8Px6KlnK3oTSj/0gRJLIuXWHNZGLjFh6FZn5LYDwF8v/6Fw6wgun7fMsJPlf6qoEq3qabhr", 
"fA0Allor96DORNNDcpJDR2tFN+Ns9cPSu5nrt6gI/vLYIuryPgstaXWYbqOeIgLCk/zIlczSEPqulW2KO1nRrLgToRL35qbHdaZ/d8xQJQRWiqhNL+kMM26+riSIYFyBzWU8shLbAEY8Ob0IbrMKlw/0X+AaMgWgT/h+kz5vJO7Na3QOPDibbyjD232DHBH0HGsK8x+e", 
"fA0AlmxeYbLu3yLvIR2lfTcJoz/QXcv73NSwVgnog1mDu7mo4BJXz8vxgJNfWSZArc2htBQzAKU7nsf99DX00vQDJyp6BLTaCxjm82dY3eT4zBEaPi6VWCwoTGZ/ZtDaJrRxQHmcDmJXpnjhteq0R4WaJOzQwgn6Tjjq5BiVg8h9d4D5E9fHzqB1JWu2KCx+T+14fKJD", 
"fA0Alvba0tu2RWu0hLZ92ZGUbtzMuLrqvYMegOR3wOP/8NsxisQKB530eD3wu225XqPuQUqPe7rnXFa0P2PguIXZOvd3ClbmQYiC+Fd43rFYiwcdfz8HKWdb3zgJKBxpo/bzuyLUTJaVzddLByqGZDVoNCcyFlwjR4bpoPPvpTWHPjMsTLP6zf4ezgMVmjP2tsT9RGyZ", 
"fA0AlpDQUzq4AAqtU2JOa0RhltSsK+xSP8WIX9r1HpyFfLhCHuad8kd3COX/SnUC72RdGDvc720OFloDnjY7lAs1RPB0K811Kb44/9Mrmg9vzQ/zt1iwGBMlFjoe2hEgsr6mZYwyFzm3CskgycPwmkY97ZGcVjmBJNAKn8ZJX5qN4cqgHv8uJM2+taEjm6e9+lDNyXfX", 
"fA0AlhDDv/+cglM4/1eCMNox40lggofokM4FaMlUA3rcML8Hz6WSbSjT/H8e4eIIi3xCVE6n7hdlBSHO/OfqxqFlzPBleGgIgoC1EqFpV1SHigYi/R9XmilGVo4oKsD7/ueayg3lY1Qzn+dsW3EF3Hw6qMOm4YdpWMw9mjpfOCCHec5aYoNKBbh6GLuYI7vTNcqFD0Bd", 
"fA0Alnvva0H6Al/+7YTc47AR7ulFLOd+YBnHyI6j7sPgkYHikiRMQaBcJkcIsrhEAcYr76fIzz9lnQFWf5hiGONWeKWk5M6RD2PZs2vhJKfG0UTxGYk6vzyB0qoKIJdlvSQJszHeblkZPn5TlVWRJgRiv81DTlTZW/VXxPMQ08t1/RVNtVIfqno3SAbg5Qdl4nbvf4oy", 
"fA0Alo9neXizyvLgKY7JW3V98ppbwMwngUo0p4O1sqKI2JHCF7ycpKj5QoUDktBWU9obRz5EiGSD+p39q6mqixMxEZzDZ6s3GE+q4JlOvM7zYPu4XNNBg/i2aHQWkaoiFzFIlI8kwf8IgW4YrHlj1b7taWiFO+5kVjmfNdsTAOxOZgIJVJpppzX5G8VBsStJGMH5Y2N2", 
"fA0AlrVb1w+r1MePcZXlsBy2vXhQYpr640RozbaJJj6aTVzlXnMNj4O3+yTfNXKor8cZKDcl1nG53zN7y7c6LBDH81lmSgx/T44/Nij6XK/MgWLpN5YCUoEdq5QxrLdWAampR0OhqW/T+G62MgirKvy+4L6KdujNibK2n4bSAnq+Hu2Culw/1VQHhal5hkLGNAW+0Er2", 
"fA0Alh73pQMgCoZ+pFK0G0k5ErmW/dnTlA1W7g1BGhFSZpMOWkLfYAexgPEbhcENm0YW74BDRZrE3NGetVojc+oxl+3Hy/BbgD6TNcxzKH2ynparmOQAxDs+Wrd7MbeuyacEZTCMMNn2X8ddxTsrkAOdgudyKzwBIqh+HIM6qGTg4PGRUcitZPJnLgGc5n3h3CpR4XBQ", 
"fA0AlhXdX+ya4Glm25/L/pHlpuAi81IG4fY2b4v9cTkGxyyxmo55acoQFl9AlJzHR7Wetr6qNqEZmgPDZYShAZKh+3Y2Yk1Rhdhj+EFJI+UZsKQRmEogY1TBNraqRByc03TH82Ud7ndHm3Oge7Ers5jBdkHL+LsVK/k1NOhRYugVW7jLGVl6jYhacNMKfZxBmwF29OAz", 
"fA0Als8iwp5RBSdzZ/COmuTLPCr5Oq9MLpUCbPt7H5VOoNkGTkifr6yJZX3hzY8G+xPm7Gbbopt0m+Qpo3tXRzbMChogHZWR5O93XRhRycBLxvX5cBX7/LWUtM3erQZODoGy5SX9+FA8XGBT9myPGNy6mJ6LT1+eWfs/bMyEDerXI1bGOUSzYLtwM1E9JkSAyNQyTpkh", 
"fA0Alg4ncH4kHPlUTBUlYOOdxPSxwBLQO0vhXwZuDlM9j+r+HIO+yRcLrjv7hUj2W6bhGsQ8WD5DLZ5u3W9hGxeB1mhXMmRWftqS9+F6T0e42KUXohj4f+76+Dk+rE1LmdfelVllomD8a8kxgcraW83uXLLmRv1kowHvX5XG4d+owJTEpn0GckaITG1Z0ETmFNhEx8c1", 
"fA0AljeXIom+OLNmfWspj0Ib9M36RR/TVK3XqCq64GQdxvzYqLgSgzxgMcrmyQoeWwdvaOGglu1uNp5eqL4pcPz8iLp/Hs8BnamAEfDrCfo6+CBHAE4Ga59HFfezcpqwAhkqMI+MCmuMqd2p95Hjg+Cn2ZuNA56HcnVMap8aduDtnHlNz2V5SkghqsanukyyBaoQ7SPK", 
"fA0Alm0gNMknPnX9du4Ykg3R3YLOCqUOAo32ayfib3wAiPOZVmH+rSaS56ApaBFzBr06rizPuKC2fCzbse8Fp945G9ZDx9yjE6UbFcBt00wDLduuFvIgdJAXrFSTTjcmoxoFM+V1YQ3nWZnlyEbqy0pP1Ybk+zkFIXNKMkuKQ3JRZcihc2IqM5JSPQPMpH2ImQCsZ0rq", 
"fA0AljWq/Aej4zwTU2uqSbleHEgy0WqqVFpLa7A6YyhwV6kIyhiOhy5r8GFemO/yQXAIjq1a0R4iGCIDaTV9Iw8WkOcf+tO3xjpM3UGZwkvJ0E2daYpS0clEqEWtTKxkRV8kGyZ+thSOo0uxF0bjSxPm1WtqLJjPCvfE2tzF4GVjlV2UbqOJ/8+W1o8bAbuCXBThCS+h", 
"fA0AlmoLbx5UOXOcLi3fEwhwdsiD7ufaaLnfPw+p2ThSGqkEwJYHY79TzayAG7Pv3yRuzPP3luybomcaNl6r3ba2neYj/NWq5bHF3swy1Z6PERGKW7GmKS58J5RD+x85/6SgYOWVnBDUgr4xqQSUp9UWPODZp2RBqJHHUpWLCP/C73Spbl8nnj0FuEGntkIWNfJP8q4g", 
"fA0Alo5bY+Kw+DwEXFJaC8bxQCwVKHuIlL04TAz7T9erOfw01OifRtlOncmxd2itgen+tcE0/zz7272TTrGBZ5JzBqYXNV3deZApYCxN/vaU1MYb7Y7ElOT4HCrlaFgWC6/2/d1UFDgET0f7XvDZe7xXmsH1TOWLgtKbiOhe9D9fILO/tuAA6gI/uCXZrUbQDXw9fGDd", 
"fA0AlsY5kVWz4Qf4bsW7ZsIrTJrVznOXw8wLRg9iwefnTgNVYVeddi2Ei0u+9PCypUU5+eqyWYqwvqVKGAEcPTey5ntE6+gmqva8hn+O50Hxqk3Ja+LBB+Tuoxrt2iMSQTokBwbBQURQ1+8uFHbMS66xh/YBmUQxI/hfn1ZY/bOz+tifOmz8u6fWIFS19xOr26J2Lgh0", 
"fA0AltG9SgNNyKnH3gZAp40QTXoWdIIUuFYDlutqaFa7bsBxApigMUD9AbT0/pgQhAkqwgXSukVfhKtlvjZqP36TiZhqm6Jv2vrCJ7a0ADYCI17ryHdgCKnCYlMmd4KkvUrtHGvxGWBlkcnSnK64XxW9wvU4hpHYjBHocQWuJVwTbHE9iy0asERKDK9Ql3UPeutf30Cg", 
"fA0AlkAbxSdEIgTczOMn/hAJ/1ApVY2pGRkv0/KMfsboeE3jtJS/FP/K0wn2dLH4vtNnT1Fa9fDr6GCOY4+Pd8pz3Oc2G3mr/A/BUBDiJpQWIo8XUl/x7yRUe6rpP57zvzlVNYW/yyAF7yFVlDFtBt7bmahCAItD9QqcFkMSzLpk1wNH/ssJyuc2WTfnuZgHbIb27yki", 
"fA0Alq+f5v9Ps3CViv2RdYKRL8FcVMXuKn9pLo044VC7Idqp1B22Y10aRoYYcmULwyWvfXj4msB674YhfhE5RVTRTEEjphhOI74JmP0uCgtz6g0EsB/8Z3HCr5c4LA4yaAI9OeIg7lQbjTxvsTZ2Fp37XhjGGnvNldG2LpNL9W3sDSBhv4XMi7a8Qq9pmMiy3WBjABTy", 
"fA0AlgvrLucfb+R25yFKwnZJuSWmpuKWxdt6AaA9WzdMCHF+jyG9eGUFZW37oTNV0sxp1HhrNd62UHTqEdRWhWLNL7tMXDNkeyOrsNTEAxd4yNFOUuWfYGTx0c//0lQrga0HAijnL6XHrExRD4oF1g2itUsChJfZpXj6Xcx3F+vtblTf8FdC6tExRxpPsTgmnDI01IgZ", 
"fA0AljxgoVdDpxfh09ORIblPjsr4nb7qo1eq4YwQdnolfd75UXD58dQfN4u7Rf6Zc7pLhW7l4DzD//LTjhl/HIP4x9673kV2zRmH+78ZB9ZjxVZYt3nwXW6wDzDgZc0gGUkJs23kQP7iJ/DfZ3jcRPVg7k79lLLHC0VBQER4LcCM5sA9aBZOY5opVfhC2kcGJMQVXzhp", 
"fA0AljzkQb7n9M9iXAbmSVS7OdoVjDNA2LSfKNwPBdLcbETVjRWXOBGT+J03Q80Z0vo5iibvo1Mxl71hN8IA76JnAIrcJbpIOISFlQlZ8oQLw+wXB0i61N0RQtulSNo1LrgnUsG6S32hQkrkE4LoghCF5i3dfoEOmWl4+0KyvhH9cQnAdD37wR6Vb89a2jdxA6sYbvGX", 
"fA0AlqdCh0m4EJKRJ/wlALBkQwlNixIkgZgkQufVDlXomU92QT8N4+FTASL5t1Fxms7JKG3qLBYH9q6Pr26WGkNG8ttkgz0FrKXV6NBRNSnSYPanHl0Oussb0DP/aP5K4Oi2M2aV5OrRIByHazBkkWMFSkiBONbJ4F543tsStFz2YKHKSCfSDiCH+0xfVheD1nxk39wa", 
"fA0AlrtsuX/ssN6ZHXb3zg3lNqkpSzQ1HEXGGbQ9+SE+lpIP7i4XN1oIWwWRymf0wSQ0DfrBdqj4vkVt+5OgaqkYPpAs2jGRFVT/9IrVlN2GVrknI7faNFUEwLeG6w9at52yKZN8iDz4Oh2dhieNluNTo0UF7oufc3x8mJHsU4w2P9mi2mj+LdtqoagVax7pHwa+LVzS", 
"fA0Alm755PMY9YRO+Btfgz5WHRjNa8BaOLCvw419Hvq4c8XPUq+I0yhXD1XCx54kj63tUeAbLTgK5vS72lehTBZ2X8AOj3BR6xK3ZC9Bs+ZttqPZlfc0XhBNGuHtN+rEA9aKD3xzv6sjDe5D5ko+zV/lwe5z4tN4NaDZkDV9Gh2fIn7IMoXM2iGGuX6IYOexT1KQIzTT", 
"fA0AlnhEeHCdF5KGLmzMBgVfqOoImVy9UzjHWrYlYM8Q2x3dGmITPFDouaWia/H+rAjUG7xRqau0EC19QeKGvpAnq0uvT5qb1c3cNjoi4DA/gBkR4KqIx9F/a5W2wtD79joknu442qCfglzO/tvjoBYG587+nzSk7RHP8el7VozjAqwC03tt20WGeqceW2fzm5qgL1OS", 
"fA0Alh0YFuXlEhlDO3yFOVS6uWEF2jjTxRKkwFlkJIvu/E+g18DUJGasJm9lh3zfhtC5ypQ4aZ1voMqE93er8MRLH3Uny4oRONLnFQAMVXs8TzTkxSwD2n6Wtc2SY/8PHUa12+IqW9OkY+xRHyXCKF0oOthV5Zw7B9vH70ZA/Re/STmEQip/8ScO0fMDICmwmqZ/X8kG", 
"fA0Alo+lXDKOwJ5Ndbfy+tge9FmF4DRUrf3FabCWmJeM0RwOeYkYRdvVlwsil40xrH6XJekWqEgAqvJbGmEj1gmRXOeeHFrZTn26MY/frMg/Duls4i/WidiOdmzXlcZy/lvz7CMl50oHkAQX+owKDDHh1GS25kWhdvOFZ0iBdReBkGJKHhPb9DdszU38wIWCzcv1caxj", 
"fA0AlreqVuGk//3G6BiKGS+g2+sLWdqpOsGkedh7rMF21Tr2urgz7+V23cKmdO8n8nfomMOW3zaPPpsYSFDSyH86rPGLJC4dJBUrMtoAD8AR36n8nHba7ZmBAg+iZN8diaDLJ6LJdgySg7z2e3k+r5bEi6upcSDWZEqEk99SHRAJ9wL5bXItzMnamkNG+3Ioim7hNkte", 
"fA0AlreAKOvwaC6La2r65EZd+pegohSpObL9Qi5jNGr6apUNGh2TL4pCDuJK6oHTZdXnpU+3oyK4z14z4QL783hsZlWflyMMLeSrfogRpiNm32+FAEQHJhCF/2sGZkCXDKNVbwrLj4xXVR/PP25eGOgy1VPjnErxQTie3U7H3XpjaHVxE7HX3/LskI6fe8ISsy9R8oPZ", 
"fA0AliSLnRYcxFE6s44fRxYSThhhA6BhBooco7AskNF5ElkoTiX9jm/+aC7E5f38Y5j1ogPqgS9CgtdkFIztL5SO9ctTg93qqEiTLNbRf5+WvW6E4yn4Me7nzrLzgmTN8On3GJyQao8ykoVjnCZvQOW9XtuYxvdz2Wp8K8G30CygDJa8czuLnuLBqHvmsyXq4EhU5Okp", 
"fA0Aln3XfCd7UlfZq/DOPXg85wehtAw1COfJIkJS6ep1N9n67D1RUINpaQw93W47Z8/EtxDXk772NLreum5NtIVIZauImdMdzNTRqYyhOjWdfg8a0IJMnKX56h2E4262WnjT14zzHaVXJEMcW63tM2tYMzS+WjVzZvVdEPyFd5/QPs5QQr/b65GjSf72HyyOdWnfLHbH", 
"fA0AlpguWxAo1EEEFDpCYnRVimSBDQ4JOeXkgmpop2Oi597hePmh89yZ9VXyJwQINbzwQ5Ns1t2ioGPo85neI1B34QqNienCXkOnmpyiD5Ra2ToxJ0S+52+nRCoiKeu60N1e15V4A7uMOK9LP2UiQzoTtEUTUzsI7f70f3ql5xCGLwJJ+ibD0ldCSfqXRkEB+tT3VNyo", 
"fA0AlumlTQrOdltxyEwHlE0YRIutsP9JzrKdgiZRZBkRS8Iv/jtCJkj9YbQLUN0grMxYQyikEQfH+Ak/06CeNgBzvMQzLRTyUWB+otRpkawRBuykHtKOTYE13/8MaEv0ynyjgnUOx2E6MCGq3mcO+ud67sm34Pbnrk6TNUx0WMmU9kMZwdmNUaJeDqVXQL0GjFIHtFIa", 
"fA0AlqQB72ge43gPnomEDNScF5cBs9+WB0Li/nrkP4vVmMUJlg0+XZIKU3c2G2f17JH/xR2mGZxhj9mmDSyCRRaL/d7wVDMBtCfGRY/WeNyCbKvyaWa4jfK4++FaE85eq+Id63711zp5pjFvID9DCzN2KyY4Sj5bjZydbyKSzIR2ywX85YbGS2cM8P2+0aKvBu58kTFL", 
"fA0Alr4QD+kUFHt/Lk+4+QKLUx5vG46bTaj/qK7zoxmySQ9YmCkoG2nxuPS1Ne0Im/cUw5Fz4OamiKYefegaz3MOsFZdhYi7CC8rZ5CsUGQOZpc1opCXZJrv4Xh2XTAFL3OTQA/xAkHhAutIkFKxcBw8KDbNg/IVYgQv+auz7j0pymee2c4wqpG8v1ktgRRXNd6i4aEN", 
"fA0AlpiLggGQ7PWbmzYwP7X/O0v9xOnNlHEx7zPJNHS/82MldOsC7H+VpmF18SHqlBNUggRdaaCW3zCRzSIPq5/VpzSZrlHLI0T16g3ydHDcTIkTazv2Y6YRJFh+/9AjqxsBaR/BombGFs+Bllbxb+frfwelDykE5YHZ5QHPfX3K5XZz4LQmrvs1KL0+N7n16+flVOCi", 
"fA0AlmuEVcrSM3wmhCJ/dUaCP3q78vMl+KSyyOcx3Via1C2WKX2hVkd5K0pxNUW5QxkdJ5nsoe5x307ljac2tsjQGTGcxvn/WWvfJfbZH52An0Gu1uyD6Z0TaMJAgccqcD9omXkrBnA83zW4pvH79YiaNqA6EU7mm/13Qeu53gAonh61/u0CXJPSv6csPy7A8uWSJmd2", 
"fA0Aluj6CN/BRPtKSWMdcOwOP1CtYDBjoO1gPII4gV/q+35trxp1bgyWXuK1tL+eKjyyA9GyBBC7kudIt6GVD1Qy4hXQXvLbd65zscR43hdbGAMo9OEbV+JWyWa33QHF6yMQnyLeGYCoWbggAFA0EzRXOt1M+RZIg96go0u37biesVQ+n3ISIESPk0HO+zWdgcyzSVUz", 
"fA0AlpB8hLJLTmLRozLbu38nMpOtkM1AcyNAHhTkjopw5Tagj4v7BnoHfh07K0w6N5zyqsjmCY0uw/cVXwNMcYn7avfFes12vvMOwDG2DxzEHzlHgEWEh/r9G+u/J95nbH3lxWWDs8ycFdZCUVZABB+p8n4tz+GqNNXneGpMNY7nIyFunTpVuMLwHHxe+fPVhwSAVOx+", 
"fA0Alk7LJal5vcVu7gNW1KJVYZ4313h8h3bzQZcKIYlC2l2YsqnGelASlRNwi/bVCbevTEluqK7CuViwuQVTEjbwMDtYgJfCKft2SkkNvZBXeyV3GDGiJUbB9s+P6QBWu4OLllaLXYZgUy83rnkmTQT+C4QS+kwsQmR6cZcF1xxqAthosa/TPESki5f+YseD6W1G3kGY", 
"fA0AlvWN6G1QMMBycOqsqxw2VlZqvLV+Lu4z5/Fjd22YdOgSNY7EyP4avmUuSNPTmnIGnI+IZNIDVQc0uTBNpVxlauYU2qUa5gGR8wAPXqr66CaLE8FNAcAGQrmUtlKmkEzSF/ry9wgOKmRe4Gumm0M+Hc4B03QXt3peKFNxxo11nPKUaLZwZaiPs9lkCaHdFpaU2cgu", 
"fA0Altz0/CtKOq0clNnsOuyrjwWfwFDEf4+H50NuCjXPFVm22Y8Bq6ntQ2Y3M+4JqtuIgOPkaJfQ+9h2/PHlfIE6m3Idpv5yTzyeinmQ9k8ddylcx9nfhXwcERAvbYiIWxn5VXu6/dHwuDS8r+ISLzPhrFh7rO0YFNUAUtvfk4Kaog1ZZL1dozRA25A61iTouB/RQVg5", 
"fA0AlvOviqAUJkCOUWcFT5n33RtEYKVhALxDMbeuFiYkuCcPYGKdyLXNJ1uki8Rj1+NwVPVHWfcyRD3xAlHJCshkoCIDTlXgGV77mKiabNssFkpGMo7ZZ42Eg+Wmpva4yY/alxvs2QFeOCqj3GyDx6a/5/5Xx5OjUJLZQUvQNK0ALb3z4gppQff8CoqVtekZXhm71tXy", 
"fA0AlhCzJe1dO+eyW9AobsDIZAEwuUuHRtO1mwN58T6Dc9Vsl24EtimKbPTQ8gLjElRz3rX7qHpf8gb4wdqgu/VhuMzrbOFlHghUtMfzksqfRM79MeZsbfnbMHyUTvqQ7VXFwa7dfCHif3BTxOODwRQI3MBiHqa3tHLV6a59wXuxN70yOuBLjaGCfWAmi1UJ2b59SvZ8", 
"fA0AlqmBzcLJlZt1hLvkCD2tXXcLK+hVUrBKu6iygDtrYpwzL5pRP5XZa9LnVN5Y2rzIULPJ0dVR8v+xvMWx3FtYWrxyd15oKc67DRXg6RqKFzW1hXxUVftj1Hpc6o4Yp4dSHwQa9XZ79vaC1qBlyFBrgDBoHiRObD7WdzrLMitHhEZ5mOM8gN5maRLX0dvbMzM1uqoa", 
"fA0AlvJuRBF60e++PMtFfOxZP5UK4ycRHZUZpv231MVCdjxeOoQVTLBzBkMyyIeUdVvu42b69LiqCRGvVqa1iMjOwTt1usX0dFLD8eiebpQPGsPBjbC4myqkbacmljqEcPraXtjJMVx7fFODipIRxqJBI3M8aMnWUru2zzemi0PwtTmSAh9I8BIMdSPaIAAq2sW5Uc8R", 
"fA0AlmwgCrRreTTAFOqWGCvAPD4Znj0pjknY0fkXIrQVwl+d+FcczrXVquNSXDzdl22MYvaiMrHncKSHD/DM2f0MQPX7Cfy/S5OloTHibLxA9iVN+a/8QA10uTAFn0CW/ruwX7HwP/frxauCDQG+BfZp+R3H6CUaJRW5Y2NaUwdVAYZXkKop+hrAO7iCgZmhtVzGNQaB", 
"fA0AlvJcV9x0/vbWR17ad51EqXzu4flh43KbvrARRUEn7SbYvIqC6NnU6JhkkkS6MklZ1fCLjloyllSrh4tXX+RlrZw9i6leuBPVbscsPd8FTmZj7akTRTabMUa9/OwHMf0fIlvBCZpy2bKCKFL4bjDD7kEjNqjOr1aPADRynkDShwx0Z7E3y2AlmabnCrO0nvo5Xk5N", 
"fA0AlkC0g6P6BfU5MOxVsbUNUdo2902lpVzHORwwDzoJAYymoFFs0iZAvsUyWF27jeAbcZn2qsQEIB2XksIsY0fxQlubh+irko4/efUg+N0LaGTPV2TxTddIAzKoSJKfKtWxPAx9aFDJe3nr9XPMRjCjME2ucTv0NF9E8QMXEiRj6c40HOhjY27ipRVnCfCgzBaq21rd", 
"fA0Alv9ULTE0H9fEDuIP1KyJildKUk09XUkizV4tLNg1Xs5bAdqh0OaHpZ2weH50o8idFlnU80TRL+fsrIPW6BAbNfuYT2n6z5Li86CXXQggT7hTbHn4/Ma6aDKOaydfFYcSjxrV/FrythvHxNuZYzlui67Pbxkx0SsWblom3eVep9rT5B1eAmYProprvOQUgZgkKNQc", 
"fA0AlgS6t3AwYV4Mgrjdra9ENjypQSp1niYjLaSpgWG9rGmFq2ibXgkflQABvwvrTIqnr3owAg2yjuVQvhFmtoiiCXq1n0NtFai6r5WnjsRZkTdUFKoTfssxfCVAMMym+ULkeUu1OXH2OfP2a784R13fOiXpl+0xjXWBOdf0vXFwMVOJXWF/EuprP09nE1e1NLrKH7gE", 
"fA0AlmJ66atU+XfoD8URImtHptXe76U8VrlW0v3zI6SPejoF5Nv+8B8wyp8e5qMw3hwdMLv5pGDk64OnzpGLuo0QlQCr4lASri8fwNDtlpxN1HmaOgCqweg580EwFFiXRCuXMQIBfvXTf6StqAZKk+09u1QzmaWBXX741ReVgK5HDWGRyT6lcqSePkS+d3j658dV1nva", 
"fA0Alo60qwIxuCr/3zA09a5rJqi12An48DY63C0dk03s/evuN5VkV2jAJsF7fSUW4lkXSXXZHCCqs3IdBaznFXoln1klddQ5J1CIeXsR5LPSDOOjiwhuGew3KsJsjiEA2iRmPtWxkx+2h+n4At6hfScRnvxVhNrQ3j84rOkuFFrkV1Ca6GhY7h1C2Ap2eI1wPV7lp3Vw", 
"fA0AlvYeeumS2RsE5IUd+9WPgygvW2HtUiOrS/ng3PsM+AIgCjhp4Og2LVK6cMT4MwFhRzdqpnlBPA3xbBR8qMj/6WQWpVaARN6axrXQMpKHcJLUAIx2X5P6rEIxiU3A1+eT2PaRb/wZ6c/9aksZQvVAq1usqZDPHcfJs7Ymlf5Goj/Yf+7CrLIdGAt0+HLKjgufJbMB", 
};
//        for (int i = 1; i <= 100; ++i)
//            System.out.println (cifra ("Com o Internet Banking Caixa você acessa e movimenta a sua conta a qualquer hora, de qualquer lugar, com todo o conforto."));
System.out.println ("Com o Internet Banking Caixa você acessa e movimenta a sua conta a qualquer hora, de qualquer lugar, com todo o conforto.".getBytes("UTF8").length);
System.out.println (Util.paraHexa ("ê".getBytes("UTF8")));
	    
        System.out.println (decifra ("fA0Alr4QD+kUFHt/Lk+4+QKLUx5vG46bTaj/qK7zoxmySQ9YmCkoG2nxuPS1Ne0Im/cUw5Fz4OamiKYefegaz3MOsFZdhYi7CC8rZ5CsUGQOZpc1opCXZJrv4Xh2XTAFL3OTQA/xAkHhAutIkFKxcBw8KDbNg/IVYgQv+auz7j0pymee2c4wqpG8v1ktgRRXNd6i4aEN"));
        
        int i;
        long t;
        t = System.currentTimeMillis();
        for (i = 0; i < 10000; ++i)
        {
            if (!decifra (ciphers[i % 100]).equals ("Com o Internet Banking Caixa você acessa e movimenta a sua conta a qualquer hora, de qualquer lugar, com todo o conforto.")) {
                throw new Exception (i + " does not match");
            }
        }
        System.out.println ("Tempo transcorrido: " + (System.currentTimeMillis()-t));
	}

*/
}
