package br.gov.caixa.comprovante.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import br.gov.caixa.comprovante.exception.ParserResponseServiceException;
import br.gov.caixa.comprovante.util.annotation.MethodResponseService;
import br.gov.caixa.comprovante.util.annotation.MethodSendService;

/**
 * 
 * @author Capgemini
 * @param <T>
 *
 */
public class SinbmParserAnnotation {
	/**
	 * 
	 * @param classIn
	 */
	public ObjectHolder parserResponseObService (Class classIn, Object object) 
			throws ParserResponseServiceException {
		 
		 Class     				classTargetRefer;
		 Object    				obValue;
		 String					targetName;
		 MethodResponseService 	methodAnnotation;
		 Method[]  				collMethod 	= 	classIn.getDeclaredMethods();
		 Object					obHolder	=	null;
		 
		  for (Method methodRefer : collMethod) {
			
			boolean existAnnotation = 
					methodRefer.isAnnotationPresent(MethodResponseService.class);
			
				if (existAnnotation) {
					
					if (obHolder == null) 
						obHolder 		= 	this.assemblyObHolder(classIn);	
					
					methodAnnotation	=	methodRefer.getAnnotation(MethodResponseService.class);
					targetName			=	methodAnnotation.TargetName();
					classTargetRefer 	= 	object.getClass();
					obValue 			= 	this.extractValue(classTargetRefer, object, targetName);
					
					this.setValueHolderObj(methodRefer, obHolder, obValue);
					
				}
			
		  }
		  
		  ObjectHolder objectReturn = 	new ObjectHolder();
		  objectReturn.classOb		=	obHolder.getClass();
		  objectReturn.objectHolder	=	obHolder;
		  
		  return objectReturn;
		
	}
	
	/**
	 * 
	 * @param nameMethod
	 * @return
	 */
	public String extractGetter(String nameMethod) {
		final String STR_GET 	= 	"get";
		String returnStr 		= 	(nameMethod.replace(STR_GET, "")).trim();
		return returnStr;
	}
	/**
	 * 
	 * @param classObjReturn
	 * @param object
	 * @return
	 * @throws ParserResponseServiceException
	 */
	public Object parseRequestServiceObj (Class classObjReturn, Object object) 
			throws ParserResponseServiceException {
		
		Object			objValue	=	null;
		Object			obReturn	=	null;
		String          methodName  =   null;
		String			idMethod	=	null;
		Class			classOrigin	=	object.getClass();
		
		Method[]  				collMethod 	= 	classOrigin.getDeclaredMethods();
		
		
		for (Method methodRefer : collMethod) {
			
			boolean existAnnotation = 
					methodRefer.isAnnotationPresent(MethodSendService.class);
		
			if (existAnnotation) {
				
				methodName 	= 	methodRefer.getName();
				idMethod	=	this.extractGetter(methodName);
				objValue	=	this.extractValue(methodRefer, object);
				
				if (obReturn == null) 
					obReturn 		= 	this.assemblyObHolder(classObjReturn);
				
				try {
					Method method = obReturn.getClass().getDeclaredMethod("set"+idMethod, Object.class);
					method.invoke(obReturn, objValue);
				} catch (NoSuchMethodException e) {
					throw new ParserResponseServiceException (e);
				} catch (SecurityException e) {
					throw new ParserResponseServiceException (e);
				} catch (IllegalAccessException e) {
					throw new ParserResponseServiceException (e);
				} catch (IllegalArgumentException e) {
					throw new ParserResponseServiceException (e);
				} catch (InvocationTargetException e) {
					throw new ParserResponseServiceException (e);
				}
			}
		
		}
		
		
		return obReturn;
	}
	
	/**
	 * 
	 * @param methodRefer
	 * @param obHolderRefer
	 * @param obValue
	 */
	private void setValueHolderObj (Method methodRefer, 
									Object obHolderRefer, 
									Object obValue) throws ParserResponseServiceException {
		
		try {
			
			methodRefer.invoke(obHolderRefer, obValue);
			
		} catch (IllegalAccessException  e) {
			throw new ParserResponseServiceException(e);
		} catch (IllegalArgumentException e){
			throw new ParserResponseServiceException(e);
		} catch (InvocationTargetException e){
			throw new ParserResponseServiceException(e);
		}
	}
	/**
	 * 
	 * @param classInRefer
	 * @return
	 */
	private Object assemblyObHolder (Class classInRefer) 
			throws ParserResponseServiceException {
		
		Object objectReturn = null;
		
		try {
			
			objectReturn = classInRefer.newInstance();
			
		} catch (InstantiationException e) {
			throw new ParserResponseServiceException(e);
		}catch (IllegalAccessException e){
			throw new ParserResponseServiceException(e);
		}
		
		return objectReturn;
	}
	
	/**
	 * 
	 * @param method
	 * @param objectExec
	 * @return
	 * @throws ParserResponseServiceException
	 */
	private Object extractValue (Method method, Object objectExec) 
			throws ParserResponseServiceException {
		
		Object objectReturn = null;
		
		try {
			
			objectReturn	=	method.invoke(objectExec, null);
		
		}catch(SecurityException e){
			throw new ParserResponseServiceException(e);
		}catch (InvocationTargetException iex) {
			throw new ParserResponseServiceException(iex);
		} catch (IllegalAccessException ilegal) {
			throw new ParserResponseServiceException(ilegal);
		}
		
		return objectReturn;
	}
	/**
	 * 
	 * @param classRefer
	 * @param objectExec
	 * @param nameMethod
	 * @return
	 * @throws ParserResponseServiceException
	 */
	private Object extractValue (Class classRefer, 
								 Object objectExec, 
								 String nameMethod) throws ParserResponseServiceException {
		
		Object objectReturn = null;
		
		try {
			Method method 	= 	classRefer.getMethod("get" + nameMethod, null);
			objectReturn	=	method.invoke(objectExec, null);
		} catch (NoSuchMethodException e) {
			throw new ParserResponseServiceException(e);
		} catch(SecurityException e){
			throw new ParserResponseServiceException(e);
		}catch (InvocationTargetException iex) {
			throw new ParserResponseServiceException(iex);
		} catch (IllegalAccessException ilegal) {
			throw new ParserResponseServiceException(ilegal);
		}
		
		return objectReturn;
	}
}
