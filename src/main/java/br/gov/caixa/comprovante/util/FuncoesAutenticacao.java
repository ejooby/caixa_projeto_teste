package br.gov.caixa.comprovante.util;


/**
 * Geração de códigos de autenticação
 * 
 * @date 2003-02-04 EEW
 * @author FSW
 * @version 1.0
 */
public class FuncoesAutenticacao {
	private static final int N_BITS_CODIGO = 80;

	/** OK */
	public static final int OK = 0;
	/** A autenticação não bateu. */
	public static final int ERRO_AUTENT = 1;
	/** Erro no formato do dado - não está em base-32 */
	public static final int ERRO_FORMATO = 2;

	/**
	 * Gera um código de autenticação de uma string.
	 * 
	 * @param dados
	 *            Dados a serem autenticados.
	 * @param chaveCifrada
	 *            A chave fixa, cifrada (normalmente obtida de uma base)
	 * @return O código de autenticação - 80 bits em Base-32 (16 caracteres
	 *         alfanuméricos).
	 * @throws java.security.GeneralSecurityException
	 *             - Houve algum problema no parâmetro chaveCifrada que impediu
	 *             sua decifração.
	 */
	public static String geraAutenticacao(String dados, String chaveCifrada)
			throws java.security.GeneralSecurityException {
		return Autenticador.autentica(dados, Cifrador.decifra2(chaveCifrada), N_BITS_CODIGO);
	}

	/**
	 * Verifica um código de autenticação.
	 * 
	 * @param dados
	 *            Dados cuja autenticação deve ser verificada.
	 * @param chaveCifrada
	 *            A chave fixa, cifrada (normalmente obtida de uma base)
	 * @param codigoAutenticacao
	 *            O código de autenticação (80 bits em base-32).
	 * @return FuncoesAutenticacao.OK - se o código bateu.
	 *         FuncoesAutenticacao.ERRO_FORMATO - se o código de autenticação
	 *         não tem 80 bits em base-32. FuncoesAutenticacao.ERRO_AUTENT - se
	 *         o código de autenticação não bateu.
	 * @throws java.security.GeneralSecurityException
	 *             - Houve algum problema no parâmetro chaveCifrada que impediu
	 *             sua decifração.
	 */
	public static int verificaAutenticacao(String dados, String chaveCifrada, String codigoAutenticacao)
			throws java.security.GeneralSecurityException {
		return Autenticador.verifica(dados, Cifrador.decifra2(chaveCifrada), codigoAutenticacao, N_BITS_CODIGO);
	}

	/**
	 * Driver de teste
	 */
	public static void main(String[] args) {
		// -- Cuidado com espaços, tabs, maiúsculas, minúsculas, acentos etc.
		// -- Para evitar problemas mais tarde, normalize os dados antes de
		// -- usar a rotina.
		String dados = "JOSE MARIA DE JESUS||001||32151||20030203||20000,00";
		// -- O dado a seguir pode ter sido obtido de uma tabela.
		String chaveCifrada = "fA0Alr4QD+kUFHt/Lk+4+QKLUx5vG46bTaj/qK7zoxmySQ9YmCkoG2nxuPS1Ne0Im/cUw5Fz4OamiKYefegaz3MOsFZdhYi7CC8rZ5CsUGQOZpc1opCXZJrv4Xh2XTAFL3OTQA/xAkHhAutIkFKxcBw8KDbNg/IVYgQv+auz7j0pymee2c4wqpG8v1ktgRRXNd6i4aEN";
		// -- Note que o código abaixo foi formatado com
		// -- caracteres de pontuação para melhorar a legibilidade, pois
		// -- a rotina ignora tais caracteres não-alfanuméricos.
		// -- Além disso, não considera a diferença entre maiúsculas e
		// -- minúsculas.
		String codigoAutenticacao = "izvL-w4j3-6jkf-3so8";
		int ret;

		try {
			System.out.println(FuncoesAutenticacao.geraAutenticacao(dados, chaveCifrada));
			ret = FuncoesAutenticacao.verificaAutenticacao(dados, chaveCifrada, codigoAutenticacao);
			switch (ret) {
			case FuncoesAutenticacao.OK:
				System.out.println("Verificou OK");
				break;
			case FuncoesAutenticacao.ERRO_FORMATO:
				System.out.println("Erro no formato (16 caracteres alfanuméricos)");
				break;
			case FuncoesAutenticacao.ERRO_AUTENT:
				System.out.println("Erro na verificação da autenticação");
				break;
			default:
				System.out.println("verificaAutenticacao retornou " + ret);
				break;
			}
		} catch (java.security.GeneralSecurityException ex) {
			System.out.println("Não conseguiu decifrar a chave fixa");
		}
	}
}
