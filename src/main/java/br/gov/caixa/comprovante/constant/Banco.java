package br.gov.caixa.comprovante.constant;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

@Configuration
@PropertySource("classpath:/com/myco/app.properties")
public class Banco {

	@Autowired
	Environment env;
	
	protected static final Logger logger = LoggerFactory.getLogger(Banco.class);
	

	private static Map<String, String> map = new HashMap<String, String>();
	


	static Resource resource = new ClassPathResource("/caixa-api-descricao-banco.properties");
	static Properties props;

	private static Banco instance = null;

	private Banco() {
		// Exists only to defeat instantiation.
	}

	public static Banco getInstance() {
		if (instance == null) {
			instance = new Banco();

			try {
				props = PropertiesLoaderUtils.loadProperties(resource);
			} catch (IOException e) {
				logger.error(" ERRO: AO ler arquivo de Properties = caixa-api-descricao-banco.properties = "+e.getMessage());
				e.printStackTrace();
			}
			System.out.println("Current Locale: " + Locale.getDefault());
			for (String key : props.stringPropertyNames()) {
				
				map.put(key, props.getProperty(key));
			}

		}
		return instance;
	}

	public String getDescricaoBanco(String cod) {
		return map.get(cod);

	}

	public static void main(String[] args) throws IOException {
		
		
		
		
		System.out.println();
		
		
		
		
		   System.out.println( new String("ÇÁÕ".getBytes(), "UTF-8") );
		   System.out.println( new String("ÇÁÕ".getBytes(), "ISO-8859-1") );
		   System.out.println( new String("ÇÁÕ".getBytes("UTF-8"), "UTF-8") );
		   System.out.println( new String("ÇÁÕ".getBytes("ISO-8859-1"), "ISO-8859-1") );
		
	}

}
