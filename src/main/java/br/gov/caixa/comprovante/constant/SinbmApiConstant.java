package br.gov.caixa.comprovante.constant;

/**
 * 
 * @author Capgemini SP
 *
 */
public final class SinbmApiConstant {
	
	public static final String 	SISTEMA_ORIGEM 				= 	"SINBM";
	public static final String 	OPERACAO_LISTA 			= 	"LISTA";
	public static final String 	OPERACAO_RESUMO 			= 	"RESUMO";
	public static final String 	OPERACAO_DETALHE 			= 	"DETALHE";
	
	
	public static final String 	OPERACAO_HISTORICO_DEZ_ANOS = 	"HISTORICO_DEZ_ANOS";
	public static final String 	DATE_FORMAT_DEFAULT 		= 	"yyyyMMdd";
	public static final String 	DATE_FORMAT_TIME_DEFAULT 	= 	"yyyyMMddHHmmss";
	public static final String 	DATE_FORMAT_BR_DEFAULT		=	"dd/MM/yyyy";
	public static final boolean 	FLAG_DADOS_CADASTRAIS  		= 	true;
	public static final boolean 	FLAG_LANCAMENTOS       		= 	true;
	public static final boolean 	FLAG_SALDOS            		= 	true;
	public static final boolean 	FLAG_COMPRAS           		= 	true;
	public static final boolean 	FLAG_AGENDAMENTO       		= 	true;
	public static final boolean 	FLAG_VALORES_BLOQUEADOS 	= 	true;
	public static final boolean 	FLAG_SALDO_DATA_LIMITE  	= 	true;
	public static final boolean 	FLAG_LANCAMENTOS_DIA    	= 	true;
	public static final String 	SALDO_DISPONIVEL_DO_DIA    	= 	"SD";
	
	
}
