package br.gov.caixa.comprovante.holder;

/**
 * 
 * @author Capgemini SP
 *
 */
public class SibarHolder {
	/**
	 * 
	 */
	private String dadosEntrada;
	/**
	 * 
	 */
	private String dadosSaida;
	/**
	 * 
	 * @return
	 */
	public String getDadosEntrada() {
		return dadosEntrada;
	}
	/**
	 * 
	 * @param dadosEntrada
	 */
	public void setDadosEntrada(String dadosEntrada) {
		this.dadosEntrada = dadosEntrada;
	}
	/**
	 * 
	 * @return
	 */
	public String getDadosSaida() {
		return dadosSaida;
	}
	/**
	 * 
	 * @param dadosSaida
	 */
	public void setDadosSaida(String dadosSaida) {
		this.dadosSaida = dadosSaida;
	}
	
}
