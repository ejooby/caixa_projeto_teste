/**
 * Dados_saida_transacao_ted_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.comprovante.holder;

public class DadosSaidaTEDHolder  implements java.io.Serializable {
    private java.lang.Long NSU;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type REMETENTE1;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type REMETENTE2;

    private java.lang.Short TIPO_CONTA_REMETENTE;

    private java.lang.String TIPO_PESSOA_REMETENTE;

    private java.math.BigDecimal VALOR;

    private java.util.Date DATA_MOVIMENTO;

    private java.util.Date DATA_TRANSFERENCIA;

    private java.lang.Long NSU_CONFIRMACAO;

    private java.lang.String TIPO_DOC;

    private java.lang.String TIPO_SERVICO;

    private java.lang.String MODO;

    private java.lang.Integer ISPB_DESTINO;

    private java.lang.Short BANCO_DESTINO;

    private java.lang.Short AGENCIA_DESTINO;

    private java.lang.Long CONTA_DESTINO;

    private java.lang.String DV_CONTA_DESTINO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type DESTINATARIO;

    private java.lang.Short TIPO_CONTA_DESTINATARIO;

    private java.lang.String TIPO_PESSOA_DESTINATARIO;

    private java.math.BigDecimal TARIFA;

    private java.lang.Long IDENTIFICACAO_DEPOSITO_JUDICIAL;

    private java.lang.Short DV_IDENTIFICACAO_DEPOSITO_JUDICIAL;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type PESSOA_CORT;

    private java.lang.String TIPO_PESSOA_CORT;

    private java.lang.String IDENTIFICACAO;

    private java.util.Calendar DATA_TRANSACAO;

    private java.lang.String CANAL_ORIGEM;

    private java.lang.Integer NUMERO_DOC_DEBITO;

    public DadosSaidaTEDHolder() {
    }

    public DadosSaidaTEDHolder(
           java.lang.Long NSU,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type REMETENTE1,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type REMETENTE2,
           java.lang.Short TIPO_CONTA_REMETENTE,
           java.lang.String TIPO_PESSOA_REMETENTE,
           java.math.BigDecimal VALOR,
           java.util.Date DATA_MOVIMENTO,
           java.util.Date DATA_TRANSFERENCIA,
           java.lang.Long NSU_CONFIRMACAO,
           java.lang.String TIPO_DOC,
           java.lang.String TIPO_SERVICO,
           java.lang.String MODO,
           java.lang.Integer ISPB_DESTINO,
           java.lang.Short BANCO_DESTINO,
           java.lang.Short AGENCIA_DESTINO,
           java.lang.Long CONTA_DESTINO,
           java.lang.String DV_CONTA_DESTINO,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type DESTINATARIO,
           java.lang.Short TIPO_CONTA_DESTINATARIO,
           java.lang.String TIPO_PESSOA_DESTINATARIO,
           java.math.BigDecimal TARIFA,
           java.lang.Long IDENTIFICACAO_DEPOSITO_JUDICIAL,
           java.lang.Short DV_IDENTIFICACAO_DEPOSITO_JUDICIAL,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type PESSOA_CORT,
           java.lang.String TIPO_PESSOA_CORT,
           java.lang.String IDENTIFICACAO,
           java.util.Calendar DATA_TRANSACAO,
           java.lang.String CANAL_ORIGEM,
           java.lang.Integer NUMERO_DOC_DEBITO) {
           this.NSU = NSU;
           this.REMETENTE1 = REMETENTE1;
           this.REMETENTE2 = REMETENTE2;
           this.TIPO_CONTA_REMETENTE = TIPO_CONTA_REMETENTE;
           this.TIPO_PESSOA_REMETENTE = TIPO_PESSOA_REMETENTE;
           this.VALOR = VALOR;
           this.DATA_MOVIMENTO = DATA_MOVIMENTO;
           this.DATA_TRANSFERENCIA = DATA_TRANSFERENCIA;
           this.NSU_CONFIRMACAO = NSU_CONFIRMACAO;
           this.TIPO_DOC = TIPO_DOC;
           this.TIPO_SERVICO = TIPO_SERVICO;
           this.MODO = MODO;
           this.ISPB_DESTINO = ISPB_DESTINO;
           this.BANCO_DESTINO = BANCO_DESTINO;
           this.AGENCIA_DESTINO = AGENCIA_DESTINO;
           this.CONTA_DESTINO = CONTA_DESTINO;
           this.DV_CONTA_DESTINO = DV_CONTA_DESTINO;
           this.DESTINATARIO = DESTINATARIO;
           this.TIPO_CONTA_DESTINATARIO = TIPO_CONTA_DESTINATARIO;
           this.TIPO_PESSOA_DESTINATARIO = TIPO_PESSOA_DESTINATARIO;
           this.TARIFA = TARIFA;
           this.IDENTIFICACAO_DEPOSITO_JUDICIAL = IDENTIFICACAO_DEPOSITO_JUDICIAL;
           this.DV_IDENTIFICACAO_DEPOSITO_JUDICIAL = DV_IDENTIFICACAO_DEPOSITO_JUDICIAL;
           this.PESSOA_CORT = PESSOA_CORT;
           this.TIPO_PESSOA_CORT = TIPO_PESSOA_CORT;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.DATA_TRANSACAO = DATA_TRANSACAO;
           this.CANAL_ORIGEM = CANAL_ORIGEM;
           this.NUMERO_DOC_DEBITO = NUMERO_DOC_DEBITO;
    }


    /**
     * Gets the NSU value for this Dados_saida_transacao_ted_Type.
     * 
     * @return NSU
     */
    public java.lang.Long getNSU() {
        return NSU;
    }


    /**
     * Sets the NSU value for this Dados_saida_transacao_ted_Type.
     * 
     * @param NSU
     */
    public void setNSU(java.lang.Long NSU) {
        this.NSU = NSU;
    }


    /**
     * Gets the REMETENTE1 value for this Dados_saida_transacao_ted_Type.
     * 
     * @return REMETENTE1
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type getREMETENTE1() {
        return REMETENTE1;
    }


    /**
     * Sets the REMETENTE1 value for this Dados_saida_transacao_ted_Type.
     * 
     * @param REMETENTE1
     */
    public void setREMETENTE1(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type REMETENTE1) {
        this.REMETENTE1 = REMETENTE1;
    }


    /**
     * Gets the REMETENTE2 value for this Dados_saida_transacao_ted_Type.
     * 
     * @return REMETENTE2
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type getREMETENTE2() {
        return REMETENTE2;
    }


    /**
     * Sets the REMETENTE2 value for this Dados_saida_transacao_ted_Type.
     * 
     * @param REMETENTE2
     */
    public void setREMETENTE2(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type REMETENTE2) {
        this.REMETENTE2 = REMETENTE2;
    }


    /**
     * Gets the TIPO_CONTA_REMETENTE value for this Dados_saida_transacao_ted_Type.
     * 
     * @return TIPO_CONTA_REMETENTE
     */
    public java.lang.Short getTIPO_CONTA_REMETENTE() {
        return TIPO_CONTA_REMETENTE;
    }


    /**
     * Sets the TIPO_CONTA_REMETENTE value for this Dados_saida_transacao_ted_Type.
     * 
     * @param TIPO_CONTA_REMETENTE
     */
    public void setTIPO_CONTA_REMETENTE(java.lang.Short TIPO_CONTA_REMETENTE) {
        this.TIPO_CONTA_REMETENTE = TIPO_CONTA_REMETENTE;
    }


    /**
     * Gets the TIPO_PESSOA_REMETENTE value for this Dados_saida_transacao_ted_Type.
     * 
     * @return TIPO_PESSOA_REMETENTE
     */
    public java.lang.String getTIPO_PESSOA_REMETENTE() {
        return TIPO_PESSOA_REMETENTE;
    }


    /**
     * Sets the TIPO_PESSOA_REMETENTE value for this Dados_saida_transacao_ted_Type.
     * 
     * @param TIPO_PESSOA_REMETENTE
     */
    public void setTIPO_PESSOA_REMETENTE(java.lang.String TIPO_PESSOA_REMETENTE) {
        this.TIPO_PESSOA_REMETENTE = TIPO_PESSOA_REMETENTE;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_ted_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_ted_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the DATA_MOVIMENTO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return DATA_MOVIMENTO
     */
    public java.util.Date getDATA_MOVIMENTO() {
        return DATA_MOVIMENTO;
    }


    /**
     * Sets the DATA_MOVIMENTO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param DATA_MOVIMENTO
     */
    public void setDATA_MOVIMENTO(java.util.Date DATA_MOVIMENTO) {
        this.DATA_MOVIMENTO = DATA_MOVIMENTO;
    }


    /**
     * Gets the DATA_TRANSFERENCIA value for this Dados_saida_transacao_ted_Type.
     * 
     * @return DATA_TRANSFERENCIA
     */
    public java.util.Date getDATA_TRANSFERENCIA() {
        return DATA_TRANSFERENCIA;
    }


    /**
     * Sets the DATA_TRANSFERENCIA value for this Dados_saida_transacao_ted_Type.
     * 
     * @param DATA_TRANSFERENCIA
     */
    public void setDATA_TRANSFERENCIA(java.util.Date DATA_TRANSFERENCIA) {
        this.DATA_TRANSFERENCIA = DATA_TRANSFERENCIA;
    }


    /**
     * Gets the NSU_CONFIRMACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return NSU_CONFIRMACAO
     */
    public java.lang.Long getNSU_CONFIRMACAO() {
        return NSU_CONFIRMACAO;
    }


    /**
     * Sets the NSU_CONFIRMACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param NSU_CONFIRMACAO
     */
    public void setNSU_CONFIRMACAO(java.lang.Long NSU_CONFIRMACAO) {
        this.NSU_CONFIRMACAO = NSU_CONFIRMACAO;
    }


    /**
     * Gets the TIPO_DOC value for this Dados_saida_transacao_ted_Type.
     * 
     * @return TIPO_DOC
     */
    public java.lang.String getTIPO_DOC() {
        return TIPO_DOC;
    }


    /**
     * Sets the TIPO_DOC value for this Dados_saida_transacao_ted_Type.
     * 
     * @param TIPO_DOC
     */
    public void setTIPO_DOC(java.lang.String TIPO_DOC) {
        this.TIPO_DOC = TIPO_DOC;
    }


    /**
     * Gets the TIPO_SERVICO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return TIPO_SERVICO
     */
    public java.lang.String getTIPO_SERVICO() {
        return TIPO_SERVICO;
    }


    /**
     * Sets the TIPO_SERVICO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param TIPO_SERVICO
     */
    public void setTIPO_SERVICO(java.lang.String TIPO_SERVICO) {
        this.TIPO_SERVICO = TIPO_SERVICO;
    }


    /**
     * Gets the MODO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return MODO
     */
    public java.lang.String getMODO() {
        return MODO;
    }


    /**
     * Sets the MODO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param MODO
     */
    public void setMODO(java.lang.String MODO) {
        this.MODO = MODO;
    }


    /**
     * Gets the ISPB_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return ISPB_DESTINO
     */
    public java.lang.Integer getISPB_DESTINO() {
        return ISPB_DESTINO;
    }


    /**
     * Sets the ISPB_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param ISPB_DESTINO
     */
    public void setISPB_DESTINO(java.lang.Integer ISPB_DESTINO) {
        this.ISPB_DESTINO = ISPB_DESTINO;
    }


    /**
     * Gets the BANCO_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return BANCO_DESTINO
     */
    public java.lang.Short getBANCO_DESTINO() {
        return BANCO_DESTINO;
    }


    /**
     * Sets the BANCO_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param BANCO_DESTINO
     */
    public void setBANCO_DESTINO(java.lang.Short BANCO_DESTINO) {
        this.BANCO_DESTINO = BANCO_DESTINO;
    }


    /**
     * Gets the AGENCIA_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return AGENCIA_DESTINO
     */
    public java.lang.Short getAGENCIA_DESTINO() {
        return AGENCIA_DESTINO;
    }


    /**
     * Sets the AGENCIA_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param AGENCIA_DESTINO
     */
    public void setAGENCIA_DESTINO(java.lang.Short AGENCIA_DESTINO) {
        this.AGENCIA_DESTINO = AGENCIA_DESTINO;
    }


    /**
     * Gets the CONTA_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return CONTA_DESTINO
     */
    public java.lang.Long getCONTA_DESTINO() {
        return CONTA_DESTINO;
    }


    /**
     * Sets the CONTA_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param CONTA_DESTINO
     */
    public void setCONTA_DESTINO(java.lang.Long CONTA_DESTINO) {
        this.CONTA_DESTINO = CONTA_DESTINO;
    }


    /**
     * Gets the DV_CONTA_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return DV_CONTA_DESTINO
     */
    public java.lang.String getDV_CONTA_DESTINO() {
        return DV_CONTA_DESTINO;
    }


    /**
     * Sets the DV_CONTA_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param DV_CONTA_DESTINO
     */
    public void setDV_CONTA_DESTINO(java.lang.String DV_CONTA_DESTINO) {
        this.DV_CONTA_DESTINO = DV_CONTA_DESTINO;
    }


    /**
     * Gets the DESTINATARIO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return DESTINATARIO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type getDESTINATARIO() {
        return DESTINATARIO;
    }


    /**
     * Sets the DESTINATARIO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param DESTINATARIO
     */
    public void setDESTINATARIO(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type DESTINATARIO) {
        this.DESTINATARIO = DESTINATARIO;
    }


    /**
     * Gets the TIPO_CONTA_DESTINATARIO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return TIPO_CONTA_DESTINATARIO
     */
    public java.lang.Short getTIPO_CONTA_DESTINATARIO() {
        return TIPO_CONTA_DESTINATARIO;
    }


    /**
     * Sets the TIPO_CONTA_DESTINATARIO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param TIPO_CONTA_DESTINATARIO
     */
    public void setTIPO_CONTA_DESTINATARIO(java.lang.Short TIPO_CONTA_DESTINATARIO) {
        this.TIPO_CONTA_DESTINATARIO = TIPO_CONTA_DESTINATARIO;
    }


    /**
     * Gets the TIPO_PESSOA_DESTINATARIO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return TIPO_PESSOA_DESTINATARIO
     */
    public java.lang.String getTIPO_PESSOA_DESTINATARIO() {
        return TIPO_PESSOA_DESTINATARIO;
    }


    /**
     * Sets the TIPO_PESSOA_DESTINATARIO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param TIPO_PESSOA_DESTINATARIO
     */
    public void setTIPO_PESSOA_DESTINATARIO(java.lang.String TIPO_PESSOA_DESTINATARIO) {
        this.TIPO_PESSOA_DESTINATARIO = TIPO_PESSOA_DESTINATARIO;
    }


    /**
     * Gets the TARIFA value for this Dados_saida_transacao_ted_Type.
     * 
     * @return TARIFA
     */
    public java.math.BigDecimal getTARIFA() {
        return TARIFA;
    }


    /**
     * Sets the TARIFA value for this Dados_saida_transacao_ted_Type.
     * 
     * @param TARIFA
     */
    public void setTARIFA(java.math.BigDecimal TARIFA) {
        this.TARIFA = TARIFA;
    }


    /**
     * Gets the IDENTIFICACAO_DEPOSITO_JUDICIAL value for this Dados_saida_transacao_ted_Type.
     * 
     * @return IDENTIFICACAO_DEPOSITO_JUDICIAL
     */
    public java.lang.Long getIDENTIFICACAO_DEPOSITO_JUDICIAL() {
        return IDENTIFICACAO_DEPOSITO_JUDICIAL;
    }


    /**
     * Sets the IDENTIFICACAO_DEPOSITO_JUDICIAL value for this Dados_saida_transacao_ted_Type.
     * 
     * @param IDENTIFICACAO_DEPOSITO_JUDICIAL
     */
    public void setIDENTIFICACAO_DEPOSITO_JUDICIAL(java.lang.Long IDENTIFICACAO_DEPOSITO_JUDICIAL) {
        this.IDENTIFICACAO_DEPOSITO_JUDICIAL = IDENTIFICACAO_DEPOSITO_JUDICIAL;
    }


    /**
     * Gets the DV_IDENTIFICACAO_DEPOSITO_JUDICIAL value for this Dados_saida_transacao_ted_Type.
     * 
     * @return DV_IDENTIFICACAO_DEPOSITO_JUDICIAL
     */
    public java.lang.Short getDV_IDENTIFICACAO_DEPOSITO_JUDICIAL() {
        return DV_IDENTIFICACAO_DEPOSITO_JUDICIAL;
    }


    /**
     * Sets the DV_IDENTIFICACAO_DEPOSITO_JUDICIAL value for this Dados_saida_transacao_ted_Type.
     * 
     * @param DV_IDENTIFICACAO_DEPOSITO_JUDICIAL
     */
    public void setDV_IDENTIFICACAO_DEPOSITO_JUDICIAL(java.lang.Short DV_IDENTIFICACAO_DEPOSITO_JUDICIAL) {
        this.DV_IDENTIFICACAO_DEPOSITO_JUDICIAL = DV_IDENTIFICACAO_DEPOSITO_JUDICIAL;
    }


    /**
     * Gets the PESSOA_CORT value for this Dados_saida_transacao_ted_Type.
     * 
     * @return PESSOA_CORT
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type getPESSOA_CORT() {
        return PESSOA_CORT;
    }


    /**
     * Sets the PESSOA_CORT value for this Dados_saida_transacao_ted_Type.
     * 
     * @param PESSOA_CORT
     */
    public void setPESSOA_CORT(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Pessoa_Type PESSOA_CORT) {
        this.PESSOA_CORT = PESSOA_CORT;
    }


    /**
     * Gets the TIPO_PESSOA_CORT value for this Dados_saida_transacao_ted_Type.
     * 
     * @return TIPO_PESSOA_CORT
     */
    public java.lang.String getTIPO_PESSOA_CORT() {
        return TIPO_PESSOA_CORT;
    }


    /**
     * Sets the TIPO_PESSOA_CORT value for this Dados_saida_transacao_ted_Type.
     * 
     * @param TIPO_PESSOA_CORT
     */
    public void setTIPO_PESSOA_CORT(java.lang.String TIPO_PESSOA_CORT) {
        this.TIPO_PESSOA_CORT = TIPO_PESSOA_CORT;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the DATA_TRANSACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return DATA_TRANSACAO
     */
    public java.util.Calendar getDATA_TRANSACAO() {
        return DATA_TRANSACAO;
    }


    /**
     * Sets the DATA_TRANSACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param DATA_TRANSACAO
     */
    public void setDATA_TRANSACAO(java.util.Calendar DATA_TRANSACAO) {
        this.DATA_TRANSACAO = DATA_TRANSACAO;
    }


    /**
     * Gets the CANAL_ORIGEM value for this Dados_saida_transacao_ted_Type.
     * 
     * @return CANAL_ORIGEM
     */
    public java.lang.String getCANAL_ORIGEM() {
        return CANAL_ORIGEM;
    }


    /**
     * Sets the CANAL_ORIGEM value for this Dados_saida_transacao_ted_Type.
     * 
     * @param CANAL_ORIGEM
     */
    public void setCANAL_ORIGEM(java.lang.String CANAL_ORIGEM) {
        this.CANAL_ORIGEM = CANAL_ORIGEM;
    }


    /**
     * Gets the NUMERO_DOC_DEBITO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return NUMERO_DOC_DEBITO
     */
    public java.lang.Integer getNUMERO_DOC_DEBITO() {
        return NUMERO_DOC_DEBITO;
    }


    /**
     * Sets the NUMERO_DOC_DEBITO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param NUMERO_DOC_DEBITO
     */
    public void setNUMERO_DOC_DEBITO(java.lang.Integer NUMERO_DOC_DEBITO) {
        this.NUMERO_DOC_DEBITO = NUMERO_DOC_DEBITO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DadosSaidaTEDHolder)) return false;
        DadosSaidaTEDHolder other = (DadosSaidaTEDHolder) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NSU==null && other.getNSU()==null) || 
             (this.NSU!=null &&
              this.NSU.equals(other.getNSU()))) &&
            ((this.REMETENTE1==null && other.getREMETENTE1()==null) || 
             (this.REMETENTE1!=null &&
              this.REMETENTE1.equals(other.getREMETENTE1()))) &&
            ((this.REMETENTE2==null && other.getREMETENTE2()==null) || 
             (this.REMETENTE2!=null &&
              this.REMETENTE2.equals(other.getREMETENTE2()))) &&
            ((this.TIPO_CONTA_REMETENTE==null && other.getTIPO_CONTA_REMETENTE()==null) || 
             (this.TIPO_CONTA_REMETENTE!=null &&
              this.TIPO_CONTA_REMETENTE.equals(other.getTIPO_CONTA_REMETENTE()))) &&
            ((this.TIPO_PESSOA_REMETENTE==null && other.getTIPO_PESSOA_REMETENTE()==null) || 
             (this.TIPO_PESSOA_REMETENTE!=null &&
              this.TIPO_PESSOA_REMETENTE.equals(other.getTIPO_PESSOA_REMETENTE()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.DATA_MOVIMENTO==null && other.getDATA_MOVIMENTO()==null) || 
             (this.DATA_MOVIMENTO!=null &&
              this.DATA_MOVIMENTO.equals(other.getDATA_MOVIMENTO()))) &&
            ((this.DATA_TRANSFERENCIA==null && other.getDATA_TRANSFERENCIA()==null) || 
             (this.DATA_TRANSFERENCIA!=null &&
              this.DATA_TRANSFERENCIA.equals(other.getDATA_TRANSFERENCIA()))) &&
            ((this.NSU_CONFIRMACAO==null && other.getNSU_CONFIRMACAO()==null) || 
             (this.NSU_CONFIRMACAO!=null &&
              this.NSU_CONFIRMACAO.equals(other.getNSU_CONFIRMACAO()))) &&
            ((this.TIPO_DOC==null && other.getTIPO_DOC()==null) || 
             (this.TIPO_DOC!=null &&
              this.TIPO_DOC.equals(other.getTIPO_DOC()))) &&
            ((this.TIPO_SERVICO==null && other.getTIPO_SERVICO()==null) || 
             (this.TIPO_SERVICO!=null &&
              this.TIPO_SERVICO.equals(other.getTIPO_SERVICO()))) &&
            ((this.MODO==null && other.getMODO()==null) || 
             (this.MODO!=null &&
              this.MODO.equals(other.getMODO()))) &&
            ((this.ISPB_DESTINO==null && other.getISPB_DESTINO()==null) || 
             (this.ISPB_DESTINO!=null &&
              this.ISPB_DESTINO.equals(other.getISPB_DESTINO()))) &&
            ((this.BANCO_DESTINO==null && other.getBANCO_DESTINO()==null) || 
             (this.BANCO_DESTINO!=null &&
              this.BANCO_DESTINO.equals(other.getBANCO_DESTINO()))) &&
            ((this.AGENCIA_DESTINO==null && other.getAGENCIA_DESTINO()==null) || 
             (this.AGENCIA_DESTINO!=null &&
              this.AGENCIA_DESTINO.equals(other.getAGENCIA_DESTINO()))) &&
            ((this.CONTA_DESTINO==null && other.getCONTA_DESTINO()==null) || 
             (this.CONTA_DESTINO!=null &&
              this.CONTA_DESTINO.equals(other.getCONTA_DESTINO()))) &&
            ((this.DV_CONTA_DESTINO==null && other.getDV_CONTA_DESTINO()==null) || 
             (this.DV_CONTA_DESTINO!=null &&
              this.DV_CONTA_DESTINO.equals(other.getDV_CONTA_DESTINO()))) &&
            ((this.DESTINATARIO==null && other.getDESTINATARIO()==null) || 
             (this.DESTINATARIO!=null &&
              this.DESTINATARIO.equals(other.getDESTINATARIO()))) &&
            ((this.TIPO_CONTA_DESTINATARIO==null && other.getTIPO_CONTA_DESTINATARIO()==null) || 
             (this.TIPO_CONTA_DESTINATARIO!=null &&
              this.TIPO_CONTA_DESTINATARIO.equals(other.getTIPO_CONTA_DESTINATARIO()))) &&
            ((this.TIPO_PESSOA_DESTINATARIO==null && other.getTIPO_PESSOA_DESTINATARIO()==null) || 
             (this.TIPO_PESSOA_DESTINATARIO!=null &&
              this.TIPO_PESSOA_DESTINATARIO.equals(other.getTIPO_PESSOA_DESTINATARIO()))) &&
            ((this.TARIFA==null && other.getTARIFA()==null) || 
             (this.TARIFA!=null &&
              this.TARIFA.equals(other.getTARIFA()))) &&
            ((this.IDENTIFICACAO_DEPOSITO_JUDICIAL==null && other.getIDENTIFICACAO_DEPOSITO_JUDICIAL()==null) || 
             (this.IDENTIFICACAO_DEPOSITO_JUDICIAL!=null &&
              this.IDENTIFICACAO_DEPOSITO_JUDICIAL.equals(other.getIDENTIFICACAO_DEPOSITO_JUDICIAL()))) &&
            ((this.DV_IDENTIFICACAO_DEPOSITO_JUDICIAL==null && other.getDV_IDENTIFICACAO_DEPOSITO_JUDICIAL()==null) || 
             (this.DV_IDENTIFICACAO_DEPOSITO_JUDICIAL!=null &&
              this.DV_IDENTIFICACAO_DEPOSITO_JUDICIAL.equals(other.getDV_IDENTIFICACAO_DEPOSITO_JUDICIAL()))) &&
            ((this.PESSOA_CORT==null && other.getPESSOA_CORT()==null) || 
             (this.PESSOA_CORT!=null &&
              this.PESSOA_CORT.equals(other.getPESSOA_CORT()))) &&
            ((this.TIPO_PESSOA_CORT==null && other.getTIPO_PESSOA_CORT()==null) || 
             (this.TIPO_PESSOA_CORT!=null &&
              this.TIPO_PESSOA_CORT.equals(other.getTIPO_PESSOA_CORT()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.DATA_TRANSACAO==null && other.getDATA_TRANSACAO()==null) || 
             (this.DATA_TRANSACAO!=null &&
              this.DATA_TRANSACAO.equals(other.getDATA_TRANSACAO()))) &&
            ((this.CANAL_ORIGEM==null && other.getCANAL_ORIGEM()==null) || 
             (this.CANAL_ORIGEM!=null &&
              this.CANAL_ORIGEM.equals(other.getCANAL_ORIGEM()))) &&
            ((this.NUMERO_DOC_DEBITO==null && other.getNUMERO_DOC_DEBITO()==null) || 
             (this.NUMERO_DOC_DEBITO!=null &&
              this.NUMERO_DOC_DEBITO.equals(other.getNUMERO_DOC_DEBITO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNSU() != null) {
            _hashCode += getNSU().hashCode();
        }
        if (getREMETENTE1() != null) {
            _hashCode += getREMETENTE1().hashCode();
        }
        if (getREMETENTE2() != null) {
            _hashCode += getREMETENTE2().hashCode();
        }
        if (getTIPO_CONTA_REMETENTE() != null) {
            _hashCode += getTIPO_CONTA_REMETENTE().hashCode();
        }
        if (getTIPO_PESSOA_REMETENTE() != null) {
            _hashCode += getTIPO_PESSOA_REMETENTE().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getDATA_MOVIMENTO() != null) {
            _hashCode += getDATA_MOVIMENTO().hashCode();
        }
        if (getDATA_TRANSFERENCIA() != null) {
            _hashCode += getDATA_TRANSFERENCIA().hashCode();
        }
        if (getNSU_CONFIRMACAO() != null) {
            _hashCode += getNSU_CONFIRMACAO().hashCode();
        }
        if (getTIPO_DOC() != null) {
            _hashCode += getTIPO_DOC().hashCode();
        }
        if (getTIPO_SERVICO() != null) {
            _hashCode += getTIPO_SERVICO().hashCode();
        }
        if (getMODO() != null) {
            _hashCode += getMODO().hashCode();
        }
        if (getISPB_DESTINO() != null) {
            _hashCode += getISPB_DESTINO().hashCode();
        }
        if (getBANCO_DESTINO() != null) {
            _hashCode += getBANCO_DESTINO().hashCode();
        }
        if (getAGENCIA_DESTINO() != null) {
            _hashCode += getAGENCIA_DESTINO().hashCode();
        }
        if (getCONTA_DESTINO() != null) {
            _hashCode += getCONTA_DESTINO().hashCode();
        }
        if (getDV_CONTA_DESTINO() != null) {
            _hashCode += getDV_CONTA_DESTINO().hashCode();
        }
        if (getDESTINATARIO() != null) {
            _hashCode += getDESTINATARIO().hashCode();
        }
        if (getTIPO_CONTA_DESTINATARIO() != null) {
            _hashCode += getTIPO_CONTA_DESTINATARIO().hashCode();
        }
        if (getTIPO_PESSOA_DESTINATARIO() != null) {
            _hashCode += getTIPO_PESSOA_DESTINATARIO().hashCode();
        }
        if (getTARIFA() != null) {
            _hashCode += getTARIFA().hashCode();
        }
        if (getIDENTIFICACAO_DEPOSITO_JUDICIAL() != null) {
            _hashCode += getIDENTIFICACAO_DEPOSITO_JUDICIAL().hashCode();
        }
        if (getDV_IDENTIFICACAO_DEPOSITO_JUDICIAL() != null) {
            _hashCode += getDV_IDENTIFICACAO_DEPOSITO_JUDICIAL().hashCode();
        }
        if (getPESSOA_CORT() != null) {
            _hashCode += getPESSOA_CORT().hashCode();
        }
        if (getTIPO_PESSOA_CORT() != null) {
            _hashCode += getTIPO_PESSOA_CORT().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getDATA_TRANSACAO() != null) {
            _hashCode += getDATA_TRANSACAO().hashCode();
        }
        if (getCANAL_ORIGEM() != null) {
            _hashCode += getCANAL_ORIGEM().hashCode();
        }
        if (getNUMERO_DOC_DEBITO() != null) {
            _hashCode += getNUMERO_DOC_DEBITO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DadosSaidaTEDHolder.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_transacao_ted_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REMETENTE1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "REMETENTE1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "pessoa_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REMETENTE2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "REMETENTE2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "pessoa_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_CONTA_REMETENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_CONTA_REMETENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_PESSOA_REMETENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_PESSOA_REMETENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_MOVIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_MOVIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_TRANSFERENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_TRANSFERENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU_CONFIRMACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU_CONFIRMACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_DOC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_DOC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_SERVICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_SERVICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MODO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MODO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISPB_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ISPB_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BANCO_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BANCO_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AGENCIA_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AGENCIA_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DV_CONTA_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DV_CONTA_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DESTINATARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DESTINATARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "pessoa_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_CONTA_DESTINATARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_CONTA_DESTINATARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_PESSOA_DESTINATARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_PESSOA_DESTINATARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TARIFA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TARIFA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO_DEPOSITO_JUDICIAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO_DEPOSITO_JUDICIAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DV_IDENTIFICACAO_DEPOSITO_JUDICIAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DV_IDENTIFICACAO_DEPOSITO_JUDICIAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PESSOA_CORT");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PESSOA_CORT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "pessoa_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_PESSOA_CORT");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_PESSOA_CORT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CANAL_ORIGEM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CANAL_ORIGEM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO_DOC_DEBITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO_DOC_DEBITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
