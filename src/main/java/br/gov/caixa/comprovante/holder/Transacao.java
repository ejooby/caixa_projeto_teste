package br.gov.caixa.comprovante.holder;

import java.util.List;

import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

public class Transacao {
	
	
	private List<TransacaoInt> transacao;
	
	private String valorTotalMes = "";

	public List<TransacaoInt> getTransacao() {
		return transacao;
	}

	public void setTransacao(List<TransacaoInt> transacao) {
		this.transacao = transacao;
	}

	public String getValorTotalMes() {
		return valorTotalMes;
	}

	public void setValorTotalMes(String valorTotalMes) {
		this.valorTotalMes = valorTotalMes;
	}
	

	
	
	
	
	
}
