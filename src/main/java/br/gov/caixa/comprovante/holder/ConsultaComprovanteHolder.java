package br.gov.caixa.comprovante.holder;

import java.util.List;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


/**
 * 
 * @author Capgemini SP
 *
 */


@JsonInclude(Include.NON_NULL)
public class ConsultaComprovanteHolder extends AbstractHolder{
	
	
    /**
     * 
     */
    private long conta;
    /**
     * 
     */
    private short produto;
    /**
     * 
     */
    private short agencia;
    /**
     * 
     */
    private short dv;
    /**
     * 
     */
    private String dataInicio;
    /**
     * 
     */
    private String dataFim;
    /**
     * 
     */
    private String data;
    
    
    private List<String> solicitacoes;
    
    
    private String serv;
    
    
    private boolean isAgendamento;
    
    
    
	
	public boolean isAgendamento() {
		return isAgendamento;
	}
	public void setAgendamento(boolean isAgendamento) {
		this.isAgendamento = isAgendamento;
	}
	public long getConta() {
		return conta;
	}
	public void setConta(long conta) {
		this.conta = conta;
	}
	public short getProduto() {
		return produto;
	}
	public void setProduto(short produto) {
		this.produto = produto;
	}
	public short getAgencia() {
		return agencia;
	}
	public void setAgencia(short agencia) {
		this.agencia = agencia;
	}
	public short getDv() {
		return dv;
	}
	public void setDv(short dv) {
		this.dv = dv;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public List<String> getSolicitacoes() {
		return solicitacoes;
	}
	public void setSolicitacoes(List<String> solicitacoes) {
		this.solicitacoes = solicitacoes;
	}
	public String getServ() {
		return serv;
	}
	public void setServ(String serv) {
		this.serv = serv;
	}

	
}
