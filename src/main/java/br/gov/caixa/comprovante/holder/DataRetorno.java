package br.gov.caixa.comprovante.holder;

public interface DataRetorno {
	
	String getData() ;
	
	void setData(String data);

}
