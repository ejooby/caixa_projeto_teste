package br.gov.caixa.comprovante.controller;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.gov.caixa.api.common.holder.ErrorHolder;
import br.gov.caixa.api.common.holder.RetornoHolder;
import br.gov.caixa.api.common.resource.util.ResourceUtil;
import br.gov.caixa.comprovante.holder.ConsultaComprovanteHolder;
import br.gov.caixa.comprovante.holder.ConsultaPagamentoDetalheHolder;
import br.gov.caixa.comprovante.service.impl.ConsultaComprovanteServiceImpl;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_entrada_solicitacoes_TypeSOLICITACAO;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Servico_saida_negocial_Type;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api/public")
@Api(value = "/api/public", description = "SINBM API")
public class ConsultaComprovanteController extends AbstractController {

	protected static final Logger logger = LoggerFactory.getLogger(ConsultaComprovanteController.class);

	ConsultaComprovanteServiceImpl consultaComprovanteService = new ConsultaComprovanteServiceImpl();

	/*******************************************************************************************************************************************************/
	/**                                                                                                                                                    */
	/*******************************************************************************************************************************************************/
	@RequestMapping(value = "/consultaDetalheComprovantePagamento", method = RequestMethod.POST, produces = {
			"application/json; charset=UTF-8" })
	@ResponseBody
	public RetornoHolder consultaDetalheComprovantePagamento(
			@RequestBody(required = false) ConsultaPagamentoDetalheHolder consultaSadoExtratoHolder,
			@RequestHeader(required = false) HttpHeaders header) {
		logger.info("INICIO consultaDetalheComprovantePagamento Controller()");
		// SinbmInputErrorAudit sinbmAudit = new SinbmInputErrorAudit (header);

		RetornoHolder retorno = new RetornoHolder();
		Servico_saida_negocial_Type saida;

		try {
			ResourceUtil.getInstance().imprimeFormatoJson(consultaSadoExtratoHolder, "ENTRADA API =");

			
			consultaSadoExtratoHolder.setDataInicio(consultaSadoExtratoHolder.getData());
			consultaSadoExtratoHolder.setDataFim(consultaSadoExtratoHolder.getData());
			retorno = consultaComprovanteService.consultaDetalhePagamento(consultaSadoExtratoHolder);
			

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

		
		ResourceUtil.getInstance().imprimeFormatoJson(retorno, "SAIDA API =");
		logger.info("FIM consultaDetalheComprovantePagamento Controller()");

		return retorno;
	}

	public static void main(String[] args) {

		ConsultaComprovanteController con = new ConsultaComprovanteController();

		ConsultaPagamentoDetalheHolder c = new ConsultaPagamentoDetalheHolder();

		c.setAgencia("1679");
		c.setConta("53");
		c.setProduto("1");
		c.setDv("5");
		c.setNsu("13000008");
		c.setData("09/11/2018");
		c.setSolicitacao("BOLETO");
		

		con.consultaDetalheComprovantePagamento(c, null);

	}

	/*******************************************************************************************************************************************************/
	/**                                                                                                                                                    */
	/*******************************************************************************************************************************************************/
	// @Audit(operacao=TipoOperacaoEnum.CONSULTA_COMPROVANTE_TRANSFERENCIA)
	@RequestMapping(value = "/consultaComprovanteTransferencia", method = RequestMethod.POST, produces = {
			"application/json; charset=UTF-8" })
	@ResponseBody
	public RetornoHolder consultaComprovanteTransferencia(
			@RequestBody(required = false) ConsultaComprovanteHolder consultaSadoExtratoHolder,
			@RequestHeader(required = false) HttpHeaders header) {

		logger.info("INICIO consultaComprovanteTransferencia Controller()");
		ResourceUtil.getInstance().imprimeFormatoJson(consultaSadoExtratoHolder, "ENTRADA API = ");

		RetornoHolder retorno = new RetornoHolder();
		Servico_saida_negocial_Type saida;

		try {

			String diaIni = "";
			String mesIni = "";
			String anoIni = "";
			DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/yyyy");
			String mobiDataIni = consultaSadoExtratoHolder.getData();
			DateTime dtHoje = new DateTime();
			if (mobiDataIni == null || mobiDataIni.equals("")) {
				consultaSadoExtratoHolder.setData(dtHoje.toString(formatter));
			}

			diaIni = "01";
			mesIni = consultaSadoExtratoHolder.getData().substring(0, 2);
			anoIni = consultaSadoExtratoHolder.getData().substring(3, 7);

			formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
			DateTime dataInicioDT = formatter.parseDateTime(diaIni + "/" + mesIni + "/" + anoIni);
			consultaSadoExtratoHolder.setDataInicio(diaIni + "/" + mesIni + "/" + anoIni);

			if ((dataInicioDT.getMonthOfYear() != dtHoje.getMonthOfYear())) {
				int ultimoDiadoMes = dataInicioDT.dayOfMonth().getMaximumValue();
				String dF = ultimoDiadoMes < 10 ? "0" + ultimoDiadoMes : "" + ultimoDiadoMes;
				dtHoje = formatter.parseDateTime(dF + "/" + mesIni + "/" + anoIni);
			} else {

				if (dataInicioDT.getYear() != dtHoje.getYear()) {
					int ultimoDiadoMes = dataInicioDT.dayOfMonth().getMaximumValue();
					String dF = ultimoDiadoMes < 10 ? "0" + ultimoDiadoMes : "" + ultimoDiadoMes;
					dtHoje = formatter.parseDateTime(dF + "/" + mesIni + "/" + anoIni);
				}
			}

			String diaFim = dtHoje.getDayOfMonth() < 10 ? "0" + dtHoje.getDayOfMonth() : "" + dtHoje.getDayOfMonth();
			String mesFim = dtHoje.getMonthOfYear() < 10 ? "0" + dtHoje.getMonthOfYear() : "" + dtHoje.getMonthOfYear();
			String anoFim = "" + dtHoje.getYear();
			consultaSadoExtratoHolder.setDataFim(diaFim + "/" + mesFim + "/" + anoFim);

			if ((dataInicioDT.getMonthOfYear() == dtHoje.getMonthOfYear()) && consultaSadoExtratoHolder.isAgendamento()
					&& dataInicioDT.getYear() == dtHoje.getYear()) {

				DateTime diaHoje = new DateTime();
				String diaAgora = (diaHoje.getDayOfMonth() < 10 ? "0" + diaHoje.getDayOfMonth()
						: "" + diaHoje.getDayOfMonth());
				consultaSadoExtratoHolder.setDataInicio(diaAgora + "/" + mesIni + "/" + anoIni);

				int ultimoDiadoMes = dataInicioDT.dayOfMonth().getMaximumValue();
				String dF = ultimoDiadoMes < 10 ? "0" + ultimoDiadoMes : "" + ultimoDiadoMes;
				consultaSadoExtratoHolder.setDataFim(dF + "/" + mesIni + "/" + anoIni);

			}

			short agencia = consultaSadoExtratoHolder.getAgencia();
			short produto = consultaSadoExtratoHolder.getProduto();
			long conta = consultaSadoExtratoHolder.getConta();
			short dv = consultaSadoExtratoHolder.getDv();

			List<String> solicitacoes = new ArrayList<String>();

			if (consultaSadoExtratoHolder.getServ() == null) {

				// solicitado pela helo para em momento de teste
				// fazer funcionar
				 solicitacoes.add(Dados_entrada_solicitacoes_TypeSOLICITACAO._TEV);
				 solicitacoes.add(Dados_entrada_solicitacoes_TypeSOLICITACAO._TED); 
				 solicitacoes.add(Dados_entrada_solicitacoes_TypeSOLICITACAO._DOC);

				
			} else {
				solicitacoes.add(consultaSadoExtratoHolder.getServ());
			}

			ConsultaComprovanteHolder consultaExtratoHolder = new ConsultaComprovanteHolder();
			consultaExtratoHolder.setSolicitacoes(solicitacoes);
			consultaExtratoHolder.setDataInicio(consultaSadoExtratoHolder.getDataInicio());
			consultaExtratoHolder.setDataFim(consultaSadoExtratoHolder.getDataFim());

			consultaExtratoHolder.setConta(conta);
			consultaExtratoHolder.setProduto(produto);
			consultaExtratoHolder.setAgencia(agencia);
			consultaExtratoHolder.setDv(dv);
			retorno = consultaComprovanteService.consultaComprovanteTransferencia(consultaExtratoHolder);

		} catch (Exception e) {
			logger.error(e.getMessage());
			ErrorHolder error = new ErrorHolder();

			error.setCodigo(999);
			error.setCausa("");
			error.setMensagem(e.getMessage());
			retorno.setError(error);

			e.printStackTrace();
		}

		ResourceUtil.getInstance().imprimeFormatoJson(retorno, "SAIDA API = ");
		logger.info("FIM consultaComprovanteTransferencia Controller()");
		return retorno;
	}

	/*******************************************************************************************************************************************************/
	/**                                                                                                                                                    */
	/*******************************************************************************************************************************************************/
	// @Audit(operacao=TipoOperacaoEnum.CONSULTA_COMPROVANTE_PAGAMENTO)
	@RequestMapping(value = "/consultaComprovantePagamento",
			method = RequestMethod.POST, produces = {
			"application/json; charset=UTF-8" })
	@ResponseBody
	public RetornoHolder consultaComprovantePagamento(
			@RequestBody(required = false) ConsultaComprovanteHolder consultaSadoExtratoHolder,
			@RequestHeader(required = false) HttpHeaders header) {

		logger.info("INICIO consultaComprovantePagamento Controller()");
		ResourceUtil.getInstance().imprimeFormatoJson(consultaSadoExtratoHolder, "ENTRADA API = ");

		RetornoHolder retorno = new RetornoHolder();
		Servico_saida_negocial_Type saida;

		try {
			String diaIni = "";
			String mesIni = "";
			String anoIni = "";
			DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/yyyy");
			String mobiDataIni = consultaSadoExtratoHolder.getData();
			DateTime dtHoje = new DateTime();
			if (mobiDataIni == null || mobiDataIni.equals("")) {
				consultaSadoExtratoHolder.setData(dtHoje.toString(formatter));
			}

			diaIni = "01";
			mesIni = consultaSadoExtratoHolder.getData().substring(0, 2);
			anoIni = consultaSadoExtratoHolder.getData().substring(3, 7);

			formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
			DateTime dataInicioDT = formatter.parseDateTime(diaIni + "/" + mesIni + "/" + anoIni);
			consultaSadoExtratoHolder.setDataInicio(diaIni + "/" + mesIni + "/" + anoIni);

			// Se Data do Saldo for diferente do mes corrente
			// , pega como ultimo dia o dia do mes solicitado
			if (dataInicioDT.getMonthOfYear() != dtHoje.getMonthOfYear()) {
				int ultimoDiadoMes = dataInicioDT.dayOfMonth().getMaximumValue();
				String dF = ultimoDiadoMes < 10 ? "0" + ultimoDiadoMes : "" + ultimoDiadoMes;
				dtHoje = formatter.parseDateTime(dF + "/" + mesIni + "/" + anoIni);
			} else {
				if (dataInicioDT.getYear() != dtHoje.getYear()) {
					int ultimoDiadoMes = dataInicioDT.dayOfMonth().getMaximumValue();
					String dF = ultimoDiadoMes < 10 ? "0" + ultimoDiadoMes : "" + ultimoDiadoMes;
					dtHoje = formatter.parseDateTime(dF + "/" + mesIni + "/" + anoIni);
				}
			}

			String diaFim = dtHoje.getDayOfMonth() < 10 ? "0" + dtHoje.getDayOfMonth() : "" + dtHoje.getDayOfMonth();
			String mesFim = dtHoje.getMonthOfYear() < 10 ? "0" + dtHoje.getMonthOfYear() : "" + dtHoje.getMonthOfYear();
			String anoFim = "" + dtHoje.getYear();
			String dataFim = diaFim + "/" + mesFim + "/" + anoFim;
			consultaSadoExtratoHolder.setDataFim(diaFim + "/" + mesFim + "/" + anoFim);

			if ((dataInicioDT.getMonthOfYear() == dtHoje.getMonthOfYear()) && consultaSadoExtratoHolder.isAgendamento()
					&& dataInicioDT.getYear() == dtHoje.getYear()) {

				DateTime diaHoje = new DateTime();
				String diaAgora = (diaHoje.getDayOfMonth() < 10 ? "0" + diaHoje.getDayOfMonth()
						: "" + diaHoje.getDayOfMonth());
				consultaSadoExtratoHolder.setDataInicio(diaAgora + "/" + mesIni + "/" + anoIni);

				int ultimoDiadoMes = dataInicioDT.dayOfMonth().getMaximumValue();
				String dF = ultimoDiadoMes < 10 ? "0" + ultimoDiadoMes : "" + ultimoDiadoMes;
				consultaSadoExtratoHolder.setDataFim(dF + "/" + mesIni + "/" + anoIni);

			}

			short agencia = consultaSadoExtratoHolder.getAgencia();
			short produto = consultaSadoExtratoHolder.getProduto();
			long conta = consultaSadoExtratoHolder.getConta();
			short dv = consultaSadoExtratoHolder.getDv();

			List<String> solicitacoes = new ArrayList<String>();
			if (consultaSadoExtratoHolder.getServ() == null) {
				// solicitacoes.add(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO._IPVA_SP);
				// solicitacoes.add(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO._DPVAT_SP);
				// solicitacoes.add(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO._MULTAS_SP);
				solicitacoes.add(
						br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO._BOLETO);
				solicitacoes.add(
						br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO._CONCESSIONARIA);
				// solicitacoes.add(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO._LICENCIAMENTO_SP);
				// solicitacoes.add(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO._DAED);
			} else {
				solicitacoes.add(consultaSadoExtratoHolder.getServ());
			}

			ConsultaComprovanteHolder consultaExtratoHolder = new ConsultaComprovanteHolder();
			consultaExtratoHolder.setSolicitacoes(solicitacoes);
			consultaExtratoHolder.setDataInicio(consultaSadoExtratoHolder.getDataInicio());
			consultaExtratoHolder.setDataFim(consultaSadoExtratoHolder.getDataFim());

			consultaExtratoHolder.setConta(conta);
			consultaExtratoHolder.setProduto(produto);
			consultaExtratoHolder.setAgencia(agencia);
			consultaExtratoHolder.setDv(dv);
			retorno = consultaComprovanteService.consultaComprovantePagamento(consultaExtratoHolder);

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

		ResourceUtil.getInstance().imprimeFormatoJson(retorno, "SAIDA API = ");
		logger.info("FIM consultaComprovantePagamento Controller()");
		return retorno;
	}

}
