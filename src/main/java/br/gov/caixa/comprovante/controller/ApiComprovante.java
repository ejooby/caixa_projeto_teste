package br.gov.caixa.comprovante.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.gov.caixa.api.common.holder.ErrorHolder;
import br.gov.caixa.api.common.holder.RetornoHolder;
import br.gov.caixa.api.common.service.impl.VersaoServiceImpl;
import br.gov.caixa.comprovante.holder.ConsultaComprovanteHolder;
import br.gov.caixa.comprovante.holder.ConsultaPagamentoDetalheHolder;

@RestController
@RequestMapping("/api/public")
public class ApiComprovante  {

	protected static final Logger logger = LoggerFactory.getLogger(ApiComprovante.class);
	
	

	@RequestMapping("/helloworld")
	@ResponseBody
	public RetornoHolder helloworld(@RequestHeader(required = false) HttpHeaders header) {
		RetornoHolder retorno = new RetornoHolder();
		try {
			retorno.setDados(new VersaoServiceImpl().getVersao());
		} catch (Exception e) {
			ErrorHolder err = new ErrorHolder();
			err.setMensagem(e.getMessage());
			retorno .setError(err);
		}
		return retorno;
	}

	
	public static void main(String[] args) {
		ApiComprovante api = new ApiComprovante();
		
	
		String data	= "11/2018"; 
		String 	agencia  = "1679";
		String conta	= "589"; 
		String produto	= "1"; 
		String dv	= "0"; 
		String solicitacao	= "BOLETO";
		String nsu	= "123456";
		
		
		api.consultaComprovanteTransferenciaTeste(data, agencia, conta, produto, dv);//, solicitacao, nsu);
	
	
		
	}
	
	
	
	@RequestMapping("/consultaComprovanteTransferenciaTeste")
	@ResponseBody
	public RetornoHolder consultaComprovanteTransferenciaTeste(
			@RequestParam(value = "data", required = true) String data,
			@RequestParam(value = "agencia", required = true) String agencia,
			@RequestParam(value = "conta", required = true) String conta,
			@RequestParam(value = "produto", required = true) String produto,
			@RequestParam(value = "dv", required = true) String dv
			) {
		 ConsultaComprovanteController dd = new ConsultaComprovanteController();
		 ConsultaComprovanteHolder x = new ConsultaComprovanteHolder();
		 
		 x.setAgencia(Short.parseShort(agencia));
		 x.setConta(Long.parseLong(conta));
		 x.setProduto(Short.parseShort(produto));
		 x.setDv(Short.parseShort(dv));
		 
		 x.setData(data);
		 
		 RetornoHolder resp = dd.consultaComprovanteTransferencia(x, null);
		 
		 return resp;
		  
	}
	
	
	

	@RequestMapping("/consultaDetalheComprovantePagamentoTeste")
	@ResponseBody
	public RetornoHolder consultaDetalheComprovantePagamentoTeste(
			@RequestParam(value = "data", required = true) String data,
			@RequestParam(value = "agencia", required = true) String agencia,
			@RequestParam(value = "conta", required = true) String conta,
			@RequestParam(value = "produto", required = true) String produto,
			@RequestParam(value = "dv", required = true) String dv,
			@RequestParam(value = "solicitacao", required = true) String solicitacao,
			@RequestParam(value = "nsu", required = true) String nsu
			
			) {
		RetornoHolder retorno = new RetornoHolder();

		 ConsultaComprovanteController dd = new ConsultaComprovanteController();
		 
		 
			//RetornoHolder resp = dd.consultaComprovantePagamento(new ConsultaComprovanteHolder(), null);
			 
			 
		 ConsultaPagamentoDetalheHolder x = new ConsultaPagamentoDetalheHolder();
			 
			 
			 x.setAgencia(agencia);
			 x.setConta(conta);
			 x.setProduto(produto);
			 x.setDv(dv);
			 x.setData(data);
			 x.setNsu(nsu);
			 x.setSolicitacao(solicitacao);
			 
			 RetornoHolder resp = dd.consultaDetalheComprovantePagamento(x, null);
			  
			 System.out.println();
			 
		
		
		return resp;
	}

	
	
	
	
	
	
	

}
