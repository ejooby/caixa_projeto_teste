package br.gov.caixa.comprovante.exception.handler;

import java.util.Locale;

import javax.inject.Inject;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import br.gov.caixa.api.common.exception.BusinessException;
import br.gov.caixa.api.common.exception.ErrorCodeEnum;

@Component
public class BusinessExceptionHandler {

	@Inject
	private MessageSource messageSourceSinbm;
	
	public BusinessException handle(ErrorCodeEnum erroCode, String... parameters) {
		return new BusinessException(erroCode.code, messageSourceSinbm
				.getMessage(String.valueOf(erroCode.code), parameters, Locale.getDefault()));
	}

}

