package br.gov.caixa.comprovante.exception;

/**
 * 
 * @author Capgemini
 *
 */
public class ParserResponseServiceException extends Exception{
	/**
	 * 
	 * @param msg
	 */
	public ParserResponseServiceException (String msg) {
		super (msg);
	}
	/**
	 * 
	 * @param msg
	 * @param throwable
	 */
	public ParserResponseServiceException (String msg, Throwable throwable) {
		super (msg,throwable);
	}
	/**
	 * 
	 * @param throwable
	 */
	public ParserResponseServiceException (Throwable throwable) {
		super (throwable);
	}
}