package br.gov.caixa.comprovante.service.builder; 
 
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.caixa.api.common.resource.chave.seguranca.AutenticadorIn;
import br.gov.caixa.api.common.resource.chave.seguranca.RegraInterface;
import br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca.RegraCartaoCredito;
import br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca.RegraDARF;
import br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca.RegraFGTS;
import br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca.RegraGPS;
import br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca.RegraPagamentoConcessionaria;
import br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_boleto_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_boleto_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_cartao_credito_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_darf_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_fgts_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_gps_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_habitacao_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_pagamento_concessionaria_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_cartao_credito_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_darf_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_fgts_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_gps_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_habitacao_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_pagamento_concessionaria_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt; 
 
public class PagamentoBuilder { 
   
  private BigDecimal valorTotalTransferencia = new BigDecimal(0); 
   
  private static PagamentoBuilder instance = null; 
 
  private PagamentoBuilder() { 
    // Exists only to defeat instantiation. 
  } 
 
  public static PagamentoBuilder getInstance() { 
    if (instance == null) { 
      instance = new PagamentoBuilder(); 
    } 
    return instance; 
  } 
 
 
  protected static final   Logger logger = LoggerFactory.getLogger(PagamentoBuilder.class);   
  private SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
  
//   
//  public List<TransacaoInt> trataCARTAO_CREDITO(Dados_saida_cartao_credito_Type retornoSaidaCARTAO_CREDITO,BigDecimal valorTotalTransferencia  ){ 
//     
//	  br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_cartao_credito_Type[] obj = retornoSaidaCARTAO_CREDITO.getTRANSACOES();
//		
//	
//	  
//	  
//    List<TransacaoInt> pagsList = new ArrayList<TransacaoInt>(); 
//     
//    //for (Iterator iterator = obj.iterator(); iterator.hasNext();) {
//    for (int i = 0; i < obj.length; i++) {
//    	
//      Dados_saida_transacao_cartao_credito_Type dados_saida_transacao_cartao_credito_Type = obj[i]; 
//         
//        if(dados_saida_transacao_cartao_credito_Type.getDATA_TRANSACAO() != null){ 
//          dados_saida_transacao_cartao_credito_Type.setData(format1.format(dados_saida_transacao_cartao_credito_Type.getDATA_TRANSACAO().getTime())); 
//          dados_saida_transacao_cartao_credito_Type.setTipoServico("CARTAO_CREDITO"); 
//           
//          this.valorTotalTransferencia =       this.valorTotalTransferencia.add(dados_saida_transacao_cartao_credito_Type.getVALOR()); 
//          pagsList.add(dados_saida_transacao_cartao_credito_Type); 
//           
//          try{ 
//            String chave               = ""; 
//            RegraInterface regraCartaoCredito     = new RegraCartaoCredito(); 
//            AutenticadorIn autenticador       = new AutenticadorIn(); 
//            autenticador.setRegraInterface(regraCartaoCredito); 
//            chave              =  "";//autenticador.runAut(transa, 8, this.agencia, this.conta); 
//            //dados_saida_transacao_cartao_credito_Type.setChaveSeguranca(chave); 
//            dados_saida_transacao_cartao_credito_Type.setChaveSeguranca("YAK200?!216$98"); 
//          }catch(Exception d){ 
//             logger.error("  ERRO AO GERAR CHAVE SEGURANCA "); 
//             d.printStackTrace(); 
//           } 
//           
//           
//        }else{ 
//           logger.info("   DATA TRANSACAO DE CARTAO_CREDITO ESTA NULO "); 
//           dados_saida_transacao_cartao_credito_Type.setData(format1.format(Calendar.getInstance().getTime())); 
//        } 
//         
//    } 
//     
//    return pagsList; 
//     
//  } 
// 

//
//  public List<TransacaoInt> trataPAGAMENTO_CONCESSIONARIA(Dados_saida_pagamento_concessionaria_Type retornoPAGAMENTO_CONCESSIONARIA,BigDecimal valorTotalTransferencia  ){ 
// 
//    List<TransacaoInt> pagsList = new ArrayList<TransacaoInt>(); 
//    br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_pagamento_concessionaria_Type[] obj = retornoPAGAMENTO_CONCESSIONARIA.getTRANSACOES();
//    
//    
//    for (Iterator iterator = pagsList.iterator(); iterator.hasNext();) {
//		//TransacaoInt transacaoInt = (TransacaoInt) iterator.next();
//    	
//        Dados_saida_transacao_pagamento_concessionaria_Type dados_saida_transacao_pagamento_concessionaria_Type = (Dados_saida_transacao_pagamento_concessionaria_Type) iterator.next(); 
//
//        if(dados_saida_transacao_pagamento_concessionaria_Type.getDATA_TRANSACAO() != null){ 
//          dados_saida_transacao_pagamento_concessionaria_Type.setData(format1.format(dados_saida_transacao_pagamento_concessionaria_Type.getDATA_TRANSACAO().getTime())); 
//          dados_saida_transacao_pagamento_concessionaria_Type.setTipoServico("PAGAMENTO_CONCESSIONARIA"); 
//           
//          this.valorTotalTransferencia =       this.valorTotalTransferencia.add(dados_saida_transacao_pagamento_concessionaria_Type.getVALOR()); 
//          pagsList.add(dados_saida_transacao_pagamento_concessionaria_Type); 
//           
//          try{ 
//            String chave                   = ""; 
//            RegraInterface regraPagamentoConcessinaria     = new RegraPagamentoConcessionaria(); 
//            AutenticadorIn autenticador           = new AutenticadorIn(); 
//            autenticador.setRegraInterface(regraPagamentoConcessinaria); 
//            chave              =  "";//autenticador.runAut(transa, 7, this.agencia, this.conta); 
//            dados_saida_transacao_pagamento_concessionaria_Type.setChaveSeguranca("YAK200?!216$98"); 
//          }catch(Exception d){ 
//             logger.error("  ERRO AO GERAR CHAVE SEGURANCA "); 
//             d.printStackTrace(); 
//           } 
//           
//        }else{ 
//           logger.info("   DATA TRANSACAO DE PAGAMENTO_CONCESSIONARIA ESTA NULO "); 
//           dados_saida_transacao_pagamento_concessionaria_Type.setData(format1.format(Calendar.getInstance().getTime())); 
//        } 
//     
//         
//    } 
//     
//    return pagsList; 
//     
//     
//  } 
//   
//   
//   
//   
//   
//  public List<TransacaoInt> trataHAB(Dados_saida_habitacao_Type retornoSaidaHAB,BigDecimal valorTotalTransferencia  ){ 
// 
//    List<TransacaoInt> pagsList = new ArrayList<TransacaoInt>(); 
//     
//    br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_habitacao_Type[] obj = retornoSaidaHAB.getTRANSACOES();
//    
//    for (Iterator iterator = pagsList.iterator(); iterator.hasNext();) {
//    	
//      Dados_saida_transacao_habitacao_Type dados_saida_habitacao_Type = (Dados_saida_transacao_habitacao_Type) iterator.next(); 
//      dados_saida_habitacao_Type.setTipoServico("HAB"); 
//       
//      if(dados_saida_habitacao_Type.getDATA_TRANSACAO() != null){ 
//        dados_saida_habitacao_Type.setData(format1.format(dados_saida_habitacao_Type.getDATA_TRANSACAO().getTime())); 
//        this.valorTotalTransferencia =       this.valorTotalTransferencia.add(dados_saida_habitacao_Type.getVALOR()); 
//         
//        try{ 
//          String chave                   = ""; 
//          RegraInterface regraPagamentoConcessinaria     = new RegraPagamentoConcessionaria(); 
//          AutenticadorIn autenticador           = new AutenticadorIn(); 
//          autenticador.setRegraInterface(regraPagamentoConcessinaria); 
//          chave              =  "";//autenticador.runAut(transa, 7, this.agencia, this.conta); 
//          dados_saida_habitacao_Type.setChaveSeguranca("YAK200?!216$98"); 
//        }catch(Exception d){ 
//           logger.error("  ERRO AO GERAR CHAVE SEGURANCA "); 
//           d.printStackTrace(); 
//         } 
//         
//        pagsList.add(dados_saida_habitacao_Type); 
//         
//      }else{ 
//         logger.info("   DATA TRANSACAO DE GPS ESTA NULO "); 
//         dados_saida_habitacao_Type.setData(format1.format(Calendar.getInstance())); 
//      } 
//     
//    } 
//    return pagsList; 
//   
//  } 
//   
//   
//  public List<TransacaoInt> trataDARF(Dados_saida_darf_Type retornoSaidaDARF,BigDecimal valorTotalTransferencia  ){ 
// 
//    List<TransacaoInt> pagsList = new ArrayList<TransacaoInt>(); 
//     
//    br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_darf_Type[] obj = retornoSaidaDARF.getTRANSACOES();
//    
//    
//    
//    for (Iterator iterator = pagsList.iterator(); iterator.hasNext();) { 
//    	
//    	
//      Dados_saida_transacao_darf_Type dados_saida_transacao_darf_Type = (Dados_saida_transacao_darf_Type) iterator.next(); 
//      dados_saida_transacao_darf_Type.setTipoServico("DARF");       
//       
//      if(dados_saida_transacao_darf_Type.getDATA_TRANSACAO() != null){ 
//        dados_saida_transacao_darf_Type.setData(format1.format(dados_saida_transacao_darf_Type.getDATA_TRANSACAO().getTime())); 
//         
//        this.valorTotalTransferencia     = this.valorTotalTransferencia.add(dados_saida_transacao_darf_Type.getVALOR()); 
//        pagsList.add(dados_saida_transacao_darf_Type); 
//         
//        try{ 
//          String chave           = ""; 
//          RegraInterface regraDarf     = new RegraDARF(); 
//          AutenticadorIn autenticador   = new AutenticadorIn(); 
//          autenticador.setRegraInterface(regraDarf); 
//          chave              =  "";//autenticador.runAut(transa, 6, this.agencia, this.conta); 
//          dados_saida_transacao_darf_Type.setChaveSeguranca("YAK200?!216$98"); 
//        }catch(Exception d){ 
//           logger.error("  ERRO AO GERAR CHAVE SEGURANCA "); 
//           d.printStackTrace(); 
//         } 
//          
//      }else{ 
//         logger.info("   DATA TRANSACAO DE DARF ESTA NULO "); 
//         dados_saida_transacao_darf_Type.setData(format1.format(Calendar.getInstance())); 
//      } 
//       
//       
//       
//    } 
//     
//    return pagsList; 
//   
//  } 
//   
   
 
//   
//  public List<TransacaoInt> trataFGTS(Dados_saida_fgts_Type retornoSaidaFGTS,BigDecimal valorTotalTransferencia  ){ 
// 
//    List<TransacaoInt> pagsList = new ArrayList<TransacaoInt>(); 
//     
//    br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_fgts_Type[] obj = retornoSaidaFGTS.getTRANSACOES();
//    
//    
//    for (Iterator iterator = pagsList.iterator(); iterator.hasNext();) { 
//    	
//    	
//      Dados_saida_transacao_fgts_Type dados_saida_transacao_fgts_Type = (Dados_saida_transacao_fgts_Type) iterator.next(); 
//      dados_saida_transacao_fgts_Type.setTipoServico("FGTS"); 
//       
//      if(dados_saida_transacao_fgts_Type.getDATA_TRANSACAO() != null){ 
//        dados_saida_transacao_fgts_Type.setData(format1.format(dados_saida_transacao_fgts_Type.getDATA_TRANSACAO().getTime())); 
//         
//         
//        this.valorTotalTransferencia =       this.valorTotalTransferencia.add(dados_saida_transacao_fgts_Type.getVALOR()); 
//        pagsList.add(dados_saida_transacao_fgts_Type); 
//         
//        try{ 
//          String chave           = ""; 
//          RegraInterface regraFgts     = new RegraFGTS(); 
//          AutenticadorIn autenticador   = new AutenticadorIn(); 
//          autenticador.setRegraInterface(regraFgts); 
//          chave              =  "";//autenticador.runAut(transa, 8, this.agencia, this.conta); 
//          dados_saida_transacao_fgts_Type.setChaveSeguranca("CXJHAJHSU289USJ"); 
//        }catch(Exception d){ 
//           logger.error("  ERRO AO GERAR CHAVE SEGURANCA "); 
//           d.printStackTrace(); 
//         } 
//         
//      }else{ 
//         logger.info("   DATA TRANSACAO DE FGTS ESTA NULO "); 
//         dados_saida_transacao_fgts_Type.setData(format1.format(Calendar.getInstance())); 
//      } 
//       
//       
//    } 
// 
//     
//     
//    return pagsList; 
//   
//  } 
//   
 
  public List<TransacaoInt> trataBOLETO(Dados_saida_boleto_Type retornoSaidaBOLETO,BigDecimal valorTotalTransferencia  ){ 
 
    List<TransacaoInt> pagsList = new ArrayList<TransacaoInt>(); 
     
    br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_boleto_Type[] obj = retornoSaidaBOLETO.getTRANSACOES();
    
    
    for (Iterator iterator = pagsList.iterator(); iterator.hasNext();) {
    	
      Dados_saida_transacao_boleto_Type dados_saida_transacao_boleto_Type = (Dados_saida_transacao_boleto_Type) iterator.next(); 
      dados_saida_transacao_boleto_Type.setTipoServico("BOLETO"); 
       
      if(dados_saida_transacao_boleto_Type.getDATA_PAGAMENTO() != null){ 
        dados_saida_transacao_boleto_Type.setData(format1.format(dados_saida_transacao_boleto_Type.getDATA_PAGAMENTO().getTime())); 
       
        pagsList.add(dados_saida_transacao_boleto_Type); 
       
        try{ 
          String chave           = ""; 
          RegraInterface regraGps     = new RegraGPS(); 
          AutenticadorIn autenticador   = new AutenticadorIn(); 
          autenticador.setRegraInterface(regraGps); 
          chave              =  "";//autenticador.runAut(transa, 7, this.agencia, this.conta); 
          dados_saida_transacao_boleto_Type.setChaveSeguranca("YAK200?!216$98"); 
        }catch(Exception d){ 
           logger.error("  ERRO AO GERAR CHAVE SEGURANCA "); 
           d.printStackTrace(); 
         } 
         
      }else{ 
         logger.info("   DATA TRANSACAO DE GPS ESTA NULO "); 
         dados_saida_transacao_boleto_Type.setData(format1.format(Calendar.getInstance())); 
      } 
       
       
       
       
    } 
     
    return pagsList; 
   
  } 
   
   
  
  
  public List<TransacaoInt> trataPAGAMENTO(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type retornoSaidaBOLETO,BigDecimal valorTotalTransferencia ,String tipo ){ 
	  
	    List<TransacaoInt> pagsList = new ArrayList<TransacaoInt>(); 
	     
	    br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_transacao_geral_Type[]  obj = retornoSaidaBOLETO.getTRANSACOES();
	    
	    for (int i = 0; i < obj.length; i++) {
	    	
	    	 br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_transacao_geral_Type dados_saida_transacao_boleto_Type = obj[i]; 
	      
	      dados_saida_transacao_boleto_Type.setTipoServico(tipo); 
	       
	      if(dados_saida_transacao_boleto_Type.getDATA_EFETIVACAO() != null){ 
	       dados_saida_transacao_boleto_Type.setData(format1.format(dados_saida_transacao_boleto_Type.getDATA_EFETIVACAO())); 
	       
	       
	       this.valorTotalTransferencia =       this.valorTotalTransferencia.add(dados_saida_transacao_boleto_Type.getVALOR()); 
	        pagsList.add(dados_saida_transacao_boleto_Type); 
	       
	        try{ 
	          String chave           = ""; 
	          RegraInterface regraGps     = new RegraGPS(); 
	          AutenticadorIn autenticador   = new AutenticadorIn(); 
	          autenticador.setRegraInterface(regraGps); 
	          chave              =  "";//autenticador.runAut(transa, 7, this.agencia, this.conta); 
	          dados_saida_transacao_boleto_Type.setChaveSeguranca("-"); 
	        }catch(Exception d){ 
	           logger.error("  ERRO AO GERAR CHAVE SEGURANCA "); 
	           d.printStackTrace(); 
	         } 
	         
	      }else{ 
	         logger.info("   DATA TRANSACAO DE GPS ESTA NULO "); 
	         dados_saida_transacao_boleto_Type.setData(format1.format(Calendar.getInstance())); 
	      } 
	       
	       
	       
	       
	    } 
	     
	    return pagsList; 
	   
	  } 
   
 
//   
//  public List<TransacaoInt> trataGPS(Dados_saida_gps_Type retornoSaidaGPS,BigDecimal valorTotalTransferencia  ){ 
// 
//    List<TransacaoInt> pagsList = new ArrayList<TransacaoInt>(); 
//     
//    br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_gps_Type[] obj = retornoSaidaGPS.getTRANSACOES();
//    
//    
//    for (Iterator iterator = pagsList.iterator(); iterator.hasNext();) { 
//    	
//    	
//      Dados_saida_transacao_gps_Type dados_saida_transacao_gps_Type = (Dados_saida_transacao_gps_Type) iterator.next(); 
//      dados_saida_transacao_gps_Type.setTipoServico("GPS"); 
//       
//      if(dados_saida_transacao_gps_Type.getDATA_TRANSACAO() != null){ 
//        dados_saida_transacao_gps_Type.setData(format1.format(dados_saida_transacao_gps_Type.getDATA_TRANSACAO().getTime())); 
//       
//        this.valorTotalTransferencia =       this.valorTotalTransferencia.add(dados_saida_transacao_gps_Type.getPAGAMENTO()); 
//        pagsList.add(dados_saida_transacao_gps_Type); 
//       
//        try{ 
//          String chave           = ""; 
//          RegraInterface regraGps     = new RegraGPS(); 
//          AutenticadorIn autenticador   = new AutenticadorIn(); 
//          autenticador.setRegraInterface(regraGps); 
//          chave              =  "";//autenticador.runAut(transa, 7, this.agencia, this.conta); 
//          dados_saida_transacao_gps_Type.setChaveSeguranca("YAK200?!216$98"); 
//        }catch(Exception d){ 
//           logger.error("  ERRO AO GERAR CHAVE SEGURANCA "); 
//           d.printStackTrace(); 
//         } 
//         
//      }else{ 
//         logger.info("   DATA TRANSACAO DE GPS ESTA NULO "); 
//         dados_saida_transacao_gps_Type.setData(format1.format(Calendar.getInstance())); 
//      } 
//       
//       
//       
//       
//    } 
//     
//    return pagsList; 
//   
//  } 
   
   
   
 
  public BigDecimal getValorTotalTransferencia() { 
    return valorTotalTransferencia; 
  } 
 
  public void setValorTotalTransferencia(BigDecimal valorTotalTransferencia) { 
    this.valorTotalTransferencia = valorTotalTransferencia; 
  } 
 
    
   
   
   
   
} 




