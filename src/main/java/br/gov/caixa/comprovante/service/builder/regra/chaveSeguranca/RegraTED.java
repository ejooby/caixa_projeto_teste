package br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca;

import java.math.BigDecimal;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.caixa.api.common.resource.chave.seguranca.RegraInterface;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_ted_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

/**
 * 
 * @author Capgemini2018
 *
 */
public class RegraTED implements RegraInterface{

	/**
	 * 
	 */
	protected static final 	Logger logger = LoggerFactory.getLogger(RegraTED.class);
	
	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat) {
		
		Dados_saida_transacao_ted_Type 	dadosSaida 		= 	null;
		TransacaoInt 					transacaoObject = 	((TransacaoInt)input);
		
		dadosSaida 										=	
				(Dados_saida_transacao_ted_Type)transacaoObject;
		
		short       tipoConta			=	dadosSaida.getTIPO_CONTA_REMETENTE();
		String      strTipoConta		=	String.valueOf(tipoConta);
		BigDecimal 	valorOperacao		=	dadosSaida.getVALOR();
		String      strValorOpeacao		=	valorOperacao.toString();
		short  		agenciaDestino 		= 	dadosSaida.getAGENCIA_DESTINO();
		String		strAgenciaDestino	=	String.valueOf(agenciaDestino);
		String 		tpConta				=	dadosSaida.getTIPO_CONTA_DESTINATARIO();
		long   		contaDest			=	dadosSaida.getCONTA_DESTINO();
		String      strContaDest		=	String.valueOf(contaDest);
		Calendar    dataOperacao  		=   dadosSaida.getDATA_TRANSACAO();
		String 		strDataOperacao		=	dataOperacao.getTime().toString();
		
		String 		chave = String.format(maskFormat,strTipoConta, 
				strValorOpeacao, strAgenciaDestino, tpConta, strContaDest, strDataOperacao);
		
		return chave;
	}

	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat, Object... add) {
		Dados_saida_transacao_ted_Type 	dadosSaida 		= 	null;
		TransacaoInt 					transacaoObject = 	((TransacaoInt)input);
		
		dadosSaida 										=	
				(Dados_saida_transacao_ted_Type)transacaoObject;
		
		String		strNumeroAgencia	=	add[0].toString();
		String      strTipoConta		=	TIPO_CONTA;
		String      strNumeroConta		=	add[1].toString();
		String		strCodOper			=	dadosSaida.getNSU().toString();
		BigDecimal 	valorOperacao		=	dadosSaida.getVALOR();
		String      strValorOperacao	=	valorOperacao.toString();
		String      strNSUConfirmacao	=	dadosSaida.getNSU_CONFIRMACAO().toString();
		String      strTipoTED			=	dadosSaida.getTIPO_TED();
		String		strCodBancoDestino	=	dadosSaida.getBANCO_DESTINO().toString();
		String		strValorTarifa		=	dadosSaida.getTARIFA().toString();
		short  		agenciaDestino 		= 	dadosSaida.getAGENCIA_DESTINO();
		String		strAgenciaDestino	=	String.valueOf(agenciaDestino);
		long   		contaDest			=	dadosSaida.getCONTA_DESTINO();
		String      strContaDest		=	String.valueOf(contaDest);
		String      strDigVerContaDest	=	dadosSaida.getDV_CONTA_DESTINO();
		String		strTipoContaDest	=	dadosSaida.getTIPO_CONTA_DESTINATARIO();
		Calendar    dataOperacao  		=   dadosSaida.getDATA_TRANSACAO();
		String 		strDataOperacao		=	dataOperacao.getTime().toString();
		
		logger.info("[REGRA TED CHAVE SEGURANCA] 14 CAMPOS INFORMADOS");
		logger.info("[REGRA TED CHAVE SEGURANCA] REGRA " + maskFormat);
		logger.info("[REGRA TED CHAVE SEGURANCA] DADOS INFORMADOS EM ORDEM: ");
		logger.info("[REGRA TED CHAVE SEGURANCA] AGENCIA: " + strNumeroAgencia);
		logger.info("[REGRA TED CHAVE SEGURANCA] TIPO CONTA: " + strTipoConta);
		logger.info("[REGRA TED CHAVE SEGURANCA] NUMERO DA CONTA: " + strNumeroConta);
		logger.info("[REGRA TED CHAVE SEGURANCA] CODIGO DA OPERACAO: " + strCodOper);
		logger.info("[REGRA TED CHAVE SEGURANCA] VALOR DA OPERACAO: " + strValorOperacao);
		logger.info("[REGRA TED CHAVE SEGURANCA] CODIGO DA TRANSACAO: " + strNSUConfirmacao);
		logger.info("[REGRA TED CHAVE SEGURANCA] TIPO TED: " + strTipoTED);
		logger.info("[REGRA TED CHAVE SEGURANCA] CODIGO BANCO DESTINO: " + strCodBancoDestino);
		logger.info("[REGRA TED CHAVE SEGURANCA] VALOR DA TARIFA: " + strValorTarifa);
		logger.info("[REGRA TED CHAVE SEGURANCA] AGENCIA DESTINO: " + strAgenciaDestino);
		logger.info("[REGRA TED CHAVE SEGURANCA] CONTA DESTINO: " + strContaDest);
		logger.info("[REGRA TED CHAVE SEGURANCA] DIGITO VERIFICADOR DA CONTA DESTINO: " + strDigVerContaDest);
		logger.info("[REGRA TED CHAVE SEGURANCA] TIPO CONTA DESTINO: " + strTipoContaDest);
		logger.info("[REGRA TED CHAVE SEGURANCA] DATA OPERACAO: " + strDataOperacao);
		
		String 	chave = String.format(maskFormat, strNumeroAgencia, strTipoConta, strNumeroConta, strCodOper, 
					strValorOperacao, strNSUConfirmacao, strTipoTED, strCodBancoDestino, strValorTarifa, strAgenciaDestino, strContaDest, strDigVerContaDest,
					strTipoContaDest, strDataOperacao);
		
		logger.info("[REGRA TED CHAVE SEGURANCA] FORMATACAO DA REGRA: " + chave);
		
		return chave;
	}

	
}
