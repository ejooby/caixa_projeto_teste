package br.gov.caixa.comprovante.service;

import br.gov.caixa.api.common.holder.RetornoHolder;
import br.gov.caixa.comprovante.holder.ConsultaComprovanteHolder;

/**
 * 
 * @author Capgemini SP
 *
 */
public interface ConsultaComprovanteService {
	 

	
	RetornoHolder consultaComprovanteTransferencia(ConsultaComprovanteHolder consultaSadoExtratoHolder) throws Exception;
	
	RetornoHolder consultaComprovantePagamento(ConsultaComprovanteHolder consultaSadoExtratoHolder) throws Exception;
	
}
