package br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import br.gov.caixa.api.common.resource.chave.seguranca.RegraInterface;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Banco_type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_cliente_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_doc;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;


/**
 * 
 * @author Capgemini SP 2018
 *
 */
public class RegraDOC implements RegraInterface {

	/**
	 * 
	 */
	protected static final 	Logger logger = LoggerFactory.getLogger(RegraDOC.class);
	
	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat, Object... add) {
		
		Dados_saida_transacao_doc dadosSaida 			= 	null;
		TransacaoInt 				   transacaoObject 	= 	((TransacaoInt)input);
		
		dadosSaida										=
				(Dados_saida_transacao_doc)transacaoObject;
		
		String 				strNumAgencia 		= 	add[0].toString();
		String 				strTpConta			=	TIPO_CONTA;
		String 				strNumConta			=	add[1].toString();
		String 				strCodOperacao		=	dadosSaida.getNSU().toString();
		BigDecimal 			valorOperacao		=	dadosSaida.getVALOR();
		String				strValorOperacao	=	valorOperacao.toString();
		String				strCodigoTransacao	=	dadosSaida.getNSU().toString();
		String				strDataDebito		=	dadosSaida.getDATA_MOVIMENTO().toString();
		String				strTpDoc			=	dadosSaida.getTIPO_DOC();
		Banco_type  		bancoType			=	dadosSaida.getBANCO_DESTINO();
		short       		codBancoDest		=	bancoType.getCODIGO();
		int         		agenciaDestino		=	bancoType.getAGENCIA();
		String      		strCodBancoDest		=	String.valueOf(codBancoDest);
		String      		strAgenciaDestino	=	String.valueOf(agenciaDestino);
		String				strDvContaDest		=	bancoType.getDV();
		Dados_cliente_Type 	dadosClientType		=	dadosSaida.getCONTA_DESTINO();
		String				strCtDestino		=	dadosClientType.getNOME();
		String				strCodFinalidade	=	dadosSaida.getFINALIDADE();
		String				strCPFCNPJDest		=	dadosSaida.getCPF().toString();
		String				strDataOper			=	dadosSaida.getDATA_MOVIMENTO().toString();
		
		logger.info("[REGRA DOC CHAVE SEGURANCA] 16 CAMPOS INFORMADOS");
		logger.info("[REGRA DOC CHAVE SEGURANCA] REGRA " + maskFormat);
		logger.info("[REGRA DOC CHAVE SEGURANCA] DADOS INFORMADOS EM ORDEM: ");
		logger.info("[REGRA DOC CHAVE SEGURANCA] AGENCIA: " + strNumAgencia);
		logger.info("[REGRA DOC CHAVE SEGURANCA] TIPO CONTA: " + strTpConta);
		logger.info("[REGRA DOC CHAVE SEGURANCA] NUMERO DA CONTA: " + strNumConta);
		logger.info("[REGRA DOC CHAVE SEGURANCA] CODIGO DA OPERACAO: " + strCodOperacao);
		logger.info("[REGRA DOC CHAVE SEGURANCA] VALOR DA OPERACAO: " + strValorOperacao);
		logger.info("[REGRA DOC CHAVE SEGURANCA] CODIGO DA TRANSACAO: " + strCodigoTransacao);
		logger.info("[REGRA DOC CHAVE SEGURANCA] DATA DE DEBITO: " + strDataDebito);
		logger.info("[REGRA DOC CHAVE SEGURANCA] TIPO DE DOC: " + strTpDoc);
		logger.info("[REGRA DOC CHAVE SEGURANCA] CODIGO DE BANCO DESTINO: " + strCodBancoDest);
		logger.info("[REGRA DOC CHAVE SEGURANCA] AGENCIA DESTINO: " + strAgenciaDestino);
		logger.info("[REGRA DOC CHAVE SEGURANCA] DIGITO VERIFICADOR DA AGENCIA DESTINO: " + strDvContaDest);
		logger.info("[REGRA DOC CHAVE SEGURANCA] CONTA DESTINO: " + strCtDestino);
		logger.info("[REGRA DOC CHAVE SEGURANCA] DIGITO VERIFICADOR DA CONTA DESTINO: " + strDvContaDest);
		logger.info("[REGRA DOC CHAVE SEGURANCA] CODIGO FINALIDADE: " + strCodFinalidade);
		logger.info("[REGRA DOC CHAVE SEGURANCA] CPF/CNPJ DESTINO: " + strCPFCNPJDest);
		logger.info("[REGRA DOC CHAVE SEGURANCA] DATA DA OPERACAO: " + strDataOper);
		
		String 	chave = String.format(maskFormat, strNumAgencia, strTpConta, strNumConta, strCodOperacao, strValorOperacao, 
				strCodigoTransacao, strDataDebito, strTpDoc, strCodBancoDest, strAgenciaDestino, strDvContaDest, strCtDestino, strCodFinalidade, strCPFCNPJDest, strDataOper);
		
		logger.info("[REGRA DOC CHAVE SEGURANCA] FORMATACAO DA REGRA: " + chave);
		
		return chave;
	}

}
