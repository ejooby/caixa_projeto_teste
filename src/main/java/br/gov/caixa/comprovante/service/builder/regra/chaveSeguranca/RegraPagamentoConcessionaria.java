package br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import br.gov.caixa.api.common.resource.chave.seguranca.RegraInterface;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_pagamento_concessionaria_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

/**
 * 
 * @author Capgemini SP 2018
 *
 */
public class RegraPagamentoConcessionaria implements RegraInterface {

	/**
	 * 
	 */
	protected static final 	Logger logger = LoggerFactory.getLogger(RegraPagamentoConcessionaria.class);
	
	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat, Object... add) {
		
		Dados_saida_transacao_pagamento_concessionaria_Type 
										dadosSaida 		= 	null;
		TransacaoInt 				   transacaoObject 	= 	((TransacaoInt)input);
		
		dadosSaida										=
				(Dados_saida_transacao_pagamento_concessionaria_Type)transacaoObject;
			
		//Composicao Codigo de Barras
		String codBar1			=	String.valueOf(dadosSaida.getBARRA1().longValue());
		String codBar2			=	String.valueOf(dadosSaida.getBARRA2().longValue());
		String codBar3			=	String.valueOf(dadosSaida.getBARRA3().longValue());
		String codBar4			=	String.valueOf(dadosSaida.getBARRA4().longValue());
		
		String strCodBarFormat	=	"%s%s%s%s";
		strCodBarFormat			=	String.format(strCodBarFormat, codBar1, codBar2, codBar3, codBar4);
		
		String strNumAgencia	=	add[0].toString();
		String strTpConta		=	TIPO_CONTA;
		String strNumConta		=	add[1].toString();
		String strNumOperacao	=	dadosSaida.getNSU().toString();
		String strValorOperacao	=	dadosSaida.getVALOR().toString();
		String strDataOperacao	=	dadosSaida.getDATA_TRANSACAO().toString();
		String strCodBar		=	strCodBarFormat;
		
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] 7 CAMPOS INFORMADOS");
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] REGRA " + maskFormat);
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] DADOS INFORMADOS EM ORDEM: ");
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] AGENCIA: " + strNumAgencia);
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] TIPO CONTA: " + strTpConta);
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] NUMERO CONTA: " + strNumConta);
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] NUMERO OPERACAO: " + strNumOperacao);
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] VALOR OPERACAO: " + strValorOperacao);
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] DATA OPERACAO: " + strDataOperacao);
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] CODIGO DE BARRAS: " + strCodBar);
		
		String 	chave = String.format(maskFormat, strNumAgencia, strTpConta, strNumConta, strNumOperacao, 
				strValorOperacao, strDataOperacao, strCodBar);
		logger.info("[REGRA PAGAMENTO CONCESSIONARIA CHAVE SEGURANCA] FORMATACAO DA REGRA: " + chave);
		
		return chave;
	}

}
