package br.gov.caixa.comprovante.service.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import br.gov.caixa.api.common.exception.GenericExceptionHandler;
import br.gov.caixa.api.common.exception.ValidationException;
import br.gov.caixa.api.common.jms.MessageHandler;
import br.gov.caixa.api.common.util.XmlUtil;
import br.gov.caixa.comprovante.exception.handler.BusinessExceptionHandler;
import br.gov.caixa.sibar.CONTROLE_NEGOCIAL_TYPE;
import br.gov.caixa.sibar.DADOS_SAIDA_TYPE;
import br.gov.caixa.sibar.SERVICO_SAIDA_TYPE;
	


/**
 * 
 * @author Capgemini SP
 *
 */
public abstract class AbstractServiceImpl {
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractServiceImpl.class);
	
	@Value("${system.prefix}")
	protected String systemPrefix;
	
	protected String connectionFactory;

	protected String requestQueueName;

	protected String responseQueueName;

	protected String serviceUser=null;

	protected final static long timeout = 5000;

	
	/**
	 * 
	 * @param responseXML
	 * @return
	 */
	protected String cleanXML(String responseXML){
		
		if (responseXML==null){
			return null;
		}
		
		Pattern p = Pattern.compile("[\\000]*"); 
		Matcher matcher = null;
		
		matcher = p.matcher(responseXML);
		if (matcher.find()) {
			return matcher.replaceAll("");
		}
		
		return responseXML;
	}
	/**
	 * 
	 * @param xml
	 * @param xsd
	 * @throws Exception
	 */
	protected void validateXML(String xml, String xsd) throws Exception{
		if (xml!=null){
			if (!XmlUtil.isValid(xml, xsd)) {
				logger.error("Mensagem de retorno inv�lida ao ser confrontada com " + xsd);
				throw new ValidationException("Mensagem de retorno n�o � v�lida para " + xsd);
			}
		}
	}
	/**
	 * 	
	 * @param dadosSaida
	 * @throws Exception
	 */
	protected void validaRetorno(SERVICO_SAIDA_TYPE dadosSaida) throws Exception{
		if ("X5".equals(dadosSaida.getCOD_RETORNO())) {
			logger.error("Barramento retornou X5!");
			//IMPLEMENTAR UM NOVO
			//throw businessExceptionHandler.handle(MensagemSistemaEnum.MN029);
		}			
	}
	/**
	 * 
	 * @param dadosSaida
	 * @throws Exception
	 */
	protected void validaDadosSaidaRetorno(DADOS_SAIDA_TYPE dadosSaida) throws Exception{
		
		if (dadosSaida.getCONTROLE_NEGOCIAL()!=null){
			
			br.gov.caixa.sibar.CONTROLE_NEGOCIAL_TYPE[] controleNegocialList = dadosSaida.getCONTROLE_NEGOCIAL();
			CONTROLE_NEGOCIAL_TYPE controleNegocial = controleNegocialList[0];
			
			if (controleNegocial!=null){
				Integer codRetorno = Integer.parseInt(controleNegocial.getCOD_RETORNO());
				if (codRetorno==99){
					logger.error("C�digo negocial 99!");
					//IMPLEMENTAR UM NOVO
					//throw businessExceptionHandler.handle(MensagemSistemaEnum.MN029);
				}
			}
		}
		
	}
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public MessageHandler getHandler() throws Exception{

		logger.info("connectionFactory=" + connectionFactory);
		logger.info("requestQueueName=" + requestQueueName);
		logger.info("responseQueueName=" + responseQueueName);

		return new MessageHandler(requestQueueName, responseQueueName, connectionFactory,"");
	}	
	
	public abstract String getBackendSystemName();
	
	

	
	
	
}
