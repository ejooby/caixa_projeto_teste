package br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import br.gov.caixa.api.common.resource.chave.seguranca.RegraInterface;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_cartao_credito_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

/**
 * 
 * @author Capgemini SP 2018
 *
 */
public class RegraCartaoCredito implements RegraInterface {

	/**
	 * 
	 */
	protected static final 	Logger logger = LoggerFactory.getLogger(RegraCartaoCredito.class);
	
	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat, Object... add) {
		
		Dados_saida_transacao_cartao_credito_Type dadosSaida 		= 	null;
		TransacaoInt 				   transacaoObject 	= 	((TransacaoInt)input);
		
		dadosSaida										=
				(Dados_saida_transacao_cartao_credito_Type)transacaoObject;
	
		String 	strNumAgencia		=	add[0].toString();
		String 	strCodOper			=	dadosSaida.getNSU().toString();
		String	strNumConta			=	add[1].toString();
		String	strDataOperacao		=	dadosSaida.getDATA_TRANSACAO().toString();
		String	strCodBarras		=	ZEROS_COD_BARRA;
		String	strDataDebito		=	dadosSaida.getDATA_PAGAMENTO().toString();
		String  strNumOper			=	dadosSaida.getNUMERO().toString();
		String	strValorOper		=	dadosSaida.getVALOR().toString();
		
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] 8 CAMPOS INFORMADOS");
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] REGRA " + maskFormat);
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] DADOS INFORMADOS EM ORDEM: ");
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] AGENCIA: " + strNumAgencia);
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] CODIGO OPERACAO: " + strCodOper);
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] NUMERO DA CONTA: " + strNumConta);
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] DATA OPERACAO: " + strDataOperacao);
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] CODIGO DE BARRAS: " + strCodBarras);
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] DATA DEBITO: " + strDataDebito);
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] NUMERO OPERACAO: " + strNumOper);
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] VALOR OPERACAO: " + strValorOper);
		
		
		String 	chave = String.format(maskFormat, strNumAgencia, strCodOper, strNumConta, 
				strDataOperacao, strCodBarras, strDataDebito, strNumOper, strValorOper);
		
		logger.info("[REGRA CARTAO CREDITO CHAVE SEGURANCA] FORMATACAO DA REGRA: " + chave);
		
		return chave;
	}

}
