package br.gov.caixa.comprovante.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.gov.caixa.api.common.holder.ErrorHolder;
import br.gov.caixa.api.common.holder.RetornoHolder;
import br.gov.caixa.api.common.resource.util.ResourceUtil;
import br.gov.caixa.comprovante.holder.ConsultaComprovanteHolder;
import br.gov.caixa.comprovante.holder.ConsultaPagamentoDetalheHolder;
import br.gov.caixa.comprovante.holder.Transacao;
import br.gov.caixa.comprovante.service.ConsultaComprovanteService;
import br.gov.caixa.comprovante.service.builder.ConsultaComprovanteDetalheBuilder;
import br.gov.caixa.comprovante.service.builder.ConsultaComprovantePagamentoBuilder;
import br.gov.caixa.comprovante.service.builder.ConsultaComprovanteTransferenciaBuilder;

/**
 * 
 * @author Capgemini SP
 * 
 */
@Service
public class ConsultaComprovanteServiceImpl extends AbstractServiceImpl implements ConsultaComprovanteService {

	private ConsultaComprovanteTransferenciaBuilder consultaComprovanteBuilder = new ConsultaComprovanteTransferenciaBuilder();

	private ConsultaComprovantePagamentoBuilder consultaComprovanteTransferenciaBuilder = new ConsultaComprovantePagamentoBuilder();

	private ConsultaComprovanteDetalheBuilder consultaComprovanteDetalheBuilder = new ConsultaComprovanteDetalheBuilder();

	/** 
	 *  
	 */
	protected static final Logger logger = LoggerFactory.getLogger(ConsultaComprovanteServiceImpl.class);

	@Override
	public String getBackendSystemName() {
		return "sinbm-ConsultaComprovante";
	}

	public RetornoHolder consultaComprovanteTransferencia(ConsultaComprovanteHolder consultaSadoExtratoHolder)
			throws Exception {

		logger.info("*****************************");
		logger.info("consultaComprovanteTransferencia INI".toUpperCase());

		ConsultaComprovanteHolder saidaHolder = new ConsultaComprovanteHolder();

		RetornoHolder ret = new RetornoHolder();
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_saida_negocial_Type saida = null;

		ret = consultaComprovanteBuilder.preparawebServiceTransferencia(consultaSadoExtratoHolder);
		if (ret.getError().getCodigo() != 0) {
			return ret;
		}

		saida = (br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_saida_negocial_Type) ret.getDados();

		if (saida.getDADOS() == null) {

			if (!saida.getCOD_RETORNO().equals("00")) {

				ret.setDados(null);
				ErrorHolder error = new ErrorHolder();
				error.setCodigo(999);
				error.setCausa(saida.getMSG_RETORNO());
				error.setMensagem(saida.getMSG_RETORNO());
				ret.setError(error);

				ResourceUtil.getInstance().imprimeFormatoJson(ret, "SAIDA DA API = ");
				return ret;

			}else {
				Transacao transa = new Transacao();
				transa.setValorTotalMes("0.0");
				ret.setDados(transa);
				ret.setError(null);
				ResourceUtil.getInstance().imprimeFormatoJson(ret, "SAIDA DA API = ");
				return ret;
				
			}
		}

		if (saida.getDADOS().getCONTROLE_NEGOCIAL() != null) {
			if (saida.getDADOS().getCONTROLE_NEGOCIAL().length > 0) {
				if (saida.getDADOS().getCONTROLE_NEGOCIAL()[0] != null) {

					logger.info("saida.getDADOS().getCONTROLE_NEGOCIAL()[0] != null");
					if (!saida.getDADOS().getCONTROLE_NEGOCIAL()[0].getCOD_RETORNO().equals("00")) {

						logger.info("!saida.getDADOS().getCONTROLE_NEGOCIAL()[0].getCOD_RETORNO().equals(\"00\")");
						if (saida.getDADOS().getCONTROLE_NEGOCIAL()[0].getMSG_RETORNO().trim()
								.equals("NAO EXISTE PAGAMENTO")) {

							Transacao transa = new Transacao();
							transa.setValorTotalMes("0.0");
							ret.setDados(transa);
							ret.setError(null);
							ResourceUtil.getInstance().imprimeFormatoJson(ret, "SAIDA DA API = ");
							return ret;
						}

						ret.setDados(null);
						ErrorHolder error = new ErrorHolder();
						error.setCodigo(997);
						error.setCausa(saida.getDADOS().getCONTROLE_NEGOCIAL()[0].getMSG_RETORNO());
						error.setMensagem(saida.getDADOS().getCONTROLE_NEGOCIAL()[0].getMSG_RETORNO());
						ret.setError(error);

						ResourceUtil.getInstance().imprimeFormatoJson(ret, "SAIDA DA API = ");
						return ret;

					}

				}
			}
		}

		RetornoHolder retorno = new RetornoHolder();

		retorno.setDados(consultaComprovanteBuilder
				.consultaComprovanteTransferencia(retorno, consultaSadoExtratoHolder, saida, saidaHolder).getDados());

		logger.info("consulta Comprovante Transferencia FIM".toUpperCase());
		logger.info("*****************************");
		return retorno;

	}

	public static void main(String[] args) {

		try {

			String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			System.out.println("   cal  " + date);

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	public RetornoHolder consultaComprovantePagamento(ConsultaComprovanteHolder consultaSadoExtratoHolder)
			throws Exception {

		logger.info("*****************************");
		logger.info("ConsultaComprovantePagamento INI".toUpperCase());

		ConsultaComprovanteHolder saidaHolder = new ConsultaComprovanteHolder();

		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_saida_negocial_Type saida = null;

		RetornoHolder ret = consultaComprovanteTransferenciaBuilder.preparawebServiceRESUMO(consultaSadoExtratoHolder);

		saida = (br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_saida_negocial_Type) ret.getDados();

		if (saida.getDADOS() == null) {
			if (!saida.getCOD_RETORNO().equals("00")) {
				ret.setDados(null);
				ErrorHolder error = new ErrorHolder();
				error.setCodigo(999);
				error.setCausa(saida.getMSG_RETORNO());
				error.setMensagem(saida.getMSG_RETORNO());
				ret.setError(error);

				ResourceUtil.getInstance().imprimeFormatoJson(ret, "SAIDA DA API = ");
				return ret;
			}else {
				Transacao transa = new Transacao();
				transa.setValorTotalMes("0.0");
				ret.setDados(transa);

				ret.setError(null);
				ResourceUtil.getInstance().imprimeFormatoJson(ret, "SAIDA DA API = ");
				return ret;
			}
			
		}

		if (saida.getDADOS().getCONTROLE_NEGOCIAL() != null) {
			if (saida.getDADOS().getCONTROLE_NEGOCIAL().length > 0) {

				if (saida.getDADOS().getCONTROLE_NEGOCIAL()[0] != null) {

					logger.info("saida.getDADOS().getCONTROLE_NEGOCIAL()[0] != null");
					if (!saida.getDADOS().getCONTROLE_NEGOCIAL()[0].getCOD_RETORNO().equals("00")) {

						logger.info("!saida.getDADOS().getCONTROLE_NEGOCIAL()[0].getCOD_RETORNO().equals(\"00\")");
						if (saida.getDADOS().getCONTROLE_NEGOCIAL()[0].getMSG_RETORNO().trim()
								.equals("NAO EXISTE PAGAMENTO")) {

							Transacao transa = new Transacao();
							transa.setValorTotalMes("0.0");
							ret.setDados(transa);

							ret.setError(null);
							ResourceUtil.getInstance().imprimeFormatoJson(ret, "SAIDA DA API = ");
							return ret;
						}

						ret.setDados(null);
						ErrorHolder error = new ErrorHolder();
						error.setCodigo(997);
						error.setCausa(saida.getDADOS().getCONTROLE_NEGOCIAL()[0].getMSG_RETORNO());
						error.setMensagem(saida.getDADOS().getCONTROLE_NEGOCIAL()[0].getMSG_RETORNO());
						ret.setError(error);

						ResourceUtil.getInstance().imprimeFormatoJson(ret, "SAIDA DA API = ");
						return ret;

					}

				}
			}
		}

		RetornoHolder retorno = new RetornoHolder();

		retorno.setDados(consultaComprovanteTransferenciaBuilder
				.consultaComprovantePagamento(retorno, consultaSadoExtratoHolder, saida, saidaHolder).getDados());

		logger.info("consultaComprovantePagamento FIM".toUpperCase());
		logger.info("*****************************");
		return retorno;
	}

	public RetornoHolder consultaDetalhePagamento(ConsultaPagamentoDetalheHolder consultaSadoExtratoHolder)
			throws Exception {
		logger.info("consultaDetalhePagamento INI");

		ConsultaComprovanteHolder saidaHolder = new ConsultaComprovanteHolder();

		RetornoHolder ret = consultaComprovanteDetalheBuilder.preparawebServiceDETALHE(consultaSadoExtratoHolder);

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_saida_negocial_Type saida = null;

		ret = consultaComprovanteBuilder.preparawebServiceDETALHE(consultaSadoExtratoHolder);

		RetornoHolder retorno = new RetornoHolder();

		if (ret.getError() != null ) {
			if (ret.getError().getCodigo() == 997) {
				ErrorHolder error = new ErrorHolder();
				error.setCodigo(997);
				error.setCausa("Sistema Temporariamente Indisponível!");
				error.setMensagem("Sistema Temporariamente Indisponível!");
				logger.info("       CodRetorno = " + error.getCodigo());
				logger.info("       Mensagem = " + error.getMensagem());
				logger.info("       Causa = " + error.getCausa());
				ret.setError(error);
				return ret;
			}
			
			if (ret.getError().getCodigo() != 0) {
				return ret;
			}
			
			
		}

		

		logger.info("consultaDetalhePagamento FIM");
		return retorno;
	}

}