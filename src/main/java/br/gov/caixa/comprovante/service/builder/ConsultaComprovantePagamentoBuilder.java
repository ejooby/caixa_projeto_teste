package br.gov.caixa.comprovante.service.builder;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

import br.gov.caixa.api.common.holder.ErrorHolder;
import br.gov.caixa.api.common.holder.RetornoHolder;
import br.gov.caixa.api.common.resource.util.ResourceUtil;
import br.gov.caixa.comprovante.holder.ConsultaComprovanteHolder;
import br.gov.caixa.comprovante.holder.Transacao;
import br.gov.caixa.comprovante.util.Util;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_entrada_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_entrada_solicitacoes_TypeSOLICITACAO;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_doc_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_ted_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_tev_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Periodo_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Servico_entrada_negocial_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Servico_saida_negocial_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;
import br.gov.caixa.sibar.consulta_transacoes_conta.ws.ConsultaTransacoesContaSOAPStub;
import br.gov.caixa.sibar.consulta_transacoes_conta.ws.Consulta_Transacoes_ContaProxy;

/**
 * 
 * @author Capgemini SP
 * 
 */
@Component
public class ConsultaComprovantePagamentoBuilder extends AbstractBuilder {


	private BigDecimal valorTotalTransferencia = new BigDecimal("0");	
	
	

	public RetornoHolder preparawebServiceRESUMO(ConsultaComprovanteHolder consultaSadoExtratoHolder) throws Exception {

		logger.info("   preparawebService INI ");

		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_saida_negocial_Type saida = null;
		RetornoHolder retorno = new RetornoHolder();
		int codReturn = 0;
		try {

				// saida = conectawebService(consultaExtratoHolder);
				br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_entrada_negocial_Type entrada = createMessageResumo(
						consultaSadoExtratoHolder);

				ResourceUtil.getInstance().imprimeFormatoJson(entrada, "ENTRADA DRTVICO = ");

				ConsultaTransacoesContaSOAPStub ext = new ConsultaTransacoesContaSOAPStub();
				Consulta_Transacoes_ContaProxy px = new Consulta_Transacoes_ContaProxy();
				saida = px.RESUMO(entrada);

				ResourceUtil.getInstance().imprimeFormatoJson(saida, "SAIDA SERVICO = ");

				codReturn = validRetorno(saida.getCOD_RETORNO());

				if (codReturn != 0) {
					ErrorHolder error = new ErrorHolder();
					error.setCodigo(977);
					error.setMensagem(saida.getMSG_RETORNO());
					error.setCausa(saida.getORIGEM_RETORNO());
					logger.info("       Cod.Retorno = " + error.getCodigo());
					logger.info("       Msg.etorno = " + error.getMensagem());
					logger.info("       Msg.Causa = " + error.getCausa());
					retorno.setError(error);

					return retorno;
				} else {
					ErrorHolder error = new ErrorHolder();
					error.setCodigo(new Integer(saida.getCOD_RETORNO()).intValue());
					retorno.setError(error);
				}

	
		} catch (Exception x) {
			logger.error(x.getMessage());
			x.printStackTrace();
			ErrorHolder error = new ErrorHolder();
			error.setCodigo(977);
			retorno.setError(error);

		}

		retorno.setDados(saida);

		logger.info("   preparawebService FIM ");
		return retorno;
	}
	
	
	
	
	public static int validRetorno(String codRetorno) {
		logger.info("validRetorno INI");
		logger.info("codRetorno = " + codRetorno);
		int resultadoAvaliacao = 0;

		try {

			resultadoAvaliacao = Integer.parseInt(codRetorno);

			if (resultadoAvaliacao != 0) {
				resultadoAvaliacao = -1;
			}

		} catch (NumberFormatException ex) {

			if ("X5".equalsIgnoreCase(codRetorno)) {
				resultadoAvaliacao = -5;
			} else {
				resultadoAvaliacao = -1;
			}

		}
		logger.info("validRetorno FIM");
		return resultadoAvaliacao;
	}
	

	
	public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_entrada_negocial_Type createMessageResumo(
			ConsultaComprovanteHolder consultaExtratoHolder) throws Exception {
		logger.info("      createMessage() INI");

		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_entrada_negocial_Type servicoEntrada = new br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_entrada_negocial_Type();

		servicoEntrada.setHEADER(this.createHeaderConsultaResumo());

		servicoEntrada.setDADOS(this.montarDadosEntradaResumo(consultaExtratoHolder));

		logger.info("      createMessage() FIM");
		return servicoEntrada;
	}
	
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_Type montarDadosEntradaResumo(
			ConsultaComprovanteHolder consultaExtratoHolder) {
		logger.info("         montarDadosEnt. INI");

		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_Type dadosEntrada = new br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_Type();

		dadosEntrada.setCONTA(this.confContaResumo(consultaExtratoHolder));
		dadosEntrada.setPERIODO(this.confPeriodoConsultaResumo(consultaExtratoHolder));
		dadosEntrada.setSOLICITACOES(this.confTipoConsultaResumo(consultaExtratoHolder));

		logger.info("         montarDadosEnt. FIM");
		return dadosEntrada;

	}

	private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Conta_Type confContaResumo(
			ConsultaComprovanteHolder consultaExtratoHolderOb) {
		logger.info("         confConta INI");
		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Conta_Type contaType = new br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Conta_Type();

		long conta = consultaExtratoHolderOb.getConta();
		short produto = consultaExtratoHolderOb.getProduto();
		java.math.BigInteger agencia = new java.math.BigInteger("" + consultaExtratoHolderOb.getAgencia()); // unidade
																											// agencia
		short dv = consultaExtratoHolderOb.getDv();

		contaType.setNUMERO(conta);
		contaType.setPRODUTO(produto);
		contaType.setUNIDADE(agencia);
		contaType.setDV(dv);

		logger.info("         NUMERO=" + conta);
		logger.info("         PRODUTO=" + produto);
		logger.info("         UNIDADE=" + agencia);
		logger.info("         DV=" + dv);
		logger.info("         confConta FIM");
		return contaType;
	}

	
	private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Periodo_Type confPeriodoConsultaResumo(
			ConsultaComprovanteHolder consultaExtratoHolderOb) {
		logger.info("         confPeriodoCons. INI ");
		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Periodo_Type periodoConsulta = new br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Periodo_Type();
		DateTimeFormatter f = DateTimeFormat.forPattern("dd/MM/yyyy");
		DateTime dataInicial = f.parseDateTime(consultaExtratoHolderOb.getDataInicio());
		DateTime dataFinal = f.parseDateTime(consultaExtratoHolderOb.getDataFim());
		periodoConsulta.setINICIO(dataInicial.toDate());
		periodoConsulta.setFIM(dataFinal.toDate());

		logger.info("         DATA_INICIO " + dataInicial.toString());
		logger.info("         DATA_FIM.   " + dataFinal.toString());

		logger.info("         confPeriodoCons. FIM ");
		return periodoConsulta;
	}
	

	private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO[] confTipoConsultaResumo(
			ConsultaComprovanteHolder consultaExtratoHolderOb) {

		logger.info("         confTipoConsulta INI ");
		List<String> solicitacoes = consultaExtratoHolderOb.getSolicitacoes();

		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO[] arraySolicitacoes = new br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO[solicitacoes
				.size()];

		for (int x = 0; x < solicitacoes.size(); x++) {
			arraySolicitacoes[x] = br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO
					.fromString(solicitacoes.get(x));
			logger.info("         FLAG_SOLICIT =  " + "" + solicitacoes.get(x));
		}
		logger.info("         confTipoConsulta FIM ");
		return arraySolicitacoes;
	}




	public RetornoHolder consultaComprovantePagamento(RetornoHolder retorno,
			ConsultaComprovanteHolder consultaSadoExtratoHolder,
			br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_saida_negocial_Type saida,
			ConsultaComprovanteHolder saidaHolder) throws Exception {
		
		logger.info("   consultaComprovantePagamento INI ");

		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type retornoSaida = null;
		List<TransacaoInt> pagsList = new ArrayList<TransacaoInt>();
		Transacao transa = new Transacao();

		//////////////////////////////////////////////////////////
		if (saida.getDADOS().getBOLETO() != null) {
			logger.info("       DADOS BOLETO IS NOT NULL ");
			retornoSaida = saida.getDADOS().getBOLETO();
			if (retornoSaida.getTRANSACOES() != null) {
				logger.info("       TRANSACOES IS NOT NULL ");
				// dadosList.addAll(convertArrayToList(retornoSaidaFGTS.getTRANSACOES(),"FGTS"));
				pagsList.addAll(PagamentoBuilder.getInstance().trataPAGAMENTO(retornoSaida,
						valorTotalTransferencia, "BOLETO"));
				valorTotalTransferencia = PagamentoBuilder.getInstance().getValorTotalTransferencia();
			} else {
				logger.info("       TRANSACOES IS NULL ");
			}
			logger.info("    Qtd DADOS BOLETO = " + retornoSaida.getTRANSACOES().length);
		} else {
			logger.info("       getDADOS BOLETO IS NULL ");
		}
		
		
		if (saida.getDADOS().getCONCESSIONARIA() != null) {
			logger.info("       DADOS CONCESSIONARIA IS NOT NULL ");
			retornoSaida = saida.getDADOS().getCONCESSIONARIA();
			if (retornoSaida.getTRANSACOES() != null) {
				logger.info("       TRANSACOES IS NOT NULL ");
				// dadosList.addAll(convertArrayToList(retornoSaidaFGTS.getTRANSACOES(),"FGTS"));
				pagsList.addAll(PagamentoBuilder.getInstance().trataPAGAMENTO(retornoSaida,
						valorTotalTransferencia, "CONCESSIONARIA"));
				valorTotalTransferencia = PagamentoBuilder.getInstance().getValorTotalTransferencia();
			} else {
				logger.info("       TRANSACOES IS NULL ");
			}
			logger.info("    Qtd DADOS CONCESSIONARIA = " + retornoSaida.getTRANSACOES().length);
		} else {
			logger.info("       getDADOS CONCESSIONARIA IS NULL ");
		}
		
		
		
		
//		//////////////////////////////////////////////////////////
//		if (saida.getDADOS().getIPVA_SP() != null) {
//			logger.info("       DADOS IPVA_SP IS NOT NULL ");
//			retornoSaida = saida.getDADOS().getIPVA_SP();
//			if (retornoSaida.getTRANSACOES() != null) {
//				logger.info("       TRANSACOES IS NOT NULL ");
//				// dadosList.addAll(convertArrayToList(retornoSaidaFGTS.getTRANSACOES(),"FGTS"));
//				pagsList.addAll(PagamentoBuilder.getInstance().trataPAGAMENTO(retornoSaida,valorTotalTransferencia, "IPVA_SP"));
//				valorTotalTransferencia = PagamentoBuilder.getInstance().getValorTotalTransferencia();
//			} else {
//				logger.info("       TRANSACOES IS NULL ");
//			}
//			logger.info("    Qtd DADOS IPVA_SP = " + retornoSaida.getTRANSACOES().length);
//		} else {
//			logger.info("       getDADOS IPVA_SP IS NULL ");
//		}
//		
//		
//		//////////////////////////////////////////////////////////
//		if (saida.getDADOS().getDPVAT_SP() != null) {
//			logger.info("       DADOS DPVAT_SP IS NOT NULL ");
//			retornoSaida = saida.getDADOS().getDPVAT_SP();
//			if (retornoSaida.getTRANSACOES() != null) {
//				logger.info("       TRANSACOES IS NOT NULL ");
//				// dadosList.addAll(convertArrayToList(retornoSaidaFGTS.getTRANSACOES(),"FGTS"));
//				pagsList.addAll(PagamentoBuilder.getInstance().trataPAGAMENTO(retornoSaida,valorTotalTransferencia, "DPVAT_SP"));
//				valorTotalTransferencia = PagamentoBuilder.getInstance().getValorTotalTransferencia();
//			} else {
//				logger.info("       TRANSACOES IS NULL ");
//			}
//			logger.info("    Qtd DADOS DPVAT_SP = " + retornoSaida.getTRANSACOES().length);
//		} else {
//			logger.info("       getDADOS DPVAT_SP IS NULL ");
//		}
//		
//		//////////////////////////////////////////////////////////
//		if (saida.getDADOS().getMULTAS_SP() != null) {
//			logger.info("       DADOS MULTAS_SP IS NOT NULL ");
//			retornoSaida = saida.getDADOS().getMULTAS_SP();
//			if (retornoSaida.getTRANSACOES() != null) {
//				logger.info("       TRANSACOES IS NOT NULL ");
//				// dadosList.addAll(convertArrayToList(retornoSaidaFGTS.getTRANSACOES(),"FGTS"));
//				pagsList.addAll(PagamentoBuilder.getInstance().trataPAGAMENTO(retornoSaida,valorTotalTransferencia, "MULTAS_SP"));
//				valorTotalTransferencia = PagamentoBuilder.getInstance().getValorTotalTransferencia();
//			} else {
//				logger.info("       TRANSACOES IS NULL ");
//			}
//			logger.info("    Qtd DADOS MULTAS_SP = " + retornoSaida.getTRANSACOES().length);
//		} else {
//			logger.info("       getDADOS MULTAS_SP IS NULL ");
//		}
//		
		
//		//////////////////////////////////////////////////////////
//		if (saida.getDADOS().getLICENCIAMENTO_SP() != null) {
//			logger.info("       DADOS LICENCIAMENTO_SP IS NOT NULL ");
//			retornoSaida = saida.getDADOS().getLICENCIAMENTO_SP();
//			if (retornoSaida.getTRANSACOES() != null) {
//				logger.info("       TRANSACOES IS NOT NULL ");
//				// dadosList.addAll(convertArrayToList(retornoSaidaFGTS.getTRANSACOES(),"FGTS"));
//				pagsList.addAll(PagamentoBuilder.getInstance().trataPAGAMENTO(retornoSaida,valorTotalTransferencia, "LICENCIAMENTO_SP"));
//				valorTotalTransferencia = PagamentoBuilder.getInstance().getValorTotalTransferencia();
//			} else {
//				logger.info("       TRANSACOES IS NULL ");
//			}
//			logger.info("    Qtd DADOS LICENCIAMENTO_SP = " + retornoSaida.getTRANSACOES().length);
//		} else {
//			logger.info("       getDADOS LICENCIAMENTO_SP IS NULL ");
//		}
//		
		
//		//////////////////////////////////////////////////////////
//		if (saida.getDADOS().getDAED() != null) {
//			logger.info("       DADOS DAED IS NOT NULL ");
//			retornoSaida = saida.getDADOS().getDAED();
//			if (retornoSaida.getTRANSACOES() != null) {
//				logger.info("       TRANSACOES IS NOT NULL ");
//				// dadosList.addAll(convertArrayToList(retornoSaidaFGTS.getTRANSACOES(),"FGTS"));
//				pagsList.addAll(PagamentoBuilder.getInstance().trataPAGAMENTO(retornoSaida,valorTotalTransferencia, "DAED"));
//				valorTotalTransferencia = PagamentoBuilder.getInstance().getValorTotalTransferencia();
//			} else {
//				logger.info("       TRANSACOES IS NULL ");
//			}
//			logger.info("    Qtd DADOS DAED = " + retornoSaida.getTRANSACOES().length);
//		} else {
//			logger.info("       getDADOS DAED IS NULL ");
//		}
//			
//		
//		
		
		
		
		
		
		transa.setValorTotalMes(valorTotalTransferencia.toString());
		logger.info("   TOTAL DE OBJETOS =  " + pagsList.size());
		
		Util.ordenaListaPorDataHora(pagsList);
		
		transa.setTransacao(pagsList);

		retorno.setDados(transa);

		logger.info("   consultaComprovantePagamento FIM ");
		return retorno;

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	protected String getPackage() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}