package br.gov.caixa.comprovante.service.builder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

import br.gov.caixa.api.common.holder.ErrorHolder;
import br.gov.caixa.api.common.holder.RetornoHolder;
import br.gov.caixa.api.common.resource.util.ResourceUtil;
import br.gov.caixa.comprovante.constant.Banco;
import br.gov.caixa.comprovante.holder.ConsultaComprovanteHolder;
import br.gov.caixa.comprovante.holder.ConsultaPagamentoDetalheHolder;
import br.gov.caixa.comprovante.holder.Transacao;
import br.gov.caixa.comprovante.util.Util;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_entrada_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_entrada_solicitacoes_TypeSOLICITACAO;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Periodo_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Servico_entrada_negocial_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;
import br.gov.caixa.sibar.consulta_transacoes_conta.ws.ConsultaTransacoesContaSOAPStub;
import br.gov.caixa.sibar.consulta_transacoes_conta.ws.Consulta_Transacoes_ContaProxy;

/**
 * 
 * @author Capgemini SP
 * 
 */
@Component
public class ConsultaComprovanteTransferenciaBuilder extends AbstractBuilder {
	@Override
	protected String getPackage() {
		// TODO Auto-generated method stub
		return null;
	}

	private BigDecimal valorTotalTransferencia = new BigDecimal("0");
	
	private short agencia;

	private long conta;

	private short dv;
	
	
	

	public RetornoHolder preparawebServiceTransferencia(ConsultaComprovanteHolder consultaSadoExtratoHolder) throws Exception {

		logger.info("   preparawebServiceTransferencia INI ");
		
		 br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_saida_negocial_Type saida = null;
		
		RetornoHolder retorno = new RetornoHolder();
		int codReturn = 0;
		try {
		
				// saida = conectawebService(consultaExtratoHolder);
			br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type entrada = createMessageTransferencia_DETALHE(consultaSadoExtratoHolder);

				ResourceUtil.getInstance().imprimeFormatoJson(entrada, "ENTRADA SERVICO = ");

				ConsultaTransacoesContaSOAPStub ext = new ConsultaTransacoesContaSOAPStub();
				Consulta_Transacoes_ContaProxy px = new Consulta_Transacoes_ContaProxy();
				
				saida = px.DETALHE(entrada);
				
				ResourceUtil.getInstance().imprimeFormatoJson(saida, "SAIDA SERVICO = ");
				
				codReturn = validRetorno(saida.getCOD_RETORNO());

				if (codReturn != 0) {
					ErrorHolder error = new ErrorHolder();
					error.setCodigo(977);
					error.setMensagem(saida.getMSG_RETORNO());
					error.setCausa(saida.getORIGEM_RETORNO());
					logger.info("       Cod.Retorno = " + error.getCodigo());
					logger.info("       Msg.etorno = " + error.getMensagem());
					logger.info("       Msg.Causa = " + error.getCausa());
					retorno.setError(error);

					return retorno;
				} else {
					ErrorHolder error = new ErrorHolder();
					error.setCodigo(new Integer(saida.getCOD_RETORNO()).intValue());
					retorno.setError(error);
				}

		} catch (Exception x) {
			logger.error(x.getMessage());
			x.printStackTrace();
			ErrorHolder error = new ErrorHolder();
			error.setCodigo(977);
			retorno.setError(error);

		}

		retorno.setDados(saida);

		logger.info("   preparawebServiceTransferencia FIM ");
		return retorno;
	}
	
	
	
	
	public Servico_entrada_negocial_Type createMessageTransferencia_lista(ConsultaComprovanteHolder consultaExtratoHolder)
			throws Exception {
		logger.info("      createMessage() INI");

		Servico_entrada_negocial_Type servicoEntrada = new Servico_entrada_negocial_Type();

		servicoEntrada.setHEADER(this.createHeaderConsulta());

		servicoEntrada.setDADOS(this.montarDadosEntrada(consultaExtratoHolder));

		logger.info("      createMessage() FIM");
		return servicoEntrada;
	}

	
	public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_entrada_negocial_Type createMessageTransferencia_RESUMO(ConsultaComprovanteHolder consultaExtratoHolder)
			throws Exception {
		logger.info("      createMessage() INI");

		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_entrada_negocial_Type servicoEntrada = new br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_entrada_negocial_Type();

		servicoEntrada.setHEADER(this.createHeaderConsultaResumo());

		servicoEntrada.setDADOS(this.montarDadosEntradaResumo(consultaExtratoHolder));

		logger.info("      createMessage() FIM");
		return servicoEntrada;
	}
	
	public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type createMessageTransferencia_DETALHE(ConsultaComprovanteHolder consultaExtratoHolder)
			throws Exception {
		logger.info("      createMessage() INI");

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type servicoEntrada = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type();

		servicoEntrada.setHEADER(this.createHeaderConsultaDetalhe());

		
		servicoEntrada.setDADOS(this.montarDadosEntradaDetalheTransferencia(consultaExtratoHolder));

		logger.info("      createMessage() FIM");
		return servicoEntrada;
	}
	
	
	

	
	
	private Dados_entrada_Type montarDadosEntrada(ConsultaComprovanteHolder consultaExtratoHolder) {
		logger.info("         montarDadosEnt. INI");

		Dados_entrada_Type dadosEntrada = new Dados_entrada_Type();

		dadosEntrada.setCONTA(this.confConta(consultaExtratoHolder));
		dadosEntrada.setPERIODO(this.confPeriodoConsulta(consultaExtratoHolder));
		dadosEntrada.setSOLICITACOES(this.confTipoConsulta(consultaExtratoHolder));

		logger.info("         montarDadosEnt. FIM");
		return dadosEntrada;

	}

	private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_Type montarDadosEntradaResumo(ConsultaComprovanteHolder consultaExtratoHolder) {
		logger.info("         montarDadosEnt. INI");

		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_Type dadosEntrada = new br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_Type();

		dadosEntrada.setCONTA(this.confContaResumo(consultaExtratoHolder));
		dadosEntrada.setPERIODO(this.confPeriodoConsultaResumo(consultaExtratoHolder));
		dadosEntrada.setSOLICITACOES(this.confTipoConsultaResumo(consultaExtratoHolder));

		logger.info("         montarDadosEnt. FIM");
		return dadosEntrada;

	}
	
	
	public static int validRetorno(String codRetorno) {
		logger.info("validRetorno INI");
		logger.info("codRetorno = " + codRetorno);
		int resultadoAvaliacao = 0;

		try {

			resultadoAvaliacao = Integer.parseInt(codRetorno);

			if (resultadoAvaliacao != 0) {
				resultadoAvaliacao = -1;
			}

		} catch (NumberFormatException ex) {

			if ("X5".equalsIgnoreCase(codRetorno)) {
				resultadoAvaliacao = -5;
			} else {
				resultadoAvaliacao = -1;
			}

		}
		logger.info("validRetorno FIM");
		return resultadoAvaliacao;
	}

	
	private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_entrada_solicitacoes_TypeSOLICITACAO[] confTipoConsulta(
			ConsultaComprovanteHolder consultaExtratoHolderOb) {

		logger.info("         confTipoConsulta INI ");
		List<String> solicitacoes = consultaExtratoHolderOb.getSolicitacoes();

		Dados_entrada_solicitacoes_TypeSOLICITACAO[] arraySolicitacoes = new Dados_entrada_solicitacoes_TypeSOLICITACAO[solicitacoes
				.size()];

		for (int x = 0; x < solicitacoes.size(); x++) {
			arraySolicitacoes[x] = Dados_entrada_solicitacoes_TypeSOLICITACAO.fromString(solicitacoes.get(x));
			logger.info("         FLAG_SOLICIT =  " + "" + solicitacoes.get(x));
		}
		logger.info("         confTipoConsulta FIM ");
		return arraySolicitacoes;
	}
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO[] confTipoConsultaResumo(
			ConsultaComprovanteHolder consultaExtratoHolderOb) {

		logger.info("         confTipoConsulta INI ");
		List<String> solicitacoes = consultaExtratoHolderOb.getSolicitacoes();

		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO[] arraySolicitacoes = new br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO[solicitacoes
				.size()];

		for (int x = 0; x < solicitacoes.size(); x++) {
			arraySolicitacoes[x] = br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_entrada_solicitacoes_TypeSOLICITACAO.fromString(solicitacoes.get(x));
			logger.info("         FLAG_SOLICIT =  " + "" + solicitacoes.get(x));
		}
		logger.info("         confTipoConsulta FIM ");
		return arraySolicitacoes;
	}
	
	public RetornoHolder consultaComprovanteTransferencia(RetornoHolder retorno,
			ConsultaComprovanteHolder consultaSadoExtratoHolder,
			br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_saida_negocial_Type saida,
			ConsultaComprovanteHolder saidaHolder) throws Exception {
		logger.info("   comprovanteTransferencia INI ");

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ted_Type  retornoSaidaTED = null;
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_tev_Type  retornoSaidaTEV = null;
		 br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_doc_Type retornoSaidaDOC = null;

		valorTotalTransferencia = new BigDecimal("0");

		this.agencia = consultaSadoExtratoHolder.getAgencia();
		this.conta = consultaSadoExtratoHolder.getConta();
		this.dv = consultaSadoExtratoHolder.getDv();

		Transacao transa = new Transacao();
		List<TransacaoInt> transList = new ArrayList<TransacaoInt>();
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		if (saida.getDADOS() != null) {

			/*** ///////////////////////////////////////////////////////// ****/

			if (saida.getDADOS().getTED() != null) {
				logger.info("       DADOS TED IS NOT NULL ");
				retornoSaidaTED = saida.getDADOS().getTED();
				if (retornoSaidaTED.getTRANSACOES() != null) {
					logger.info("       TED TRANSACOES IS NOT NULL ");

					List<TransacaoInt> transLis = TransferenciaBuilder.getInstance().trataTED(retornoSaidaTED, valorTotalTransferencia);

					List<br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_ted_Type> transacoesTedList = new ArrayList<br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_ted_Type>();

					for (Iterator iterator = transLis.iterator(); iterator.hasNext();) {
						
						br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ted_Type transacaoInt = (br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ted_Type) iterator.next();
					
						br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_ted_Type list_ted = new br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_ted_Type(); 
						list_ted.setNSU(transacaoInt.getDADOS_TRANSACAO().getNSU().longValue());

						list_ted.setTipoServico("TED");
						
						
						if(transacaoInt.getDADOS_REMETENTE()[0] != null) {
							list_ted.getREMETENTE1().setNOME(transacaoInt.getDADOS_REMETENTE()[0].getNOME());	
							list_ted.getREMETENTE1().setCPF(transacaoInt.getDADOS_REMETENTE()[0].getDOCUMENTO().getCPF().longValue());
							//list_ted.setTIPO_CONTA_REMETENTE(transacaoInt.getDADOS_REMETENTE()[0].getTIPO_CONTA());
						}
						
						list_ted.setVALOR(transacaoInt.getVALOR());
						list_ted.setDATA_MOVIMENTO(transacaoInt.getDADOS_TRANSACAO().getDATA_MOVIMENTO());
						//list_ted.setDATA_TRANSFERENCIA(transacaoInt);
						list_ted.setNSU_CONFIRMACAO(transacaoInt.getNSU_CONFIRMACAO().longValue());
						list_ted.setISPB_DESTINO(transacaoInt.getISPB_DESTINO().intValue());
						
						list_ted.setBANCO_DESTINO(transacaoInt.getCONTA_DESTINATARIO().getBANCO().shortValue());
						list_ted.setAGENCIA_DESTINO(transacaoInt.getCONTA_DESTINATARIO().getAGENCIA().shortValue());
						list_ted.setCONTA_DESTINO(transacaoInt.getCONTA_DESTINATARIO().getCONTA().longValue());
						
						list_ted.setDV_CONTA_DESTINO( Byte.toString(transacaoInt.getCONTA_DESTINATARIO().getDV()));
						
						
						list_ted.getDESTINATARIO().setNOME(transacaoInt.getDADOS_DESTINATARIO().getNOME());
						list_ted.getDESTINATARIO().setCPF(transacaoInt.getDADOS_DESTINATARIO().getDOCUMENTO().getCPF().longValue());
						
						//list_ted.setTIPO_CONTA_DESTINATARIO(TIPO_CONTA_DESTINATARIO);
						
						list_ted.setTARIFA(transacaoInt.getVALOR_TARIFA());
						
					      try {
					    	Date data = transacaoInt.getDADOS_TRANSACAO().getDATA_MOVIMENTO();
					  		Calendar cal = Calendar.getInstance();
					  		cal.setTime(data);
					  		list_ted.setDATA_TRANSACAO(cal);	
					  	} catch (Exception e) {
					  		ResourceUtil.getInstance().imprimeFormatoJson(e, "ERROR = ");
					  	}
						
						transacoesTedList.add(list_ted);
					}
					
					transList.addAll(transacoesTedList);
					
					valorTotalTransferencia = TransferenciaBuilder.getInstance().getValorTotalTransferencia();

					// dadosList.addAll();
				} else {
					logger.info("       TED TRANSACOES IS NULL ");
				}
				logger.info("       getDADOS TED = " + retornoSaidaTED.getTRANSACOES());
			} else {
				logger.info("       getDADOS TED IS NULL ");
			}

			/*** ///////////////////////////////////////////////////////// ****/

			if (saida.getDADOS().getTEV() != null) {
				logger.info("       DADOS TEV IS NOT NULL ");
				retornoSaidaTEV = saida.getDADOS().getTEV();
				if (retornoSaidaTEV.getTRANSACOES() != null) {
					logger.info("       TEV TRANSACOES IS NOT NULL ");

					List<TransacaoInt> transLis = TransferenciaBuilder.getInstance().trataTEV(retornoSaidaTEV, valorTotalTransferencia);
					
					List<br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_tev_Type> transacoesTedList = new ArrayList<br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_tev_Type>();

					for (Iterator iterator = transLis.iterator(); iterator.hasNext();) {
						br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_tev_Type transacaoInt = (br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_tev_Type) iterator.next();

						br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_tev_Type list_tev = new br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_tev_Type();
						
						list_tev.setTipoServico("TEV");
						
						list_tev.setNSU(transacaoInt.getDADOS_TRANSACAO().getNSU().intValue());
						list_tev.setCPF_DESTINO(new Long(transacaoInt.getCPF_CNPJ_DESTINO()).longValue());

						//list_ted.setTIPO_CONTA_DEBITO();
						//list_ted.getTIPO_CONTA_DEBITO()
						list_tev.setAGENCIA_DEBITO(transacaoInt.getDADOS_DESTINO().getNOME_AGENCIA());
						list_tev.setAGENCIA_CREDITO(transacaoInt.getDADOS_ORIGEM().getNOME_AGENCIA());
						list_tev.setCLIENTE_CREDITO(transacaoInt.getDADOS_ORIGEM().getNOME_CLIENTE());
						
						//list_ted.setID_CONTA(ID_CONTA);
						
						list_tev.getCONTA_CREDITO().setUNIDADE(transacaoInt.getDADOS_DESTINO().getCONTA().getUNIDADE().intValue());
						list_tev.getCONTA_CREDITO().setPRODUTO(transacaoInt.getDADOS_DESTINO().getCONTA().getPRODUTO().shortValue());
						list_tev.getCONTA_CREDITO().setNUMERO(transacaoInt.getDADOS_DESTINO().getCONTA().getNUMERO().longValue());
						list_tev.getCONTA_CREDITO().setDV(transacaoInt.getDADOS_DESTINO().getCONTA().getDV());
						
						
						String date = new SimpleDateFormat("yyyy-MM-dd").format(transacaoInt.getDADOS_TRANSACAO().getDATA_VALIDACAO());
						list_tev.setDATA_TRANSFERENCIA( date );

						list_tev.setVALOR(transacaoInt.getDADOS_TRANSACAO().getVALOR_TOTAL());
						
					   try {
					    	Date data = transacaoInt.getDADOS_TRANSACAO().getDATA_MOVIMENTO();
					  		Calendar cal = Calendar.getInstance();
					  		cal.setTime(data);
					  		list_tev.setDATA_TRANSACAO(cal);	
					  	} catch (Exception e) {
					  		ResourceUtil.getInstance().imprimeFormatoJson(e, "ERROR = ");
					  	}

					   transacoesTedList.add(list_tev);
					}
					
					transList.addAll(transacoesTedList);
					
					valorTotalTransferencia = TransferenciaBuilder.getInstance().getValorTotalTransferencia();

				} else {
					logger.info("       TEV TRANSACOES IS NULL ");
				}
				logger.info("       getDADOS TEV = " + retornoSaidaTEV.getTRANSACOES());
			} else {
				logger.info("       getDADOS TEV IS NULL ");
			}

			/*** ///////////////////////////////////////////////////////// ****/

			if (saida.getDADOS().getDOC() != null) {
				logger.info("       DADOS DOC IS NOT NULL ");
				retornoSaidaDOC = saida.getDADOS().getDOC();
				if (retornoSaidaDOC.getTRANSACOES() != null) {
					logger.info("       DOV TRANSACOES IS NOT NULL ");
					// onvertArrayToList(retornoSaidaDOC.getTRANSACOESLIST().getTRANSACAO(),"DOC");

					List<TransacaoInt> transLis =  TransferenciaBuilder.getInstance().trataDOC(retornoSaidaDOC, valorTotalTransferencia);
					
					List<br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_doc> transacoesTedList = new ArrayList<br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_doc>();

					for (Iterator iterator = transLis.iterator(); iterator.hasNext();) {
						
						br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_doc_Type transacaoInt = (br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_doc_Type) iterator.next();

						br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_doc list_doc = new br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_doc();
						
						list_doc.setTipoServico("DOC");

						list_doc.setBanco_Descricao(Banco.getInstance().getDescricaoBanco(transacaoInt.getCONTA_DESTINO().getAGENCIA().getCODIGO_BANCO().toString()));

						list_doc.setNSU(transacaoInt.getDADOS_TRANSACAO().getNSU().intValue());
						list_doc.setDATA_MOVIMENTO(transacaoInt.getDADOS_TRANSACAO().getDATA_MOVIMENTO());
						list_doc.getBANCO_DESTINO().setAGENCIA(transacaoInt.getCONTA_DESTINO().getAGENCIA().getNUMERO().intValue());
						list_doc.getBANCO_DESTINO().setCODIGO(transacaoInt.getCONTA_DESTINO().getCONTA().shortValue());
						list_doc.getBANCO_DESTINO().setDV(  transacaoInt.getCONTA_DESTINO().getDV().toString()  );
						list_doc.setCPF(transacaoInt.getCONTA_DESTINO().getCPF_CNPJ());
						
						list_doc.getCONTA_DESTINO().setNOME(transacaoInt.getCONTA_DESTINO().getNOME_CLIENTE());
						list_doc.getCONTA_DESTINO().setNOME_AGENCIA(transacaoInt.getCONTA_DESTINO().getAGENCIA().getNOME());
						//list_doc.getCONTA_DEBITO().setTIPO_CONTA(TIPO_CONTA);
						
						list_doc.getCONTA_DEBITO().setNOME(transacaoInt.getCONTA_ORIGEM().getNOME_CLIENTE());
						list_doc.getCONTA_DEBITO().setNOME_AGENCIA(transacaoInt.getCONTA_ORIGEM().getAGENCIA().getNOME());
						//list_doc.getCONTA_DEBITO().setTIPO_CONTA(TIPO_CONTA);
						
						list_doc.setVALOR(transacaoInt.getDADOS_TRANSACAO().getVALOR_TOTAL());
						list_doc.setTARIFA_EMISSAO(transacaoInt.getTARIFA_EMISSAO());
						//list_doc.setTARIFA_AGENDAMENTO(TARIFA_AGENDAMENTO);
						
						list_doc.setIDENTIFICACAO(transacaoInt.getIDENTIFICACAO());
						try {
					    	Date data = transacaoInt.getDADOS_TRANSACAO().getDATA_MOVIMENTO();
					  		Calendar cal = Calendar.getInstance();
					  		cal.setTime(data);
					  		list_doc.setDATA_TRANSACAO(cal);	
					  	} catch (Exception e) {
					  		ResourceUtil.getInstance().imprimeFormatoJson(e, "ERROR = ");
					  	}
						//list_doc.setCANAL_ORIGEM("");
						//list_doc.setNUMERO_DOCUMENTO(NUMERO_DOCUMENTO);
						transacoesTedList.add(list_doc);
					}
					
					transList.addAll(transacoesTedList);
					
					valorTotalTransferencia = TransferenciaBuilder.getInstance().getValorTotalTransferencia();
					

					// dadosList.addAll();
				} else {
					logger.info("       DOC TRANSACOES IS NULL ");
				}
				logger.info("       getDADOS DOC = " + retornoSaidaDOC.getTRANSACOES());
			} else {
				logger.info("       getDADOS DOC IS NULL ");
			}

		}

		transa.setValorTotalMes(valorTotalTransferencia.toString());

		Util.ordenaListaPorDataHora(transList);

		transa.setTransacao(transList);

		retorno.setDados(transa);

		logger.info("   comprovanteTransferencia FIM ");
		return retorno;
	}
	
	
	private Conta_Type confConta(ConsultaComprovanteHolder consultaExtratoHolderOb) {
		logger.info("         confConta INI");
		Conta_Type contaType = new Conta_Type();

		long conta = consultaExtratoHolderOb.getConta();
		short produto = consultaExtratoHolderOb.getProduto();
		short agencia = consultaExtratoHolderOb.getAgencia(); // unidade agencia
		short dv = consultaExtratoHolderOb.getDv();

		contaType.setNUMERO(conta);
		contaType.setPRODUTO(produto);
		contaType.setUNIDADE(agencia);
		contaType.setDV(dv);

		logger.info("         NUMERO=" + conta);
		logger.info("         PRODUTO=" + produto);
		logger.info("         UNIDADE=" + agencia);
		logger.info("         DV=" + dv);
		logger.info("         confConta FIM");
		return contaType;
	}
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Conta_Type confContaResumo(ConsultaComprovanteHolder consultaExtratoHolderOb) {
		logger.info("         confConta INI");
		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Conta_Type contaType = new br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Conta_Type();

		long conta = consultaExtratoHolderOb.getConta();
		short produto = consultaExtratoHolderOb.getProduto();
		BigInteger agencia = BigInteger.valueOf(consultaExtratoHolderOb.getAgencia()); // unidade agencia
		short dv = consultaExtratoHolderOb.getDv();

		contaType.setNUMERO(conta);
		contaType.setPRODUTO(produto);
		contaType.setUNIDADE(agencia);
		contaType.setDV(dv);

		logger.info("         NUMERO=" + conta);
		logger.info("         PRODUTO=" + produto);
		logger.info("         UNIDADE=" + agencia);
		logger.info("         DV=" + dv);
		logger.info("         confConta FIM");
		return contaType;
	}
	
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Periodo_Type confPeriodoConsultaResumo(ConsultaComprovanteHolder consultaExtratoHolderOb) {
		logger.info("         confPeriodoCons. INI ");
		br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Periodo_Type periodoConsulta = new br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Periodo_Type();
		DateTimeFormatter f = DateTimeFormat.forPattern("dd/MM/yyyy");
		DateTime dataInicial = f.parseDateTime(consultaExtratoHolderOb.getDataInicio());
		DateTime dataFinal = f.parseDateTime(consultaExtratoHolderOb.getDataFim());
		periodoConsulta.setINICIO(dataInicial.toDate());
		periodoConsulta.setFIM(dataFinal.toDate()); 

		logger.info("         DATA_INICIO " + dataInicial.toString());
		logger.info("         DATA_FIM.   " + dataFinal.toString());

		logger.info("         confPeriodoCons. FIM ");
		return periodoConsulta;
	}

	private Periodo_Type confPeriodoConsulta(ConsultaComprovanteHolder consultaExtratoHolderOb) {
		logger.info("         confPeriodoCons. INI ");
		Periodo_Type periodoConsulta = new Periodo_Type();
		DateTimeFormatter f = DateTimeFormat.forPattern("dd/MM/yyyy");
		DateTime dataInicial = f.parseDateTime(consultaExtratoHolderOb.getDataInicio());
		DateTime dataFinal = f.parseDateTime(consultaExtratoHolderOb.getDataFim());
		periodoConsulta.setINICIO(dataInicial.toDate());
		periodoConsulta.setFIM(dataFinal.toDate()); 

		logger.info("         DATA_INICIO " + dataInicial.toString());
		logger.info("         DATA_FIM.   " + dataFinal.toString());

		logger.info("         confPeriodoCons. FIM ");
		return periodoConsulta;
	}
	

	public RetornoHolder preparawebServiceDETALHE(ConsultaPagamentoDetalheHolder consultaSadoExtratoHolder) throws Exception {

		logger.info("   preparawebService INI ");

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_saida_negocial_Type saida = null;
		RetornoHolder retorno = new RetornoHolder();
		int codReturn = 0;
		try {

				// saida = conectawebService(consultaExtratoHolder);
				br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type entrada = createMessageDetalhe(consultaSadoExtratoHolder);

				ResourceUtil.getInstance().imprimeFormatoJson(consultaSadoExtratoHolder, "ENTRADA FORMATO SOA- ");

				ConsultaTransacoesContaSOAPStub ext = new ConsultaTransacoesContaSOAPStub();
				Consulta_Transacoes_ContaProxy px = new Consulta_Transacoes_ContaProxy();
				logger.info("   Chama WEBSERVICE CONSULTA ");
				
				ResourceUtil.getInstance().imprimeFormatoJson(entrada, "ENTRADA SOAP = ");
				
				saida = px.DETALHE(entrada);
				
				ResourceUtil.getInstance().imprimeFormatoJson(saida, "SAIDA SOAP = ");
				
				logger.info("   Chama WEBSERVICE CONSULTA OK");

				codReturn = validRetorno(saida.getCOD_RETORNO());

				if (codReturn != 0) {
					ErrorHolder error = new ErrorHolder();
					error.setCodigo(977);
					error.setMensagem(saida.getMSG_RETORNO());
					error.setCausa(saida.getORIGEM_RETORNO());
					logger.info("       Cod.Retorno = " + error.getCodigo());
					logger.info("       Msg.etorno = " + error.getMensagem());
					logger.info("       Msg.Causa = " + error.getCausa());
					retorno.setError(error);

					return retorno;
				} else 
					
					
					if (saida.getDADOS().getCONTROLE_NEGOCIAL() != null){
							
							if (saida.getDADOS().getCONTROLE_NEGOCIAL().length > 0 ){
		
									br.gov.caixa.sibar.CONTROLE_NEGOCIAL_TYPE[] r = saida.getDADOS().getCONTROLE_NEGOCIAL();
									codReturn = validRetorno(r[0].getCOD_RETORNO());
									 
									ErrorHolder error = new ErrorHolder();
									error.setCodigo(codReturn);
									error.setMensagem(r[0].getMSG_RETORNO());
									error.setCausa(r[0].getORIGEM_RETORNO());
									
									logger.info("       Cod.Retorno = " + error.getCodigo());
									logger.info("       Msg.etorno = " + error.getMensagem());
									logger.info("       Msg.Causa = " + error.getCausa());
									retorno.setError(error);
				
									return retorno;
						  }

					}
				
	
		} catch (Exception x) {
			logger.error(x.getMessage());
			x.printStackTrace();
			ErrorHolder error = new ErrorHolder();
			error.setCodigo(977);
			retorno.setError(error);

		}

		retorno.setDados(saida);

		logger.info("   preparawebService FIM ");
		return retorno;
	}
		
	

	public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type createMessageDetalhe(
			ConsultaPagamentoDetalheHolder consultaExtratoHolder) throws Exception {
		logger.info("      createMessage() INI");

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type servicoEntrada = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type();

		servicoEntrada.setHEADER(this.createHeaderConsultaDetalhe());

		servicoEntrada.setDADOS(this.montarDadosEntradaDetalhe(consultaExtratoHolder));

		logger.info("      createMessage() FIM");
		return servicoEntrada;
	}
	
	
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_Type montarDadosEntradaDetalheTransferencia(
			ConsultaComprovanteHolder consultaExtratoHolder) {
		logger.info("         montarDadosEnt. INI");

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_Type dadosEntrada = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_Type();

		dadosEntrada.setCONTA(this.confContaDetalheTransferencia(consultaExtratoHolder));
		dadosEntrada.setPERIODO(this.confPeriodoConsultaDetalheTransferencia(consultaExtratoHolder));
		//dadosEntrada.setNSU(new org.apache.axis.types.UnsignedInt(consultaExtratoHolder.getNsu()));
		dadosEntrada.setSOLICITACOES(this.confTipoConsultaDetalheTransferencia(consultaExtratoHolder));
		org.apache.axis.types.UnsignedInt nsu = new org.apache.axis.types.UnsignedInt(0);
		dadosEntrada.setNSU(nsu);
		
		logger.info("         montarDadosEnt. FIM");
		return dadosEntrada;

	}

	
	
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_Type montarDadosEntradaDetalhe(
			ConsultaPagamentoDetalheHolder consultaExtratoHolder) {
		logger.info("         montarDadosEnt. INI");

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_Type dadosEntrada = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_Type();

		dadosEntrada.setCONTA(this.confContaDetalhe(consultaExtratoHolder));
		dadosEntrada.setPERIODO(this.confPeriodoConsultaDetalhe(consultaExtratoHolder));
		dadosEntrada.setNSU(new org.apache.axis.types.UnsignedInt(consultaExtratoHolder.getNsu()));
		dadosEntrada.setSOLICITACOES(this.confTipoConsultaDetalhe(consultaExtratoHolder));

		logger.info("         montarDadosEnt. FIM");
		return dadosEntrada;

	}
	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type confContaDetalheTransferencia(
			ConsultaComprovanteHolder consultaExtratoHolderOb) {
		logger.info("         confConta INI");
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type contaType = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type();

		long conta = consultaExtratoHolderOb.getConta();
		short produto = consultaExtratoHolderOb.getProduto();
		short dv = consultaExtratoHolderOb.getDv();

		contaType.setNUMERO(new org.apache.axis.types.UnsignedLong(conta));
		contaType.setPRODUTO(new org.apache.axis.types.UnsignedShort(produto));
		contaType.setUNIDADE(new org.apache.axis.types.UnsignedInt(consultaExtratoHolderOb.getAgencia()));   // unidade  -  agencia
		contaType.setDV((byte)dv);

		logger.info("         NUMERO=" + conta);
		logger.info("         PRODUTO=" + produto);
		logger.info("         UNIDADE=" + agencia);
		logger.info("         DV=" + dv);
		logger.info("         confConta FIM");
		return contaType;
	}
	
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type confContaDetalhe(
			ConsultaPagamentoDetalheHolder consultaExtratoHolderOb) {
		logger.info("         confConta INI");
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type contaType = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type();

		String conta = consultaExtratoHolderOb.getConta();
		String produto = consultaExtratoHolderOb.getProduto();
		String dv = consultaExtratoHolderOb.getDv();

		contaType.setNUMERO(new org.apache.axis.types.UnsignedLong(conta));
		contaType.setPRODUTO(new org.apache.axis.types.UnsignedShort(produto));
		contaType.setUNIDADE(new org.apache.axis.types.UnsignedInt(consultaExtratoHolderOb.getAgencia()));   // unidade  -  agencia
		
		Byte b = new Byte(dv);
		
		contaType.setDV(b.byteValue());

		logger.info("         NUMERO=" + conta);
		logger.info("         PRODUTO=" + produto);
		logger.info("         UNIDADE=" + agencia);
		logger.info("         DV=" + dv);
		logger.info("         confConta FIM");
		return contaType;
	}
	
	
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type confPeriodoConsultaDetalheTransferencia(
			ConsultaComprovanteHolder consultaExtratoHolderOb) {
		logger.info("         confPeriodoCons. INI ");
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type periodoConsulta = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type();
		DateTimeFormatter f = DateTimeFormat.forPattern("dd/MM/yyyy");
		DateTime dataInicial = f.parseDateTime(consultaExtratoHolderOb.getDataInicio());

		periodoConsulta.setINICIO(dataInicial.toDate());
		logger.info("         DATA_INICIO " + dataInicial.toString());

		
		DateTime dataFinal = f.parseDateTime(consultaExtratoHolderOb.getDataFim());
		periodoConsulta.setFIM(dataFinal.toDate());
		logger.info("         DATA_FIM.   " + dataFinal.toString());

		logger.info("         confPeriodoCons. FIM ");
		return periodoConsulta;
	}
	
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type confPeriodoConsultaDetalhe(
			ConsultaPagamentoDetalheHolder consultaExtratoHolderOb) {
		logger.info("         confPeriodoCons. INI ");
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type periodoConsulta = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type();
		DateTimeFormatter f = DateTimeFormat.forPattern("dd/MM/yyyy");
		DateTime dataInicial = f.parseDateTime(consultaExtratoHolderOb.getData());

		periodoConsulta.setINICIO(dataInicial.toDate());
		
		
		//DateTime dataFinal = f.parseDateTime(consultaExtratoHolderOb.getDataFim());
		//periodoConsulta.setFIM(dataFinal.toDate());
		//logger.info("         DATA_FIM.   " + dataFinal.toString());
		

		logger.info("         DATA_INICIO " + dataInicial.toString());


		logger.info("         confPeriodoCons. FIM ");
		return periodoConsulta;
	}
	
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO[] confTipoConsultaDetalhe(
			ConsultaPagamentoDetalheHolder consultaExtratoHolderOb) {

		logger.info("         confTipoConsulta INI ");
		List<String> solicitacoes = new ArrayList<String>();
		solicitacoes.add(consultaExtratoHolderOb.getSolicitacao());

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO[] arraySolicitacoes = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO[solicitacoes
				.size()];

		for (int x = 0; x < solicitacoes.size(); x++) {
			arraySolicitacoes[x] = br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO.fromString(solicitacoes.get(x));
			logger.info("         FLAG_SOLICIT =  " + "" + solicitacoes.get(x));
		}
		logger.info("         confTipoConsulta FIM ");
		return arraySolicitacoes;
	}

	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO[] confTipoConsultaDetalheTransferencia(
			ConsultaComprovanteHolder consultaExtratoHolderOb) {

		logger.info("         confTipoConsulta INI ");
		List<String> solicitacoes = new ArrayList<String>();
		solicitacoes.addAll(consultaExtratoHolderOb.getSolicitacoes());

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO[] arraySolicitacoes = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO[solicitacoes
				.size()];

		for (int x = 0; x < solicitacoes.size(); x++) {
			arraySolicitacoes[x] = br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO.fromString(solicitacoes.get(x));
			logger.info("         FLAG_SOLICIT =  " + "" + solicitacoes.get(x));
		}
		logger.info("         confTipoConsulta FIM ");
		return arraySolicitacoes;
	}
	
	
	
}

