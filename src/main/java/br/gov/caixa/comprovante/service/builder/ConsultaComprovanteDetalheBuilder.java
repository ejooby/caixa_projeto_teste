package br.gov.caixa.comprovante.service.builder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

import br.gov.caixa.api.common.holder.ErrorHolder;
import br.gov.caixa.api.common.holder.RetornoHolder;
import br.gov.caixa.api.common.resource.util.ResourceUtil;
import br.gov.caixa.comprovante.holder.ConsultaPagamentoDetalheHolder;
import br.gov.caixa.sibar.consulta_transacoes_conta.ws.ConsultaTransacoesContaSOAPStub;
import br.gov.caixa.sibar.consulta_transacoes_conta.ws.Consulta_Transacoes_ContaProxy;

/**
 * 
 * @author Capgemini SP
 * 
 */
@Component
public class ConsultaComprovanteDetalheBuilder extends AbstractBuilder {
	@Override
	protected String getPackage() {
		// TODO Auto-generated method stub
		return null;
	}

	private BigDecimal valorTotalTransferencia = new BigDecimal("0");
	
	private short agencia;

	private long conta;

	private short dv;
	
	
	

	public RetornoHolder preparawebServiceDETALHE(ConsultaPagamentoDetalheHolder consultaSadoExtratoHolder) throws Exception {

		logger.info("   preparawebService INI ");

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_saida_negocial_Type saida = null;
		RetornoHolder retorno = new RetornoHolder();
		int codReturn = 0;
		try {

				br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type entrada = createMessageDetalhe(consultaSadoExtratoHolder);

				
				
				ResourceUtil.getInstance().imprimeFormatoJson(consultaSadoExtratoHolder, "ENTRADA FORMATO SOA- ");

				ConsultaTransacoesContaSOAPStub ext = new ConsultaTransacoesContaSOAPStub();
				Consulta_Transacoes_ContaProxy px = new Consulta_Transacoes_ContaProxy();
				
				ResourceUtil.getInstance().imprimeFormatoJson(entrada, "ENTRADA SERVICO = ");
				
				saida = px.DETALHE(entrada);
				
				ResourceUtil.getInstance().imprimeFormatoJson(saida, "SAIDA SERVICO = ");
				

				codReturn = validRetorno(saida.getCOD_RETORNO());

				if (codReturn != 0) {
					ErrorHolder error = new ErrorHolder();
					error.setCodigo(977);
					error.setMensagem(saida.getMSG_RETORNO());
					error.setCausa(saida.getORIGEM_RETORNO());
					logger.info("       Cod.Retorno = " + error.getCodigo());
					logger.info("       Msg.etorno = " + error.getMensagem());
					logger.info("       Msg.Causa = " + error.getCausa());
					retorno.setError(error);

					return retorno;
				} else if (saida.getDADOS().getCONTROLE_NEGOCIAL().length > 0 ){
					

					 br.gov.caixa.sibar.CONTROLE_NEGOCIAL_TYPE[] r = saida.getDADOS().getCONTROLE_NEGOCIAL();
					
					 codReturn = validRetorno(r[0].getCOD_RETORNO());
					 
					 
					ErrorHolder error = new ErrorHolder();
					
					error.setCodigo(codReturn);
					error.setMensagem(r[0].getMSG_RETORNO());
					error.setCausa(r[0].getORIGEM_RETORNO());
					
					logger.info("       Cod.Retorno = " + error.getCodigo());
					logger.info("       Msg.etorno = " + error.getMensagem());
					logger.info("       Msg.Causa = " + error.getCausa());
					retorno.setError(error);

					return retorno;


				}

				
				
				

		} catch (Exception x) {
			logger.error(x.getMessage());
			x.printStackTrace();
			ErrorHolder error = new ErrorHolder();
			error.setCodigo(977);
			retorno.setError(error);

		}

		retorno.setDados(saida);

		logger.info("   preparawebService FIM ");
		return retorno;
	}
	
	public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type createMessageDetalhe(
			ConsultaPagamentoDetalheHolder consultaExtratoHolder) throws Exception {
		logger.info("      createMessage() INI");

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type servicoEntrada = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type();

		servicoEntrada.setHEADER(this.createHeaderConsultaDetalhe());

		servicoEntrada.setDADOS(this.montarDadosEntradaDetalhe(consultaExtratoHolder));

		logger.info("      createMessage() FIM");
		return servicoEntrada;
	}

	
	
	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_Type montarDadosEntradaDetalhe(
			ConsultaPagamentoDetalheHolder consultaExtratoHolder) {
		logger.info("         montarDadosEnt. INI");

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_Type dadosEntrada = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_Type();

		dadosEntrada.setCONTA(this.confContaDetalhe(consultaExtratoHolder));
		dadosEntrada.setPERIODO(this.confPeriodoConsultaDetalhe(consultaExtratoHolder));
		dadosEntrada.setNSU(new org.apache.axis.types.UnsignedInt(consultaExtratoHolder.getNsu()));
		dadosEntrada.setSOLICITACOES(this.confTipoConsultaDetalhe(consultaExtratoHolder));
		dadosEntrada.setACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_TypeACAO.CONSULTAR);

		logger.info("         montarDadosEnt. FIM");
		return dadosEntrada;

	}
	
	
	

	
	public static int validRetorno(String codRetorno) {
		logger.info("validRetorno INI");
		logger.info("codRetorno = " + codRetorno);
		int resultadoAvaliacao = 0;

		try {

			resultadoAvaliacao = Integer.parseInt(codRetorno);

			if (resultadoAvaliacao != 0) {
				resultadoAvaliacao = -1;
			}

		} catch (NumberFormatException ex) {

			if ("X5".equalsIgnoreCase(codRetorno)) {
				resultadoAvaliacao = -5;
			} else {
				resultadoAvaliacao = -1;
			}

		}
		logger.info("validRetorno FIM");
		return resultadoAvaliacao;
	}
	

	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type confContaDetalhe(
			ConsultaPagamentoDetalheHolder consultaExtratoHolderOb) {
		logger.info("         confConta INI");
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type contaType = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type();

		String conta = consultaExtratoHolderOb.getConta();
		String produto = consultaExtratoHolderOb.getProduto();
		String dv = consultaExtratoHolderOb.getDv();

		contaType.setNUMERO(new org.apache.axis.types.UnsignedLong(conta));
		contaType.setPRODUTO(new org.apache.axis.types.UnsignedShort(produto));
		contaType.setUNIDADE(new org.apache.axis.types.UnsignedInt(consultaExtratoHolderOb.getAgencia()));   // unidade  -  agencia
		
		Byte b = new Byte(dv);
		contaType.setDV(b.byteValue());

		logger.info("         NUMERO=" + conta);
		logger.info("         PRODUTO=" + produto);
		logger.info("         UNIDADE=" + agencia);
		logger.info("         DV=" + dv);
		logger.info("         confConta FIM");
		return contaType;
	}

	
	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type confPeriodoConsultaDetalhe(
			ConsultaPagamentoDetalheHolder consultaExtratoHolderOb) {
		logger.info("         confPeriodoCons. INI ");
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type periodoConsulta = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type();
		DateTimeFormatter f = DateTimeFormat.forPattern("dd/MM/yyyy");
		DateTime dataInicial = f.parseDateTime(consultaExtratoHolderOb.getDataInicio());
		DateTime dataFinal = f.parseDateTime(consultaExtratoHolderOb.getDataFim());
		periodoConsulta.setINICIO(dataInicial.toDate());
		periodoConsulta.setFIM(dataInicial.toDate());

		logger.info("         DATA_INICIO " + dataInicial.toString());
		logger.info("         DATA_FIM.   " + dataInicial.toString());

		logger.info("         confPeriodoCons. FIM ");
		return periodoConsulta;
	}
	
	

	private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO[] confTipoConsultaDetalhe(
			ConsultaPagamentoDetalheHolder consultaExtratoHolderOb) {

		logger.info("         confTipoConsulta INI ");
		List<String> solicitacoes = new ArrayList<String>();
		solicitacoes.add(consultaExtratoHolderOb.getSolicitacao());

		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO[] arraySolicitacoes = new br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO[solicitacoes
				.size()];

		for (int x = 0; x < solicitacoes.size(); x++) {
			arraySolicitacoes[x] = br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_entrada_solicitacoes_TypeSOLICITACAO.fromString(solicitacoes.get(x));
			logger.info("         FLAG_SOLICIT =  " + "" + solicitacoes.get(x));
		}
		logger.info("         confTipoConsulta FIM ");
		return arraySolicitacoes;
	}
	
	
	
}