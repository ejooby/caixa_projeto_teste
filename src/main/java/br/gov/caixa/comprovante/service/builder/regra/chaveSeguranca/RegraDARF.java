package br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.caixa.api.common.resource.chave.seguranca.RegraInterface;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_darf_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;


/**
 * 
 * @author Capgemini2018
 *
 */
public class RegraDARF implements RegraInterface {

	/**
	 * 
	 */
	protected static final 	Logger logger = LoggerFactory.getLogger(RegraDARF.class);
	
	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat) {
		
		Dados_saida_transacao_darf_Type 	dadosSaida 		= 	null;
		TransacaoInt 						transacaoObject = 	((TransacaoInt)input);
		
		dadosSaida 										=	
				(Dados_saida_transacao_darf_Type)transacaoObject;
		
		
		Calendar dataOperacao		=	dadosSaida.getDATA_TRANSACAO();
		String   strDataOperacao	=	dataOperacao.getTime().toString();
		Date     dataDebito			=	dadosSaida.getDATA_PAGAMENTO();
		String   strDataDebito		=	dataDebito.toString();
		
		String strResult 			=	String.format(maskFormat, strDataOperacao,strDataDebito);
		
		return strResult;
	}

	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat, Object... add) {
		Dados_saida_transacao_darf_Type 	dadosSaida 		= 	null;
		TransacaoInt 						transacaoObject = 	((TransacaoInt)input);
		
		dadosSaida 										=	
				(Dados_saida_transacao_darf_Type)transacaoObject;
		

		String   	strNumAgencia		=	add[0].toString();
		String   	strCodOper			=	dadosSaida.getNSU().toString();
		String   	strNumConta			=	add[1].toString();	
		Calendar 	dataOperacao		=	dadosSaida.getDATA_TRANSACAO();
		String		strCodBarras		=	ZEROS_COD_BARRA;	
		String   	strDataOperacao		=	dataOperacao.getTime().toString();
		Date     	dataDebito			=	dadosSaida.getDATA_PAGAMENTO();
		String   	strDataDebito		=	dataDebito.toString();
		
		logger.info("[REGRA DARF CHAVE SEGURANCA] 6 CAMPOS INFORMADOS");
		logger.info("[REGRA DARF CHAVE SEGURANCA] REGRA " + maskFormat);
		logger.info("[REGRA DARF CHAVE SEGURANCA] DADOS INFORMADOS EM ORDEM: ");
		logger.info("[REGRA DARF CHAVE SEGURANCA] AGENCIA: " + strNumAgencia);
		logger.info("[REGRA DARF CHAVE SEGURANCA] COD OPERACAO: " + strCodOper);
		logger.info("[REGRA DARF CHAVE SEGURANCA] NUMERO DA CONTA: " + strNumConta);
		logger.info("[REGRA DARF CHAVE SEGURANCA] CODIGO DE BARRAS: " + strCodBarras);
		logger.info("[REGRA DARF CHAVE SEGURANCA] DATA OPERACAO: " + strDataOperacao);
		logger.info("[REGRA DARF CHAVE SEGURANCA] DATA DEBITO: " + strDataDebito);
		String strResult 				=	String.format(maskFormat, strNumAgencia, strCodOper, strNumConta, strCodBarras, strDataOperacao, strDataDebito);
		logger.info("[REGRA DARF CHAVE SEGURANCA] FORMATACAO DA REGRA: " + strResult);
		
		
		return strResult;
	}
	
	

}
