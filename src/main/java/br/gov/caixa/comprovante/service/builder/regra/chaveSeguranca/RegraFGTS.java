package br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.caixa.api.common.resource.chave.seguranca.RegraInterface;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_fgts_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_tev_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

/**
 * 
 * @author Capgemini SP 2018
 *
 */
public class RegraFGTS implements RegraInterface {

	
	/**
	 * 
	 */
	protected static final 	Logger logger = LoggerFactory.getLogger(RegraFGTS.class);
	
	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat, Object... add) {
		
		Dados_saida_transacao_fgts_Type dadosSaida 		= 	null;
		TransacaoInt 				   transacaoObject 	= 	((TransacaoInt)input);
		
		dadosSaida										=
				(Dados_saida_transacao_fgts_Type)transacaoObject;
		
		String 		strNumAgencia	=	add[0].toString();
		String		strTpConta		=	TIPO_CONTA;
		String		strNumeroConta	=	add[1].toString();
		String  	strNumOperacao	=	dadosSaida.getNSU().toString();
		BigDecimal 	valor			=	dadosSaida.getVALOR();
		String		strValor		=	valor.toString();
		String		strCodOper		=	strNumOperacao;
		String		strCodBarras	=	ZEROS_COD_BARRA;
		String		strDataValidade	=	dadosSaida.getDATA_VALIDADE().toString();
		
		logger.info("[REGRA FGTS CHAVE SEGURANCA] 8 CAMPOS INFORMADOS");
		logger.info("[REGRA FGTS CHAVE SEGURANCA] REGRA " + maskFormat);
		logger.info("[REGRA FGTS CHAVE SEGURANCA] DADOS INFORMADOS EM ORDEM: ");
		logger.info("[REGRA FGTS CHAVE SEGURANCA] AGENCIA: " + strNumAgencia);
		logger.info("[REGRA FGTS CHAVE SEGURANCA] TIPO DA CONTA: " + strTpConta);
		logger.info("[REGRA FGTS CHAVE SEGURANCA] NUMERO DA CONTA: " + strNumeroConta);
		logger.info("[REGRA FGTS CHAVE SEGURANCA] NUMERO OPERACAO: " + strNumOperacao);
		logger.info("[REGRA FGTS CHAVE SEGURANCA] VALOR OPERACAO: " + strValor);
		logger.info("[REGRA FGTS CHAVE SEGURANCA] CODIGO OPERACAO: " + strCodOper);
		logger.info("[REGRA FGTS CHAVE SEGURANCA] CODIGO DE BARRAS: " + strCodBarras);
		logger.info("[REGRA FGTS CHAVE SEGURANCA] DATA VALIDADE: " + strDataValidade);
		
		String 		chave = String.format(maskFormat,strNumAgencia, strTpConta, 
				strNumeroConta, strNumOperacao, strValor, strCodOper, strCodBarras, strDataValidade);
		
		logger.info("[REGRA FGTS CHAVE SEGURANCA] FORMATACAO DA REGRA: " + chave);
		
		
		return chave;
	}

}
