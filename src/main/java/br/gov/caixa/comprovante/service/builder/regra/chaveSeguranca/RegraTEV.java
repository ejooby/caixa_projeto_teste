package br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.caixa.api.common.resource.chave.seguranca.RegraInterface;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_tev_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;


/**
 * 
 * @author Capgemini2018
 *
 */
public class RegraTEV implements RegraInterface {

	/**
	 * 
	 */
	protected static final 	Logger logger = LoggerFactory.getLogger(RegraTEV.class);
	
	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat) {
		
		
		Dados_saida_transacao_tev_Type dadosSaida 		= 	null;
		TransacaoInt 				   transacaoObject 	= 	((TransacaoInt)input);
		
		dadosSaida										=
				(Dados_saida_transacao_tev_Type)transacaoObject;
		
		String 		numAgencia			=	dadosSaida.getAGENCIA_DEBITO();
		String 		tpConta				=	dadosSaida.getTIPO_CONTA_DEBITO();
		BigDecimal 	valorOperacao		=	dadosSaida.getVALOR();
		String		strValorOperacao	=	valorOperacao.toString();
		String		agDestino			=	dadosSaida.getAGENCIA_CREDITO();
		String      tpContaDest			=	dadosSaida.getTIPO_CONTA_CREDITO();
		Conta_Type  contaDestino		=	dadosSaida.getCONTA_CREDITO();
		long		numContaDest		=	contaDestino.getNUMERO();
		String      strNumContaDest		=	String.valueOf(numContaDest);
		String 		dataOperacao		=	dadosSaida.getDATA_TRANSFERENCIA();
		
		String 		chave = String.format(maskFormat,numAgencia, tpConta, 
				strValorOperacao, agDestino, 
				tpContaDest, strNumContaDest, dataOperacao);
		
		return chave;
	}

	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat, Object... add) {
		Dados_saida_transacao_tev_Type dadosSaida 		= 	null;
		TransacaoInt 				   transacaoObject 	= 	((TransacaoInt)input);
		
		dadosSaida										=
				(Dados_saida_transacao_tev_Type)transacaoObject;
		
		
		String 		strNumAgencia		=	add[0].toString();
		String 		strTpConta			=	TIPO_CONTA;
		String      strNumConta			=	add[1].toString();
		String      strCodOper			=	dadosSaida.getNSU().toString();
		BigDecimal 	valorOperacao		=	dadosSaida.getVALOR();
		String		strValorOperacao	=	valorOperacao.toString();
		String      strCodTransacao		=	dadosSaida.getNSU().toString();
		String		strAgDestino		=	dadosSaida.getAGENCIA_CREDITO();
		String      strTpContaDest		=	dadosSaida.getTIPO_CONTA_CREDITO();
		Conta_Type  contaDestino		=	dadosSaida.getCONTA_CREDITO();
		long		numContaDest		=	contaDestino.getNUMERO();
		String      strNumContaDest		=	String.valueOf(numContaDest);
		String 		strDataOperacao		=	dadosSaida.getDATA_TRANSFERENCIA();

		logger.info("[REGRA TEV CHAVE SEGURANCA] 10 CAMPOS INFORMADOS");
		logger.info("[REGRA TEV CHAVE SEGURANCA] REGRA " + maskFormat);
		logger.info("[REGRA TEV CHAVE SEGURANCA] DADOS INFORMADOS EM ORDEM: ");
		logger.info("[REGRA TEV CHAVE SEGURANCA] AGENCIA: " + strNumAgencia);
		logger.info("[REGRA TEV CHAVE SEGURANCA] TIPO CONTA: " + strTpConta);
		logger.info("[REGRA TEV CHAVE SEGURANCA] NUMERO DA CONTA: " + strNumConta );
		logger.info("[REGRA TEV CHAVE SEGURANCA] CODIGO DA OPERACAO: " + strCodOper);
		logger.info("[REGRA TEV CHAVE SEGURANCA] VALOR DA OPERACAO: " + strValorOperacao);
		logger.info("[REGRA TEV CHAVE SEGURANCA] CODIGO DA TRANSACAO: " + strCodTransacao);
		logger.info("[REGRA TEV CHAVE SEGURANCA] AGENCIA DE DESTINO: " + strAgDestino);
		logger.info("[REGRA TEV CHAVE SEGURANCA] TIPO CONTA DESTINO: " + strTpContaDest);
		logger.info("[REGRA TEV CHAVE SEGURANCA] CONTA DESTINO: " + strNumContaDest);
		logger.info("[REGRA TEV CHAVE SEGURANCA] DATA DA OPERACAO: " + strDataOperacao);
		
		String 		chave = String.format(maskFormat, strNumAgencia, strTpConta, strNumConta, strCodOper, 
					strValorOperacao, strCodTransacao, strAgDestino, strTpContaDest, strNumContaDest, strDataOperacao);
		logger.info("[REGRA TEV CHAVE SEGURANCA] FORMATACAO DA REGRA: " + chave);
		
		return chave;
	}

	
}
