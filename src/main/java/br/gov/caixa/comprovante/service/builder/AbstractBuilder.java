package br.gov.caixa.comprovante.service.builder;

import java.io.Reader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import br.gov.caixa.comprovante.constant.SinbmApiConstant;
import br.gov.caixa.comprovante.holder.ConsultaComprovanteHolder;
import br.gov.caixa.comprovante.util.Util;
import br.gov.caixa.sibar.HEADER_BARRAMENTO_TYPE;


/**
 * 
 * @author Capgemini SP
 *
 */
public abstract class AbstractBuilder {
	
	protected static final Logger logger = LoggerFactory.getLogger(AbstractBuilder.class);
	
	// Roda Mock ?
	@Value("${app.sinbm.mock}")
	protected String mock ;

	// ByPass no WebService ?	
	@Value("${app.sinbm.mockWs}")
	protected String mockWs ;
	
	/**
	 * 
	 */
	private static final String VERSAO_BARRAMENTO = "1.0";
	
	
	//private static final String USUARIO_SERVICO = "SAPPNBMP";
		private static final String USUARIO_SERVICO = "SAPPNBMD";
	 
	
	
	
	/**
	 * 
	 */
	protected final String DATE_PATTERN_SIBAR = "yyyyMMddHHmmss";
	/**
	 * 
	 */
	protected final String DATE_PATTERN = "yyyy-MM-dd";
	/**
	 * 
	 */
	protected final String TIME_PATTERN = "HH:mm:ss";
	/**
	 * 
	 */
	protected JAXBContext jaxBContext = null;
	/**
	 * 
	 */
	protected Marshaller marshaller = null;
	/**
	 * 
	 */
	protected Unmarshaller unmarshaller = null;
	/**
	 * 
	 */
	protected Object objectUnmarshall = null;
	/**
	 * 
	 */
	protected final short REDE_TRANSMISSORA_9698 =   9698;
	protected final short REDE_TRANSMISSORA_9885 =   9885;
	
	
	/**
	 * 
	 * @return
	 */
	protected abstract String getPackage();

//	@PostConstruct
	public void init() throws JAXBException {
		jaxBContext = JAXBContext.newInstance(getPackage());
	}

	public <T> T umMarshal(String mensagem, Class<T> classType) throws JAXBException, XMLStreamException {

		//jaxBContext = JAXBContext.newInstance(getPackage());
		unmarshaller = jaxBContext.createUnmarshaller();
		Reader reader = new StringReader(mensagem);
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader xmlReader = factory.createXMLStreamReader(reader);
		T value = unmarshaller.unmarshal(xmlReader, classType ).getValue();
		
		return value;
	}

	protected String format(Date date, String pattern) {

		SimpleDateFormat formater = new SimpleDateFormat();
		formater.applyPattern(pattern);
		return formater.format(date);
	}
	
	protected HEADER_BARRAMENTO_TYPE createHeaderConsulta() {
		logger.info("         createHeader() INI");
		
		
		HEADER_BARRAMENTO_TYPE header = new HEADER_BARRAMENTO_TYPE();
		header.setVERSAO(VERSAO_BARRAMENTO);
		//header.setAUTENTICACAO("");
		header.setUSUARIO_SERVICO(USUARIO_SERVICO);
		//header.setUSUARIO("");
		header.setOPERACAO(SinbmApiConstant.OPERACAO_LISTA);
		//header.setINDICE(1);
		header.setSISTEMA_ORIGEM(SinbmApiConstant.SISTEMA_ORIGEM);
		//header.setUNIDADE("7266");
		//header.setIDENTIFICADOR_ORIGEM(SinbmApiConstant.SISTEMA_ORIGEM);
		header.setDATA_HORA(format(new Date(), DATE_PATTERN_SIBAR));
		//header.setID_PROCESSO("");
		
		logger.info("         VERSAO = "+VERSAO_BARRAMENTO);
		logger.info("         USUARIO_SERV.="+USUARIO_SERVICO);
		logger.info("         OPERACAO = "+SinbmApiConstant.OPERACAO_LISTA);
		//logger.info("         INDICE = 1");
		logger.info("         SISTEMA_ORIGEM = "+SinbmApiConstant.SISTEMA_ORIGEM);
//		logger.info("         UNIDADE = 7266");
		//logger.info("         IDENT.ORIGEM = "+SinbmApiConstant.SISTEMA_ORIGEM);
		logger.info("         DATA_HR="+format(new Date(), DATE_PATTERN_SIBAR));
		logger.info("         createHeader() FIM");
		
		return header;
	}

	protected HEADER_BARRAMENTO_TYPE createHeaderConsultaResumo() {
		logger.info("         createHeader() INI");
		
		
		HEADER_BARRAMENTO_TYPE header = new HEADER_BARRAMENTO_TYPE();
		header.setVERSAO(VERSAO_BARRAMENTO);
		//header.setAUTENTICACAO("");
		header.setUSUARIO_SERVICO(USUARIO_SERVICO);
		//header.setUSUARIO("");
		header.setOPERACAO(SinbmApiConstant.OPERACAO_RESUMO);
		//header.setINDICE(1);
		header.setSISTEMA_ORIGEM(SinbmApiConstant.SISTEMA_ORIGEM);
		//header.setUNIDADE("7266");
		//header.setIDENTIFICADOR_ORIGEM(SinbmApiConstant.SISTEMA_ORIGEM);
		header.setDATA_HORA(format(new Date(), DATE_PATTERN_SIBAR));
		//header.setID_PROCESSO("");
		
		logger.info("         VERSAO = "+VERSAO_BARRAMENTO);
		logger.info("         USUARIO_SERV.="+USUARIO_SERVICO);
		logger.info("         OPERACAO = "+SinbmApiConstant.OPERACAO_RESUMO);
		//logger.info("         INDICE = 1");
		logger.info("         SISTEMA_ORIGEM = "+SinbmApiConstant.SISTEMA_ORIGEM);
//		logger.info("         UNIDADE = 7266");
		//logger.info("         IDENT.ORIGEM = "+SinbmApiConstant.SISTEMA_ORIGEM);
		logger.info("         DATA_HR="+format(new Date(), DATE_PATTERN_SIBAR));
		logger.info("         createHeader() FIM");
		
		return header;
	}
	
		
	protected HEADER_BARRAMENTO_TYPE createHeaderConsultaDetalhe() {
		logger.info("         createHeader() INI");
		
		
		HEADER_BARRAMENTO_TYPE header = new HEADER_BARRAMENTO_TYPE();
		header.setVERSAO(VERSAO_BARRAMENTO);
		header.setAUTENTICACAO("");
		header.setUSUARIO_SERVICO(USUARIO_SERVICO);
		header.setUSUARIO("");
		header.setOPERACAO(SinbmApiConstant.OPERACAO_DETALHE);
		header.setINDICE(1);
		header.setSISTEMA_ORIGEM(SinbmApiConstant.SISTEMA_ORIGEM);
		header.setUNIDADE("7266");
		header.setIDENTIFICADOR_ORIGEM(SinbmApiConstant.SISTEMA_ORIGEM);
		header.setDATA_HORA(format(new Date(), DATE_PATTERN_SIBAR));
		header.setID_PROCESSO("");
		
		
		
		logger.info("         VERSAO = "+VERSAO_BARRAMENTO);
		logger.info("         USUARIO_SERV.="+USUARIO_SERVICO);
		logger.info("         OPERACAO = "+SinbmApiConstant.OPERACAO_DETALHE);
		logger.info("         INDICE = 1");
		logger.info("         SISTEMA_ORIGEM = "+SinbmApiConstant.SISTEMA_ORIGEM);
		logger.info("         UNIDADE = 7266");
		logger.info("         IDENT.ORIGEM = "+SinbmApiConstant.SISTEMA_ORIGEM);
		logger.info("         DATA_HR="+format(new Date(), DATE_PATTERN_SIBAR));
		logger.info("         createHeader() FIM");
		
		return header;
	}
	
	protected HEADER_BARRAMENTO_TYPE createHeaderConsultaPagamento() {
		logger.info("         createHeader() INI");
		
		
		HEADER_BARRAMENTO_TYPE header = new HEADER_BARRAMENTO_TYPE();
		header.setVERSAO(VERSAO_BARRAMENTO);
		//header.setAUTENTICACAO("");
		header.setUSUARIO_SERVICO(USUARIO_SERVICO);
		//header.setUSUARIO("");
		header.setOPERACAO(SinbmApiConstant.OPERACAO_RESUMO);
		header.setINDICE(1);
		header.setSISTEMA_ORIGEM(SinbmApiConstant.SISTEMA_ORIGEM);
		header.setUNIDADE("7266");
		header.setIDENTIFICADOR_ORIGEM(SinbmApiConstant.SISTEMA_ORIGEM);
		header.setDATA_HORA(format(new Date(), DATE_PATTERN_SIBAR));
		//header.setID_PROCESSO("");
		
		logger.info("         VERSAO = "+VERSAO_BARRAMENTO);
		logger.info("         USUARIO_SERV.="+USUARIO_SERVICO);
		logger.info("         OPERACAO = "+SinbmApiConstant.OPERACAO_LISTA);
		logger.info("         INDICE = 1");
		logger.info("         SISTEMA_ORIGEM = "+SinbmApiConstant.SISTEMA_ORIGEM);
		logger.info("         UNIDADE = 7266");
		logger.info("         IDENT.ORIGEM = "+SinbmApiConstant.SISTEMA_ORIGEM);
		logger.info("         DATA_HR="+format(new Date(), DATE_PATTERN_SIBAR));
		logger.info("         createHeader() FIM");
		
		return header;
	}
	
	/**
	 * 
	 * @param dateStr
	 * @return
	 */
	protected XMLGregorianCalendar getDateGregCalendar (String dateStr) {
		
		Date                 dateRefer             = null;
		XMLGregorianCalendar returnXmlGregCalendar = null;
		GregorianCalendar    gregorianCalendar     = new GregorianCalendar();
		
		dateRefer	=	Util.stringToJavaDate(dateStr,SinbmApiConstant.DATE_FORMAT_DEFAULT);
		
		gregorianCalendar.setTime(dateRefer);
	
		try {
			returnXmlGregCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		
		return returnXmlGregCalendar;
	}
}
