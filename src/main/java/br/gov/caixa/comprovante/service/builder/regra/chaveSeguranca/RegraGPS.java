package br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca;

import java.util.Calendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import br.gov.caixa.api.common.resource.chave.seguranca.RegraInterface;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_transacao_gps_Type;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

/**
 * 
 * @author Capgemini2018
 *
 */
public class RegraGPS implements RegraInterface{

	/**
	 * 
	 */
	protected static final 	Logger logger = LoggerFactory.getLogger(RegraGPS.class);
	
	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat) {
		
		
		Dados_saida_transacao_gps_Type 		dadosSaida 		= 	null;
		TransacaoInt 						transacaoObject = 	((TransacaoInt)input);
		
		dadosSaida											=
				(Dados_saida_transacao_gps_Type) transacaoObject;
		
		String 		codigoTransacao 			= 	dadosSaida.getCODIGO();
		String 		identificacaoContribuinte 	= 	dadosSaida.getNOME();
		Calendar 	dataTransacao				=	dadosSaida.getDATA_TRANSACAO();
		String   	strDataTransacao			=	dataTransacao.getTime().toString();
		
		String 		strResult 					=	String.format(maskFormat, codigoTransacao, identificacaoContribuinte,
														strDataTransacao);
		
		return strResult;
	}

	/**
	 * 
	 */
	public String getDadosChave(Object input, String maskFormat, Object... add) {
		Dados_saida_transacao_gps_Type 		dadosSaida 		= 	null;
		TransacaoInt 						transacaoObject = 	((TransacaoInt)input);
		
		dadosSaida											=
				(Dados_saida_transacao_gps_Type) transacaoObject;
		
		String		strNumAgencia				=	add[0].toString();
		String		strTipoConta				=	TIPO_CONTA;
		String		strNumConta					=	add[1].toString();
		String 		strNumOperacao 				= 	dadosSaida.getNSU().toString();
		String      strValorOper				=	dadosSaida.getPAGAMENTO().toString();
		Calendar 	dataTransacao				=	dadosSaida.getDATA_TRANSACAO();
		String   	strDataTransacao			=	dataTransacao.getTime().toString();
		String      strCodBarras				=	ZEROS_COD_BARRA;
		
		logger.info("[REGRA GPS CHAVE SEGURANCA] 7 CAMPOS INFORMADOS");
		logger.info("[REGRA GPS CHAVE SEGURANCA] REGRA " + maskFormat);
		logger.info("[REGRA GPS CHAVE SEGURANCA] DADOS INFORMADOS EM ORDEM: ");
		logger.info("[REGRA GPS CHAVE SEGURANCA] AGENCIA: " + strNumAgencia);
		logger.info("[REGRA GPS CHAVE SEGURANCA] TIPO CONTA: " + strTipoConta);
		logger.info("[REGRA GPS CHAVE SEGURANCA] NUMERO DA CONTA: " + strNumConta);
		logger.info("[REGRA GPS CHAVE SEGURANCA] NUMERO DA OPERACAO: " + strNumOperacao);
		logger.info("[REGRA GPS CHAVE SEGURANCA] VALOR DA OPERACAO: " + strValorOper);
		logger.info("[REGRA GPS CHAVE SEGURANCA] DATA TRANSACAO: " + strDataTransacao);
		logger.info("[REGRA GPS CHAVE SEGURANCA] CODIGO DE BARRAS: " + strCodBarras);
		
		String 		strResult 					=	String.format(maskFormat, strNumAgencia, strTipoConta, 
				strNumConta, strNumOperacao, strValorOper, strDataTransacao, strCodBarras);
		logger.info("[REGRA GPS CHAVE SEGURANCA] FORMATACAO DA REGRA: " + strResult);
		
		
		return strResult;
	}
	
	

}
