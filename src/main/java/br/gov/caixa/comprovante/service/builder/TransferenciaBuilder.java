package br.gov.caixa.comprovante.service.builder;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.caixa.api.common.resource.chave.seguranca.AutenticadorIn;
import br.gov.caixa.api.common.resource.chave.seguranca.RegraInterface;
import br.gov.caixa.comprovante.constant.Banco;
import br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca.RegraDOC;
import br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca.RegraTED;
import br.gov.caixa.comprovante.service.builder.regra.chaveSeguranca.RegraTEV;
import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

public class TransferenciaBuilder {
	
	private BigDecimal valorTotalTransferencia = new BigDecimal(0);
	
	private static TransferenciaBuilder instance = null;

	private TransferenciaBuilder() {
		// Exists only to defeat instantiation.
	}

	public static TransferenciaBuilder getInstance() {
		if (instance == null) {
			instance = new TransferenciaBuilder();
		}
		return instance;
	}


	protected static final 	Logger logger = LoggerFactory.getLogger(TransferenciaBuilder.class);	
	private SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	
	
	
	
	

	public List<TransacaoInt> trataTED(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ted_Type retornoSaidaTED,BigDecimal valorTotalTransferencia  ){
		
		logger.info("trataTED INI");
		
		List<TransacaoInt> transList = new ArrayList<TransacaoInt>();
		
		

		
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ted_Type[] TRANSACOES = retornoSaidaTED.getTRANSACOES();
		for (int i = 0; i < TRANSACOES.length; i++) {
			
			
			
			br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ted_Type dados_saida_transacao_ted_Type = TRANSACOES[i];
		
		
			
			
			logger.info("iterator = "+i);
			
			dados_saida_transacao_ted_Type.setTipoServico("TED");
			if(dados_saida_transacao_ted_Type.getDADOS_TRANSACAO().getDATA_MOVIMENTO() != null){
				 try{
					 dados_saida_transacao_ted_Type.setBanco_Descricao(Banco.getInstance().getDescricaoBanco(dados_saida_transacao_ted_Type.getCONTA_DESTINATARIO().getBANCO().toString()));
				 }catch(Exception c){
					 dados_saida_transacao_ted_Type.setBanco_Descricao("Descrição Banco N/A");
				 }
				 
				 BigDecimal x = new BigDecimal("0");
				 
				 try{
					 if (dados_saida_transacao_ted_Type.getVALOR() == null ){
						 x = new BigDecimal("0");
					 }
					 this.valorTotalTransferencia = this.valorTotalTransferencia.add(dados_saida_transacao_ted_Type.getVALOR());
				 }catch(Exception c){
					 logger.error("   ERRO : "+c.getMessage());
				 }
				
				 try{
						 String 		chave 			= 	"";
						 RegraInterface regraTED 		= 	new RegraTED();
						 AutenticadorIn autenticador 	= 	new AutenticadorIn();
						 autenticador.setRegraInterface(regraTED);
						 chave							=	"CXJHAJHSU289USJ";//autenticador.runAut(transa, 14, this.agencia, this.conta);
						 dados_saida_transacao_ted_Type.setChaveSeguranca(chave);
				 }catch(Exception d){
					 logger.info("  ERRO AO GERAR CHAVE SEGURANCA ");
					 d.printStackTrace();
				 }
				 
			}else{
				 logger.info("   DATA TRANSACAO DE TED ESTA NULO ");
				 dados_saida_transacao_ted_Type.setData(format1.format(Calendar.getInstance()));
			}
		
		
			transList.add(dados_saida_transacao_ted_Type);

		}
		
		logger.info("trataTED FIM");
		return transList;
		
	}
	
	
	
	
	
	
	

	public List<TransacaoInt> trataTEV(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_tev_Type retornoSaidaTEV,BigDecimal valorTotalTransferencia  ){
		
		
		List<TransacaoInt> transList = new ArrayList<TransacaoInt>();
		
		
		
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_tev_Type[] TRANSACOES = retornoSaidaTEV.getTRANSACOES();
		
		for (int i = 0; i < TRANSACOES.length; i++) {
			
			br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_tev_Type dados_saida_transacao_tev_Type = TRANSACOES[i];
			
			logger.info("iterator = "+i);
			
			dados_saida_transacao_tev_Type.setTipoServico("TEV");
			
			if(dados_saida_transacao_tev_Type.getDADOS_TRANSACAO().getDATA_MOVIMENTO() != null){
				
				//this.valorTotalTransferencia 		= this.valorTotalTransferencia.add(dados_saida_transacao_tev_Type.getVALOR());
				
				try{	
					String 			chave 			= "";
					RegraInterface 	regraTEV 		= new RegraTEV();
					AutenticadorIn 	autenticador 	= new AutenticadorIn();
					autenticador.setRegraInterface(regraTEV);
					chave							= "CXJHAJHSU289USJ";//autenticador.runAut(transa, 10, this.conta);
					dados_saida_transacao_tev_Type.setChaveSeguranca(chave);
				}catch(Exception d){
					 logger.error("  ERRO AO GERAR CHAVE SEGURANCA ");
					 d.printStackTrace();
				 }
				
				
			}else{
				 logger.info("   DATA TRANSACAO DE TEV ESTA NULO ");
				 dados_saida_transacao_tev_Type.setData(format1.format(Calendar.getInstance().getTime()));
			}
			transList.add(dados_saida_transacao_tev_Type);
		}
		logger.info("       TEV valorTotalTransferencia =  "+valorTotalTransferencia);
		logger.info("       TEV dadosList QTD =  "+transList.size());
		
		return transList;		
		
	}
	

	public List<TransacaoInt> trataDOC( br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_doc_Type retornoSaidaDOC,BigDecimal valorTotalTransferencia  ){
		List<TransacaoInt> transList = new ArrayList<TransacaoInt>();
		
		
		br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_doc_Type[] TRANSACOES = retornoSaidaDOC.getTRANSACOES();
		
		for (int i = 0; i < TRANSACOES.length; i++) {
			
			br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_doc_Type dados_saida_transacao_doc = TRANSACOES[i];
			
			logger.info("iterator = "+i);
			
			dados_saida_transacao_doc.setTipoServico("DOC");
				
				
				if(dados_saida_transacao_doc.getDADOS_TRANSACAO().getDATA_MOVIMENTO() != null){
					
					//dados_saida_transacao_doc.setData(format1.format(dados_saida_transacao_doc.getDATA_TRANSACAO().getTime())); 			 
					//dados_saida_transacao_doc.setBanco_Descricao(Banco.getInstance().getDescricaoBanco(""+(dados_saida_transacao_doc.getDADOS_TRANSACAO().getBANCO_DESTINO().getCODIGO())));

					//this.valorTotalTransferencia = 			this.valorTotalTransferencia.add(dados_saida_transacao_doc.getVALOR());

					try{	
						String chave 									= "";
						RegraInterface regraDOC 						= new RegraDOC();
						AutenticadorIn autenticador 					= new AutenticadorIn();
						autenticador.setRegraInterface(regraDOC);
						chave							=	"CXJHAJHSU289USJ";//autenticador.runAut(transa, 16, this.agencia, this.conta);
						dados_saida_transacao_doc.setChaveSeguranca(chave);
					}catch(Exception d){
						 logger.error("  ERRO AO GERAR CHAVE SEGURANCA ");
						 d.printStackTrace();
					 }

					}else{
						 logger.info("   DATA TRANSACAO DE DOC ESTA NULO ");
						 dados_saida_transacao_doc.setData(format1.format(Calendar.getInstance()));
					}						
				
				transList.add(dados_saida_transacao_doc);
		}
		
		return transList;		
		
	}
	
	
	
	
	

	public BigDecimal getValorTotalTransferencia() {
		return valorTotalTransferencia;
	}

	public void setValorTotalTransferencia(BigDecimal valorTotalTransferencia) {
		this.valorTotalTransferencia = valorTotalTransferencia;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
