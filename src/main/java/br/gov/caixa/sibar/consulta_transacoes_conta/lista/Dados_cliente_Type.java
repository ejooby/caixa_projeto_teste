/**
 * Dados_cliente_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Dados_cliente_Type  implements java.io.Serializable {
    private short TIPO_CONTA;

    private java.lang.String NOME_AGENCIA;

    private java.lang.String NOME;

    public Dados_cliente_Type() {
    }

    public Dados_cliente_Type(
           short TIPO_CONTA,
           java.lang.String NOME_AGENCIA,
           java.lang.String NOME) {
           this.TIPO_CONTA = TIPO_CONTA;
           this.NOME_AGENCIA = NOME_AGENCIA;
           this.NOME = NOME;
    }


    /**
     * Gets the TIPO_CONTA value for this Dados_cliente_Type.
     * 
     * @return TIPO_CONTA
     */
    public short getTIPO_CONTA() {
        return TIPO_CONTA;
    }


    /**
     * Sets the TIPO_CONTA value for this Dados_cliente_Type.
     * 
     * @param TIPO_CONTA
     */
    public void setTIPO_CONTA(short TIPO_CONTA) {
        this.TIPO_CONTA = TIPO_CONTA;
    }


    /**
     * Gets the NOME_AGENCIA value for this Dados_cliente_Type.
     * 
     * @return NOME_AGENCIA
     */
    public java.lang.String getNOME_AGENCIA() {
        return NOME_AGENCIA;
    }


    /**
     * Sets the NOME_AGENCIA value for this Dados_cliente_Type.
     * 
     * @param NOME_AGENCIA
     */
    public void setNOME_AGENCIA(java.lang.String NOME_AGENCIA) {
        this.NOME_AGENCIA = NOME_AGENCIA;
    }


    /**
     * Gets the NOME value for this Dados_cliente_Type.
     * 
     * @return NOME
     */
    public java.lang.String getNOME() {
        return NOME;
    }


    /**
     * Sets the NOME value for this Dados_cliente_Type.
     * 
     * @param NOME
     */
    public void setNOME(java.lang.String NOME) {
        this.NOME = NOME;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_cliente_Type)) return false;
        Dados_cliente_Type other = (Dados_cliente_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.TIPO_CONTA == other.getTIPO_CONTA() &&
            ((this.NOME_AGENCIA==null && other.getNOME_AGENCIA()==null) || 
             (this.NOME_AGENCIA!=null &&
              this.NOME_AGENCIA.equals(other.getNOME_AGENCIA()))) &&
            ((this.NOME==null && other.getNOME()==null) || 
             (this.NOME!=null &&
              this.NOME.equals(other.getNOME())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getTIPO_CONTA();
        if (getNOME_AGENCIA() != null) {
            _hashCode += getNOME_AGENCIA().hashCode();
        }
        if (getNOME() != null) {
            _hashCode += getNOME().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_cliente_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_cliente_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_AGENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_AGENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
