/**
 * Dados_saida_transacao_recarga_celular_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_recarga_celular_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type OPERADORA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Telefone_Type TELEFONE;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_Type ASSINANTE;

    private java.lang.String REFERENCIA;

    private java.lang.String CODIGO_BARRAS;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_recarga_celular_Type() {
    }

    public Dados_saida_transacao_recarga_celular_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type OPERADORA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Telefone_Type TELEFONE,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_Type ASSINANTE,
           java.lang.String REFERENCIA,
           java.lang.String CODIGO_BARRAS,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.OPERADORA = OPERADORA;
           this.TELEFONE = TELEFONE;
           this.ASSINANTE = ASSINANTE;
           this.REFERENCIA = REFERENCIA;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the OPERADORA value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @return OPERADORA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type getOPERADORA() {
        return OPERADORA;
    }


    /**
     * Sets the OPERADORA value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @param OPERADORA
     */
    public void setOPERADORA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type OPERADORA) {
        this.OPERADORA = OPERADORA;
    }


    /**
     * Gets the TELEFONE value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @return TELEFONE
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Telefone_Type getTELEFONE() {
        return TELEFONE;
    }


    /**
     * Sets the TELEFONE value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @param TELEFONE
     */
    public void setTELEFONE(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Telefone_Type TELEFONE) {
        this.TELEFONE = TELEFONE;
    }


    /**
     * Gets the ASSINANTE value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @return ASSINANTE
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_Type getASSINANTE() {
        return ASSINANTE;
    }


    /**
     * Sets the ASSINANTE value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @param ASSINANTE
     */
    public void setASSINANTE(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_Type ASSINANTE) {
        this.ASSINANTE = ASSINANTE;
    }


    /**
     * Gets the REFERENCIA value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @return REFERENCIA
     */
    public java.lang.String getREFERENCIA() {
        return REFERENCIA;
    }


    /**
     * Sets the REFERENCIA value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @param REFERENCIA
     */
    public void setREFERENCIA(java.lang.String REFERENCIA) {
        this.REFERENCIA = REFERENCIA;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_recarga_celular_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_recarga_celular_Type)) return false;
        Dados_saida_transacao_recarga_celular_Type other = (Dados_saida_transacao_recarga_celular_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.OPERADORA==null && other.getOPERADORA()==null) || 
             (this.OPERADORA!=null &&
              this.OPERADORA.equals(other.getOPERADORA()))) &&
            ((this.TELEFONE==null && other.getTELEFONE()==null) || 
             (this.TELEFONE!=null &&
              this.TELEFONE.equals(other.getTELEFONE()))) &&
            ((this.ASSINANTE==null && other.getASSINANTE()==null) || 
             (this.ASSINANTE!=null &&
              this.ASSINANTE.equals(other.getASSINANTE()))) &&
            ((this.REFERENCIA==null && other.getREFERENCIA()==null) || 
             (this.REFERENCIA!=null &&
              this.REFERENCIA.equals(other.getREFERENCIA()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              this.CODIGO_BARRAS.equals(other.getCODIGO_BARRAS()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getOPERADORA() != null) {
            _hashCode += getOPERADORA().hashCode();
        }
        if (getTELEFONE() != null) {
            _hashCode += getTELEFONE().hashCode();
        }
        if (getASSINANTE() != null) {
            _hashCode += getASSINANTE().hashCode();
        }
        if (getREFERENCIA() != null) {
            _hashCode += getREFERENCIA().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            _hashCode += getCODIGO_BARRAS().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_recarga_celular_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_recarga_celular_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OPERADORA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OPERADORA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "convenio_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TELEFONE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TELEFONE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "telefone_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ASSINANTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ASSINANTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "pessoa_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REFERENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "REFERENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
