/**
 * Corretora_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Corretora_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Corretora_TypeTIPO_PESSOA TIPO_PESSOA;

    private java.lang.String NOME;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Cpf_cgc_Type DOCUMENTO_INVESTIDOR;

    public Corretora_Type() {
    }

    public Corretora_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Corretora_TypeTIPO_PESSOA TIPO_PESSOA,
           java.lang.String NOME,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Cpf_cgc_Type DOCUMENTO_INVESTIDOR) {
           this.TIPO_PESSOA = TIPO_PESSOA;
           this.NOME = NOME;
           this.DOCUMENTO_INVESTIDOR = DOCUMENTO_INVESTIDOR;
    }


    /**
     * Gets the TIPO_PESSOA value for this Corretora_Type.
     * 
     * @return TIPO_PESSOA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Corretora_TypeTIPO_PESSOA getTIPO_PESSOA() {
        return TIPO_PESSOA;
    }


    /**
     * Sets the TIPO_PESSOA value for this Corretora_Type.
     * 
     * @param TIPO_PESSOA
     */
    public void setTIPO_PESSOA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Corretora_TypeTIPO_PESSOA TIPO_PESSOA) {
        this.TIPO_PESSOA = TIPO_PESSOA;
    }


    /**
     * Gets the NOME value for this Corretora_Type.
     * 
     * @return NOME
     */
    public java.lang.String getNOME() {
        return NOME;
    }


    /**
     * Sets the NOME value for this Corretora_Type.
     * 
     * @param NOME
     */
    public void setNOME(java.lang.String NOME) {
        this.NOME = NOME;
    }


    /**
     * Gets the DOCUMENTO_INVESTIDOR value for this Corretora_Type.
     * 
     * @return DOCUMENTO_INVESTIDOR
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Cpf_cgc_Type getDOCUMENTO_INVESTIDOR() {
        return DOCUMENTO_INVESTIDOR;
    }


    /**
     * Sets the DOCUMENTO_INVESTIDOR value for this Corretora_Type.
     * 
     * @param DOCUMENTO_INVESTIDOR
     */
    public void setDOCUMENTO_INVESTIDOR(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Cpf_cgc_Type DOCUMENTO_INVESTIDOR) {
        this.DOCUMENTO_INVESTIDOR = DOCUMENTO_INVESTIDOR;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Corretora_Type)) return false;
        Corretora_Type other = (Corretora_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.TIPO_PESSOA==null && other.getTIPO_PESSOA()==null) || 
             (this.TIPO_PESSOA!=null &&
              this.TIPO_PESSOA.equals(other.getTIPO_PESSOA()))) &&
            ((this.NOME==null && other.getNOME()==null) || 
             (this.NOME!=null &&
              this.NOME.equals(other.getNOME()))) &&
            ((this.DOCUMENTO_INVESTIDOR==null && other.getDOCUMENTO_INVESTIDOR()==null) || 
             (this.DOCUMENTO_INVESTIDOR!=null &&
              this.DOCUMENTO_INVESTIDOR.equals(other.getDOCUMENTO_INVESTIDOR())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTIPO_PESSOA() != null) {
            _hashCode += getTIPO_PESSOA().hashCode();
        }
        if (getNOME() != null) {
            _hashCode += getNOME().hashCode();
        }
        if (getDOCUMENTO_INVESTIDOR() != null) {
            _hashCode += getDOCUMENTO_INVESTIDOR().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Corretora_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "corretora_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_PESSOA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_PESSOA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">corretora_Type>TIPO_PESSOA"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOCUMENTO_INVESTIDOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DOCUMENTO_INVESTIDOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "cpf_cgc_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
