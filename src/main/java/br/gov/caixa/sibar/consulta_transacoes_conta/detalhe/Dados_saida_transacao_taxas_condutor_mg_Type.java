/**
 * Dados_saida_transacao_taxas_condutor_mg_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_taxas_condutor_mg_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private org.apache.axis.types.UnsignedByte SEGMENTO;

    private org.apache.axis.types.UnsignedInt CONVENIO;

    private org.apache.axis.types.UnsignedInt NSU_DETRAN;

    private org.apache.axis.types.UnsignedLong CPF;

    private org.apache.axis.types.UnsignedLong CNPJ;

    private org.apache.axis.types.UnsignedLong RENAVAM;

    private java.lang.String CODIGO_BARRAS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_detran_mg_Type SERVICO;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_taxas_condutor_mg_Type() {
    }

    public Dados_saida_transacao_taxas_condutor_mg_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           org.apache.axis.types.UnsignedByte SEGMENTO,
           org.apache.axis.types.UnsignedInt CONVENIO,
           org.apache.axis.types.UnsignedInt NSU_DETRAN,
           org.apache.axis.types.UnsignedLong CPF,
           org.apache.axis.types.UnsignedLong CNPJ,
           org.apache.axis.types.UnsignedLong RENAVAM,
           java.lang.String CODIGO_BARRAS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_detran_mg_Type SERVICO,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.SEGMENTO = SEGMENTO;
           this.CONVENIO = CONVENIO;
           this.NSU_DETRAN = NSU_DETRAN;
           this.CPF = CPF;
           this.CNPJ = CNPJ;
           this.RENAVAM = RENAVAM;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.SERVICO = SERVICO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the SEGMENTO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return SEGMENTO
     */
    public org.apache.axis.types.UnsignedByte getSEGMENTO() {
        return SEGMENTO;
    }


    /**
     * Sets the SEGMENTO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param SEGMENTO
     */
    public void setSEGMENTO(org.apache.axis.types.UnsignedByte SEGMENTO) {
        this.SEGMENTO = SEGMENTO;
    }


    /**
     * Gets the CONVENIO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return CONVENIO
     */
    public org.apache.axis.types.UnsignedInt getCONVENIO() {
        return CONVENIO;
    }


    /**
     * Sets the CONVENIO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param CONVENIO
     */
    public void setCONVENIO(org.apache.axis.types.UnsignedInt CONVENIO) {
        this.CONVENIO = CONVENIO;
    }


    /**
     * Gets the NSU_DETRAN value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return NSU_DETRAN
     */
    public org.apache.axis.types.UnsignedInt getNSU_DETRAN() {
        return NSU_DETRAN;
    }


    /**
     * Sets the NSU_DETRAN value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param NSU_DETRAN
     */
    public void setNSU_DETRAN(org.apache.axis.types.UnsignedInt NSU_DETRAN) {
        this.NSU_DETRAN = NSU_DETRAN;
    }


    /**
     * Gets the CPF value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return CPF
     */
    public org.apache.axis.types.UnsignedLong getCPF() {
        return CPF;
    }


    /**
     * Sets the CPF value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param CPF
     */
    public void setCPF(org.apache.axis.types.UnsignedLong CPF) {
        this.CPF = CPF;
    }


    /**
     * Gets the CNPJ value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return CNPJ
     */
    public org.apache.axis.types.UnsignedLong getCNPJ() {
        return CNPJ;
    }


    /**
     * Sets the CNPJ value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param CNPJ
     */
    public void setCNPJ(org.apache.axis.types.UnsignedLong CNPJ) {
        this.CNPJ = CNPJ;
    }


    /**
     * Gets the RENAVAM value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return RENAVAM
     */
    public org.apache.axis.types.UnsignedLong getRENAVAM() {
        return RENAVAM;
    }


    /**
     * Sets the RENAVAM value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param RENAVAM
     */
    public void setRENAVAM(org.apache.axis.types.UnsignedLong RENAVAM) {
        this.RENAVAM = RENAVAM;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }


    /**
     * Gets the SERVICO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return SERVICO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_detran_mg_Type getSERVICO() {
        return SERVICO;
    }


    /**
     * Sets the SERVICO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param SERVICO
     */
    public void setSERVICO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_detran_mg_Type SERVICO) {
        this.SERVICO = SERVICO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_taxas_condutor_mg_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_taxas_condutor_mg_Type)) return false;
        Dados_saida_transacao_taxas_condutor_mg_Type other = (Dados_saida_transacao_taxas_condutor_mg_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.SEGMENTO==null && other.getSEGMENTO()==null) || 
             (this.SEGMENTO!=null &&
              this.SEGMENTO.equals(other.getSEGMENTO()))) &&
            ((this.CONVENIO==null && other.getCONVENIO()==null) || 
             (this.CONVENIO!=null &&
              this.CONVENIO.equals(other.getCONVENIO()))) &&
            ((this.NSU_DETRAN==null && other.getNSU_DETRAN()==null) || 
             (this.NSU_DETRAN!=null &&
              this.NSU_DETRAN.equals(other.getNSU_DETRAN()))) &&
            ((this.CPF==null && other.getCPF()==null) || 
             (this.CPF!=null &&
              this.CPF.equals(other.getCPF()))) &&
            ((this.CNPJ==null && other.getCNPJ()==null) || 
             (this.CNPJ!=null &&
              this.CNPJ.equals(other.getCNPJ()))) &&
            ((this.RENAVAM==null && other.getRENAVAM()==null) || 
             (this.RENAVAM!=null &&
              this.RENAVAM.equals(other.getRENAVAM()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              this.CODIGO_BARRAS.equals(other.getCODIGO_BARRAS()))) &&
            ((this.SERVICO==null && other.getSERVICO()==null) || 
             (this.SERVICO!=null &&
              this.SERVICO.equals(other.getSERVICO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getSEGMENTO() != null) {
            _hashCode += getSEGMENTO().hashCode();
        }
        if (getCONVENIO() != null) {
            _hashCode += getCONVENIO().hashCode();
        }
        if (getNSU_DETRAN() != null) {
            _hashCode += getNSU_DETRAN().hashCode();
        }
        if (getCPF() != null) {
            _hashCode += getCPF().hashCode();
        }
        if (getCNPJ() != null) {
            _hashCode += getCNPJ().hashCode();
        }
        if (getRENAVAM() != null) {
            _hashCode += getRENAVAM().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            _hashCode += getCODIGO_BARRAS().hashCode();
        }
        if (getSERVICO() != null) {
            _hashCode += getSERVICO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_taxas_condutor_mg_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_taxas_condutor_mg_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONVENIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONVENIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU_DETRAN");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU_DETRAN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RENAVAM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RENAVAM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SERVICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SERVICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "servico_detran_mg_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
