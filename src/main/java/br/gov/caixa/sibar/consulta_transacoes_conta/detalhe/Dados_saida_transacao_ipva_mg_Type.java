/**
 * Dados_saida_transacao_ipva_mg_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_ipva_mg_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private org.apache.axis.types.UnsignedByte SEGMENTO;

    private org.apache.axis.types.UnsignedInt CONVENIO;

    private org.apache.axis.types.UnsignedLong NSU_DETRAN;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Veiculo_Type VEICULO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Municipio_Type MUNICIPIO;

    private org.apache.axis.types.UnsignedShort EXERCICIO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ipva_mg_TypeTIPO TIPO;

    private java.math.BigDecimal VALOR_IPVA;

    private java.util.Date DATA_VENCIMENTO;

    private java.lang.String CODIGO_BARRAS;

    private java.lang.String IDENTIFICACAO;

    public Dados_saida_transacao_ipva_mg_Type() {
    }

    public Dados_saida_transacao_ipva_mg_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           org.apache.axis.types.UnsignedByte SEGMENTO,
           org.apache.axis.types.UnsignedInt CONVENIO,
           org.apache.axis.types.UnsignedLong NSU_DETRAN,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Veiculo_Type VEICULO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Municipio_Type MUNICIPIO,
           org.apache.axis.types.UnsignedShort EXERCICIO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ipva_mg_TypeTIPO TIPO,
           java.math.BigDecimal VALOR_IPVA,
           java.util.Date DATA_VENCIMENTO,
           java.lang.String CODIGO_BARRAS,
           java.lang.String IDENTIFICACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.SEGMENTO = SEGMENTO;
           this.CONVENIO = CONVENIO;
           this.NSU_DETRAN = NSU_DETRAN;
           this.VEICULO = VEICULO;
           this.MUNICIPIO = MUNICIPIO;
           this.EXERCICIO = EXERCICIO;
           this.TIPO = TIPO;
           this.VALOR_IPVA = VALOR_IPVA;
           this.DATA_VENCIMENTO = DATA_VENCIMENTO;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the SEGMENTO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return SEGMENTO
     */
    public org.apache.axis.types.UnsignedByte getSEGMENTO() {
        return SEGMENTO;
    }


    /**
     * Sets the SEGMENTO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param SEGMENTO
     */
    public void setSEGMENTO(org.apache.axis.types.UnsignedByte SEGMENTO) {
        this.SEGMENTO = SEGMENTO;
    }


    /**
     * Gets the CONVENIO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return CONVENIO
     */
    public org.apache.axis.types.UnsignedInt getCONVENIO() {
        return CONVENIO;
    }


    /**
     * Sets the CONVENIO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param CONVENIO
     */
    public void setCONVENIO(org.apache.axis.types.UnsignedInt CONVENIO) {
        this.CONVENIO = CONVENIO;
    }


    /**
     * Gets the NSU_DETRAN value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return NSU_DETRAN
     */
    public org.apache.axis.types.UnsignedLong getNSU_DETRAN() {
        return NSU_DETRAN;
    }


    /**
     * Sets the NSU_DETRAN value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param NSU_DETRAN
     */
    public void setNSU_DETRAN(org.apache.axis.types.UnsignedLong NSU_DETRAN) {
        this.NSU_DETRAN = NSU_DETRAN;
    }


    /**
     * Gets the VEICULO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return VEICULO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Veiculo_Type getVEICULO() {
        return VEICULO;
    }


    /**
     * Sets the VEICULO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param VEICULO
     */
    public void setVEICULO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Veiculo_Type VEICULO) {
        this.VEICULO = VEICULO;
    }


    /**
     * Gets the MUNICIPIO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return MUNICIPIO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Municipio_Type getMUNICIPIO() {
        return MUNICIPIO;
    }


    /**
     * Sets the MUNICIPIO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param MUNICIPIO
     */
    public void setMUNICIPIO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Municipio_Type MUNICIPIO) {
        this.MUNICIPIO = MUNICIPIO;
    }


    /**
     * Gets the EXERCICIO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return EXERCICIO
     */
    public org.apache.axis.types.UnsignedShort getEXERCICIO() {
        return EXERCICIO;
    }


    /**
     * Sets the EXERCICIO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param EXERCICIO
     */
    public void setEXERCICIO(org.apache.axis.types.UnsignedShort EXERCICIO) {
        this.EXERCICIO = EXERCICIO;
    }


    /**
     * Gets the TIPO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return TIPO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ipva_mg_TypeTIPO getTIPO() {
        return TIPO;
    }


    /**
     * Sets the TIPO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param TIPO
     */
    public void setTIPO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ipva_mg_TypeTIPO TIPO) {
        this.TIPO = TIPO;
    }


    /**
     * Gets the VALOR_IPVA value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return VALOR_IPVA
     */
    public java.math.BigDecimal getVALOR_IPVA() {
        return VALOR_IPVA;
    }


    /**
     * Sets the VALOR_IPVA value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param VALOR_IPVA
     */
    public void setVALOR_IPVA(java.math.BigDecimal VALOR_IPVA) {
        this.VALOR_IPVA = VALOR_IPVA;
    }


    /**
     * Gets the DATA_VENCIMENTO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return DATA_VENCIMENTO
     */
    public java.util.Date getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }


    /**
     * Sets the DATA_VENCIMENTO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param DATA_VENCIMENTO
     */
    public void setDATA_VENCIMENTO(java.util.Date DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_ipva_mg_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_ipva_mg_Type)) return false;
        Dados_saida_transacao_ipva_mg_Type other = (Dados_saida_transacao_ipva_mg_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.SEGMENTO==null && other.getSEGMENTO()==null) || 
             (this.SEGMENTO!=null &&
              this.SEGMENTO.equals(other.getSEGMENTO()))) &&
            ((this.CONVENIO==null && other.getCONVENIO()==null) || 
             (this.CONVENIO!=null &&
              this.CONVENIO.equals(other.getCONVENIO()))) &&
            ((this.NSU_DETRAN==null && other.getNSU_DETRAN()==null) || 
             (this.NSU_DETRAN!=null &&
              this.NSU_DETRAN.equals(other.getNSU_DETRAN()))) &&
            ((this.VEICULO==null && other.getVEICULO()==null) || 
             (this.VEICULO!=null &&
              this.VEICULO.equals(other.getVEICULO()))) &&
            ((this.MUNICIPIO==null && other.getMUNICIPIO()==null) || 
             (this.MUNICIPIO!=null &&
              this.MUNICIPIO.equals(other.getMUNICIPIO()))) &&
            ((this.EXERCICIO==null && other.getEXERCICIO()==null) || 
             (this.EXERCICIO!=null &&
              this.EXERCICIO.equals(other.getEXERCICIO()))) &&
            ((this.TIPO==null && other.getTIPO()==null) || 
             (this.TIPO!=null &&
              this.TIPO.equals(other.getTIPO()))) &&
            ((this.VALOR_IPVA==null && other.getVALOR_IPVA()==null) || 
             (this.VALOR_IPVA!=null &&
              this.VALOR_IPVA.equals(other.getVALOR_IPVA()))) &&
            ((this.DATA_VENCIMENTO==null && other.getDATA_VENCIMENTO()==null) || 
             (this.DATA_VENCIMENTO!=null &&
              this.DATA_VENCIMENTO.equals(other.getDATA_VENCIMENTO()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              this.CODIGO_BARRAS.equals(other.getCODIGO_BARRAS()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getSEGMENTO() != null) {
            _hashCode += getSEGMENTO().hashCode();
        }
        if (getCONVENIO() != null) {
            _hashCode += getCONVENIO().hashCode();
        }
        if (getNSU_DETRAN() != null) {
            _hashCode += getNSU_DETRAN().hashCode();
        }
        if (getVEICULO() != null) {
            _hashCode += getVEICULO().hashCode();
        }
        if (getMUNICIPIO() != null) {
            _hashCode += getMUNICIPIO().hashCode();
        }
        if (getEXERCICIO() != null) {
            _hashCode += getEXERCICIO().hashCode();
        }
        if (getTIPO() != null) {
            _hashCode += getTIPO().hashCode();
        }
        if (getVALOR_IPVA() != null) {
            _hashCode += getVALOR_IPVA().hashCode();
        }
        if (getDATA_VENCIMENTO() != null) {
            _hashCode += getDATA_VENCIMENTO().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            _hashCode += getCODIGO_BARRAS().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_ipva_mg_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_ipva_mg_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONVENIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONVENIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU_DETRAN");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU_DETRAN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VEICULO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VEICULO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "veiculo_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MUNICIPIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MUNICIPIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "municipio_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EXERCICIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EXERCICIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_ipva_mg_Type>TIPO"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_IPVA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_IPVA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
