/**
 * Dados_entrada_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Dados_entrada_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type CONTA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Periodo_Type PERIODO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_entrada_solicitacoes_TypeSOLICITACAO[] SOLICITACOES;

    public Dados_entrada_Type() {
    }

    public Dados_entrada_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type CONTA,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Periodo_Type PERIODO,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_entrada_solicitacoes_TypeSOLICITACAO[] SOLICITACOES) {
           this.CONTA = CONTA;
           this.PERIODO = PERIODO;
           this.SOLICITACOES = SOLICITACOES;
    }


    /**
     * Gets the CONTA value for this Dados_entrada_Type.
     * 
     * @return CONTA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type getCONTA() {
        return CONTA;
    }


    /**
     * Sets the CONTA value for this Dados_entrada_Type.
     * 
     * @param CONTA
     */
    public void setCONTA(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type CONTA) {
        this.CONTA = CONTA;
    }


    /**
     * Gets the PERIODO value for this Dados_entrada_Type.
     * 
     * @return PERIODO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Periodo_Type getPERIODO() {
        return PERIODO;
    }


    /**
     * Sets the PERIODO value for this Dados_entrada_Type.
     * 
     * @param PERIODO
     */
    public void setPERIODO(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Periodo_Type PERIODO) {
        this.PERIODO = PERIODO;
    }


    /**
     * Gets the SOLICITACOES value for this Dados_entrada_Type.
     * 
     * @return SOLICITACOES
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_entrada_solicitacoes_TypeSOLICITACAO[] getSOLICITACOES() {
        return SOLICITACOES;
    }


    /**
     * Sets the SOLICITACOES value for this Dados_entrada_Type.
     * 
     * @param SOLICITACOES
     */
    public void setSOLICITACOES(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_entrada_solicitacoes_TypeSOLICITACAO[] SOLICITACOES) {
        this.SOLICITACOES = SOLICITACOES;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_entrada_Type)) return false;
        Dados_entrada_Type other = (Dados_entrada_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CONTA==null && other.getCONTA()==null) || 
             (this.CONTA!=null &&
              this.CONTA.equals(other.getCONTA()))) &&
            ((this.PERIODO==null && other.getPERIODO()==null) || 
             (this.PERIODO!=null &&
              this.PERIODO.equals(other.getPERIODO()))) &&
            ((this.SOLICITACOES==null && other.getSOLICITACOES()==null) || 
             (this.SOLICITACOES!=null &&
              java.util.Arrays.equals(this.SOLICITACOES, other.getSOLICITACOES())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCONTA() != null) {
            _hashCode += getCONTA().hashCode();
        }
        if (getPERIODO() != null) {
            _hashCode += getPERIODO().hashCode();
        }
        if (getSOLICITACOES() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSOLICITACOES());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSOLICITACOES(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_entrada_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_entrada_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "conta_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PERIODO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PERIODO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "periodo_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SOLICITACOES");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SOLICITACOES"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", ">dados_entrada_solicitacoes_Type>SOLICITACAO"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "SOLICITACAO"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
