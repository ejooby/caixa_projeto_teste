/**
 * Dados_saida_transacao_taxas_detran_sp_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_taxas_detran_sp_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private org.apache.axis.types.UnsignedByte SEGMENTO;

    private org.apache.axis.types.UnsignedInt CONVENIO;

    private org.apache.axis.types.UnsignedInt NSU_DETRAN;

    private org.apache.axis.types.UnsignedByte CODIGO_SERVICO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Sub_servico_Type SUB_SERVICO;

    private java.lang.String NOME_CLIENTE;

    private java.lang.String AUTENTICACAO_DIGITAL;

    private org.apache.axis.types.UnsignedLong CPF;

    private org.apache.axis.types.UnsignedLong CNPJ;

    private org.apache.axis.types.UnsignedLong RENAVAM;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_taxas_detran_sp_TypeENTREGA ENTREGA;

    private java.math.BigDecimal VALOR;

    private java.math.BigDecimal TAXA_CORREIO;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_taxas_detran_sp_Type() {
    }

    public Dados_saida_transacao_taxas_detran_sp_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           org.apache.axis.types.UnsignedByte SEGMENTO,
           org.apache.axis.types.UnsignedInt CONVENIO,
           org.apache.axis.types.UnsignedInt NSU_DETRAN,
           org.apache.axis.types.UnsignedByte CODIGO_SERVICO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Sub_servico_Type SUB_SERVICO,
           java.lang.String NOME_CLIENTE,
           java.lang.String AUTENTICACAO_DIGITAL,
           org.apache.axis.types.UnsignedLong CPF,
           org.apache.axis.types.UnsignedLong CNPJ,
           org.apache.axis.types.UnsignedLong RENAVAM,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_taxas_detran_sp_TypeENTREGA ENTREGA,
           java.math.BigDecimal VALOR,
           java.math.BigDecimal TAXA_CORREIO,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.SEGMENTO = SEGMENTO;
           this.CONVENIO = CONVENIO;
           this.NSU_DETRAN = NSU_DETRAN;
           this.CODIGO_SERVICO = CODIGO_SERVICO;
           this.SUB_SERVICO = SUB_SERVICO;
           this.NOME_CLIENTE = NOME_CLIENTE;
           this.AUTENTICACAO_DIGITAL = AUTENTICACAO_DIGITAL;
           this.CPF = CPF;
           this.CNPJ = CNPJ;
           this.RENAVAM = RENAVAM;
           this.ENTREGA = ENTREGA;
           this.VALOR = VALOR;
           this.TAXA_CORREIO = TAXA_CORREIO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the SEGMENTO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return SEGMENTO
     */
    public org.apache.axis.types.UnsignedByte getSEGMENTO() {
        return SEGMENTO;
    }


    /**
     * Sets the SEGMENTO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param SEGMENTO
     */
    public void setSEGMENTO(org.apache.axis.types.UnsignedByte SEGMENTO) {
        this.SEGMENTO = SEGMENTO;
    }


    /**
     * Gets the CONVENIO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return CONVENIO
     */
    public org.apache.axis.types.UnsignedInt getCONVENIO() {
        return CONVENIO;
    }


    /**
     * Sets the CONVENIO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param CONVENIO
     */
    public void setCONVENIO(org.apache.axis.types.UnsignedInt CONVENIO) {
        this.CONVENIO = CONVENIO;
    }


    /**
     * Gets the NSU_DETRAN value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return NSU_DETRAN
     */
    public org.apache.axis.types.UnsignedInt getNSU_DETRAN() {
        return NSU_DETRAN;
    }


    /**
     * Sets the NSU_DETRAN value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param NSU_DETRAN
     */
    public void setNSU_DETRAN(org.apache.axis.types.UnsignedInt NSU_DETRAN) {
        this.NSU_DETRAN = NSU_DETRAN;
    }


    /**
     * Gets the CODIGO_SERVICO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return CODIGO_SERVICO
     */
    public org.apache.axis.types.UnsignedByte getCODIGO_SERVICO() {
        return CODIGO_SERVICO;
    }


    /**
     * Sets the CODIGO_SERVICO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param CODIGO_SERVICO
     */
    public void setCODIGO_SERVICO(org.apache.axis.types.UnsignedByte CODIGO_SERVICO) {
        this.CODIGO_SERVICO = CODIGO_SERVICO;
    }


    /**
     * Gets the SUB_SERVICO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return SUB_SERVICO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Sub_servico_Type getSUB_SERVICO() {
        return SUB_SERVICO;
    }


    /**
     * Sets the SUB_SERVICO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param SUB_SERVICO
     */
    public void setSUB_SERVICO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Sub_servico_Type SUB_SERVICO) {
        this.SUB_SERVICO = SUB_SERVICO;
    }


    /**
     * Gets the NOME_CLIENTE value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return NOME_CLIENTE
     */
    public java.lang.String getNOME_CLIENTE() {
        return NOME_CLIENTE;
    }


    /**
     * Sets the NOME_CLIENTE value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param NOME_CLIENTE
     */
    public void setNOME_CLIENTE(java.lang.String NOME_CLIENTE) {
        this.NOME_CLIENTE = NOME_CLIENTE;
    }


    /**
     * Gets the AUTENTICACAO_DIGITAL value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return AUTENTICACAO_DIGITAL
     */
    public java.lang.String getAUTENTICACAO_DIGITAL() {
        return AUTENTICACAO_DIGITAL;
    }


    /**
     * Sets the AUTENTICACAO_DIGITAL value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param AUTENTICACAO_DIGITAL
     */
    public void setAUTENTICACAO_DIGITAL(java.lang.String AUTENTICACAO_DIGITAL) {
        this.AUTENTICACAO_DIGITAL = AUTENTICACAO_DIGITAL;
    }


    /**
     * Gets the CPF value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return CPF
     */
    public org.apache.axis.types.UnsignedLong getCPF() {
        return CPF;
    }


    /**
     * Sets the CPF value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param CPF
     */
    public void setCPF(org.apache.axis.types.UnsignedLong CPF) {
        this.CPF = CPF;
    }


    /**
     * Gets the CNPJ value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return CNPJ
     */
    public org.apache.axis.types.UnsignedLong getCNPJ() {
        return CNPJ;
    }


    /**
     * Sets the CNPJ value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param CNPJ
     */
    public void setCNPJ(org.apache.axis.types.UnsignedLong CNPJ) {
        this.CNPJ = CNPJ;
    }


    /**
     * Gets the RENAVAM value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return RENAVAM
     */
    public org.apache.axis.types.UnsignedLong getRENAVAM() {
        return RENAVAM;
    }


    /**
     * Sets the RENAVAM value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param RENAVAM
     */
    public void setRENAVAM(org.apache.axis.types.UnsignedLong RENAVAM) {
        this.RENAVAM = RENAVAM;
    }


    /**
     * Gets the ENTREGA value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return ENTREGA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_taxas_detran_sp_TypeENTREGA getENTREGA() {
        return ENTREGA;
    }


    /**
     * Sets the ENTREGA value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param ENTREGA
     */
    public void setENTREGA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_taxas_detran_sp_TypeENTREGA ENTREGA) {
        this.ENTREGA = ENTREGA;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the TAXA_CORREIO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return TAXA_CORREIO
     */
    public java.math.BigDecimal getTAXA_CORREIO() {
        return TAXA_CORREIO;
    }


    /**
     * Sets the TAXA_CORREIO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param TAXA_CORREIO
     */
    public void setTAXA_CORREIO(java.math.BigDecimal TAXA_CORREIO) {
        this.TAXA_CORREIO = TAXA_CORREIO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_taxas_detran_sp_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_taxas_detran_sp_Type)) return false;
        Dados_saida_transacao_taxas_detran_sp_Type other = (Dados_saida_transacao_taxas_detran_sp_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.SEGMENTO==null && other.getSEGMENTO()==null) || 
             (this.SEGMENTO!=null &&
              this.SEGMENTO.equals(other.getSEGMENTO()))) &&
            ((this.CONVENIO==null && other.getCONVENIO()==null) || 
             (this.CONVENIO!=null &&
              this.CONVENIO.equals(other.getCONVENIO()))) &&
            ((this.NSU_DETRAN==null && other.getNSU_DETRAN()==null) || 
             (this.NSU_DETRAN!=null &&
              this.NSU_DETRAN.equals(other.getNSU_DETRAN()))) &&
            ((this.CODIGO_SERVICO==null && other.getCODIGO_SERVICO()==null) || 
             (this.CODIGO_SERVICO!=null &&
              this.CODIGO_SERVICO.equals(other.getCODIGO_SERVICO()))) &&
            ((this.SUB_SERVICO==null && other.getSUB_SERVICO()==null) || 
             (this.SUB_SERVICO!=null &&
              this.SUB_SERVICO.equals(other.getSUB_SERVICO()))) &&
            ((this.NOME_CLIENTE==null && other.getNOME_CLIENTE()==null) || 
             (this.NOME_CLIENTE!=null &&
              this.NOME_CLIENTE.equals(other.getNOME_CLIENTE()))) &&
            ((this.AUTENTICACAO_DIGITAL==null && other.getAUTENTICACAO_DIGITAL()==null) || 
             (this.AUTENTICACAO_DIGITAL!=null &&
              this.AUTENTICACAO_DIGITAL.equals(other.getAUTENTICACAO_DIGITAL()))) &&
            ((this.CPF==null && other.getCPF()==null) || 
             (this.CPF!=null &&
              this.CPF.equals(other.getCPF()))) &&
            ((this.CNPJ==null && other.getCNPJ()==null) || 
             (this.CNPJ!=null &&
              this.CNPJ.equals(other.getCNPJ()))) &&
            ((this.RENAVAM==null && other.getRENAVAM()==null) || 
             (this.RENAVAM!=null &&
              this.RENAVAM.equals(other.getRENAVAM()))) &&
            ((this.ENTREGA==null && other.getENTREGA()==null) || 
             (this.ENTREGA!=null &&
              this.ENTREGA.equals(other.getENTREGA()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.TAXA_CORREIO==null && other.getTAXA_CORREIO()==null) || 
             (this.TAXA_CORREIO!=null &&
              this.TAXA_CORREIO.equals(other.getTAXA_CORREIO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getSEGMENTO() != null) {
            _hashCode += getSEGMENTO().hashCode();
        }
        if (getCONVENIO() != null) {
            _hashCode += getCONVENIO().hashCode();
        }
        if (getNSU_DETRAN() != null) {
            _hashCode += getNSU_DETRAN().hashCode();
        }
        if (getCODIGO_SERVICO() != null) {
            _hashCode += getCODIGO_SERVICO().hashCode();
        }
        if (getSUB_SERVICO() != null) {
            _hashCode += getSUB_SERVICO().hashCode();
        }
        if (getNOME_CLIENTE() != null) {
            _hashCode += getNOME_CLIENTE().hashCode();
        }
        if (getAUTENTICACAO_DIGITAL() != null) {
            _hashCode += getAUTENTICACAO_DIGITAL().hashCode();
        }
        if (getCPF() != null) {
            _hashCode += getCPF().hashCode();
        }
        if (getCNPJ() != null) {
            _hashCode += getCNPJ().hashCode();
        }
        if (getRENAVAM() != null) {
            _hashCode += getRENAVAM().hashCode();
        }
        if (getENTREGA() != null) {
            _hashCode += getENTREGA().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getTAXA_CORREIO() != null) {
            _hashCode += getTAXA_CORREIO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_taxas_detran_sp_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_taxas_detran_sp_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONVENIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONVENIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU_DETRAN");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU_DETRAN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_SERVICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_SERVICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUB_SERVICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SUB_SERVICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "sub_servico_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_CLIENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_CLIENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AUTENTICACAO_DIGITAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AUTENTICACAO_DIGITAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RENAVAM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RENAVAM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ENTREGA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ENTREGA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_taxas_detran_sp_Type>ENTREGA"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TAXA_CORREIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TAXA_CORREIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
