/**
 * Dados_saida_transacao_gare_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_gare_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private java.lang.String UF;

    /* GARE - Campo3 */
    private org.apache.axis.types.UnsignedShort CODIGO_RECEITA;

    /* GARE - Campo1 */
    private java.lang.String COTA_IPVA;

    /* GARE - Campo2 */
    private java.util.Date VENCIMENTO;

    /* GARE -Campo4 */
    private org.apache.axis.types.UnsignedLong INSCRICAO_ESTADUAL;

    /* GARE - Campo5 */
    private org.apache.axis.types.UnsignedLong CNPJ_CPF_RENAVAM_PLACA;

    /* GARE - Campo6 */
    private org.apache.axis.types.UnsignedLong INSCRICAO_DIVIDA;

    /* GARE -Campo7 */
    private org.apache.axis.types.UnsignedInt REFERENCIA;

    /* GARE - Campo8 */
    private org.apache.axis.types.UnsignedLong AIMM_NOTIFICACAO_GUIA;

    /* GARE - Campo9 */
    private java.math.BigDecimal VALOR_RECEITA;

    /* GARE - Campo10 */
    private java.math.BigDecimal VALOR_JUROS;

    /* GARE - Campo11 */
    private java.math.BigDecimal VALOR_MULTA;

    /* GARE - Campo12 */
    private java.math.BigDecimal VALOR_ACRESCIMO;

    /* GARE - Campo13 */
    private java.math.BigDecimal VALOR_HONORARIO;

    private java.lang.String AUTENTICACAO_DIGITAL;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_gare_TypeTIPO TIPO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_gare_Type() {
    }

    public Dados_saida_transacao_gare_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           java.lang.String UF,
           org.apache.axis.types.UnsignedShort CODIGO_RECEITA,
           java.lang.String COTA_IPVA,
           java.util.Date VENCIMENTO,
           org.apache.axis.types.UnsignedLong INSCRICAO_ESTADUAL,
           org.apache.axis.types.UnsignedLong CNPJ_CPF_RENAVAM_PLACA,
           org.apache.axis.types.UnsignedLong INSCRICAO_DIVIDA,
           org.apache.axis.types.UnsignedInt REFERENCIA,
           org.apache.axis.types.UnsignedLong AIMM_NOTIFICACAO_GUIA,
           java.math.BigDecimal VALOR_RECEITA,
           java.math.BigDecimal VALOR_JUROS,
           java.math.BigDecimal VALOR_MULTA,
           java.math.BigDecimal VALOR_ACRESCIMO,
           java.math.BigDecimal VALOR_HONORARIO,
           java.lang.String AUTENTICACAO_DIGITAL,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_gare_TypeTIPO TIPO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.UF = UF;
           this.CODIGO_RECEITA = CODIGO_RECEITA;
           this.COTA_IPVA = COTA_IPVA;
           this.VENCIMENTO = VENCIMENTO;
           this.INSCRICAO_ESTADUAL = INSCRICAO_ESTADUAL;
           this.CNPJ_CPF_RENAVAM_PLACA = CNPJ_CPF_RENAVAM_PLACA;
           this.INSCRICAO_DIVIDA = INSCRICAO_DIVIDA;
           this.REFERENCIA = REFERENCIA;
           this.AIMM_NOTIFICACAO_GUIA = AIMM_NOTIFICACAO_GUIA;
           this.VALOR_RECEITA = VALOR_RECEITA;
           this.VALOR_JUROS = VALOR_JUROS;
           this.VALOR_MULTA = VALOR_MULTA;
           this.VALOR_ACRESCIMO = VALOR_ACRESCIMO;
           this.VALOR_HONORARIO = VALOR_HONORARIO;
           this.AUTENTICACAO_DIGITAL = AUTENTICACAO_DIGITAL;
           this.TIPO = TIPO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_gare_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_gare_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the UF value for this Dados_saida_transacao_gare_Type.
     * 
     * @return UF
     */
    public java.lang.String getUF() {
        return UF;
    }


    /**
     * Sets the UF value for this Dados_saida_transacao_gare_Type.
     * 
     * @param UF
     */
    public void setUF(java.lang.String UF) {
        this.UF = UF;
    }


    /**
     * Gets the CODIGO_RECEITA value for this Dados_saida_transacao_gare_Type.
     * 
     * @return CODIGO_RECEITA   * GARE - Campo3
     */
    public org.apache.axis.types.UnsignedShort getCODIGO_RECEITA() {
        return CODIGO_RECEITA;
    }


    /**
     * Sets the CODIGO_RECEITA value for this Dados_saida_transacao_gare_Type.
     * 
     * @param CODIGO_RECEITA   * GARE - Campo3
     */
    public void setCODIGO_RECEITA(org.apache.axis.types.UnsignedShort CODIGO_RECEITA) {
        this.CODIGO_RECEITA = CODIGO_RECEITA;
    }


    /**
     * Gets the COTA_IPVA value for this Dados_saida_transacao_gare_Type.
     * 
     * @return COTA_IPVA   * GARE - Campo1
     */
    public java.lang.String getCOTA_IPVA() {
        return COTA_IPVA;
    }


    /**
     * Sets the COTA_IPVA value for this Dados_saida_transacao_gare_Type.
     * 
     * @param COTA_IPVA   * GARE - Campo1
     */
    public void setCOTA_IPVA(java.lang.String COTA_IPVA) {
        this.COTA_IPVA = COTA_IPVA;
    }


    /**
     * Gets the VENCIMENTO value for this Dados_saida_transacao_gare_Type.
     * 
     * @return VENCIMENTO   * GARE - Campo2
     */
    public java.util.Date getVENCIMENTO() {
        return VENCIMENTO;
    }


    /**
     * Sets the VENCIMENTO value for this Dados_saida_transacao_gare_Type.
     * 
     * @param VENCIMENTO   * GARE - Campo2
     */
    public void setVENCIMENTO(java.util.Date VENCIMENTO) {
        this.VENCIMENTO = VENCIMENTO;
    }


    /**
     * Gets the INSCRICAO_ESTADUAL value for this Dados_saida_transacao_gare_Type.
     * 
     * @return INSCRICAO_ESTADUAL   * GARE -Campo4
     */
    public org.apache.axis.types.UnsignedLong getINSCRICAO_ESTADUAL() {
        return INSCRICAO_ESTADUAL;
    }


    /**
     * Sets the INSCRICAO_ESTADUAL value for this Dados_saida_transacao_gare_Type.
     * 
     * @param INSCRICAO_ESTADUAL   * GARE -Campo4
     */
    public void setINSCRICAO_ESTADUAL(org.apache.axis.types.UnsignedLong INSCRICAO_ESTADUAL) {
        this.INSCRICAO_ESTADUAL = INSCRICAO_ESTADUAL;
    }


    /**
     * Gets the CNPJ_CPF_RENAVAM_PLACA value for this Dados_saida_transacao_gare_Type.
     * 
     * @return CNPJ_CPF_RENAVAM_PLACA   * GARE - Campo5
     */
    public org.apache.axis.types.UnsignedLong getCNPJ_CPF_RENAVAM_PLACA() {
        return CNPJ_CPF_RENAVAM_PLACA;
    }


    /**
     * Sets the CNPJ_CPF_RENAVAM_PLACA value for this Dados_saida_transacao_gare_Type.
     * 
     * @param CNPJ_CPF_RENAVAM_PLACA   * GARE - Campo5
     */
    public void setCNPJ_CPF_RENAVAM_PLACA(org.apache.axis.types.UnsignedLong CNPJ_CPF_RENAVAM_PLACA) {
        this.CNPJ_CPF_RENAVAM_PLACA = CNPJ_CPF_RENAVAM_PLACA;
    }


    /**
     * Gets the INSCRICAO_DIVIDA value for this Dados_saida_transacao_gare_Type.
     * 
     * @return INSCRICAO_DIVIDA   * GARE - Campo6
     */
    public org.apache.axis.types.UnsignedLong getINSCRICAO_DIVIDA() {
        return INSCRICAO_DIVIDA;
    }


    /**
     * Sets the INSCRICAO_DIVIDA value for this Dados_saida_transacao_gare_Type.
     * 
     * @param INSCRICAO_DIVIDA   * GARE - Campo6
     */
    public void setINSCRICAO_DIVIDA(org.apache.axis.types.UnsignedLong INSCRICAO_DIVIDA) {
        this.INSCRICAO_DIVIDA = INSCRICAO_DIVIDA;
    }


    /**
     * Gets the REFERENCIA value for this Dados_saida_transacao_gare_Type.
     * 
     * @return REFERENCIA   * GARE -Campo7
     */
    public org.apache.axis.types.UnsignedInt getREFERENCIA() {
        return REFERENCIA;
    }


    /**
     * Sets the REFERENCIA value for this Dados_saida_transacao_gare_Type.
     * 
     * @param REFERENCIA   * GARE -Campo7
     */
    public void setREFERENCIA(org.apache.axis.types.UnsignedInt REFERENCIA) {
        this.REFERENCIA = REFERENCIA;
    }


    /**
     * Gets the AIMM_NOTIFICACAO_GUIA value for this Dados_saida_transacao_gare_Type.
     * 
     * @return AIMM_NOTIFICACAO_GUIA   * GARE - Campo8
     */
    public org.apache.axis.types.UnsignedLong getAIMM_NOTIFICACAO_GUIA() {
        return AIMM_NOTIFICACAO_GUIA;
    }


    /**
     * Sets the AIMM_NOTIFICACAO_GUIA value for this Dados_saida_transacao_gare_Type.
     * 
     * @param AIMM_NOTIFICACAO_GUIA   * GARE - Campo8
     */
    public void setAIMM_NOTIFICACAO_GUIA(org.apache.axis.types.UnsignedLong AIMM_NOTIFICACAO_GUIA) {
        this.AIMM_NOTIFICACAO_GUIA = AIMM_NOTIFICACAO_GUIA;
    }


    /**
     * Gets the VALOR_RECEITA value for this Dados_saida_transacao_gare_Type.
     * 
     * @return VALOR_RECEITA   * GARE - Campo9
     */
    public java.math.BigDecimal getVALOR_RECEITA() {
        return VALOR_RECEITA;
    }


    /**
     * Sets the VALOR_RECEITA value for this Dados_saida_transacao_gare_Type.
     * 
     * @param VALOR_RECEITA   * GARE - Campo9
     */
    public void setVALOR_RECEITA(java.math.BigDecimal VALOR_RECEITA) {
        this.VALOR_RECEITA = VALOR_RECEITA;
    }


    /**
     * Gets the VALOR_JUROS value for this Dados_saida_transacao_gare_Type.
     * 
     * @return VALOR_JUROS   * GARE - Campo10
     */
    public java.math.BigDecimal getVALOR_JUROS() {
        return VALOR_JUROS;
    }


    /**
     * Sets the VALOR_JUROS value for this Dados_saida_transacao_gare_Type.
     * 
     * @param VALOR_JUROS   * GARE - Campo10
     */
    public void setVALOR_JUROS(java.math.BigDecimal VALOR_JUROS) {
        this.VALOR_JUROS = VALOR_JUROS;
    }


    /**
     * Gets the VALOR_MULTA value for this Dados_saida_transacao_gare_Type.
     * 
     * @return VALOR_MULTA   * GARE - Campo11
     */
    public java.math.BigDecimal getVALOR_MULTA() {
        return VALOR_MULTA;
    }


    /**
     * Sets the VALOR_MULTA value for this Dados_saida_transacao_gare_Type.
     * 
     * @param VALOR_MULTA   * GARE - Campo11
     */
    public void setVALOR_MULTA(java.math.BigDecimal VALOR_MULTA) {
        this.VALOR_MULTA = VALOR_MULTA;
    }


    /**
     * Gets the VALOR_ACRESCIMO value for this Dados_saida_transacao_gare_Type.
     * 
     * @return VALOR_ACRESCIMO   * GARE - Campo12
     */
    public java.math.BigDecimal getVALOR_ACRESCIMO() {
        return VALOR_ACRESCIMO;
    }


    /**
     * Sets the VALOR_ACRESCIMO value for this Dados_saida_transacao_gare_Type.
     * 
     * @param VALOR_ACRESCIMO   * GARE - Campo12
     */
    public void setVALOR_ACRESCIMO(java.math.BigDecimal VALOR_ACRESCIMO) {
        this.VALOR_ACRESCIMO = VALOR_ACRESCIMO;
    }


    /**
     * Gets the VALOR_HONORARIO value for this Dados_saida_transacao_gare_Type.
     * 
     * @return VALOR_HONORARIO   * GARE - Campo13
     */
    public java.math.BigDecimal getVALOR_HONORARIO() {
        return VALOR_HONORARIO;
    }


    /**
     * Sets the VALOR_HONORARIO value for this Dados_saida_transacao_gare_Type.
     * 
     * @param VALOR_HONORARIO   * GARE - Campo13
     */
    public void setVALOR_HONORARIO(java.math.BigDecimal VALOR_HONORARIO) {
        this.VALOR_HONORARIO = VALOR_HONORARIO;
    }


    /**
     * Gets the AUTENTICACAO_DIGITAL value for this Dados_saida_transacao_gare_Type.
     * 
     * @return AUTENTICACAO_DIGITAL
     */
    public java.lang.String getAUTENTICACAO_DIGITAL() {
        return AUTENTICACAO_DIGITAL;
    }


    /**
     * Sets the AUTENTICACAO_DIGITAL value for this Dados_saida_transacao_gare_Type.
     * 
     * @param AUTENTICACAO_DIGITAL
     */
    public void setAUTENTICACAO_DIGITAL(java.lang.String AUTENTICACAO_DIGITAL) {
        this.AUTENTICACAO_DIGITAL = AUTENTICACAO_DIGITAL;
    }


    /**
     * Gets the TIPO value for this Dados_saida_transacao_gare_Type.
     * 
     * @return TIPO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_gare_TypeTIPO getTIPO() {
        return TIPO;
    }


    /**
     * Sets the TIPO value for this Dados_saida_transacao_gare_Type.
     * 
     * @param TIPO
     */
    public void setTIPO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_gare_TypeTIPO TIPO) {
        this.TIPO = TIPO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_gare_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_gare_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_gare_Type)) return false;
        Dados_saida_transacao_gare_Type other = (Dados_saida_transacao_gare_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.UF==null && other.getUF()==null) || 
             (this.UF!=null &&
              this.UF.equals(other.getUF()))) &&
            ((this.CODIGO_RECEITA==null && other.getCODIGO_RECEITA()==null) || 
             (this.CODIGO_RECEITA!=null &&
              this.CODIGO_RECEITA.equals(other.getCODIGO_RECEITA()))) &&
            ((this.COTA_IPVA==null && other.getCOTA_IPVA()==null) || 
             (this.COTA_IPVA!=null &&
              this.COTA_IPVA.equals(other.getCOTA_IPVA()))) &&
            ((this.VENCIMENTO==null && other.getVENCIMENTO()==null) || 
             (this.VENCIMENTO!=null &&
              this.VENCIMENTO.equals(other.getVENCIMENTO()))) &&
            ((this.INSCRICAO_ESTADUAL==null && other.getINSCRICAO_ESTADUAL()==null) || 
             (this.INSCRICAO_ESTADUAL!=null &&
              this.INSCRICAO_ESTADUAL.equals(other.getINSCRICAO_ESTADUAL()))) &&
            ((this.CNPJ_CPF_RENAVAM_PLACA==null && other.getCNPJ_CPF_RENAVAM_PLACA()==null) || 
             (this.CNPJ_CPF_RENAVAM_PLACA!=null &&
              this.CNPJ_CPF_RENAVAM_PLACA.equals(other.getCNPJ_CPF_RENAVAM_PLACA()))) &&
            ((this.INSCRICAO_DIVIDA==null && other.getINSCRICAO_DIVIDA()==null) || 
             (this.INSCRICAO_DIVIDA!=null &&
              this.INSCRICAO_DIVIDA.equals(other.getINSCRICAO_DIVIDA()))) &&
            ((this.REFERENCIA==null && other.getREFERENCIA()==null) || 
             (this.REFERENCIA!=null &&
              this.REFERENCIA.equals(other.getREFERENCIA()))) &&
            ((this.AIMM_NOTIFICACAO_GUIA==null && other.getAIMM_NOTIFICACAO_GUIA()==null) || 
             (this.AIMM_NOTIFICACAO_GUIA!=null &&
              this.AIMM_NOTIFICACAO_GUIA.equals(other.getAIMM_NOTIFICACAO_GUIA()))) &&
            ((this.VALOR_RECEITA==null && other.getVALOR_RECEITA()==null) || 
             (this.VALOR_RECEITA!=null &&
              this.VALOR_RECEITA.equals(other.getVALOR_RECEITA()))) &&
            ((this.VALOR_JUROS==null && other.getVALOR_JUROS()==null) || 
             (this.VALOR_JUROS!=null &&
              this.VALOR_JUROS.equals(other.getVALOR_JUROS()))) &&
            ((this.VALOR_MULTA==null && other.getVALOR_MULTA()==null) || 
             (this.VALOR_MULTA!=null &&
              this.VALOR_MULTA.equals(other.getVALOR_MULTA()))) &&
            ((this.VALOR_ACRESCIMO==null && other.getVALOR_ACRESCIMO()==null) || 
             (this.VALOR_ACRESCIMO!=null &&
              this.VALOR_ACRESCIMO.equals(other.getVALOR_ACRESCIMO()))) &&
            ((this.VALOR_HONORARIO==null && other.getVALOR_HONORARIO()==null) || 
             (this.VALOR_HONORARIO!=null &&
              this.VALOR_HONORARIO.equals(other.getVALOR_HONORARIO()))) &&
            ((this.AUTENTICACAO_DIGITAL==null && other.getAUTENTICACAO_DIGITAL()==null) || 
             (this.AUTENTICACAO_DIGITAL!=null &&
              this.AUTENTICACAO_DIGITAL.equals(other.getAUTENTICACAO_DIGITAL()))) &&
            ((this.TIPO==null && other.getTIPO()==null) || 
             (this.TIPO!=null &&
              this.TIPO.equals(other.getTIPO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getUF() != null) {
            _hashCode += getUF().hashCode();
        }
        if (getCODIGO_RECEITA() != null) {
            _hashCode += getCODIGO_RECEITA().hashCode();
        }
        if (getCOTA_IPVA() != null) {
            _hashCode += getCOTA_IPVA().hashCode();
        }
        if (getVENCIMENTO() != null) {
            _hashCode += getVENCIMENTO().hashCode();
        }
        if (getINSCRICAO_ESTADUAL() != null) {
            _hashCode += getINSCRICAO_ESTADUAL().hashCode();
        }
        if (getCNPJ_CPF_RENAVAM_PLACA() != null) {
            _hashCode += getCNPJ_CPF_RENAVAM_PLACA().hashCode();
        }
        if (getINSCRICAO_DIVIDA() != null) {
            _hashCode += getINSCRICAO_DIVIDA().hashCode();
        }
        if (getREFERENCIA() != null) {
            _hashCode += getREFERENCIA().hashCode();
        }
        if (getAIMM_NOTIFICACAO_GUIA() != null) {
            _hashCode += getAIMM_NOTIFICACAO_GUIA().hashCode();
        }
        if (getVALOR_RECEITA() != null) {
            _hashCode += getVALOR_RECEITA().hashCode();
        }
        if (getVALOR_JUROS() != null) {
            _hashCode += getVALOR_JUROS().hashCode();
        }
        if (getVALOR_MULTA() != null) {
            _hashCode += getVALOR_MULTA().hashCode();
        }
        if (getVALOR_ACRESCIMO() != null) {
            _hashCode += getVALOR_ACRESCIMO().hashCode();
        }
        if (getVALOR_HONORARIO() != null) {
            _hashCode += getVALOR_HONORARIO().hashCode();
        }
        if (getAUTENTICACAO_DIGITAL() != null) {
            _hashCode += getAUTENTICACAO_DIGITAL().hashCode();
        }
        if (getTIPO() != null) {
            _hashCode += getTIPO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_gare_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_gare_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_RECEITA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_RECEITA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("COTA_IPVA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "COTA_IPVA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("INSCRICAO_ESTADUAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "INSCRICAO_ESTADUAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNPJ_CPF_RENAVAM_PLACA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CNPJ_CPF_RENAVAM_PLACA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("INSCRICAO_DIVIDA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "INSCRICAO_DIVIDA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REFERENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "REFERENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AIMM_NOTIFICACAO_GUIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AIMM_NOTIFICACAO_GUIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_RECEITA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_RECEITA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_JUROS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_JUROS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_MULTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_MULTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_ACRESCIMO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_ACRESCIMO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_HONORARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_HONORARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AUTENTICACAO_DIGITAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AUTENTICACAO_DIGITAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_gare_Type>TIPO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
