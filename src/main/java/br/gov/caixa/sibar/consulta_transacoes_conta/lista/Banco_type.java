/**
 * Banco_type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Banco_type  implements java.io.Serializable {
    private short CODIGO;

    private int AGENCIA;

    private java.lang.String DV;

    public Banco_type() {
    }

    public Banco_type(
           short CODIGO,
           int AGENCIA,
           java.lang.String DV) {
           this.CODIGO = CODIGO;
           this.AGENCIA = AGENCIA;
           this.DV = DV;
    }


    /**
     * Gets the CODIGO value for this Banco_type.
     * 
     * @return CODIGO
     */
    public short getCODIGO() {
        return CODIGO;
    }


    /**
     * Sets the CODIGO value for this Banco_type.
     * 
     * @param CODIGO
     */
    public void setCODIGO(short CODIGO) {
        this.CODIGO = CODIGO;
    }


    /**
     * Gets the AGENCIA value for this Banco_type.
     * 
     * @return AGENCIA
     */
    public int getAGENCIA() {
        return AGENCIA;
    }


    /**
     * Sets the AGENCIA value for this Banco_type.
     * 
     * @param AGENCIA
     */
    public void setAGENCIA(int AGENCIA) {
        this.AGENCIA = AGENCIA;
    }


    /**
     * Gets the DV value for this Banco_type.
     * 
     * @return DV
     */
    public java.lang.String getDV() {
        return DV;
    }


    /**
     * Sets the DV value for this Banco_type.
     * 
     * @param DV
     */
    public void setDV(java.lang.String DV) {
        this.DV = DV;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Banco_type)) return false;
        Banco_type other = (Banco_type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.CODIGO == other.getCODIGO() &&
            this.AGENCIA == other.getAGENCIA() &&
            ((this.DV==null && other.getDV()==null) || 
             (this.DV!=null &&
              this.DV.equals(other.getDV())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getCODIGO();
        _hashCode += getAGENCIA();
        if (getDV() != null) {
            _hashCode += getDV().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Banco_type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "banco_type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AGENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AGENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DV");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
