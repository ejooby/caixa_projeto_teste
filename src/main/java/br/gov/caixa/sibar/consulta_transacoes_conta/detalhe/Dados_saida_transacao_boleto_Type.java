/**
 * Dados_saida_transacao_boleto_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

public class Dados_saida_transacao_boleto_Type  implements java.io.Serializable ,TransacaoInt {

	public String tipoServico;
	public String data;
	public String banco_Descricao;
	public String chaveSeguranca;

	
	
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private java.lang.String NOSSO_NUMERO;

    private java.lang.String HISTORICO;

    private java.util.Date DATA_PAGAMENTO;

    private java.util.Date DATA_VENCIMENTO;

    private java.util.Date DATA_DEBITO;

    private java.math.BigDecimal VALOR;

    private java.math.BigDecimal JUROS;

    private java.math.BigDecimal IOF;

    private java.math.BigDecimal MULTA;

    private java.math.BigDecimal DESCONTO;

    private java.math.BigDecimal ABATIMENTO;

    private java.math.BigDecimal VALOR_PAGO;

    private java.math.BigDecimal VALORES_CALCULADOS;

    private java.lang.String CODIGO_BARRAS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Destinatario_boleto_Type PARTICIPANTE_DESTINATARIO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoas_boleto_Type BENEFICIARIOS;

    private org.apache.axis.types.UnsignedInt CODIGO_BENEFICIARIO_ORIGINAL;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoas_boleto_Type PAGADORES;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type SACADOR_AVALISTA;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_boleto_Type() {
    }

    public Dados_saida_transacao_boleto_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           java.lang.String NOSSO_NUMERO,
           java.lang.String HISTORICO,
           java.util.Date DATA_PAGAMENTO,
           java.util.Date DATA_VENCIMENTO,
           java.util.Date DATA_DEBITO,
           java.math.BigDecimal VALOR,
           java.math.BigDecimal JUROS,
           java.math.BigDecimal IOF,
           java.math.BigDecimal MULTA,
           java.math.BigDecimal DESCONTO,
           java.math.BigDecimal ABATIMENTO,
           java.math.BigDecimal VALOR_PAGO,
           java.math.BigDecimal VALORES_CALCULADOS,
           java.lang.String CODIGO_BARRAS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Destinatario_boleto_Type PARTICIPANTE_DESTINATARIO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoas_boleto_Type BENEFICIARIOS,
           org.apache.axis.types.UnsignedInt CODIGO_BENEFICIARIO_ORIGINAL,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoas_boleto_Type PAGADORES,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type SACADOR_AVALISTA,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.NOSSO_NUMERO = NOSSO_NUMERO;
           this.HISTORICO = HISTORICO;
           this.DATA_PAGAMENTO = DATA_PAGAMENTO;
           this.DATA_VENCIMENTO = DATA_VENCIMENTO;
           this.DATA_DEBITO = DATA_DEBITO;
           this.VALOR = VALOR;
           this.JUROS = JUROS;
           this.IOF = IOF;
           this.MULTA = MULTA;
           this.DESCONTO = DESCONTO;
           this.ABATIMENTO = ABATIMENTO;
           this.VALOR_PAGO = VALOR_PAGO;
           this.VALORES_CALCULADOS = VALORES_CALCULADOS;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.PARTICIPANTE_DESTINATARIO = PARTICIPANTE_DESTINATARIO;
           this.BENEFICIARIOS = BENEFICIARIOS;
           this.CODIGO_BENEFICIARIO_ORIGINAL = CODIGO_BENEFICIARIO_ORIGINAL;
           this.PAGADORES = PAGADORES;
           this.SACADOR_AVALISTA = SACADOR_AVALISTA;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the NOSSO_NUMERO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return NOSSO_NUMERO
     */
    public java.lang.String getNOSSO_NUMERO() {
        return NOSSO_NUMERO;
    }


    /**
     * Sets the NOSSO_NUMERO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param NOSSO_NUMERO
     */
    public void setNOSSO_NUMERO(java.lang.String NOSSO_NUMERO) {
        this.NOSSO_NUMERO = NOSSO_NUMERO;
    }


    /**
     * Gets the HISTORICO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return HISTORICO
     */
    public java.lang.String getHISTORICO() {
        return HISTORICO;
    }


    /**
     * Sets the HISTORICO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param HISTORICO
     */
    public void setHISTORICO(java.lang.String HISTORICO) {
        this.HISTORICO = HISTORICO;
    }


    /**
     * Gets the DATA_PAGAMENTO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return DATA_PAGAMENTO
     */
    public java.util.Date getDATA_PAGAMENTO() {
        return DATA_PAGAMENTO;
    }


    /**
     * Sets the DATA_PAGAMENTO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param DATA_PAGAMENTO
     */
    public void setDATA_PAGAMENTO(java.util.Date DATA_PAGAMENTO) {
        this.DATA_PAGAMENTO = DATA_PAGAMENTO;
    }


    /**
     * Gets the DATA_VENCIMENTO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return DATA_VENCIMENTO
     */
    public java.util.Date getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }


    /**
     * Sets the DATA_VENCIMENTO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param DATA_VENCIMENTO
     */
    public void setDATA_VENCIMENTO(java.util.Date DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }


    /**
     * Gets the DATA_DEBITO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return DATA_DEBITO
     */
    public java.util.Date getDATA_DEBITO() {
        return DATA_DEBITO;
    }


    /**
     * Sets the DATA_DEBITO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param DATA_DEBITO
     */
    public void setDATA_DEBITO(java.util.Date DATA_DEBITO) {
        this.DATA_DEBITO = DATA_DEBITO;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the JUROS value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return JUROS
     */
    public java.math.BigDecimal getJUROS() {
        return JUROS;
    }


    /**
     * Sets the JUROS value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param JUROS
     */
    public void setJUROS(java.math.BigDecimal JUROS) {
        this.JUROS = JUROS;
    }


    /**
     * Gets the IOF value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return IOF
     */
    public java.math.BigDecimal getIOF() {
        return IOF;
    }


    /**
     * Sets the IOF value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param IOF
     */
    public void setIOF(java.math.BigDecimal IOF) {
        this.IOF = IOF;
    }


    /**
     * Gets the MULTA value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return MULTA
     */
    public java.math.BigDecimal getMULTA() {
        return MULTA;
    }


    /**
     * Sets the MULTA value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param MULTA
     */
    public void setMULTA(java.math.BigDecimal MULTA) {
        this.MULTA = MULTA;
    }


    /**
     * Gets the DESCONTO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return DESCONTO
     */
    public java.math.BigDecimal getDESCONTO() {
        return DESCONTO;
    }


    /**
     * Sets the DESCONTO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param DESCONTO
     */
    public void setDESCONTO(java.math.BigDecimal DESCONTO) {
        this.DESCONTO = DESCONTO;
    }


    /**
     * Gets the ABATIMENTO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return ABATIMENTO
     */
    public java.math.BigDecimal getABATIMENTO() {
        return ABATIMENTO;
    }


    /**
     * Sets the ABATIMENTO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param ABATIMENTO
     */
    public void setABATIMENTO(java.math.BigDecimal ABATIMENTO) {
        this.ABATIMENTO = ABATIMENTO;
    }


    /**
     * Gets the VALOR_PAGO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return VALOR_PAGO
     */
    public java.math.BigDecimal getVALOR_PAGO() {
        return VALOR_PAGO;
    }


    /**
     * Sets the VALOR_PAGO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param VALOR_PAGO
     */
    public void setVALOR_PAGO(java.math.BigDecimal VALOR_PAGO) {
        this.VALOR_PAGO = VALOR_PAGO;
    }


    /**
     * Gets the VALORES_CALCULADOS value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return VALORES_CALCULADOS
     */
    public java.math.BigDecimal getVALORES_CALCULADOS() {
        return VALORES_CALCULADOS;
    }


    /**
     * Sets the VALORES_CALCULADOS value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param VALORES_CALCULADOS
     */
    public void setVALORES_CALCULADOS(java.math.BigDecimal VALORES_CALCULADOS) {
        this.VALORES_CALCULADOS = VALORES_CALCULADOS;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }


    /**
     * Gets the PARTICIPANTE_DESTINATARIO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return PARTICIPANTE_DESTINATARIO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Destinatario_boleto_Type getPARTICIPANTE_DESTINATARIO() {
        return PARTICIPANTE_DESTINATARIO;
    }


    /**
     * Sets the PARTICIPANTE_DESTINATARIO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param PARTICIPANTE_DESTINATARIO
     */
    public void setPARTICIPANTE_DESTINATARIO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Destinatario_boleto_Type PARTICIPANTE_DESTINATARIO) {
        this.PARTICIPANTE_DESTINATARIO = PARTICIPANTE_DESTINATARIO;
    }


    /**
     * Gets the BENEFICIARIOS value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return BENEFICIARIOS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoas_boleto_Type getBENEFICIARIOS() {
        return BENEFICIARIOS;
    }


    /**
     * Sets the BENEFICIARIOS value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param BENEFICIARIOS
     */
    public void setBENEFICIARIOS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoas_boleto_Type BENEFICIARIOS) {
        this.BENEFICIARIOS = BENEFICIARIOS;
    }


    /**
     * Gets the CODIGO_BENEFICIARIO_ORIGINAL value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return CODIGO_BENEFICIARIO_ORIGINAL
     */
    public org.apache.axis.types.UnsignedInt getCODIGO_BENEFICIARIO_ORIGINAL() {
        return CODIGO_BENEFICIARIO_ORIGINAL;
    }


    /**
     * Sets the CODIGO_BENEFICIARIO_ORIGINAL value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param CODIGO_BENEFICIARIO_ORIGINAL
     */
    public void setCODIGO_BENEFICIARIO_ORIGINAL(org.apache.axis.types.UnsignedInt CODIGO_BENEFICIARIO_ORIGINAL) {
        this.CODIGO_BENEFICIARIO_ORIGINAL = CODIGO_BENEFICIARIO_ORIGINAL;
    }


    /**
     * Gets the PAGADORES value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return PAGADORES
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoas_boleto_Type getPAGADORES() {
        return PAGADORES;
    }


    /**
     * Sets the PAGADORES value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param PAGADORES
     */
    public void setPAGADORES(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoas_boleto_Type PAGADORES) {
        this.PAGADORES = PAGADORES;
    }


    /**
     * Gets the SACADOR_AVALISTA value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return SACADOR_AVALISTA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type getSACADOR_AVALISTA() {
        return SACADOR_AVALISTA;
    }


    /**
     * Sets the SACADOR_AVALISTA value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param SACADOR_AVALISTA
     */
    public void setSACADOR_AVALISTA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type SACADOR_AVALISTA) {
        this.SACADOR_AVALISTA = SACADOR_AVALISTA;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_boleto_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_boleto_Type)) return false;
        Dados_saida_transacao_boleto_Type other = (Dados_saida_transacao_boleto_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.NOSSO_NUMERO==null && other.getNOSSO_NUMERO()==null) || 
             (this.NOSSO_NUMERO!=null &&
              this.NOSSO_NUMERO.equals(other.getNOSSO_NUMERO()))) &&
            ((this.HISTORICO==null && other.getHISTORICO()==null) || 
             (this.HISTORICO!=null &&
              this.HISTORICO.equals(other.getHISTORICO()))) &&
            ((this.DATA_PAGAMENTO==null && other.getDATA_PAGAMENTO()==null) || 
             (this.DATA_PAGAMENTO!=null &&
              this.DATA_PAGAMENTO.equals(other.getDATA_PAGAMENTO()))) &&
            ((this.DATA_VENCIMENTO==null && other.getDATA_VENCIMENTO()==null) || 
             (this.DATA_VENCIMENTO!=null &&
              this.DATA_VENCIMENTO.equals(other.getDATA_VENCIMENTO()))) &&
            ((this.DATA_DEBITO==null && other.getDATA_DEBITO()==null) || 
             (this.DATA_DEBITO!=null &&
              this.DATA_DEBITO.equals(other.getDATA_DEBITO()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.JUROS==null && other.getJUROS()==null) || 
             (this.JUROS!=null &&
              this.JUROS.equals(other.getJUROS()))) &&
            ((this.IOF==null && other.getIOF()==null) || 
             (this.IOF!=null &&
              this.IOF.equals(other.getIOF()))) &&
            ((this.MULTA==null && other.getMULTA()==null) || 
             (this.MULTA!=null &&
              this.MULTA.equals(other.getMULTA()))) &&
            ((this.DESCONTO==null && other.getDESCONTO()==null) || 
             (this.DESCONTO!=null &&
              this.DESCONTO.equals(other.getDESCONTO()))) &&
            ((this.ABATIMENTO==null && other.getABATIMENTO()==null) || 
             (this.ABATIMENTO!=null &&
              this.ABATIMENTO.equals(other.getABATIMENTO()))) &&
            ((this.VALOR_PAGO==null && other.getVALOR_PAGO()==null) || 
             (this.VALOR_PAGO!=null &&
              this.VALOR_PAGO.equals(other.getVALOR_PAGO()))) &&
            ((this.VALORES_CALCULADOS==null && other.getVALORES_CALCULADOS()==null) || 
             (this.VALORES_CALCULADOS!=null &&
              this.VALORES_CALCULADOS.equals(other.getVALORES_CALCULADOS()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              this.CODIGO_BARRAS.equals(other.getCODIGO_BARRAS()))) &&
            ((this.PARTICIPANTE_DESTINATARIO==null && other.getPARTICIPANTE_DESTINATARIO()==null) || 
             (this.PARTICIPANTE_DESTINATARIO!=null &&
              this.PARTICIPANTE_DESTINATARIO.equals(other.getPARTICIPANTE_DESTINATARIO()))) &&
            ((this.BENEFICIARIOS==null && other.getBENEFICIARIOS()==null) || 
             (this.BENEFICIARIOS!=null &&
              this.BENEFICIARIOS.equals(other.getBENEFICIARIOS()))) &&
            ((this.CODIGO_BENEFICIARIO_ORIGINAL==null && other.getCODIGO_BENEFICIARIO_ORIGINAL()==null) || 
             (this.CODIGO_BENEFICIARIO_ORIGINAL!=null &&
              this.CODIGO_BENEFICIARIO_ORIGINAL.equals(other.getCODIGO_BENEFICIARIO_ORIGINAL()))) &&
            ((this.PAGADORES==null && other.getPAGADORES()==null) || 
             (this.PAGADORES!=null &&
              this.PAGADORES.equals(other.getPAGADORES()))) &&
            ((this.SACADOR_AVALISTA==null && other.getSACADOR_AVALISTA()==null) || 
             (this.SACADOR_AVALISTA!=null &&
              this.SACADOR_AVALISTA.equals(other.getSACADOR_AVALISTA()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getNOSSO_NUMERO() != null) {
            _hashCode += getNOSSO_NUMERO().hashCode();
        }
        if (getHISTORICO() != null) {
            _hashCode += getHISTORICO().hashCode();
        }
        if (getDATA_PAGAMENTO() != null) {
            _hashCode += getDATA_PAGAMENTO().hashCode();
        }
        if (getDATA_VENCIMENTO() != null) {
            _hashCode += getDATA_VENCIMENTO().hashCode();
        }
        if (getDATA_DEBITO() != null) {
            _hashCode += getDATA_DEBITO().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getJUROS() != null) {
            _hashCode += getJUROS().hashCode();
        }
        if (getIOF() != null) {
            _hashCode += getIOF().hashCode();
        }
        if (getMULTA() != null) {
            _hashCode += getMULTA().hashCode();
        }
        if (getDESCONTO() != null) {
            _hashCode += getDESCONTO().hashCode();
        }
        if (getABATIMENTO() != null) {
            _hashCode += getABATIMENTO().hashCode();
        }
        if (getVALOR_PAGO() != null) {
            _hashCode += getVALOR_PAGO().hashCode();
        }
        if (getVALORES_CALCULADOS() != null) {
            _hashCode += getVALORES_CALCULADOS().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            _hashCode += getCODIGO_BARRAS().hashCode();
        }
        if (getPARTICIPANTE_DESTINATARIO() != null) {
            _hashCode += getPARTICIPANTE_DESTINATARIO().hashCode();
        }
        if (getBENEFICIARIOS() != null) {
            _hashCode += getBENEFICIARIOS().hashCode();
        }
        if (getCODIGO_BENEFICIARIO_ORIGINAL() != null) {
            _hashCode += getCODIGO_BENEFICIARIO_ORIGINAL().hashCode();
        }
        if (getPAGADORES() != null) {
            _hashCode += getPAGADORES().hashCode();
        }
        if (getSACADOR_AVALISTA() != null) {
            _hashCode += getSACADOR_AVALISTA().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_boleto_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_boleto_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOSSO_NUMERO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOSSO_NUMERO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HISTORICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HISTORICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_PAGAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_PAGAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_DEBITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_DEBITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("JUROS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "JUROS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IOF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IOF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DESCONTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DESCONTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ABATIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ABATIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_PAGO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_PAGO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALORES_CALCULADOS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALORES_CALCULADOS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARTICIPANTE_DESTINATARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PARTICIPANTE_DESTINATARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "destinatario_boleto_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BENEFICIARIOS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BENEFICIARIOS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "pessoas_boleto_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BENEFICIARIO_ORIGINAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BENEFICIARIO_ORIGINAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAGADORES");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PAGADORES"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "pessoas_boleto_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SACADOR_AVALISTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SACADOR_AVALISTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "pessoa_boleto_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public String getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getBanco_Descricao() {
		return banco_Descricao;
	}

	public void setBanco_Descricao(String banco_Descricao) {
		this.banco_Descricao = banco_Descricao;
	}

	public String getChaveSeguranca() {
		return chaveSeguranca;
	}

	public void setChaveSeguranca(String chaveSeguranca) {
		this.chaveSeguranca = chaveSeguranca;
	}

    
    
    
}
