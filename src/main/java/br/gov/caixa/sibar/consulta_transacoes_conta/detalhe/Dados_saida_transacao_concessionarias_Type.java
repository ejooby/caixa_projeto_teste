/**
 * Dados_saida_transacao_concessionarias_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_concessionarias_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private java.lang.String[] CODIGO_BARRAS;

    private java.util.Date DATA_VENCIMENTO;

    private java.lang.String EMPRESA;

    private java.lang.String IDENTIFICACAO;

    private java.lang.String IDENTIFICACAO_EMPRESA;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    private java.lang.String[] MENSAGENS;

    public Dados_saida_transacao_concessionarias_Type() {
    }

    public Dados_saida_transacao_concessionarias_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           java.lang.String[] CODIGO_BARRAS,
           java.util.Date DATA_VENCIMENTO,
           java.lang.String EMPRESA,
           java.lang.String IDENTIFICACAO,
           java.lang.String IDENTIFICACAO_EMPRESA,
           org.apache.axis.types.Time HORA_TRANSACAO,
           java.lang.String[] MENSAGENS) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.DATA_VENCIMENTO = DATA_VENCIMENTO;
           this.EMPRESA = EMPRESA;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.IDENTIFICACAO_EMPRESA = IDENTIFICACAO_EMPRESA;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
           this.MENSAGENS = MENSAGENS;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String[] getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String[] CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }

    public java.lang.String getCODIGO_BARRAS(int i) {
        return this.CODIGO_BARRAS[i];
    }

    public void setCODIGO_BARRAS(int i, java.lang.String _value) {
        this.CODIGO_BARRAS[i] = _value;
    }


    /**
     * Gets the DATA_VENCIMENTO value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @return DATA_VENCIMENTO
     */
    public java.util.Date getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }


    /**
     * Sets the DATA_VENCIMENTO value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @param DATA_VENCIMENTO
     */
    public void setDATA_VENCIMENTO(java.util.Date DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }


    /**
     * Gets the EMPRESA value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @return EMPRESA
     */
    public java.lang.String getEMPRESA() {
        return EMPRESA;
    }


    /**
     * Sets the EMPRESA value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @param EMPRESA
     */
    public void setEMPRESA(java.lang.String EMPRESA) {
        this.EMPRESA = EMPRESA;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the IDENTIFICACAO_EMPRESA value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @return IDENTIFICACAO_EMPRESA
     */
    public java.lang.String getIDENTIFICACAO_EMPRESA() {
        return IDENTIFICACAO_EMPRESA;
    }


    /**
     * Sets the IDENTIFICACAO_EMPRESA value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @param IDENTIFICACAO_EMPRESA
     */
    public void setIDENTIFICACAO_EMPRESA(java.lang.String IDENTIFICACAO_EMPRESA) {
        this.IDENTIFICACAO_EMPRESA = IDENTIFICACAO_EMPRESA;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the MENSAGENS value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @return MENSAGENS
     */
    public java.lang.String[] getMENSAGENS() {
        return MENSAGENS;
    }


    /**
     * Sets the MENSAGENS value for this Dados_saida_transacao_concessionarias_Type.
     * 
     * @param MENSAGENS
     */
    public void setMENSAGENS(java.lang.String[] MENSAGENS) {
        this.MENSAGENS = MENSAGENS;
    }

    public java.lang.String getMENSAGENS(int i) {
        return this.MENSAGENS[i];
    }

    public void setMENSAGENS(int i, java.lang.String _value) {
        this.MENSAGENS[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_concessionarias_Type)) return false;
        Dados_saida_transacao_concessionarias_Type other = (Dados_saida_transacao_concessionarias_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              java.util.Arrays.equals(this.CODIGO_BARRAS, other.getCODIGO_BARRAS()))) &&
            ((this.DATA_VENCIMENTO==null && other.getDATA_VENCIMENTO()==null) || 
             (this.DATA_VENCIMENTO!=null &&
              this.DATA_VENCIMENTO.equals(other.getDATA_VENCIMENTO()))) &&
            ((this.EMPRESA==null && other.getEMPRESA()==null) || 
             (this.EMPRESA!=null &&
              this.EMPRESA.equals(other.getEMPRESA()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.IDENTIFICACAO_EMPRESA==null && other.getIDENTIFICACAO_EMPRESA()==null) || 
             (this.IDENTIFICACAO_EMPRESA!=null &&
              this.IDENTIFICACAO_EMPRESA.equals(other.getIDENTIFICACAO_EMPRESA()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO()))) &&
            ((this.MENSAGENS==null && other.getMENSAGENS()==null) || 
             (this.MENSAGENS!=null &&
              java.util.Arrays.equals(this.MENSAGENS, other.getMENSAGENS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCODIGO_BARRAS());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCODIGO_BARRAS(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDATA_VENCIMENTO() != null) {
            _hashCode += getDATA_VENCIMENTO().hashCode();
        }
        if (getEMPRESA() != null) {
            _hashCode += getEMPRESA().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getIDENTIFICACAO_EMPRESA() != null) {
            _hashCode += getIDENTIFICACAO_EMPRESA().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        if (getMENSAGENS() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMENSAGENS());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMENSAGENS(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_concessionarias_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_concessionarias_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_concessionarias_Type>CODIGO_BARRAS"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMPRESA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EMPRESA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO_EMPRESA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO_EMPRESA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MENSAGENS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MENSAGENS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_concessionarias_Type>MENSAGENS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
