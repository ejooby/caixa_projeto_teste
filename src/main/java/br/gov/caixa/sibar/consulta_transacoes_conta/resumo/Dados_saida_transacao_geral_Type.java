/**
 * Dados_saida_transacao_geral_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.resumo;

import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

public class Dados_saida_transacao_geral_Type  implements java.io.Serializable ,TransacaoInt {

	public String tipoServico;
	public String data;
	public String banco_Descricao;
	public String chaveSeguranca;

	
	
	
    private org.apache.axis.types.UnsignedLong NSU;

    private java.util.Date DATA_PAGAMENTO;

    private java.util.Date DATA_EFETIVACAO;

    private java.math.BigDecimal VALOR;

    private java.lang.String IDENTIFICADOR;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_transacao_geral_TypeSITUACAO SITUACAO;

    public Dados_saida_transacao_geral_Type() {
    }

    public Dados_saida_transacao_geral_Type(
           org.apache.axis.types.UnsignedLong NSU,
           java.util.Date DATA_PAGAMENTO,
           java.util.Date DATA_EFETIVACAO,
           java.math.BigDecimal VALOR,
           java.lang.String IDENTIFICADOR,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_transacao_geral_TypeSITUACAO SITUACAO) {
           this.NSU = NSU;
           this.DATA_PAGAMENTO = DATA_PAGAMENTO;
           this.DATA_EFETIVACAO = DATA_EFETIVACAO;
           this.VALOR = VALOR;
           this.IDENTIFICADOR = IDENTIFICADOR;
           this.SITUACAO = SITUACAO;
    }


    /**
     * Gets the NSU value for this Dados_saida_transacao_geral_Type.
     * 
     * @return NSU
     */
    public org.apache.axis.types.UnsignedLong getNSU() {
        return NSU;
    }


    /**
     * Sets the NSU value for this Dados_saida_transacao_geral_Type.
     * 
     * @param NSU
     */
    public void setNSU(org.apache.axis.types.UnsignedLong NSU) {
        this.NSU = NSU;
    }


    /**
     * Gets the DATA_PAGAMENTO value for this Dados_saida_transacao_geral_Type.
     * 
     * @return DATA_PAGAMENTO
     */
    public java.util.Date getDATA_PAGAMENTO() {
        return DATA_PAGAMENTO;
    }


    /**
     * Sets the DATA_PAGAMENTO value for this Dados_saida_transacao_geral_Type.
     * 
     * @param DATA_PAGAMENTO
     */
    public void setDATA_PAGAMENTO(java.util.Date DATA_PAGAMENTO) {
        this.DATA_PAGAMENTO = DATA_PAGAMENTO;
    }


    /**
     * Gets the DATA_EFETIVACAO value for this Dados_saida_transacao_geral_Type.
     * 
     * @return DATA_EFETIVACAO
     */
    public java.util.Date getDATA_EFETIVACAO() {
        return DATA_EFETIVACAO;
    }


    /**
     * Sets the DATA_EFETIVACAO value for this Dados_saida_transacao_geral_Type.
     * 
     * @param DATA_EFETIVACAO
     */
    public void setDATA_EFETIVACAO(java.util.Date DATA_EFETIVACAO) {
        this.DATA_EFETIVACAO = DATA_EFETIVACAO;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_geral_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_geral_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the IDENTIFICADOR value for this Dados_saida_transacao_geral_Type.
     * 
     * @return IDENTIFICADOR
     */
    public java.lang.String getIDENTIFICADOR() {
        return IDENTIFICADOR;
    }


    /**
     * Sets the IDENTIFICADOR value for this Dados_saida_transacao_geral_Type.
     * 
     * @param IDENTIFICADOR
     */
    public void setIDENTIFICADOR(java.lang.String IDENTIFICADOR) {
        this.IDENTIFICADOR = IDENTIFICADOR;
    }


    /**
     * Gets the SITUACAO value for this Dados_saida_transacao_geral_Type.
     * 
     * @return SITUACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_transacao_geral_TypeSITUACAO getSITUACAO() {
        return SITUACAO;
    }


    /**
     * Sets the SITUACAO value for this Dados_saida_transacao_geral_Type.
     * 
     * @param SITUACAO
     */
    public void setSITUACAO(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_transacao_geral_TypeSITUACAO SITUACAO) {
        this.SITUACAO = SITUACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_geral_Type)) return false;
        Dados_saida_transacao_geral_Type other = (Dados_saida_transacao_geral_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NSU==null && other.getNSU()==null) || 
             (this.NSU!=null &&
              this.NSU.equals(other.getNSU()))) &&
            ((this.DATA_PAGAMENTO==null && other.getDATA_PAGAMENTO()==null) || 
             (this.DATA_PAGAMENTO!=null &&
              this.DATA_PAGAMENTO.equals(other.getDATA_PAGAMENTO()))) &&
            ((this.DATA_EFETIVACAO==null && other.getDATA_EFETIVACAO()==null) || 
             (this.DATA_EFETIVACAO!=null &&
              this.DATA_EFETIVACAO.equals(other.getDATA_EFETIVACAO()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.IDENTIFICADOR==null && other.getIDENTIFICADOR()==null) || 
             (this.IDENTIFICADOR!=null &&
              this.IDENTIFICADOR.equals(other.getIDENTIFICADOR()))) &&
            ((this.SITUACAO==null && other.getSITUACAO()==null) || 
             (this.SITUACAO!=null &&
              this.SITUACAO.equals(other.getSITUACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNSU() != null) {
            _hashCode += getNSU().hashCode();
        }
        if (getDATA_PAGAMENTO() != null) {
            _hashCode += getDATA_PAGAMENTO().hashCode();
        }
        if (getDATA_EFETIVACAO() != null) {
            _hashCode += getDATA_EFETIVACAO().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getIDENTIFICADOR() != null) {
            _hashCode += getIDENTIFICADOR().hashCode();
        }
        if (getSITUACAO() != null) {
            _hashCode += getSITUACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_geral_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_transacao_geral_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_PAGAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_PAGAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_EFETIVACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_EFETIVACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICADOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICADOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SITUACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SITUACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", ">dados_saida_transacao_geral_Type>SITUACAO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public String getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getBanco_Descricao() {
		return banco_Descricao;
	}

	public void setBanco_Descricao(String banco_Descricao) {
		this.banco_Descricao = banco_Descricao;
	}

	public String getChaveSeguranca() {
		return chaveSeguranca;
	}

	public void setChaveSeguranca(String chaveSeguranca) {
		this.chaveSeguranca = chaveSeguranca;
	}

    
    
}
