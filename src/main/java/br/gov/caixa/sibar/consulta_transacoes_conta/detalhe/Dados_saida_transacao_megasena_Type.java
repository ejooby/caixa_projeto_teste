/**
 * Dados_saida_transacao_megasena_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_megasena_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Apostador_Type APOSTADOR;

    private java.lang.String LOTERICO;

    private java.lang.String BILHETE;

    private java.lang.String CVB;

    private org.apache.axis.types.UnsignedShort CONCURSO;

    private java.lang.String DATA_SORTEIO;

    private java.lang.String DATA_APOSTA;

    private java.math.BigDecimal VALOR_APOSTA;

    private org.apache.axis.types.UnsignedByte QUANTIDADE_DEZENAS;

    private java.lang.String[] JOGO;

    public Dados_saida_transacao_megasena_Type() {
    }

    public Dados_saida_transacao_megasena_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Apostador_Type APOSTADOR,
           java.lang.String LOTERICO,
           java.lang.String BILHETE,
           java.lang.String CVB,
           org.apache.axis.types.UnsignedShort CONCURSO,
           java.lang.String DATA_SORTEIO,
           java.lang.String DATA_APOSTA,
           java.math.BigDecimal VALOR_APOSTA,
           org.apache.axis.types.UnsignedByte QUANTIDADE_DEZENAS,
           java.lang.String[] JOGO) {
           this.APOSTADOR = APOSTADOR;
           this.LOTERICO = LOTERICO;
           this.BILHETE = BILHETE;
           this.CVB = CVB;
           this.CONCURSO = CONCURSO;
           this.DATA_SORTEIO = DATA_SORTEIO;
           this.DATA_APOSTA = DATA_APOSTA;
           this.VALOR_APOSTA = VALOR_APOSTA;
           this.QUANTIDADE_DEZENAS = QUANTIDADE_DEZENAS;
           this.JOGO = JOGO;
    }


    /**
     * Gets the APOSTADOR value for this Dados_saida_transacao_megasena_Type.
     * 
     * @return APOSTADOR
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Apostador_Type getAPOSTADOR() {
        return APOSTADOR;
    }


    /**
     * Sets the APOSTADOR value for this Dados_saida_transacao_megasena_Type.
     * 
     * @param APOSTADOR
     */
    public void setAPOSTADOR(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Apostador_Type APOSTADOR) {
        this.APOSTADOR = APOSTADOR;
    }


    /**
     * Gets the LOTERICO value for this Dados_saida_transacao_megasena_Type.
     * 
     * @return LOTERICO
     */
    public java.lang.String getLOTERICO() {
        return LOTERICO;
    }


    /**
     * Sets the LOTERICO value for this Dados_saida_transacao_megasena_Type.
     * 
     * @param LOTERICO
     */
    public void setLOTERICO(java.lang.String LOTERICO) {
        this.LOTERICO = LOTERICO;
    }


    /**
     * Gets the BILHETE value for this Dados_saida_transacao_megasena_Type.
     * 
     * @return BILHETE
     */
    public java.lang.String getBILHETE() {
        return BILHETE;
    }


    /**
     * Sets the BILHETE value for this Dados_saida_transacao_megasena_Type.
     * 
     * @param BILHETE
     */
    public void setBILHETE(java.lang.String BILHETE) {
        this.BILHETE = BILHETE;
    }


    /**
     * Gets the CVB value for this Dados_saida_transacao_megasena_Type.
     * 
     * @return CVB
     */
    public java.lang.String getCVB() {
        return CVB;
    }


    /**
     * Sets the CVB value for this Dados_saida_transacao_megasena_Type.
     * 
     * @param CVB
     */
    public void setCVB(java.lang.String CVB) {
        this.CVB = CVB;
    }


    /**
     * Gets the CONCURSO value for this Dados_saida_transacao_megasena_Type.
     * 
     * @return CONCURSO
     */
    public org.apache.axis.types.UnsignedShort getCONCURSO() {
        return CONCURSO;
    }


    /**
     * Sets the CONCURSO value for this Dados_saida_transacao_megasena_Type.
     * 
     * @param CONCURSO
     */
    public void setCONCURSO(org.apache.axis.types.UnsignedShort CONCURSO) {
        this.CONCURSO = CONCURSO;
    }


    /**
     * Gets the DATA_SORTEIO value for this Dados_saida_transacao_megasena_Type.
     * 
     * @return DATA_SORTEIO
     */
    public java.lang.String getDATA_SORTEIO() {
        return DATA_SORTEIO;
    }


    /**
     * Sets the DATA_SORTEIO value for this Dados_saida_transacao_megasena_Type.
     * 
     * @param DATA_SORTEIO
     */
    public void setDATA_SORTEIO(java.lang.String DATA_SORTEIO) {
        this.DATA_SORTEIO = DATA_SORTEIO;
    }


    /**
     * Gets the DATA_APOSTA value for this Dados_saida_transacao_megasena_Type.
     * 
     * @return DATA_APOSTA
     */
    public java.lang.String getDATA_APOSTA() {
        return DATA_APOSTA;
    }


    /**
     * Sets the DATA_APOSTA value for this Dados_saida_transacao_megasena_Type.
     * 
     * @param DATA_APOSTA
     */
    public void setDATA_APOSTA(java.lang.String DATA_APOSTA) {
        this.DATA_APOSTA = DATA_APOSTA;
    }


    /**
     * Gets the VALOR_APOSTA value for this Dados_saida_transacao_megasena_Type.
     * 
     * @return VALOR_APOSTA
     */
    public java.math.BigDecimal getVALOR_APOSTA() {
        return VALOR_APOSTA;
    }


    /**
     * Sets the VALOR_APOSTA value for this Dados_saida_transacao_megasena_Type.
     * 
     * @param VALOR_APOSTA
     */
    public void setVALOR_APOSTA(java.math.BigDecimal VALOR_APOSTA) {
        this.VALOR_APOSTA = VALOR_APOSTA;
    }


    /**
     * Gets the QUANTIDADE_DEZENAS value for this Dados_saida_transacao_megasena_Type.
     * 
     * @return QUANTIDADE_DEZENAS
     */
    public org.apache.axis.types.UnsignedByte getQUANTIDADE_DEZENAS() {
        return QUANTIDADE_DEZENAS;
    }


    /**
     * Sets the QUANTIDADE_DEZENAS value for this Dados_saida_transacao_megasena_Type.
     * 
     * @param QUANTIDADE_DEZENAS
     */
    public void setQUANTIDADE_DEZENAS(org.apache.axis.types.UnsignedByte QUANTIDADE_DEZENAS) {
        this.QUANTIDADE_DEZENAS = QUANTIDADE_DEZENAS;
    }


    /**
     * Gets the JOGO value for this Dados_saida_transacao_megasena_Type.
     * 
     * @return JOGO
     */
    public java.lang.String[] getJOGO() {
        return JOGO;
    }


    /**
     * Sets the JOGO value for this Dados_saida_transacao_megasena_Type.
     * 
     * @param JOGO
     */
    public void setJOGO(java.lang.String[] JOGO) {
        this.JOGO = JOGO;
    }

    public java.lang.String getJOGO(int i) {
        return this.JOGO[i];
    }

    public void setJOGO(int i, java.lang.String _value) {
        this.JOGO[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_megasena_Type)) return false;
        Dados_saida_transacao_megasena_Type other = (Dados_saida_transacao_megasena_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.APOSTADOR==null && other.getAPOSTADOR()==null) || 
             (this.APOSTADOR!=null &&
              this.APOSTADOR.equals(other.getAPOSTADOR()))) &&
            ((this.LOTERICO==null && other.getLOTERICO()==null) || 
             (this.LOTERICO!=null &&
              this.LOTERICO.equals(other.getLOTERICO()))) &&
            ((this.BILHETE==null && other.getBILHETE()==null) || 
             (this.BILHETE!=null &&
              this.BILHETE.equals(other.getBILHETE()))) &&
            ((this.CVB==null && other.getCVB()==null) || 
             (this.CVB!=null &&
              this.CVB.equals(other.getCVB()))) &&
            ((this.CONCURSO==null && other.getCONCURSO()==null) || 
             (this.CONCURSO!=null &&
              this.CONCURSO.equals(other.getCONCURSO()))) &&
            ((this.DATA_SORTEIO==null && other.getDATA_SORTEIO()==null) || 
             (this.DATA_SORTEIO!=null &&
              this.DATA_SORTEIO.equals(other.getDATA_SORTEIO()))) &&
            ((this.DATA_APOSTA==null && other.getDATA_APOSTA()==null) || 
             (this.DATA_APOSTA!=null &&
              this.DATA_APOSTA.equals(other.getDATA_APOSTA()))) &&
            ((this.VALOR_APOSTA==null && other.getVALOR_APOSTA()==null) || 
             (this.VALOR_APOSTA!=null &&
              this.VALOR_APOSTA.equals(other.getVALOR_APOSTA()))) &&
            ((this.QUANTIDADE_DEZENAS==null && other.getQUANTIDADE_DEZENAS()==null) || 
             (this.QUANTIDADE_DEZENAS!=null &&
              this.QUANTIDADE_DEZENAS.equals(other.getQUANTIDADE_DEZENAS()))) &&
            ((this.JOGO==null && other.getJOGO()==null) || 
             (this.JOGO!=null &&
              java.util.Arrays.equals(this.JOGO, other.getJOGO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAPOSTADOR() != null) {
            _hashCode += getAPOSTADOR().hashCode();
        }
        if (getLOTERICO() != null) {
            _hashCode += getLOTERICO().hashCode();
        }
        if (getBILHETE() != null) {
            _hashCode += getBILHETE().hashCode();
        }
        if (getCVB() != null) {
            _hashCode += getCVB().hashCode();
        }
        if (getCONCURSO() != null) {
            _hashCode += getCONCURSO().hashCode();
        }
        if (getDATA_SORTEIO() != null) {
            _hashCode += getDATA_SORTEIO().hashCode();
        }
        if (getDATA_APOSTA() != null) {
            _hashCode += getDATA_APOSTA().hashCode();
        }
        if (getVALOR_APOSTA() != null) {
            _hashCode += getVALOR_APOSTA().hashCode();
        }
        if (getQUANTIDADE_DEZENAS() != null) {
            _hashCode += getQUANTIDADE_DEZENAS().hashCode();
        }
        if (getJOGO() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getJOGO());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getJOGO(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_megasena_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_megasena_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("APOSTADOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "APOSTADOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "apostador_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOTERICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LOTERICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BILHETE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BILHETE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVB");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CVB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONCURSO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONCURSO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_SORTEIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_SORTEIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_APOSTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_APOSTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_APOSTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_APOSTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("QUANTIDADE_DEZENAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "QUANTIDADE_DEZENAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("JOGO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "JOGO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_megasena_Type>JOGO"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
