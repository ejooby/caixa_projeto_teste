/**
 * Dados_saida_boleto_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_boleto_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_boleto_Type[] TRANSACOES;

    public Dados_saida_boleto_Type() {
    }

    public Dados_saida_boleto_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_boleto_Type[] TRANSACOES) {
           this.TRANSACOES = TRANSACOES;
    }


    /**
     * Gets the TRANSACOES value for this Dados_saida_boleto_Type.
     * 
     * @return TRANSACOES
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_boleto_Type[] getTRANSACOES() {
        return TRANSACOES;
    }


    /**
     * Sets the TRANSACOES value for this Dados_saida_boleto_Type.
     * 
     * @param TRANSACOES
     */
    public void setTRANSACOES(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_boleto_Type[] TRANSACOES) {
        this.TRANSACOES = TRANSACOES;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_boleto_Type)) return false;
        Dados_saida_boleto_Type other = (Dados_saida_boleto_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.TRANSACOES==null && other.getTRANSACOES()==null) || 
             (this.TRANSACOES!=null &&
              java.util.Arrays.equals(this.TRANSACOES, other.getTRANSACOES())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTRANSACOES() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTRANSACOES());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTRANSACOES(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_boleto_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_boleto_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSACOES");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TRANSACOES"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_boleto_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "TRANSACAO"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
