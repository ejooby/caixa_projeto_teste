/**
 * Dados_saida_transacao_poupanca_programada_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_poupanca_programada_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_conta_Type DADOS_ORIGEM;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_conta_Type DADOS_DESTINO;

    private java.lang.String CPF_CNPJ_DESTINO;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_poupanca_programada_Type() {
    }

    public Dados_saida_transacao_poupanca_programada_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_conta_Type DADOS_ORIGEM,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_conta_Type DADOS_DESTINO,
           java.lang.String CPF_CNPJ_DESTINO,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.DADOS_ORIGEM = DADOS_ORIGEM;
           this.DADOS_DESTINO = DADOS_DESTINO;
           this.CPF_CNPJ_DESTINO = CPF_CNPJ_DESTINO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the DADOS_ORIGEM value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @return DADOS_ORIGEM
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_conta_Type getDADOS_ORIGEM() {
        return DADOS_ORIGEM;
    }


    /**
     * Sets the DADOS_ORIGEM value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @param DADOS_ORIGEM
     */
    public void setDADOS_ORIGEM(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_conta_Type DADOS_ORIGEM) {
        this.DADOS_ORIGEM = DADOS_ORIGEM;
    }


    /**
     * Gets the DADOS_DESTINO value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @return DADOS_DESTINO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_conta_Type getDADOS_DESTINO() {
        return DADOS_DESTINO;
    }


    /**
     * Sets the DADOS_DESTINO value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @param DADOS_DESTINO
     */
    public void setDADOS_DESTINO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_conta_Type DADOS_DESTINO) {
        this.DADOS_DESTINO = DADOS_DESTINO;
    }


    /**
     * Gets the CPF_CNPJ_DESTINO value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @return CPF_CNPJ_DESTINO
     */
    public java.lang.String getCPF_CNPJ_DESTINO() {
        return CPF_CNPJ_DESTINO;
    }


    /**
     * Sets the CPF_CNPJ_DESTINO value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @param CPF_CNPJ_DESTINO
     */
    public void setCPF_CNPJ_DESTINO(java.lang.String CPF_CNPJ_DESTINO) {
        this.CPF_CNPJ_DESTINO = CPF_CNPJ_DESTINO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_poupanca_programada_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_poupanca_programada_Type)) return false;
        Dados_saida_transacao_poupanca_programada_Type other = (Dados_saida_transacao_poupanca_programada_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.DADOS_ORIGEM==null && other.getDADOS_ORIGEM()==null) || 
             (this.DADOS_ORIGEM!=null &&
              this.DADOS_ORIGEM.equals(other.getDADOS_ORIGEM()))) &&
            ((this.DADOS_DESTINO==null && other.getDADOS_DESTINO()==null) || 
             (this.DADOS_DESTINO!=null &&
              this.DADOS_DESTINO.equals(other.getDADOS_DESTINO()))) &&
            ((this.CPF_CNPJ_DESTINO==null && other.getCPF_CNPJ_DESTINO()==null) || 
             (this.CPF_CNPJ_DESTINO!=null &&
              this.CPF_CNPJ_DESTINO.equals(other.getCPF_CNPJ_DESTINO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getDADOS_ORIGEM() != null) {
            _hashCode += getDADOS_ORIGEM().hashCode();
        }
        if (getDADOS_DESTINO() != null) {
            _hashCode += getDADOS_DESTINO().hashCode();
        }
        if (getCPF_CNPJ_DESTINO() != null) {
            _hashCode += getCPF_CNPJ_DESTINO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_poupanca_programada_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_poupanca_programada_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_ORIGEM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_ORIGEM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_conta_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_conta_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF_CNPJ_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF_CNPJ_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
