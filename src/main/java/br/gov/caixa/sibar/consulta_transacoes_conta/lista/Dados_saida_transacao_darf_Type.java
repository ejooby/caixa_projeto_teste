/**
 * Dados_saida_transacao_darf_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Dados_saida_transacao_darf_Type  implements java.io.Serializable {
    private java.lang.Long NSU;

    private java.lang.String TIPO;

    private java.lang.Integer CODIGO_RECEITA;

    private java.lang.Long CPF_CNPJ;

    private java.util.Date DATA_APURACAO;

    private java.math.BigDecimal VALOR;

    private java.math.BigDecimal PRINCIPAL;

    private java.math.BigDecimal MULTA;

    private java.math.BigDecimal JUROS;

    private java.lang.String REFERENCIA;

    private java.util.Date DATA_PAGAMENTO;

    private java.math.BigDecimal RECEITA_BRUTA;

    private java.math.BigDecimal PERCENTUAL;

    private java.lang.String CONTRIBUINTE;

    private java.lang.Short DDD;

    private java.lang.Integer TELEFONE;

    private java.lang.String IDENTIFICACAO;

    private java.util.Calendar DATA_TRANSACAO;

    private java.lang.String CANAL_ORIGEM;

    private java.lang.Integer NUMERO_DOCUMENTO;

    private java.util.Date DATA_VENCIMENTO;

    public Dados_saida_transacao_darf_Type() {
    }

    public Dados_saida_transacao_darf_Type(
           java.lang.Long NSU,
           java.lang.String TIPO,
           java.lang.Integer CODIGO_RECEITA,
           java.lang.Long CPF_CNPJ,
           java.util.Date DATA_APURACAO,
           java.math.BigDecimal VALOR,
           java.math.BigDecimal PRINCIPAL,
           java.math.BigDecimal MULTA,
           java.math.BigDecimal JUROS,
           java.lang.String REFERENCIA,
           java.util.Date DATA_PAGAMENTO,
           java.math.BigDecimal RECEITA_BRUTA,
           java.math.BigDecimal PERCENTUAL,
           java.lang.String CONTRIBUINTE,
           java.lang.Short DDD,
           java.lang.Integer TELEFONE,
           java.lang.String IDENTIFICACAO,
           java.util.Calendar DATA_TRANSACAO,
           java.lang.String CANAL_ORIGEM,
           java.lang.Integer NUMERO_DOCUMENTO,
           java.util.Date DATA_VENCIMENTO) {
           this.NSU = NSU;
           this.TIPO = TIPO;
           this.CODIGO_RECEITA = CODIGO_RECEITA;
           this.CPF_CNPJ = CPF_CNPJ;
           this.DATA_APURACAO = DATA_APURACAO;
           this.VALOR = VALOR;
           this.PRINCIPAL = PRINCIPAL;
           this.MULTA = MULTA;
           this.JUROS = JUROS;
           this.REFERENCIA = REFERENCIA;
           this.DATA_PAGAMENTO = DATA_PAGAMENTO;
           this.RECEITA_BRUTA = RECEITA_BRUTA;
           this.PERCENTUAL = PERCENTUAL;
           this.CONTRIBUINTE = CONTRIBUINTE;
           this.DDD = DDD;
           this.TELEFONE = TELEFONE;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.DATA_TRANSACAO = DATA_TRANSACAO;
           this.CANAL_ORIGEM = CANAL_ORIGEM;
           this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
           this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }


    /**
     * Gets the NSU value for this Dados_saida_transacao_darf_Type.
     * 
     * @return NSU
     */
    public java.lang.Long getNSU() {
        return NSU;
    }


    /**
     * Sets the NSU value for this Dados_saida_transacao_darf_Type.
     * 
     * @param NSU
     */
    public void setNSU(java.lang.Long NSU) {
        this.NSU = NSU;
    }


    /**
     * Gets the TIPO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return TIPO
     */
    public java.lang.String getTIPO() {
        return TIPO;
    }


    /**
     * Sets the TIPO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param TIPO
     */
    public void setTIPO(java.lang.String TIPO) {
        this.TIPO = TIPO;
    }


    /**
     * Gets the CODIGO_RECEITA value for this Dados_saida_transacao_darf_Type.
     * 
     * @return CODIGO_RECEITA
     */
    public java.lang.Integer getCODIGO_RECEITA() {
        return CODIGO_RECEITA;
    }


    /**
     * Sets the CODIGO_RECEITA value for this Dados_saida_transacao_darf_Type.
     * 
     * @param CODIGO_RECEITA
     */
    public void setCODIGO_RECEITA(java.lang.Integer CODIGO_RECEITA) {
        this.CODIGO_RECEITA = CODIGO_RECEITA;
    }


    /**
     * Gets the CPF_CNPJ value for this Dados_saida_transacao_darf_Type.
     * 
     * @return CPF_CNPJ
     */
    public java.lang.Long getCPF_CNPJ() {
        return CPF_CNPJ;
    }


    /**
     * Sets the CPF_CNPJ value for this Dados_saida_transacao_darf_Type.
     * 
     * @param CPF_CNPJ
     */
    public void setCPF_CNPJ(java.lang.Long CPF_CNPJ) {
        this.CPF_CNPJ = CPF_CNPJ;
    }


    /**
     * Gets the DATA_APURACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return DATA_APURACAO
     */
    public java.util.Date getDATA_APURACAO() {
        return DATA_APURACAO;
    }


    /**
     * Sets the DATA_APURACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param DATA_APURACAO
     */
    public void setDATA_APURACAO(java.util.Date DATA_APURACAO) {
        this.DATA_APURACAO = DATA_APURACAO;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_darf_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_darf_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the PRINCIPAL value for this Dados_saida_transacao_darf_Type.
     * 
     * @return PRINCIPAL
     */
    public java.math.BigDecimal getPRINCIPAL() {
        return PRINCIPAL;
    }


    /**
     * Sets the PRINCIPAL value for this Dados_saida_transacao_darf_Type.
     * 
     * @param PRINCIPAL
     */
    public void setPRINCIPAL(java.math.BigDecimal PRINCIPAL) {
        this.PRINCIPAL = PRINCIPAL;
    }


    /**
     * Gets the MULTA value for this Dados_saida_transacao_darf_Type.
     * 
     * @return MULTA
     */
    public java.math.BigDecimal getMULTA() {
        return MULTA;
    }


    /**
     * Sets the MULTA value for this Dados_saida_transacao_darf_Type.
     * 
     * @param MULTA
     */
    public void setMULTA(java.math.BigDecimal MULTA) {
        this.MULTA = MULTA;
    }


    /**
     * Gets the JUROS value for this Dados_saida_transacao_darf_Type.
     * 
     * @return JUROS
     */
    public java.math.BigDecimal getJUROS() {
        return JUROS;
    }


    /**
     * Sets the JUROS value for this Dados_saida_transacao_darf_Type.
     * 
     * @param JUROS
     */
    public void setJUROS(java.math.BigDecimal JUROS) {
        this.JUROS = JUROS;
    }


    /**
     * Gets the REFERENCIA value for this Dados_saida_transacao_darf_Type.
     * 
     * @return REFERENCIA
     */
    public java.lang.String getREFERENCIA() {
        return REFERENCIA;
    }


    /**
     * Sets the REFERENCIA value for this Dados_saida_transacao_darf_Type.
     * 
     * @param REFERENCIA
     */
    public void setREFERENCIA(java.lang.String REFERENCIA) {
        this.REFERENCIA = REFERENCIA;
    }


    /**
     * Gets the DATA_PAGAMENTO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return DATA_PAGAMENTO
     */
    public java.util.Date getDATA_PAGAMENTO() {
        return DATA_PAGAMENTO;
    }


    /**
     * Sets the DATA_PAGAMENTO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param DATA_PAGAMENTO
     */
    public void setDATA_PAGAMENTO(java.util.Date DATA_PAGAMENTO) {
        this.DATA_PAGAMENTO = DATA_PAGAMENTO;
    }


    /**
     * Gets the RECEITA_BRUTA value for this Dados_saida_transacao_darf_Type.
     * 
     * @return RECEITA_BRUTA
     */
    public java.math.BigDecimal getRECEITA_BRUTA() {
        return RECEITA_BRUTA;
    }


    /**
     * Sets the RECEITA_BRUTA value for this Dados_saida_transacao_darf_Type.
     * 
     * @param RECEITA_BRUTA
     */
    public void setRECEITA_BRUTA(java.math.BigDecimal RECEITA_BRUTA) {
        this.RECEITA_BRUTA = RECEITA_BRUTA;
    }


    /**
     * Gets the PERCENTUAL value for this Dados_saida_transacao_darf_Type.
     * 
     * @return PERCENTUAL
     */
    public java.math.BigDecimal getPERCENTUAL() {
        return PERCENTUAL;
    }


    /**
     * Sets the PERCENTUAL value for this Dados_saida_transacao_darf_Type.
     * 
     * @param PERCENTUAL
     */
    public void setPERCENTUAL(java.math.BigDecimal PERCENTUAL) {
        this.PERCENTUAL = PERCENTUAL;
    }


    /**
     * Gets the CONTRIBUINTE value for this Dados_saida_transacao_darf_Type.
     * 
     * @return CONTRIBUINTE
     */
    public java.lang.String getCONTRIBUINTE() {
        return CONTRIBUINTE;
    }


    /**
     * Sets the CONTRIBUINTE value for this Dados_saida_transacao_darf_Type.
     * 
     * @param CONTRIBUINTE
     */
    public void setCONTRIBUINTE(java.lang.String CONTRIBUINTE) {
        this.CONTRIBUINTE = CONTRIBUINTE;
    }


    /**
     * Gets the DDD value for this Dados_saida_transacao_darf_Type.
     * 
     * @return DDD
     */
    public java.lang.Short getDDD() {
        return DDD;
    }


    /**
     * Sets the DDD value for this Dados_saida_transacao_darf_Type.
     * 
     * @param DDD
     */
    public void setDDD(java.lang.Short DDD) {
        this.DDD = DDD;
    }


    /**
     * Gets the TELEFONE value for this Dados_saida_transacao_darf_Type.
     * 
     * @return TELEFONE
     */
    public java.lang.Integer getTELEFONE() {
        return TELEFONE;
    }


    /**
     * Sets the TELEFONE value for this Dados_saida_transacao_darf_Type.
     * 
     * @param TELEFONE
     */
    public void setTELEFONE(java.lang.Integer TELEFONE) {
        this.TELEFONE = TELEFONE;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the DATA_TRANSACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return DATA_TRANSACAO
     */
    public java.util.Calendar getDATA_TRANSACAO() {
        return DATA_TRANSACAO;
    }


    /**
     * Sets the DATA_TRANSACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param DATA_TRANSACAO
     */
    public void setDATA_TRANSACAO(java.util.Calendar DATA_TRANSACAO) {
        this.DATA_TRANSACAO = DATA_TRANSACAO;
    }


    /**
     * Gets the CANAL_ORIGEM value for this Dados_saida_transacao_darf_Type.
     * 
     * @return CANAL_ORIGEM
     */
    public java.lang.String getCANAL_ORIGEM() {
        return CANAL_ORIGEM;
    }


    /**
     * Sets the CANAL_ORIGEM value for this Dados_saida_transacao_darf_Type.
     * 
     * @param CANAL_ORIGEM
     */
    public void setCANAL_ORIGEM(java.lang.String CANAL_ORIGEM) {
        this.CANAL_ORIGEM = CANAL_ORIGEM;
    }


    /**
     * Gets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return NUMERO_DOCUMENTO
     */
    public java.lang.Integer getNUMERO_DOCUMENTO() {
        return NUMERO_DOCUMENTO;
    }


    /**
     * Sets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param NUMERO_DOCUMENTO
     */
    public void setNUMERO_DOCUMENTO(java.lang.Integer NUMERO_DOCUMENTO) {
        this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
    }


    /**
     * Gets the DATA_VENCIMENTO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return DATA_VENCIMENTO
     */
    public java.util.Date getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }


    /**
     * Sets the DATA_VENCIMENTO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param DATA_VENCIMENTO
     */
    public void setDATA_VENCIMENTO(java.util.Date DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_darf_Type)) return false;
        Dados_saida_transacao_darf_Type other = (Dados_saida_transacao_darf_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NSU==null && other.getNSU()==null) || 
             (this.NSU!=null &&
              this.NSU.equals(other.getNSU()))) &&
            ((this.TIPO==null && other.getTIPO()==null) || 
             (this.TIPO!=null &&
              this.TIPO.equals(other.getTIPO()))) &&
            ((this.CODIGO_RECEITA==null && other.getCODIGO_RECEITA()==null) || 
             (this.CODIGO_RECEITA!=null &&
              this.CODIGO_RECEITA.equals(other.getCODIGO_RECEITA()))) &&
            ((this.CPF_CNPJ==null && other.getCPF_CNPJ()==null) || 
             (this.CPF_CNPJ!=null &&
              this.CPF_CNPJ.equals(other.getCPF_CNPJ()))) &&
            ((this.DATA_APURACAO==null && other.getDATA_APURACAO()==null) || 
             (this.DATA_APURACAO!=null &&
              this.DATA_APURACAO.equals(other.getDATA_APURACAO()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.PRINCIPAL==null && other.getPRINCIPAL()==null) || 
             (this.PRINCIPAL!=null &&
              this.PRINCIPAL.equals(other.getPRINCIPAL()))) &&
            ((this.MULTA==null && other.getMULTA()==null) || 
             (this.MULTA!=null &&
              this.MULTA.equals(other.getMULTA()))) &&
            ((this.JUROS==null && other.getJUROS()==null) || 
             (this.JUROS!=null &&
              this.JUROS.equals(other.getJUROS()))) &&
            ((this.REFERENCIA==null && other.getREFERENCIA()==null) || 
             (this.REFERENCIA!=null &&
              this.REFERENCIA.equals(other.getREFERENCIA()))) &&
            ((this.DATA_PAGAMENTO==null && other.getDATA_PAGAMENTO()==null) || 
             (this.DATA_PAGAMENTO!=null &&
              this.DATA_PAGAMENTO.equals(other.getDATA_PAGAMENTO()))) &&
            ((this.RECEITA_BRUTA==null && other.getRECEITA_BRUTA()==null) || 
             (this.RECEITA_BRUTA!=null &&
              this.RECEITA_BRUTA.equals(other.getRECEITA_BRUTA()))) &&
            ((this.PERCENTUAL==null && other.getPERCENTUAL()==null) || 
             (this.PERCENTUAL!=null &&
              this.PERCENTUAL.equals(other.getPERCENTUAL()))) &&
            ((this.CONTRIBUINTE==null && other.getCONTRIBUINTE()==null) || 
             (this.CONTRIBUINTE!=null &&
              this.CONTRIBUINTE.equals(other.getCONTRIBUINTE()))) &&
            ((this.DDD==null && other.getDDD()==null) || 
             (this.DDD!=null &&
              this.DDD.equals(other.getDDD()))) &&
            ((this.TELEFONE==null && other.getTELEFONE()==null) || 
             (this.TELEFONE!=null &&
              this.TELEFONE.equals(other.getTELEFONE()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.DATA_TRANSACAO==null && other.getDATA_TRANSACAO()==null) || 
             (this.DATA_TRANSACAO!=null &&
              this.DATA_TRANSACAO.equals(other.getDATA_TRANSACAO()))) &&
            ((this.CANAL_ORIGEM==null && other.getCANAL_ORIGEM()==null) || 
             (this.CANAL_ORIGEM!=null &&
              this.CANAL_ORIGEM.equals(other.getCANAL_ORIGEM()))) &&
            ((this.NUMERO_DOCUMENTO==null && other.getNUMERO_DOCUMENTO()==null) || 
             (this.NUMERO_DOCUMENTO!=null &&
              this.NUMERO_DOCUMENTO.equals(other.getNUMERO_DOCUMENTO()))) &&
            ((this.DATA_VENCIMENTO==null && other.getDATA_VENCIMENTO()==null) || 
             (this.DATA_VENCIMENTO!=null &&
              this.DATA_VENCIMENTO.equals(other.getDATA_VENCIMENTO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNSU() != null) {
            _hashCode += getNSU().hashCode();
        }
        if (getTIPO() != null) {
            _hashCode += getTIPO().hashCode();
        }
        if (getCODIGO_RECEITA() != null) {
            _hashCode += getCODIGO_RECEITA().hashCode();
        }
        if (getCPF_CNPJ() != null) {
            _hashCode += getCPF_CNPJ().hashCode();
        }
        if (getDATA_APURACAO() != null) {
            _hashCode += getDATA_APURACAO().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getPRINCIPAL() != null) {
            _hashCode += getPRINCIPAL().hashCode();
        }
        if (getMULTA() != null) {
            _hashCode += getMULTA().hashCode();
        }
        if (getJUROS() != null) {
            _hashCode += getJUROS().hashCode();
        }
        if (getREFERENCIA() != null) {
            _hashCode += getREFERENCIA().hashCode();
        }
        if (getDATA_PAGAMENTO() != null) {
            _hashCode += getDATA_PAGAMENTO().hashCode();
        }
        if (getRECEITA_BRUTA() != null) {
            _hashCode += getRECEITA_BRUTA().hashCode();
        }
        if (getPERCENTUAL() != null) {
            _hashCode += getPERCENTUAL().hashCode();
        }
        if (getCONTRIBUINTE() != null) {
            _hashCode += getCONTRIBUINTE().hashCode();
        }
        if (getDDD() != null) {
            _hashCode += getDDD().hashCode();
        }
        if (getTELEFONE() != null) {
            _hashCode += getTELEFONE().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getDATA_TRANSACAO() != null) {
            _hashCode += getDATA_TRANSACAO().hashCode();
        }
        if (getCANAL_ORIGEM() != null) {
            _hashCode += getCANAL_ORIGEM().hashCode();
        }
        if (getNUMERO_DOCUMENTO() != null) {
            _hashCode += getNUMERO_DOCUMENTO().hashCode();
        }
        if (getDATA_VENCIMENTO() != null) {
            _hashCode += getDATA_VENCIMENTO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_darf_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_transacao_darf_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_RECEITA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_RECEITA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF_CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF_CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_APURACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_APURACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRINCIPAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRINCIPAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("JUROS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "JUROS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REFERENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "REFERENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_PAGAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_PAGAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECEITA_BRUTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RECEITA_BRUTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PERCENTUAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PERCENTUAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTRIBUINTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTRIBUINTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDD");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DDD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TELEFONE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TELEFONE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CANAL_ORIGEM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CANAL_ORIGEM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO_DOCUMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO_DOCUMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
