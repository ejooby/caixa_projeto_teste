/**
 * Conta_ted_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Conta_ted_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedShort BANCO;

    private org.apache.axis.types.UnsignedShort AGENCIA;

    private org.apache.axis.types.UnsignedLong CONTA;

    private byte DV;

    public Conta_ted_Type() {
    }

    public Conta_ted_Type(
           org.apache.axis.types.UnsignedShort BANCO,
           org.apache.axis.types.UnsignedShort AGENCIA,
           org.apache.axis.types.UnsignedLong CONTA,
           byte DV) {
           this.BANCO = BANCO;
           this.AGENCIA = AGENCIA;
           this.CONTA = CONTA;
           this.DV = DV;
    }


    /**
     * Gets the BANCO value for this Conta_ted_Type.
     * 
     * @return BANCO
     */
    public org.apache.axis.types.UnsignedShort getBANCO() {
        return BANCO;
    }


    /**
     * Sets the BANCO value for this Conta_ted_Type.
     * 
     * @param BANCO
     */
    public void setBANCO(org.apache.axis.types.UnsignedShort BANCO) {
        this.BANCO = BANCO;
    }


    /**
     * Gets the AGENCIA value for this Conta_ted_Type.
     * 
     * @return AGENCIA
     */
    public org.apache.axis.types.UnsignedShort getAGENCIA() {
        return AGENCIA;
    }


    /**
     * Sets the AGENCIA value for this Conta_ted_Type.
     * 
     * @param AGENCIA
     */
    public void setAGENCIA(org.apache.axis.types.UnsignedShort AGENCIA) {
        this.AGENCIA = AGENCIA;
    }


    /**
     * Gets the CONTA value for this Conta_ted_Type.
     * 
     * @return CONTA
     */
    public org.apache.axis.types.UnsignedLong getCONTA() {
        return CONTA;
    }


    /**
     * Sets the CONTA value for this Conta_ted_Type.
     * 
     * @param CONTA
     */
    public void setCONTA(org.apache.axis.types.UnsignedLong CONTA) {
        this.CONTA = CONTA;
    }


    /**
     * Gets the DV value for this Conta_ted_Type.
     * 
     * @return DV
     */
    public byte getDV() {
        return DV;
    }


    /**
     * Sets the DV value for this Conta_ted_Type.
     * 
     * @param DV
     */
    public void setDV(byte DV) {
        this.DV = DV;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Conta_ted_Type)) return false;
        Conta_ted_Type other = (Conta_ted_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.BANCO==null && other.getBANCO()==null) || 
             (this.BANCO!=null &&
              this.BANCO.equals(other.getBANCO()))) &&
            ((this.AGENCIA==null && other.getAGENCIA()==null) || 
             (this.AGENCIA!=null &&
              this.AGENCIA.equals(other.getAGENCIA()))) &&
            ((this.CONTA==null && other.getCONTA()==null) || 
             (this.CONTA!=null &&
              this.CONTA.equals(other.getCONTA()))) &&
            this.DV == other.getDV();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBANCO() != null) {
            _hashCode += getBANCO().hashCode();
        }
        if (getAGENCIA() != null) {
            _hashCode += getAGENCIA().hashCode();
        }
        if (getCONTA() != null) {
            _hashCode += getCONTA().hashCode();
        }
        _hashCode += getDV();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Conta_ted_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "conta_ted_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BANCO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BANCO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AGENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AGENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DV");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
