/**
 * Dados_saida_transacao_carteira_eletronica_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_carteira_eletronica_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedByte SEGMENTO;

    private org.apache.axis.types.UnsignedInt CONVENIO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Telefone_Type TELEFONE;

    private java.math.BigDecimal VALOR_PAGAMENTO;

    private java.util.Date DATA_PAGAMENTO;

    private java.util.Date DATA_EFETIVACAO;

    private java.lang.String TIPO_CARTEIRA;

    private java.lang.String NOME_BENEFICIARIO;

    public Dados_saida_transacao_carteira_eletronica_Type() {
    }

    public Dados_saida_transacao_carteira_eletronica_Type(
           org.apache.axis.types.UnsignedByte SEGMENTO,
           org.apache.axis.types.UnsignedInt CONVENIO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Telefone_Type TELEFONE,
           java.math.BigDecimal VALOR_PAGAMENTO,
           java.util.Date DATA_PAGAMENTO,
           java.util.Date DATA_EFETIVACAO,
           java.lang.String TIPO_CARTEIRA,
           java.lang.String NOME_BENEFICIARIO) {
           this.SEGMENTO = SEGMENTO;
           this.CONVENIO = CONVENIO;
           this.TELEFONE = TELEFONE;
           this.VALOR_PAGAMENTO = VALOR_PAGAMENTO;
           this.DATA_PAGAMENTO = DATA_PAGAMENTO;
           this.DATA_EFETIVACAO = DATA_EFETIVACAO;
           this.TIPO_CARTEIRA = TIPO_CARTEIRA;
           this.NOME_BENEFICIARIO = NOME_BENEFICIARIO;
    }


    /**
     * Gets the SEGMENTO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @return SEGMENTO
     */
    public org.apache.axis.types.UnsignedByte getSEGMENTO() {
        return SEGMENTO;
    }


    /**
     * Sets the SEGMENTO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @param SEGMENTO
     */
    public void setSEGMENTO(org.apache.axis.types.UnsignedByte SEGMENTO) {
        this.SEGMENTO = SEGMENTO;
    }


    /**
     * Gets the CONVENIO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @return CONVENIO
     */
    public org.apache.axis.types.UnsignedInt getCONVENIO() {
        return CONVENIO;
    }


    /**
     * Sets the CONVENIO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @param CONVENIO
     */
    public void setCONVENIO(org.apache.axis.types.UnsignedInt CONVENIO) {
        this.CONVENIO = CONVENIO;
    }


    /**
     * Gets the TELEFONE value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @return TELEFONE
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Telefone_Type getTELEFONE() {
        return TELEFONE;
    }


    /**
     * Sets the TELEFONE value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @param TELEFONE
     */
    public void setTELEFONE(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Telefone_Type TELEFONE) {
        this.TELEFONE = TELEFONE;
    }


    /**
     * Gets the VALOR_PAGAMENTO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @return VALOR_PAGAMENTO
     */
    public java.math.BigDecimal getVALOR_PAGAMENTO() {
        return VALOR_PAGAMENTO;
    }


    /**
     * Sets the VALOR_PAGAMENTO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @param VALOR_PAGAMENTO
     */
    public void setVALOR_PAGAMENTO(java.math.BigDecimal VALOR_PAGAMENTO) {
        this.VALOR_PAGAMENTO = VALOR_PAGAMENTO;
    }


    /**
     * Gets the DATA_PAGAMENTO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @return DATA_PAGAMENTO
     */
    public java.util.Date getDATA_PAGAMENTO() {
        return DATA_PAGAMENTO;
    }


    /**
     * Sets the DATA_PAGAMENTO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @param DATA_PAGAMENTO
     */
    public void setDATA_PAGAMENTO(java.util.Date DATA_PAGAMENTO) {
        this.DATA_PAGAMENTO = DATA_PAGAMENTO;
    }


    /**
     * Gets the DATA_EFETIVACAO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @return DATA_EFETIVACAO
     */
    public java.util.Date getDATA_EFETIVACAO() {
        return DATA_EFETIVACAO;
    }


    /**
     * Sets the DATA_EFETIVACAO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @param DATA_EFETIVACAO
     */
    public void setDATA_EFETIVACAO(java.util.Date DATA_EFETIVACAO) {
        this.DATA_EFETIVACAO = DATA_EFETIVACAO;
    }


    /**
     * Gets the TIPO_CARTEIRA value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @return TIPO_CARTEIRA
     */
    public java.lang.String getTIPO_CARTEIRA() {
        return TIPO_CARTEIRA;
    }


    /**
     * Sets the TIPO_CARTEIRA value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @param TIPO_CARTEIRA
     */
    public void setTIPO_CARTEIRA(java.lang.String TIPO_CARTEIRA) {
        this.TIPO_CARTEIRA = TIPO_CARTEIRA;
    }


    /**
     * Gets the NOME_BENEFICIARIO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @return NOME_BENEFICIARIO
     */
    public java.lang.String getNOME_BENEFICIARIO() {
        return NOME_BENEFICIARIO;
    }


    /**
     * Sets the NOME_BENEFICIARIO value for this Dados_saida_transacao_carteira_eletronica_Type.
     * 
     * @param NOME_BENEFICIARIO
     */
    public void setNOME_BENEFICIARIO(java.lang.String NOME_BENEFICIARIO) {
        this.NOME_BENEFICIARIO = NOME_BENEFICIARIO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_carteira_eletronica_Type)) return false;
        Dados_saida_transacao_carteira_eletronica_Type other = (Dados_saida_transacao_carteira_eletronica_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.SEGMENTO==null && other.getSEGMENTO()==null) || 
             (this.SEGMENTO!=null &&
              this.SEGMENTO.equals(other.getSEGMENTO()))) &&
            ((this.CONVENIO==null && other.getCONVENIO()==null) || 
             (this.CONVENIO!=null &&
              this.CONVENIO.equals(other.getCONVENIO()))) &&
            ((this.TELEFONE==null && other.getTELEFONE()==null) || 
             (this.TELEFONE!=null &&
              this.TELEFONE.equals(other.getTELEFONE()))) &&
            ((this.VALOR_PAGAMENTO==null && other.getVALOR_PAGAMENTO()==null) || 
             (this.VALOR_PAGAMENTO!=null &&
              this.VALOR_PAGAMENTO.equals(other.getVALOR_PAGAMENTO()))) &&
            ((this.DATA_PAGAMENTO==null && other.getDATA_PAGAMENTO()==null) || 
             (this.DATA_PAGAMENTO!=null &&
              this.DATA_PAGAMENTO.equals(other.getDATA_PAGAMENTO()))) &&
            ((this.DATA_EFETIVACAO==null && other.getDATA_EFETIVACAO()==null) || 
             (this.DATA_EFETIVACAO!=null &&
              this.DATA_EFETIVACAO.equals(other.getDATA_EFETIVACAO()))) &&
            ((this.TIPO_CARTEIRA==null && other.getTIPO_CARTEIRA()==null) || 
             (this.TIPO_CARTEIRA!=null &&
              this.TIPO_CARTEIRA.equals(other.getTIPO_CARTEIRA()))) &&
            ((this.NOME_BENEFICIARIO==null && other.getNOME_BENEFICIARIO()==null) || 
             (this.NOME_BENEFICIARIO!=null &&
              this.NOME_BENEFICIARIO.equals(other.getNOME_BENEFICIARIO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSEGMENTO() != null) {
            _hashCode += getSEGMENTO().hashCode();
        }
        if (getCONVENIO() != null) {
            _hashCode += getCONVENIO().hashCode();
        }
        if (getTELEFONE() != null) {
            _hashCode += getTELEFONE().hashCode();
        }
        if (getVALOR_PAGAMENTO() != null) {
            _hashCode += getVALOR_PAGAMENTO().hashCode();
        }
        if (getDATA_PAGAMENTO() != null) {
            _hashCode += getDATA_PAGAMENTO().hashCode();
        }
        if (getDATA_EFETIVACAO() != null) {
            _hashCode += getDATA_EFETIVACAO().hashCode();
        }
        if (getTIPO_CARTEIRA() != null) {
            _hashCode += getTIPO_CARTEIRA().hashCode();
        }
        if (getNOME_BENEFICIARIO() != null) {
            _hashCode += getNOME_BENEFICIARIO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_carteira_eletronica_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_carteira_eletronica_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONVENIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONVENIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TELEFONE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TELEFONE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "telefone_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_PAGAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_PAGAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_PAGAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_PAGAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_EFETIVACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_EFETIVACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_CARTEIRA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_CARTEIRA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_BENEFICIARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_BENEFICIARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
