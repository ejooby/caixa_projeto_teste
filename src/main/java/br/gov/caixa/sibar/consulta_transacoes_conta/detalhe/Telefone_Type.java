/**
 * Telefone_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Telefone_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedByte DDD;

    private org.apache.axis.types.UnsignedInt NUMERO;

    public Telefone_Type() {
    }

    public Telefone_Type(
           org.apache.axis.types.UnsignedByte DDD,
           org.apache.axis.types.UnsignedInt NUMERO) {
           this.DDD = DDD;
           this.NUMERO = NUMERO;
    }


    /**
     * Gets the DDD value for this Telefone_Type.
     * 
     * @return DDD
     */
    public org.apache.axis.types.UnsignedByte getDDD() {
        return DDD;
    }


    /**
     * Sets the DDD value for this Telefone_Type.
     * 
     * @param DDD
     */
    public void setDDD(org.apache.axis.types.UnsignedByte DDD) {
        this.DDD = DDD;
    }


    /**
     * Gets the NUMERO value for this Telefone_Type.
     * 
     * @return NUMERO
     */
    public org.apache.axis.types.UnsignedInt getNUMERO() {
        return NUMERO;
    }


    /**
     * Sets the NUMERO value for this Telefone_Type.
     * 
     * @param NUMERO
     */
    public void setNUMERO(org.apache.axis.types.UnsignedInt NUMERO) {
        this.NUMERO = NUMERO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Telefone_Type)) return false;
        Telefone_Type other = (Telefone_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DDD==null && other.getDDD()==null) || 
             (this.DDD!=null &&
              this.DDD.equals(other.getDDD()))) &&
            ((this.NUMERO==null && other.getNUMERO()==null) || 
             (this.NUMERO!=null &&
              this.NUMERO.equals(other.getNUMERO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDDD() != null) {
            _hashCode += getDDD().hashCode();
        }
        if (getNUMERO() != null) {
            _hashCode += getNUMERO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Telefone_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "telefone_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDD");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DDD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
