/**
 * Dados_saida_transacao_deposito_judicial_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_deposito_judicial_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_judicial_Type ID_CONTA_DEBITO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_judicial_Type ID_CONTA_CREDITO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type CONTA_CREDITO;

    private org.apache.axis.types.UnsignedLong ID_DEPOSITO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Processo_Type PROCESSO;

    private java.util.Date DATA_EFETIVACAO;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_deposito_judicial_Type() {
    }

    public Dados_saida_transacao_deposito_judicial_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_judicial_Type ID_CONTA_DEBITO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_judicial_Type ID_CONTA_CREDITO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type CONTA_CREDITO,
           org.apache.axis.types.UnsignedLong ID_DEPOSITO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Processo_Type PROCESSO,
           java.util.Date DATA_EFETIVACAO,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.ID_CONTA_DEBITO = ID_CONTA_DEBITO;
           this.ID_CONTA_CREDITO = ID_CONTA_CREDITO;
           this.CONTA_CREDITO = CONTA_CREDITO;
           this.ID_DEPOSITO = ID_DEPOSITO;
           this.PROCESSO = PROCESSO;
           this.DATA_EFETIVACAO = DATA_EFETIVACAO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the ID_CONTA_DEBITO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @return ID_CONTA_DEBITO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_judicial_Type getID_CONTA_DEBITO() {
        return ID_CONTA_DEBITO;
    }


    /**
     * Sets the ID_CONTA_DEBITO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @param ID_CONTA_DEBITO
     */
    public void setID_CONTA_DEBITO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_judicial_Type ID_CONTA_DEBITO) {
        this.ID_CONTA_DEBITO = ID_CONTA_DEBITO;
    }


    /**
     * Gets the ID_CONTA_CREDITO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @return ID_CONTA_CREDITO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_judicial_Type getID_CONTA_CREDITO() {
        return ID_CONTA_CREDITO;
    }


    /**
     * Sets the ID_CONTA_CREDITO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @param ID_CONTA_CREDITO
     */
    public void setID_CONTA_CREDITO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_judicial_Type ID_CONTA_CREDITO) {
        this.ID_CONTA_CREDITO = ID_CONTA_CREDITO;
    }


    /**
     * Gets the CONTA_CREDITO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @return CONTA_CREDITO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type getCONTA_CREDITO() {
        return CONTA_CREDITO;
    }


    /**
     * Sets the CONTA_CREDITO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @param CONTA_CREDITO
     */
    public void setCONTA_CREDITO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type CONTA_CREDITO) {
        this.CONTA_CREDITO = CONTA_CREDITO;
    }


    /**
     * Gets the ID_DEPOSITO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @return ID_DEPOSITO
     */
    public org.apache.axis.types.UnsignedLong getID_DEPOSITO() {
        return ID_DEPOSITO;
    }


    /**
     * Sets the ID_DEPOSITO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @param ID_DEPOSITO
     */
    public void setID_DEPOSITO(org.apache.axis.types.UnsignedLong ID_DEPOSITO) {
        this.ID_DEPOSITO = ID_DEPOSITO;
    }


    /**
     * Gets the PROCESSO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @return PROCESSO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Processo_Type getPROCESSO() {
        return PROCESSO;
    }


    /**
     * Sets the PROCESSO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @param PROCESSO
     */
    public void setPROCESSO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Processo_Type PROCESSO) {
        this.PROCESSO = PROCESSO;
    }


    /**
     * Gets the DATA_EFETIVACAO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @return DATA_EFETIVACAO
     */
    public java.util.Date getDATA_EFETIVACAO() {
        return DATA_EFETIVACAO;
    }


    /**
     * Sets the DATA_EFETIVACAO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @param DATA_EFETIVACAO
     */
    public void setDATA_EFETIVACAO(java.util.Date DATA_EFETIVACAO) {
        this.DATA_EFETIVACAO = DATA_EFETIVACAO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_deposito_judicial_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_deposito_judicial_Type)) return false;
        Dados_saida_transacao_deposito_judicial_Type other = (Dados_saida_transacao_deposito_judicial_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ID_CONTA_DEBITO==null && other.getID_CONTA_DEBITO()==null) || 
             (this.ID_CONTA_DEBITO!=null &&
              this.ID_CONTA_DEBITO.equals(other.getID_CONTA_DEBITO()))) &&
            ((this.ID_CONTA_CREDITO==null && other.getID_CONTA_CREDITO()==null) || 
             (this.ID_CONTA_CREDITO!=null &&
              this.ID_CONTA_CREDITO.equals(other.getID_CONTA_CREDITO()))) &&
            ((this.CONTA_CREDITO==null && other.getCONTA_CREDITO()==null) || 
             (this.CONTA_CREDITO!=null &&
              this.CONTA_CREDITO.equals(other.getCONTA_CREDITO()))) &&
            ((this.ID_DEPOSITO==null && other.getID_DEPOSITO()==null) || 
             (this.ID_DEPOSITO!=null &&
              this.ID_DEPOSITO.equals(other.getID_DEPOSITO()))) &&
            ((this.PROCESSO==null && other.getPROCESSO()==null) || 
             (this.PROCESSO!=null &&
              this.PROCESSO.equals(other.getPROCESSO()))) &&
            ((this.DATA_EFETIVACAO==null && other.getDATA_EFETIVACAO()==null) || 
             (this.DATA_EFETIVACAO!=null &&
              this.DATA_EFETIVACAO.equals(other.getDATA_EFETIVACAO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getID_CONTA_DEBITO() != null) {
            _hashCode += getID_CONTA_DEBITO().hashCode();
        }
        if (getID_CONTA_CREDITO() != null) {
            _hashCode += getID_CONTA_CREDITO().hashCode();
        }
        if (getCONTA_CREDITO() != null) {
            _hashCode += getCONTA_CREDITO().hashCode();
        }
        if (getID_DEPOSITO() != null) {
            _hashCode += getID_DEPOSITO().hashCode();
        }
        if (getPROCESSO() != null) {
            _hashCode += getPROCESSO().hashCode();
        }
        if (getDATA_EFETIVACAO() != null) {
            _hashCode += getDATA_EFETIVACAO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_deposito_judicial_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_deposito_judicial_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_CONTA_DEBITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ID_CONTA_DEBITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "conta_judicial_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_CONTA_CREDITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ID_CONTA_CREDITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "conta_judicial_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA_CREDITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA_CREDITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "conta_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_DEPOSITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ID_DEPOSITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROCESSO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PROCESSO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "processo_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_EFETIVACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_EFETIVACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
