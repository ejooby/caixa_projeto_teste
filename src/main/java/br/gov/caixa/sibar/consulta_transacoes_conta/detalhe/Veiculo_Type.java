/**
 * Veiculo_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Veiculo_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedLong RENAVAM;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Placa_Type PLACA;

    public Veiculo_Type() {
    }

    public Veiculo_Type(
           org.apache.axis.types.UnsignedLong RENAVAM,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Placa_Type PLACA) {
           this.RENAVAM = RENAVAM;
           this.PLACA = PLACA;
    }


    /**
     * Gets the RENAVAM value for this Veiculo_Type.
     * 
     * @return RENAVAM
     */
    public org.apache.axis.types.UnsignedLong getRENAVAM() {
        return RENAVAM;
    }


    /**
     * Sets the RENAVAM value for this Veiculo_Type.
     * 
     * @param RENAVAM
     */
    public void setRENAVAM(org.apache.axis.types.UnsignedLong RENAVAM) {
        this.RENAVAM = RENAVAM;
    }


    /**
     * Gets the PLACA value for this Veiculo_Type.
     * 
     * @return PLACA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Placa_Type getPLACA() {
        return PLACA;
    }


    /**
     * Sets the PLACA value for this Veiculo_Type.
     * 
     * @param PLACA
     */
    public void setPLACA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Placa_Type PLACA) {
        this.PLACA = PLACA;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Veiculo_Type)) return false;
        Veiculo_Type other = (Veiculo_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.RENAVAM==null && other.getRENAVAM()==null) || 
             (this.RENAVAM!=null &&
              this.RENAVAM.equals(other.getRENAVAM()))) &&
            ((this.PLACA==null && other.getPLACA()==null) || 
             (this.PLACA!=null &&
              this.PLACA.equals(other.getPLACA())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRENAVAM() != null) {
            _hashCode += getRENAVAM().hashCode();
        }
        if (getPLACA() != null) {
            _hashCode += getPLACA().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Veiculo_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "veiculo_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RENAVAM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RENAVAM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PLACA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PLACA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "placa_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
