/**
 * Dados_saida_transacao_doc.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Dados_saida_transacao_doc  implements java.io.Serializable ,TransacaoInt {
	
	
	public String tipoServico;
	public String data;
	public String banco_Descricao;
	public String chaveSeguranca;
	
	
	
    private java.lang.Integer NSU;

    private java.util.Date DATA_MOVIMENTO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Banco_type BANCO_DESTINO;

    private java.lang.Long CONTA;

    private java.lang.Short DV;

    private java.lang.Long CPF;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_cliente_Type CONTA_DEBITO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_cliente_Type CONTA_DESTINO;

    private java.math.BigDecimal VALOR;

    private java.math.BigDecimal TARIFA_EMISSAO;

    private java.math.BigDecimal TARIFA_AGENDAMENTO;

    private java.lang.String IDENTIFICACAO;

    private java.util.Calendar DATA_TRANSACAO;

    private java.lang.String CANAL_ORIGEM;

    private java.lang.Integer NUMERO_DOCUMENTO;

    private java.lang.String TIPO_DOC;

    private java.lang.String FINALIDADE;

    public Dados_saida_transacao_doc() {
    }

    public Dados_saida_transacao_doc(
           java.lang.Integer NSU,
           java.util.Date DATA_MOVIMENTO,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Banco_type BANCO_DESTINO,
           java.lang.Long CONTA,
           java.lang.Short DV,
           java.lang.Long CPF,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_cliente_Type CONTA_DEBITO,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_cliente_Type CONTA_DESTINO,
           java.math.BigDecimal VALOR,
           java.math.BigDecimal TARIFA_EMISSAO,
           java.math.BigDecimal TARIFA_AGENDAMENTO,
           java.lang.String IDENTIFICACAO,
           java.util.Calendar DATA_TRANSACAO,
           java.lang.String CANAL_ORIGEM,
           java.lang.Integer NUMERO_DOCUMENTO,
           java.lang.String TIPO_DOC,
           java.lang.String FINALIDADE) {
           this.NSU = NSU;
           this.DATA_MOVIMENTO = DATA_MOVIMENTO;
           this.BANCO_DESTINO = BANCO_DESTINO;
           this.CONTA = CONTA;
           this.DV = DV;
           this.CPF = CPF;
           this.CONTA_DEBITO = CONTA_DEBITO;
           this.CONTA_DESTINO = CONTA_DESTINO;
           this.VALOR = VALOR;
           this.TARIFA_EMISSAO = TARIFA_EMISSAO;
           this.TARIFA_AGENDAMENTO = TARIFA_AGENDAMENTO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.DATA_TRANSACAO = DATA_TRANSACAO;
           this.CANAL_ORIGEM = CANAL_ORIGEM;
           this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
           this.TIPO_DOC = TIPO_DOC;
           this.FINALIDADE = FINALIDADE;
    }


    /**
     * Gets the NSU value for this Dados_saida_transacao_doc.
     * 
     * @return NSU
     */
    public java.lang.Integer getNSU() {
        return NSU;
    }


    /**
     * Sets the NSU value for this Dados_saida_transacao_doc.
     * 
     * @param NSU
     */
    public void setNSU(java.lang.Integer NSU) {
        this.NSU = NSU;
    }


    /**
     * Gets the DATA_MOVIMENTO value for this Dados_saida_transacao_doc.
     * 
     * @return DATA_MOVIMENTO
     */
    public java.util.Date getDATA_MOVIMENTO() {
        return DATA_MOVIMENTO;
    }


    /**
     * Sets the DATA_MOVIMENTO value for this Dados_saida_transacao_doc.
     * 
     * @param DATA_MOVIMENTO
     */
    public void setDATA_MOVIMENTO(java.util.Date DATA_MOVIMENTO) {
        this.DATA_MOVIMENTO = DATA_MOVIMENTO;
    }


    /**
     * Gets the BANCO_DESTINO value for this Dados_saida_transacao_doc.
     * 
     * @return BANCO_DESTINO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Banco_type getBANCO_DESTINO() {
        return BANCO_DESTINO;
    }


    /**
     * Sets the BANCO_DESTINO value for this Dados_saida_transacao_doc.
     * 
     * @param BANCO_DESTINO
     */
    public void setBANCO_DESTINO(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Banco_type BANCO_DESTINO) {
        this.BANCO_DESTINO = BANCO_DESTINO;
    }


    /**
     * Gets the CONTA value for this Dados_saida_transacao_doc.
     * 
     * @return CONTA
     */
    public java.lang.Long getCONTA() {
        return CONTA;
    }


    /**
     * Sets the CONTA value for this Dados_saida_transacao_doc.
     * 
     * @param CONTA
     */
    public void setCONTA(java.lang.Long CONTA) {
        this.CONTA = CONTA;
    }


    /**
     * Gets the DV value for this Dados_saida_transacao_doc.
     * 
     * @return DV
     */
    public java.lang.Short getDV() {
        return DV;
    }


    /**
     * Sets the DV value for this Dados_saida_transacao_doc.
     * 
     * @param DV
     */
    public void setDV(java.lang.Short DV) {
        this.DV = DV;
    }


    /**
     * Gets the CPF value for this Dados_saida_transacao_doc.
     * 
     * @return CPF
     */
    public java.lang.Long getCPF() {
        return CPF;
    }


    /**
     * Sets the CPF value for this Dados_saida_transacao_doc.
     * 
     * @param CPF
     */
    public void setCPF(java.lang.Long CPF) {
        this.CPF = CPF;
    }


    /**
     * Gets the CONTA_DEBITO value for this Dados_saida_transacao_doc.
     * 
     * @return CONTA_DEBITO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_cliente_Type getCONTA_DEBITO() {
        return CONTA_DEBITO;
    }


    /**
     * Sets the CONTA_DEBITO value for this Dados_saida_transacao_doc.
     * 
     * @param CONTA_DEBITO
     */
    public void setCONTA_DEBITO(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_cliente_Type CONTA_DEBITO) {
        this.CONTA_DEBITO = CONTA_DEBITO;
    }


    /**
     * Gets the CONTA_DESTINO value for this Dados_saida_transacao_doc.
     * 
     * @return CONTA_DESTINO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_cliente_Type getCONTA_DESTINO() {
        return CONTA_DESTINO;
    }


    /**
     * Sets the CONTA_DESTINO value for this Dados_saida_transacao_doc.
     * 
     * @param CONTA_DESTINO
     */
    public void setCONTA_DESTINO(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_cliente_Type CONTA_DESTINO) {
        this.CONTA_DESTINO = CONTA_DESTINO;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_doc.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_doc.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the TARIFA_EMISSAO value for this Dados_saida_transacao_doc.
     * 
     * @return TARIFA_EMISSAO
     */
    public java.math.BigDecimal getTARIFA_EMISSAO() {
        return TARIFA_EMISSAO;
    }


    /**
     * Sets the TARIFA_EMISSAO value for this Dados_saida_transacao_doc.
     * 
     * @param TARIFA_EMISSAO
     */
    public void setTARIFA_EMISSAO(java.math.BigDecimal TARIFA_EMISSAO) {
        this.TARIFA_EMISSAO = TARIFA_EMISSAO;
    }


    /**
     * Gets the TARIFA_AGENDAMENTO value for this Dados_saida_transacao_doc.
     * 
     * @return TARIFA_AGENDAMENTO
     */
    public java.math.BigDecimal getTARIFA_AGENDAMENTO() {
        return TARIFA_AGENDAMENTO;
    }


    /**
     * Sets the TARIFA_AGENDAMENTO value for this Dados_saida_transacao_doc.
     * 
     * @param TARIFA_AGENDAMENTO
     */
    public void setTARIFA_AGENDAMENTO(java.math.BigDecimal TARIFA_AGENDAMENTO) {
        this.TARIFA_AGENDAMENTO = TARIFA_AGENDAMENTO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_doc.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_doc.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the DATA_TRANSACAO value for this Dados_saida_transacao_doc.
     * 
     * @return DATA_TRANSACAO
     */
    public java.util.Calendar getDATA_TRANSACAO() {
        return DATA_TRANSACAO;
    }


    /**
     * Sets the DATA_TRANSACAO value for this Dados_saida_transacao_doc.
     * 
     * @param DATA_TRANSACAO
     */
    public void setDATA_TRANSACAO(java.util.Calendar DATA_TRANSACAO) {
        this.DATA_TRANSACAO = DATA_TRANSACAO;
    }


    /**
     * Gets the CANAL_ORIGEM value for this Dados_saida_transacao_doc.
     * 
     * @return CANAL_ORIGEM
     */
    public java.lang.String getCANAL_ORIGEM() {
        return CANAL_ORIGEM;
    }


    /**
     * Sets the CANAL_ORIGEM value for this Dados_saida_transacao_doc.
     * 
     * @param CANAL_ORIGEM
     */
    public void setCANAL_ORIGEM(java.lang.String CANAL_ORIGEM) {
        this.CANAL_ORIGEM = CANAL_ORIGEM;
    }


    /**
     * Gets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_doc.
     * 
     * @return NUMERO_DOCUMENTO
     */
    public java.lang.Integer getNUMERO_DOCUMENTO() {
        return NUMERO_DOCUMENTO;
    }


    /**
     * Sets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_doc.
     * 
     * @param NUMERO_DOCUMENTO
     */
    public void setNUMERO_DOCUMENTO(java.lang.Integer NUMERO_DOCUMENTO) {
        this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
    }


    /**
     * Gets the TIPO_DOC value for this Dados_saida_transacao_doc.
     * 
     * @return TIPO_DOC
     */
    public java.lang.String getTIPO_DOC() {
        return TIPO_DOC;
    }


    /**
     * Sets the TIPO_DOC value for this Dados_saida_transacao_doc.
     * 
     * @param TIPO_DOC
     */
    public void setTIPO_DOC(java.lang.String TIPO_DOC) {
        this.TIPO_DOC = TIPO_DOC;
    }


    /**
     * Gets the FINALIDADE value for this Dados_saida_transacao_doc.
     * 
     * @return FINALIDADE
     */
    public java.lang.String getFINALIDADE() {
        return FINALIDADE;
    }


    /**
     * Sets the FINALIDADE value for this Dados_saida_transacao_doc.
     * 
     * @param FINALIDADE
     */
    public void setFINALIDADE(java.lang.String FINALIDADE) {
        this.FINALIDADE = FINALIDADE;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_doc)) return false;
        Dados_saida_transacao_doc other = (Dados_saida_transacao_doc) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NSU==null && other.getNSU()==null) || 
             (this.NSU!=null &&
              this.NSU.equals(other.getNSU()))) &&
            ((this.DATA_MOVIMENTO==null && other.getDATA_MOVIMENTO()==null) || 
             (this.DATA_MOVIMENTO!=null &&
              this.DATA_MOVIMENTO.equals(other.getDATA_MOVIMENTO()))) &&
            ((this.BANCO_DESTINO==null && other.getBANCO_DESTINO()==null) || 
             (this.BANCO_DESTINO!=null &&
              this.BANCO_DESTINO.equals(other.getBANCO_DESTINO()))) &&
            ((this.CONTA==null && other.getCONTA()==null) || 
             (this.CONTA!=null &&
              this.CONTA.equals(other.getCONTA()))) &&
            ((this.DV==null && other.getDV()==null) || 
             (this.DV!=null &&
              this.DV.equals(other.getDV()))) &&
            ((this.CPF==null && other.getCPF()==null) || 
             (this.CPF!=null &&
              this.CPF.equals(other.getCPF()))) &&
            ((this.CONTA_DEBITO==null && other.getCONTA_DEBITO()==null) || 
             (this.CONTA_DEBITO!=null &&
              this.CONTA_DEBITO.equals(other.getCONTA_DEBITO()))) &&
            ((this.CONTA_DESTINO==null && other.getCONTA_DESTINO()==null) || 
             (this.CONTA_DESTINO!=null &&
              this.CONTA_DESTINO.equals(other.getCONTA_DESTINO()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.TARIFA_EMISSAO==null && other.getTARIFA_EMISSAO()==null) || 
             (this.TARIFA_EMISSAO!=null &&
              this.TARIFA_EMISSAO.equals(other.getTARIFA_EMISSAO()))) &&
            ((this.TARIFA_AGENDAMENTO==null && other.getTARIFA_AGENDAMENTO()==null) || 
             (this.TARIFA_AGENDAMENTO!=null &&
              this.TARIFA_AGENDAMENTO.equals(other.getTARIFA_AGENDAMENTO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.DATA_TRANSACAO==null && other.getDATA_TRANSACAO()==null) || 
             (this.DATA_TRANSACAO!=null &&
              this.DATA_TRANSACAO.equals(other.getDATA_TRANSACAO()))) &&
            ((this.CANAL_ORIGEM==null && other.getCANAL_ORIGEM()==null) || 
             (this.CANAL_ORIGEM!=null &&
              this.CANAL_ORIGEM.equals(other.getCANAL_ORIGEM()))) &&
            ((this.NUMERO_DOCUMENTO==null && other.getNUMERO_DOCUMENTO()==null) || 
             (this.NUMERO_DOCUMENTO!=null &&
              this.NUMERO_DOCUMENTO.equals(other.getNUMERO_DOCUMENTO()))) &&
            ((this.TIPO_DOC==null && other.getTIPO_DOC()==null) || 
             (this.TIPO_DOC!=null &&
              this.TIPO_DOC.equals(other.getTIPO_DOC()))) &&
            ((this.FINALIDADE==null && other.getFINALIDADE()==null) || 
             (this.FINALIDADE!=null &&
              this.FINALIDADE.equals(other.getFINALIDADE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNSU() != null) {
            _hashCode += getNSU().hashCode();
        }
        if (getDATA_MOVIMENTO() != null) {
            _hashCode += getDATA_MOVIMENTO().hashCode();
        }
        if (getBANCO_DESTINO() != null) {
            _hashCode += getBANCO_DESTINO().hashCode();
        }
        if (getCONTA() != null) {
            _hashCode += getCONTA().hashCode();
        }
        if (getDV() != null) {
            _hashCode += getDV().hashCode();
        }
        if (getCPF() != null) {
            _hashCode += getCPF().hashCode();
        }
        if (getCONTA_DEBITO() != null) {
            _hashCode += getCONTA_DEBITO().hashCode();
        }
        if (getCONTA_DESTINO() != null) {
            _hashCode += getCONTA_DESTINO().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getTARIFA_EMISSAO() != null) {
            _hashCode += getTARIFA_EMISSAO().hashCode();
        }
        if (getTARIFA_AGENDAMENTO() != null) {
            _hashCode += getTARIFA_AGENDAMENTO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getDATA_TRANSACAO() != null) {
            _hashCode += getDATA_TRANSACAO().hashCode();
        }
        if (getCANAL_ORIGEM() != null) {
            _hashCode += getCANAL_ORIGEM().hashCode();
        }
        if (getNUMERO_DOCUMENTO() != null) {
            _hashCode += getNUMERO_DOCUMENTO().hashCode();
        }
        if (getTIPO_DOC() != null) {
            _hashCode += getTIPO_DOC().hashCode();
        }
        if (getFINALIDADE() != null) {
            _hashCode += getFINALIDADE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_doc.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_transacao_doc"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_MOVIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_MOVIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BANCO_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BANCO_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "banco_type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DV");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA_DEBITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA_DEBITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_cliente_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_cliente_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TARIFA_EMISSAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TARIFA_EMISSAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TARIFA_AGENDAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TARIFA_AGENDAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CANAL_ORIGEM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CANAL_ORIGEM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO_DOCUMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO_DOCUMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_DOC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_DOC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FINALIDADE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FINALIDADE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public String getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getBanco_Descricao() {
		return banco_Descricao;
	}

	public void setBanco_Descricao(String banco_Descricao) {
		this.banco_Descricao = banco_Descricao;
	}

	public String getChaveSeguranca() {
		return chaveSeguranca;
	}

	public void setChaveSeguranca(String chaveSeguranca) {
		this.chaveSeguranca = chaveSeguranca;
	}

    
    
    
}
