/**
 * Unidade_consumidora_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Unidade_consumidora_Type  implements java.io.Serializable {
    private java.lang.String CODIGO;

    private java.lang.String NOME;

    private org.apache.axis.types.UnsignedLong[] CPF_CGC;

    public Unidade_consumidora_Type() {
    }

    public Unidade_consumidora_Type(
           java.lang.String CODIGO,
           java.lang.String NOME,
           org.apache.axis.types.UnsignedLong[] CPF_CGC) {
           this.CODIGO = CODIGO;
           this.NOME = NOME;
           this.CPF_CGC = CPF_CGC;
    }


    /**
     * Gets the CODIGO value for this Unidade_consumidora_Type.
     * 
     * @return CODIGO
     */
    public java.lang.String getCODIGO() {
        return CODIGO;
    }


    /**
     * Sets the CODIGO value for this Unidade_consumidora_Type.
     * 
     * @param CODIGO
     */
    public void setCODIGO(java.lang.String CODIGO) {
        this.CODIGO = CODIGO;
    }


    /**
     * Gets the NOME value for this Unidade_consumidora_Type.
     * 
     * @return NOME
     */
    public java.lang.String getNOME() {
        return NOME;
    }


    /**
     * Sets the NOME value for this Unidade_consumidora_Type.
     * 
     * @param NOME
     */
    public void setNOME(java.lang.String NOME) {
        this.NOME = NOME;
    }


    /**
     * Gets the CPF_CGC value for this Unidade_consumidora_Type.
     * 
     * @return CPF_CGC
     */
    public org.apache.axis.types.UnsignedLong[] getCPF_CGC() {
        return CPF_CGC;
    }


    /**
     * Sets the CPF_CGC value for this Unidade_consumidora_Type.
     * 
     * @param CPF_CGC
     */
    public void setCPF_CGC(org.apache.axis.types.UnsignedLong[] CPF_CGC) {
        this.CPF_CGC = CPF_CGC;
    }

    public org.apache.axis.types.UnsignedLong getCPF_CGC(int i) {
        return this.CPF_CGC[i];
    }

    public void setCPF_CGC(int i, org.apache.axis.types.UnsignedLong _value) {
        this.CPF_CGC[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Unidade_consumidora_Type)) return false;
        Unidade_consumidora_Type other = (Unidade_consumidora_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CODIGO==null && other.getCODIGO()==null) || 
             (this.CODIGO!=null &&
              this.CODIGO.equals(other.getCODIGO()))) &&
            ((this.NOME==null && other.getNOME()==null) || 
             (this.NOME!=null &&
              this.NOME.equals(other.getNOME()))) &&
            ((this.CPF_CGC==null && other.getCPF_CGC()==null) || 
             (this.CPF_CGC!=null &&
              java.util.Arrays.equals(this.CPF_CGC, other.getCPF_CGC())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCODIGO() != null) {
            _hashCode += getCODIGO().hashCode();
        }
        if (getNOME() != null) {
            _hashCode += getNOME().hashCode();
        }
        if (getCPF_CGC() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCPF_CGC());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCPF_CGC(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Unidade_consumidora_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "unidade_consumidora_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF_CGC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF_CGC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">unidade_consumidora_Type>CPF_CGC"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
