/**
 * Dados_saida_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_Type  extends br.gov.caixa.sibar.DADOS_SAIDA_TYPE  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_concessionarias_Type CONCESSIONARIAS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_habitacao_sem_codigo_barras_Type HABITACAO_SEM_CODIGO_BARRAS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_icms_importacao_Type ICMS_IMPORTACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_doc_Type DOC;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_tev_Type TEV;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_darf_Type DARF;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_fgts_Type FGTS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_gps_Type GPS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_poupanca_programada_Type POUPANCA_PROGRAMADA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ted_Type TED;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_recarga_celular_Type RECARGA_CELULAR;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_concessionaria_telecomunicacao_Type CONCESSIONARIA_TELECOMUNICACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_iptu_Type IPTU;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_iss_taxas_Type ISS_TAXAS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_concessionaria_energia_Type CONCESSIONARIA_ENERGIA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_gare_Type GARE;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_deposito_judicial_Type DEPOSITO_JUDICIAL;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_primeiro_registro_estado_sp_Type PRIMEIRO_REGISTRO_ESTADO_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_segunda_via_licenciamento_sp_Type SEGUNDA_VIA_LICENCIAMENTO_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_boleto_dda_Type BOLETO_DDA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_carteira_eletronica_Type CARTEIRA_ELETRONICA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_daed_Type DAED;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_debitos_pendentes_sp_Type DEBITOS_PENDENTES_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_sp_Type DPVAT_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_sp_Type IPVA_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_detran_sp_Type TAXAS_DETRAN_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_sp_Type LICENCIAMENTO_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_sp_Type MULTAS_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_renainfo_Type MULTAS_RENAINFO_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_pec_generico_Type PEC_GENERICO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transferencia_sp_Type TRANSFERENCIA_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_segunda_via_transferencia_sp_Type SEGUNDA_VIA_TRANSFERENCIA_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_darf_com_codigo_barras_Type DARF_COM_CODIGO_BARRAS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_recebimento_eletronico_Type RECEBIMENTO_ELETRONICO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_habitacao_Type HABITACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_boleto_Type BOLETO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_mg_Type IPVA_MG;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_mg_Type DPVAT_MG;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_mg_Type LICENCIAMENTO_MG;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_veiculos_mg_Type TAXAS_VEICULOS_MG;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_condutor_mg_Type TAXAS_CONDUTOR_MG;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_megasena_Type MEGASENA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_rs_Type IPVA_RS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_rs_Type DPVAT_RS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_rs_Type MULTAS_RS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_rs_Type LICENCIAMENTO_RS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_pagamento_unificado_rs_Type PAGAMENTO_UNIFICADO_RS;

    public Dados_saida_Type() {
    }

    public Dados_saida_Type(
           br.gov.caixa.sibar.CONTROLE_NEGOCIAL_TYPE[] CONTROLE_NEGOCIAL,
           java.lang.String EXCECAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_concessionarias_Type CONCESSIONARIAS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_habitacao_sem_codigo_barras_Type HABITACAO_SEM_CODIGO_BARRAS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_icms_importacao_Type ICMS_IMPORTACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_doc_Type DOC,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_tev_Type TEV,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_darf_Type DARF,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_fgts_Type FGTS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_gps_Type GPS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_poupanca_programada_Type POUPANCA_PROGRAMADA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ted_Type TED,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_recarga_celular_Type RECARGA_CELULAR,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_concessionaria_telecomunicacao_Type CONCESSIONARIA_TELECOMUNICACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_iptu_Type IPTU,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_iss_taxas_Type ISS_TAXAS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_concessionaria_energia_Type CONCESSIONARIA_ENERGIA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_gare_Type GARE,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_deposito_judicial_Type DEPOSITO_JUDICIAL,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_primeiro_registro_estado_sp_Type PRIMEIRO_REGISTRO_ESTADO_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_segunda_via_licenciamento_sp_Type SEGUNDA_VIA_LICENCIAMENTO_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_boleto_dda_Type BOLETO_DDA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_carteira_eletronica_Type CARTEIRA_ELETRONICA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_daed_Type DAED,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_debitos_pendentes_sp_Type DEBITOS_PENDENTES_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_sp_Type DPVAT_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_sp_Type IPVA_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_detran_sp_Type TAXAS_DETRAN_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_sp_Type LICENCIAMENTO_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_sp_Type MULTAS_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_renainfo_Type MULTAS_RENAINFO_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_pec_generico_Type PEC_GENERICO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transferencia_sp_Type TRANSFERENCIA_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_segunda_via_transferencia_sp_Type SEGUNDA_VIA_TRANSFERENCIA_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_darf_com_codigo_barras_Type DARF_COM_CODIGO_BARRAS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_recebimento_eletronico_Type RECEBIMENTO_ELETRONICO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_habitacao_Type HABITACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_boleto_Type BOLETO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_mg_Type IPVA_MG,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_mg_Type DPVAT_MG,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_mg_Type LICENCIAMENTO_MG,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_veiculos_mg_Type TAXAS_VEICULOS_MG,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_condutor_mg_Type TAXAS_CONDUTOR_MG,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_megasena_Type MEGASENA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_rs_Type IPVA_RS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_rs_Type DPVAT_RS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_rs_Type MULTAS_RS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_rs_Type LICENCIAMENTO_RS,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_pagamento_unificado_rs_Type PAGAMENTO_UNIFICADO_RS) {
        super(
            CONTROLE_NEGOCIAL,
            EXCECAO);
        this.CONCESSIONARIAS = CONCESSIONARIAS;
        this.HABITACAO_SEM_CODIGO_BARRAS = HABITACAO_SEM_CODIGO_BARRAS;
        this.ICMS_IMPORTACAO = ICMS_IMPORTACAO;
        this.DOC = DOC;
        this.TEV = TEV;
        this.DARF = DARF;
        this.FGTS = FGTS;
        this.GPS = GPS;
        this.POUPANCA_PROGRAMADA = POUPANCA_PROGRAMADA;
        this.TED = TED;
        this.RECARGA_CELULAR = RECARGA_CELULAR;
        this.CONCESSIONARIA_TELECOMUNICACAO = CONCESSIONARIA_TELECOMUNICACAO;
        this.IPTU = IPTU;
        this.ISS_TAXAS = ISS_TAXAS;
        this.CONCESSIONARIA_ENERGIA = CONCESSIONARIA_ENERGIA;
        this.GARE = GARE;
        this.DEPOSITO_JUDICIAL = DEPOSITO_JUDICIAL;
        this.PRIMEIRO_REGISTRO_ESTADO_SP = PRIMEIRO_REGISTRO_ESTADO_SP;
        this.SEGUNDA_VIA_LICENCIAMENTO_SP = SEGUNDA_VIA_LICENCIAMENTO_SP;
        this.BOLETO_DDA = BOLETO_DDA;
        this.CARTEIRA_ELETRONICA = CARTEIRA_ELETRONICA;
        this.DAED = DAED;
        this.DEBITOS_PENDENTES_SP = DEBITOS_PENDENTES_SP;
        this.DPVAT_SP = DPVAT_SP;
        this.IPVA_SP = IPVA_SP;
        this.TAXAS_DETRAN_SP = TAXAS_DETRAN_SP;
        this.LICENCIAMENTO_SP = LICENCIAMENTO_SP;
        this.MULTAS_SP = MULTAS_SP;
        this.MULTAS_RENAINFO_SP = MULTAS_RENAINFO_SP;
        this.PEC_GENERICO = PEC_GENERICO;
        this.TRANSFERENCIA_SP = TRANSFERENCIA_SP;
        this.SEGUNDA_VIA_TRANSFERENCIA_SP = SEGUNDA_VIA_TRANSFERENCIA_SP;
        this.DARF_COM_CODIGO_BARRAS = DARF_COM_CODIGO_BARRAS;
        this.RECEBIMENTO_ELETRONICO = RECEBIMENTO_ELETRONICO;
        this.HABITACAO = HABITACAO;
        this.BOLETO = BOLETO;
        this.IPVA_MG = IPVA_MG;
        this.DPVAT_MG = DPVAT_MG;
        this.LICENCIAMENTO_MG = LICENCIAMENTO_MG;
        this.TAXAS_VEICULOS_MG = TAXAS_VEICULOS_MG;
        this.TAXAS_CONDUTOR_MG = TAXAS_CONDUTOR_MG;
        this.MEGASENA = MEGASENA;
        this.IPVA_RS = IPVA_RS;
        this.DPVAT_RS = DPVAT_RS;
        this.MULTAS_RS = MULTAS_RS;
        this.LICENCIAMENTO_RS = LICENCIAMENTO_RS;
        this.PAGAMENTO_UNIFICADO_RS = PAGAMENTO_UNIFICADO_RS;
    }


    /**
     * Gets the CONCESSIONARIAS value for this Dados_saida_Type.
     * 
     * @return CONCESSIONARIAS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_concessionarias_Type getCONCESSIONARIAS() {
        return CONCESSIONARIAS;
    }


    /**
     * Sets the CONCESSIONARIAS value for this Dados_saida_Type.
     * 
     * @param CONCESSIONARIAS
     */
    public void setCONCESSIONARIAS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_concessionarias_Type CONCESSIONARIAS) {
        this.CONCESSIONARIAS = CONCESSIONARIAS;
    }


    /**
     * Gets the HABITACAO_SEM_CODIGO_BARRAS value for this Dados_saida_Type.
     * 
     * @return HABITACAO_SEM_CODIGO_BARRAS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_habitacao_sem_codigo_barras_Type getHABITACAO_SEM_CODIGO_BARRAS() {
        return HABITACAO_SEM_CODIGO_BARRAS;
    }


    /**
     * Sets the HABITACAO_SEM_CODIGO_BARRAS value for this Dados_saida_Type.
     * 
     * @param HABITACAO_SEM_CODIGO_BARRAS
     */
    public void setHABITACAO_SEM_CODIGO_BARRAS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_habitacao_sem_codigo_barras_Type HABITACAO_SEM_CODIGO_BARRAS) {
        this.HABITACAO_SEM_CODIGO_BARRAS = HABITACAO_SEM_CODIGO_BARRAS;
    }


    /**
     * Gets the ICMS_IMPORTACAO value for this Dados_saida_Type.
     * 
     * @return ICMS_IMPORTACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_icms_importacao_Type getICMS_IMPORTACAO() {
        return ICMS_IMPORTACAO;
    }


    /**
     * Sets the ICMS_IMPORTACAO value for this Dados_saida_Type.
     * 
     * @param ICMS_IMPORTACAO
     */
    public void setICMS_IMPORTACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_icms_importacao_Type ICMS_IMPORTACAO) {
        this.ICMS_IMPORTACAO = ICMS_IMPORTACAO;
    }


    /**
     * Gets the DOC value for this Dados_saida_Type.
     * 
     * @return DOC
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_doc_Type getDOC() {
        return DOC;
    }


    /**
     * Sets the DOC value for this Dados_saida_Type.
     * 
     * @param DOC
     */
    public void setDOC(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_doc_Type DOC) {
        this.DOC = DOC;
    }


    /**
     * Gets the TEV value for this Dados_saida_Type.
     * 
     * @return TEV
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_tev_Type getTEV() {
        return TEV;
    }


    /**
     * Sets the TEV value for this Dados_saida_Type.
     * 
     * @param TEV
     */
    public void setTEV(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_tev_Type TEV) {
        this.TEV = TEV;
    }


    /**
     * Gets the DARF value for this Dados_saida_Type.
     * 
     * @return DARF
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_darf_Type getDARF() {
        return DARF;
    }


    /**
     * Sets the DARF value for this Dados_saida_Type.
     * 
     * @param DARF
     */
    public void setDARF(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_darf_Type DARF) {
        this.DARF = DARF;
    }


    /**
     * Gets the FGTS value for this Dados_saida_Type.
     * 
     * @return FGTS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_fgts_Type getFGTS() {
        return FGTS;
    }


    /**
     * Sets the FGTS value for this Dados_saida_Type.
     * 
     * @param FGTS
     */
    public void setFGTS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_fgts_Type FGTS) {
        this.FGTS = FGTS;
    }


    /**
     * Gets the GPS value for this Dados_saida_Type.
     * 
     * @return GPS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_gps_Type getGPS() {
        return GPS;
    }


    /**
     * Sets the GPS value for this Dados_saida_Type.
     * 
     * @param GPS
     */
    public void setGPS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_gps_Type GPS) {
        this.GPS = GPS;
    }


    /**
     * Gets the POUPANCA_PROGRAMADA value for this Dados_saida_Type.
     * 
     * @return POUPANCA_PROGRAMADA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_poupanca_programada_Type getPOUPANCA_PROGRAMADA() {
        return POUPANCA_PROGRAMADA;
    }


    /**
     * Sets the POUPANCA_PROGRAMADA value for this Dados_saida_Type.
     * 
     * @param POUPANCA_PROGRAMADA
     */
    public void setPOUPANCA_PROGRAMADA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_poupanca_programada_Type POUPANCA_PROGRAMADA) {
        this.POUPANCA_PROGRAMADA = POUPANCA_PROGRAMADA;
    }


    /**
     * Gets the TED value for this Dados_saida_Type.
     * 
     * @return TED
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ted_Type getTED() {
        return TED;
    }


    /**
     * Sets the TED value for this Dados_saida_Type.
     * 
     * @param TED
     */
    public void setTED(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ted_Type TED) {
        this.TED = TED;
    }


    /**
     * Gets the RECARGA_CELULAR value for this Dados_saida_Type.
     * 
     * @return RECARGA_CELULAR
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_recarga_celular_Type getRECARGA_CELULAR() {
        return RECARGA_CELULAR;
    }


    /**
     * Sets the RECARGA_CELULAR value for this Dados_saida_Type.
     * 
     * @param RECARGA_CELULAR
     */
    public void setRECARGA_CELULAR(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_recarga_celular_Type RECARGA_CELULAR) {
        this.RECARGA_CELULAR = RECARGA_CELULAR;
    }


    /**
     * Gets the CONCESSIONARIA_TELECOMUNICACAO value for this Dados_saida_Type.
     * 
     * @return CONCESSIONARIA_TELECOMUNICACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_concessionaria_telecomunicacao_Type getCONCESSIONARIA_TELECOMUNICACAO() {
        return CONCESSIONARIA_TELECOMUNICACAO;
    }


    /**
     * Sets the CONCESSIONARIA_TELECOMUNICACAO value for this Dados_saida_Type.
     * 
     * @param CONCESSIONARIA_TELECOMUNICACAO
     */
    public void setCONCESSIONARIA_TELECOMUNICACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_concessionaria_telecomunicacao_Type CONCESSIONARIA_TELECOMUNICACAO) {
        this.CONCESSIONARIA_TELECOMUNICACAO = CONCESSIONARIA_TELECOMUNICACAO;
    }


    /**
     * Gets the IPTU value for this Dados_saida_Type.
     * 
     * @return IPTU
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_iptu_Type getIPTU() {
        return IPTU;
    }


    /**
     * Sets the IPTU value for this Dados_saida_Type.
     * 
     * @param IPTU
     */
    public void setIPTU(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_iptu_Type IPTU) {
        this.IPTU = IPTU;
    }


    /**
     * Gets the ISS_TAXAS value for this Dados_saida_Type.
     * 
     * @return ISS_TAXAS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_iss_taxas_Type getISS_TAXAS() {
        return ISS_TAXAS;
    }


    /**
     * Sets the ISS_TAXAS value for this Dados_saida_Type.
     * 
     * @param ISS_TAXAS
     */
    public void setISS_TAXAS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_iss_taxas_Type ISS_TAXAS) {
        this.ISS_TAXAS = ISS_TAXAS;
    }


    /**
     * Gets the CONCESSIONARIA_ENERGIA value for this Dados_saida_Type.
     * 
     * @return CONCESSIONARIA_ENERGIA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_concessionaria_energia_Type getCONCESSIONARIA_ENERGIA() {
        return CONCESSIONARIA_ENERGIA;
    }


    /**
     * Sets the CONCESSIONARIA_ENERGIA value for this Dados_saida_Type.
     * 
     * @param CONCESSIONARIA_ENERGIA
     */
    public void setCONCESSIONARIA_ENERGIA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_concessionaria_energia_Type CONCESSIONARIA_ENERGIA) {
        this.CONCESSIONARIA_ENERGIA = CONCESSIONARIA_ENERGIA;
    }


    /**
     * Gets the GARE value for this Dados_saida_Type.
     * 
     * @return GARE
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_gare_Type getGARE() {
        return GARE;
    }


    /**
     * Sets the GARE value for this Dados_saida_Type.
     * 
     * @param GARE
     */
    public void setGARE(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_gare_Type GARE) {
        this.GARE = GARE;
    }


    /**
     * Gets the DEPOSITO_JUDICIAL value for this Dados_saida_Type.
     * 
     * @return DEPOSITO_JUDICIAL
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_deposito_judicial_Type getDEPOSITO_JUDICIAL() {
        return DEPOSITO_JUDICIAL;
    }


    /**
     * Sets the DEPOSITO_JUDICIAL value for this Dados_saida_Type.
     * 
     * @param DEPOSITO_JUDICIAL
     */
    public void setDEPOSITO_JUDICIAL(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_deposito_judicial_Type DEPOSITO_JUDICIAL) {
        this.DEPOSITO_JUDICIAL = DEPOSITO_JUDICIAL;
    }


    /**
     * Gets the PRIMEIRO_REGISTRO_ESTADO_SP value for this Dados_saida_Type.
     * 
     * @return PRIMEIRO_REGISTRO_ESTADO_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_primeiro_registro_estado_sp_Type getPRIMEIRO_REGISTRO_ESTADO_SP() {
        return PRIMEIRO_REGISTRO_ESTADO_SP;
    }


    /**
     * Sets the PRIMEIRO_REGISTRO_ESTADO_SP value for this Dados_saida_Type.
     * 
     * @param PRIMEIRO_REGISTRO_ESTADO_SP
     */
    public void setPRIMEIRO_REGISTRO_ESTADO_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_primeiro_registro_estado_sp_Type PRIMEIRO_REGISTRO_ESTADO_SP) {
        this.PRIMEIRO_REGISTRO_ESTADO_SP = PRIMEIRO_REGISTRO_ESTADO_SP;
    }


    /**
     * Gets the SEGUNDA_VIA_LICENCIAMENTO_SP value for this Dados_saida_Type.
     * 
     * @return SEGUNDA_VIA_LICENCIAMENTO_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_segunda_via_licenciamento_sp_Type getSEGUNDA_VIA_LICENCIAMENTO_SP() {
        return SEGUNDA_VIA_LICENCIAMENTO_SP;
    }


    /**
     * Sets the SEGUNDA_VIA_LICENCIAMENTO_SP value for this Dados_saida_Type.
     * 
     * @param SEGUNDA_VIA_LICENCIAMENTO_SP
     */
    public void setSEGUNDA_VIA_LICENCIAMENTO_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_segunda_via_licenciamento_sp_Type SEGUNDA_VIA_LICENCIAMENTO_SP) {
        this.SEGUNDA_VIA_LICENCIAMENTO_SP = SEGUNDA_VIA_LICENCIAMENTO_SP;
    }


    /**
     * Gets the BOLETO_DDA value for this Dados_saida_Type.
     * 
     * @return BOLETO_DDA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_boleto_dda_Type getBOLETO_DDA() {
        return BOLETO_DDA;
    }


    /**
     * Sets the BOLETO_DDA value for this Dados_saida_Type.
     * 
     * @param BOLETO_DDA
     */
    public void setBOLETO_DDA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_boleto_dda_Type BOLETO_DDA) {
        this.BOLETO_DDA = BOLETO_DDA;
    }


    /**
     * Gets the CARTEIRA_ELETRONICA value for this Dados_saida_Type.
     * 
     * @return CARTEIRA_ELETRONICA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_carteira_eletronica_Type getCARTEIRA_ELETRONICA() {
        return CARTEIRA_ELETRONICA;
    }


    /**
     * Sets the CARTEIRA_ELETRONICA value for this Dados_saida_Type.
     * 
     * @param CARTEIRA_ELETRONICA
     */
    public void setCARTEIRA_ELETRONICA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_carteira_eletronica_Type CARTEIRA_ELETRONICA) {
        this.CARTEIRA_ELETRONICA = CARTEIRA_ELETRONICA;
    }


    /**
     * Gets the DAED value for this Dados_saida_Type.
     * 
     * @return DAED
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_daed_Type getDAED() {
        return DAED;
    }


    /**
     * Sets the DAED value for this Dados_saida_Type.
     * 
     * @param DAED
     */
    public void setDAED(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_daed_Type DAED) {
        this.DAED = DAED;
    }


    /**
     * Gets the DEBITOS_PENDENTES_SP value for this Dados_saida_Type.
     * 
     * @return DEBITOS_PENDENTES_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_debitos_pendentes_sp_Type getDEBITOS_PENDENTES_SP() {
        return DEBITOS_PENDENTES_SP;
    }


    /**
     * Sets the DEBITOS_PENDENTES_SP value for this Dados_saida_Type.
     * 
     * @param DEBITOS_PENDENTES_SP
     */
    public void setDEBITOS_PENDENTES_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_debitos_pendentes_sp_Type DEBITOS_PENDENTES_SP) {
        this.DEBITOS_PENDENTES_SP = DEBITOS_PENDENTES_SP;
    }


    /**
     * Gets the DPVAT_SP value for this Dados_saida_Type.
     * 
     * @return DPVAT_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_sp_Type getDPVAT_SP() {
        return DPVAT_SP;
    }


    /**
     * Sets the DPVAT_SP value for this Dados_saida_Type.
     * 
     * @param DPVAT_SP
     */
    public void setDPVAT_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_sp_Type DPVAT_SP) {
        this.DPVAT_SP = DPVAT_SP;
    }


    /**
     * Gets the IPVA_SP value for this Dados_saida_Type.
     * 
     * @return IPVA_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_sp_Type getIPVA_SP() {
        return IPVA_SP;
    }


    /**
     * Sets the IPVA_SP value for this Dados_saida_Type.
     * 
     * @param IPVA_SP
     */
    public void setIPVA_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_sp_Type IPVA_SP) {
        this.IPVA_SP = IPVA_SP;
    }


    /**
     * Gets the TAXAS_DETRAN_SP value for this Dados_saida_Type.
     * 
     * @return TAXAS_DETRAN_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_detran_sp_Type getTAXAS_DETRAN_SP() {
        return TAXAS_DETRAN_SP;
    }


    /**
     * Sets the TAXAS_DETRAN_SP value for this Dados_saida_Type.
     * 
     * @param TAXAS_DETRAN_SP
     */
    public void setTAXAS_DETRAN_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_detran_sp_Type TAXAS_DETRAN_SP) {
        this.TAXAS_DETRAN_SP = TAXAS_DETRAN_SP;
    }


    /**
     * Gets the LICENCIAMENTO_SP value for this Dados_saida_Type.
     * 
     * @return LICENCIAMENTO_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_sp_Type getLICENCIAMENTO_SP() {
        return LICENCIAMENTO_SP;
    }


    /**
     * Sets the LICENCIAMENTO_SP value for this Dados_saida_Type.
     * 
     * @param LICENCIAMENTO_SP
     */
    public void setLICENCIAMENTO_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_sp_Type LICENCIAMENTO_SP) {
        this.LICENCIAMENTO_SP = LICENCIAMENTO_SP;
    }


    /**
     * Gets the MULTAS_SP value for this Dados_saida_Type.
     * 
     * @return MULTAS_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_sp_Type getMULTAS_SP() {
        return MULTAS_SP;
    }


    /**
     * Sets the MULTAS_SP value for this Dados_saida_Type.
     * 
     * @param MULTAS_SP
     */
    public void setMULTAS_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_sp_Type MULTAS_SP) {
        this.MULTAS_SP = MULTAS_SP;
    }


    /**
     * Gets the MULTAS_RENAINFO_SP value for this Dados_saida_Type.
     * 
     * @return MULTAS_RENAINFO_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_renainfo_Type getMULTAS_RENAINFO_SP() {
        return MULTAS_RENAINFO_SP;
    }


    /**
     * Sets the MULTAS_RENAINFO_SP value for this Dados_saida_Type.
     * 
     * @param MULTAS_RENAINFO_SP
     */
    public void setMULTAS_RENAINFO_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_renainfo_Type MULTAS_RENAINFO_SP) {
        this.MULTAS_RENAINFO_SP = MULTAS_RENAINFO_SP;
    }


    /**
     * Gets the PEC_GENERICO value for this Dados_saida_Type.
     * 
     * @return PEC_GENERICO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_pec_generico_Type getPEC_GENERICO() {
        return PEC_GENERICO;
    }


    /**
     * Sets the PEC_GENERICO value for this Dados_saida_Type.
     * 
     * @param PEC_GENERICO
     */
    public void setPEC_GENERICO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_pec_generico_Type PEC_GENERICO) {
        this.PEC_GENERICO = PEC_GENERICO;
    }


    /**
     * Gets the TRANSFERENCIA_SP value for this Dados_saida_Type.
     * 
     * @return TRANSFERENCIA_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transferencia_sp_Type getTRANSFERENCIA_SP() {
        return TRANSFERENCIA_SP;
    }


    /**
     * Sets the TRANSFERENCIA_SP value for this Dados_saida_Type.
     * 
     * @param TRANSFERENCIA_SP
     */
    public void setTRANSFERENCIA_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transferencia_sp_Type TRANSFERENCIA_SP) {
        this.TRANSFERENCIA_SP = TRANSFERENCIA_SP;
    }


    /**
     * Gets the SEGUNDA_VIA_TRANSFERENCIA_SP value for this Dados_saida_Type.
     * 
     * @return SEGUNDA_VIA_TRANSFERENCIA_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_segunda_via_transferencia_sp_Type getSEGUNDA_VIA_TRANSFERENCIA_SP() {
        return SEGUNDA_VIA_TRANSFERENCIA_SP;
    }


    /**
     * Sets the SEGUNDA_VIA_TRANSFERENCIA_SP value for this Dados_saida_Type.
     * 
     * @param SEGUNDA_VIA_TRANSFERENCIA_SP
     */
    public void setSEGUNDA_VIA_TRANSFERENCIA_SP(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_segunda_via_transferencia_sp_Type SEGUNDA_VIA_TRANSFERENCIA_SP) {
        this.SEGUNDA_VIA_TRANSFERENCIA_SP = SEGUNDA_VIA_TRANSFERENCIA_SP;
    }


    /**
     * Gets the DARF_COM_CODIGO_BARRAS value for this Dados_saida_Type.
     * 
     * @return DARF_COM_CODIGO_BARRAS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_darf_com_codigo_barras_Type getDARF_COM_CODIGO_BARRAS() {
        return DARF_COM_CODIGO_BARRAS;
    }


    /**
     * Sets the DARF_COM_CODIGO_BARRAS value for this Dados_saida_Type.
     * 
     * @param DARF_COM_CODIGO_BARRAS
     */
    public void setDARF_COM_CODIGO_BARRAS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_darf_com_codigo_barras_Type DARF_COM_CODIGO_BARRAS) {
        this.DARF_COM_CODIGO_BARRAS = DARF_COM_CODIGO_BARRAS;
    }


    /**
     * Gets the RECEBIMENTO_ELETRONICO value for this Dados_saida_Type.
     * 
     * @return RECEBIMENTO_ELETRONICO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_recebimento_eletronico_Type getRECEBIMENTO_ELETRONICO() {
        return RECEBIMENTO_ELETRONICO;
    }


    /**
     * Sets the RECEBIMENTO_ELETRONICO value for this Dados_saida_Type.
     * 
     * @param RECEBIMENTO_ELETRONICO
     */
    public void setRECEBIMENTO_ELETRONICO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_recebimento_eletronico_Type RECEBIMENTO_ELETRONICO) {
        this.RECEBIMENTO_ELETRONICO = RECEBIMENTO_ELETRONICO;
    }


    /**
     * Gets the HABITACAO value for this Dados_saida_Type.
     * 
     * @return HABITACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_habitacao_Type getHABITACAO() {
        return HABITACAO;
    }


    /**
     * Sets the HABITACAO value for this Dados_saida_Type.
     * 
     * @param HABITACAO
     */
    public void setHABITACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_habitacao_Type HABITACAO) {
        this.HABITACAO = HABITACAO;
    }


    /**
     * Gets the BOLETO value for this Dados_saida_Type.
     * 
     * @return BOLETO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_boleto_Type getBOLETO() {
        return BOLETO;
    }


    /**
     * Sets the BOLETO value for this Dados_saida_Type.
     * 
     * @param BOLETO
     */
    public void setBOLETO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_boleto_Type BOLETO) {
        this.BOLETO = BOLETO;
    }


    /**
     * Gets the IPVA_MG value for this Dados_saida_Type.
     * 
     * @return IPVA_MG
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_mg_Type getIPVA_MG() {
        return IPVA_MG;
    }


    /**
     * Sets the IPVA_MG value for this Dados_saida_Type.
     * 
     * @param IPVA_MG
     */
    public void setIPVA_MG(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_mg_Type IPVA_MG) {
        this.IPVA_MG = IPVA_MG;
    }


    /**
     * Gets the DPVAT_MG value for this Dados_saida_Type.
     * 
     * @return DPVAT_MG
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_mg_Type getDPVAT_MG() {
        return DPVAT_MG;
    }


    /**
     * Sets the DPVAT_MG value for this Dados_saida_Type.
     * 
     * @param DPVAT_MG
     */
    public void setDPVAT_MG(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_mg_Type DPVAT_MG) {
        this.DPVAT_MG = DPVAT_MG;
    }


    /**
     * Gets the LICENCIAMENTO_MG value for this Dados_saida_Type.
     * 
     * @return LICENCIAMENTO_MG
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_mg_Type getLICENCIAMENTO_MG() {
        return LICENCIAMENTO_MG;
    }


    /**
     * Sets the LICENCIAMENTO_MG value for this Dados_saida_Type.
     * 
     * @param LICENCIAMENTO_MG
     */
    public void setLICENCIAMENTO_MG(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_mg_Type LICENCIAMENTO_MG) {
        this.LICENCIAMENTO_MG = LICENCIAMENTO_MG;
    }


    /**
     * Gets the TAXAS_VEICULOS_MG value for this Dados_saida_Type.
     * 
     * @return TAXAS_VEICULOS_MG
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_veiculos_mg_Type getTAXAS_VEICULOS_MG() {
        return TAXAS_VEICULOS_MG;
    }


    /**
     * Sets the TAXAS_VEICULOS_MG value for this Dados_saida_Type.
     * 
     * @param TAXAS_VEICULOS_MG
     */
    public void setTAXAS_VEICULOS_MG(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_veiculos_mg_Type TAXAS_VEICULOS_MG) {
        this.TAXAS_VEICULOS_MG = TAXAS_VEICULOS_MG;
    }


    /**
     * Gets the TAXAS_CONDUTOR_MG value for this Dados_saida_Type.
     * 
     * @return TAXAS_CONDUTOR_MG
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_condutor_mg_Type getTAXAS_CONDUTOR_MG() {
        return TAXAS_CONDUTOR_MG;
    }


    /**
     * Sets the TAXAS_CONDUTOR_MG value for this Dados_saida_Type.
     * 
     * @param TAXAS_CONDUTOR_MG
     */
    public void setTAXAS_CONDUTOR_MG(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_taxas_condutor_mg_Type TAXAS_CONDUTOR_MG) {
        this.TAXAS_CONDUTOR_MG = TAXAS_CONDUTOR_MG;
    }


    /**
     * Gets the MEGASENA value for this Dados_saida_Type.
     * 
     * @return MEGASENA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_megasena_Type getMEGASENA() {
        return MEGASENA;
    }


    /**
     * Sets the MEGASENA value for this Dados_saida_Type.
     * 
     * @param MEGASENA
     */
    public void setMEGASENA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_megasena_Type MEGASENA) {
        this.MEGASENA = MEGASENA;
    }


    /**
     * Gets the IPVA_RS value for this Dados_saida_Type.
     * 
     * @return IPVA_RS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_rs_Type getIPVA_RS() {
        return IPVA_RS;
    }


    /**
     * Sets the IPVA_RS value for this Dados_saida_Type.
     * 
     * @param IPVA_RS
     */
    public void setIPVA_RS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_ipva_rs_Type IPVA_RS) {
        this.IPVA_RS = IPVA_RS;
    }


    /**
     * Gets the DPVAT_RS value for this Dados_saida_Type.
     * 
     * @return DPVAT_RS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_rs_Type getDPVAT_RS() {
        return DPVAT_RS;
    }


    /**
     * Sets the DPVAT_RS value for this Dados_saida_Type.
     * 
     * @param DPVAT_RS
     */
    public void setDPVAT_RS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_dpvat_rs_Type DPVAT_RS) {
        this.DPVAT_RS = DPVAT_RS;
    }


    /**
     * Gets the MULTAS_RS value for this Dados_saida_Type.
     * 
     * @return MULTAS_RS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_rs_Type getMULTAS_RS() {
        return MULTAS_RS;
    }


    /**
     * Sets the MULTAS_RS value for this Dados_saida_Type.
     * 
     * @param MULTAS_RS
     */
    public void setMULTAS_RS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_multas_rs_Type MULTAS_RS) {
        this.MULTAS_RS = MULTAS_RS;
    }


    /**
     * Gets the LICENCIAMENTO_RS value for this Dados_saida_Type.
     * 
     * @return LICENCIAMENTO_RS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_rs_Type getLICENCIAMENTO_RS() {
        return LICENCIAMENTO_RS;
    }


    /**
     * Sets the LICENCIAMENTO_RS value for this Dados_saida_Type.
     * 
     * @param LICENCIAMENTO_RS
     */
    public void setLICENCIAMENTO_RS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_licenciamento_rs_Type LICENCIAMENTO_RS) {
        this.LICENCIAMENTO_RS = LICENCIAMENTO_RS;
    }


    /**
     * Gets the PAGAMENTO_UNIFICADO_RS value for this Dados_saida_Type.
     * 
     * @return PAGAMENTO_UNIFICADO_RS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_pagamento_unificado_rs_Type getPAGAMENTO_UNIFICADO_RS() {
        return PAGAMENTO_UNIFICADO_RS;
    }


    /**
     * Sets the PAGAMENTO_UNIFICADO_RS value for this Dados_saida_Type.
     * 
     * @param PAGAMENTO_UNIFICADO_RS
     */
    public void setPAGAMENTO_UNIFICADO_RS(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_pagamento_unificado_rs_Type PAGAMENTO_UNIFICADO_RS) {
        this.PAGAMENTO_UNIFICADO_RS = PAGAMENTO_UNIFICADO_RS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_Type)) return false;
        Dados_saida_Type other = (Dados_saida_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.CONCESSIONARIAS==null && other.getCONCESSIONARIAS()==null) || 
             (this.CONCESSIONARIAS!=null &&
              this.CONCESSIONARIAS.equals(other.getCONCESSIONARIAS()))) &&
            ((this.HABITACAO_SEM_CODIGO_BARRAS==null && other.getHABITACAO_SEM_CODIGO_BARRAS()==null) || 
             (this.HABITACAO_SEM_CODIGO_BARRAS!=null &&
              this.HABITACAO_SEM_CODIGO_BARRAS.equals(other.getHABITACAO_SEM_CODIGO_BARRAS()))) &&
            ((this.ICMS_IMPORTACAO==null && other.getICMS_IMPORTACAO()==null) || 
             (this.ICMS_IMPORTACAO!=null &&
              this.ICMS_IMPORTACAO.equals(other.getICMS_IMPORTACAO()))) &&
            ((this.DOC==null && other.getDOC()==null) || 
             (this.DOC!=null &&
              this.DOC.equals(other.getDOC()))) &&
            ((this.TEV==null && other.getTEV()==null) || 
             (this.TEV!=null &&
              this.TEV.equals(other.getTEV()))) &&
            ((this.DARF==null && other.getDARF()==null) || 
             (this.DARF!=null &&
              this.DARF.equals(other.getDARF()))) &&
            ((this.FGTS==null && other.getFGTS()==null) || 
             (this.FGTS!=null &&
              this.FGTS.equals(other.getFGTS()))) &&
            ((this.GPS==null && other.getGPS()==null) || 
             (this.GPS!=null &&
              this.GPS.equals(other.getGPS()))) &&
            ((this.POUPANCA_PROGRAMADA==null && other.getPOUPANCA_PROGRAMADA()==null) || 
             (this.POUPANCA_PROGRAMADA!=null &&
              this.POUPANCA_PROGRAMADA.equals(other.getPOUPANCA_PROGRAMADA()))) &&
            ((this.TED==null && other.getTED()==null) || 
             (this.TED!=null &&
              this.TED.equals(other.getTED()))) &&
            ((this.RECARGA_CELULAR==null && other.getRECARGA_CELULAR()==null) || 
             (this.RECARGA_CELULAR!=null &&
              this.RECARGA_CELULAR.equals(other.getRECARGA_CELULAR()))) &&
            ((this.CONCESSIONARIA_TELECOMUNICACAO==null && other.getCONCESSIONARIA_TELECOMUNICACAO()==null) || 
             (this.CONCESSIONARIA_TELECOMUNICACAO!=null &&
              this.CONCESSIONARIA_TELECOMUNICACAO.equals(other.getCONCESSIONARIA_TELECOMUNICACAO()))) &&
            ((this.IPTU==null && other.getIPTU()==null) || 
             (this.IPTU!=null &&
              this.IPTU.equals(other.getIPTU()))) &&
            ((this.ISS_TAXAS==null && other.getISS_TAXAS()==null) || 
             (this.ISS_TAXAS!=null &&
              this.ISS_TAXAS.equals(other.getISS_TAXAS()))) &&
            ((this.CONCESSIONARIA_ENERGIA==null && other.getCONCESSIONARIA_ENERGIA()==null) || 
             (this.CONCESSIONARIA_ENERGIA!=null &&
              this.CONCESSIONARIA_ENERGIA.equals(other.getCONCESSIONARIA_ENERGIA()))) &&
            ((this.GARE==null && other.getGARE()==null) || 
             (this.GARE!=null &&
              this.GARE.equals(other.getGARE()))) &&
            ((this.DEPOSITO_JUDICIAL==null && other.getDEPOSITO_JUDICIAL()==null) || 
             (this.DEPOSITO_JUDICIAL!=null &&
              this.DEPOSITO_JUDICIAL.equals(other.getDEPOSITO_JUDICIAL()))) &&
            ((this.PRIMEIRO_REGISTRO_ESTADO_SP==null && other.getPRIMEIRO_REGISTRO_ESTADO_SP()==null) || 
             (this.PRIMEIRO_REGISTRO_ESTADO_SP!=null &&
              this.PRIMEIRO_REGISTRO_ESTADO_SP.equals(other.getPRIMEIRO_REGISTRO_ESTADO_SP()))) &&
            ((this.SEGUNDA_VIA_LICENCIAMENTO_SP==null && other.getSEGUNDA_VIA_LICENCIAMENTO_SP()==null) || 
             (this.SEGUNDA_VIA_LICENCIAMENTO_SP!=null &&
              this.SEGUNDA_VIA_LICENCIAMENTO_SP.equals(other.getSEGUNDA_VIA_LICENCIAMENTO_SP()))) &&
            ((this.BOLETO_DDA==null && other.getBOLETO_DDA()==null) || 
             (this.BOLETO_DDA!=null &&
              this.BOLETO_DDA.equals(other.getBOLETO_DDA()))) &&
            ((this.CARTEIRA_ELETRONICA==null && other.getCARTEIRA_ELETRONICA()==null) || 
             (this.CARTEIRA_ELETRONICA!=null &&
              this.CARTEIRA_ELETRONICA.equals(other.getCARTEIRA_ELETRONICA()))) &&
            ((this.DAED==null && other.getDAED()==null) || 
             (this.DAED!=null &&
              this.DAED.equals(other.getDAED()))) &&
            ((this.DEBITOS_PENDENTES_SP==null && other.getDEBITOS_PENDENTES_SP()==null) || 
             (this.DEBITOS_PENDENTES_SP!=null &&
              this.DEBITOS_PENDENTES_SP.equals(other.getDEBITOS_PENDENTES_SP()))) &&
            ((this.DPVAT_SP==null && other.getDPVAT_SP()==null) || 
             (this.DPVAT_SP!=null &&
              this.DPVAT_SP.equals(other.getDPVAT_SP()))) &&
            ((this.IPVA_SP==null && other.getIPVA_SP()==null) || 
             (this.IPVA_SP!=null &&
              this.IPVA_SP.equals(other.getIPVA_SP()))) &&
            ((this.TAXAS_DETRAN_SP==null && other.getTAXAS_DETRAN_SP()==null) || 
             (this.TAXAS_DETRAN_SP!=null &&
              this.TAXAS_DETRAN_SP.equals(other.getTAXAS_DETRAN_SP()))) &&
            ((this.LICENCIAMENTO_SP==null && other.getLICENCIAMENTO_SP()==null) || 
             (this.LICENCIAMENTO_SP!=null &&
              this.LICENCIAMENTO_SP.equals(other.getLICENCIAMENTO_SP()))) &&
            ((this.MULTAS_SP==null && other.getMULTAS_SP()==null) || 
             (this.MULTAS_SP!=null &&
              this.MULTAS_SP.equals(other.getMULTAS_SP()))) &&
            ((this.MULTAS_RENAINFO_SP==null && other.getMULTAS_RENAINFO_SP()==null) || 
             (this.MULTAS_RENAINFO_SP!=null &&
              this.MULTAS_RENAINFO_SP.equals(other.getMULTAS_RENAINFO_SP()))) &&
            ((this.PEC_GENERICO==null && other.getPEC_GENERICO()==null) || 
             (this.PEC_GENERICO!=null &&
              this.PEC_GENERICO.equals(other.getPEC_GENERICO()))) &&
            ((this.TRANSFERENCIA_SP==null && other.getTRANSFERENCIA_SP()==null) || 
             (this.TRANSFERENCIA_SP!=null &&
              this.TRANSFERENCIA_SP.equals(other.getTRANSFERENCIA_SP()))) &&
            ((this.SEGUNDA_VIA_TRANSFERENCIA_SP==null && other.getSEGUNDA_VIA_TRANSFERENCIA_SP()==null) || 
             (this.SEGUNDA_VIA_TRANSFERENCIA_SP!=null &&
              this.SEGUNDA_VIA_TRANSFERENCIA_SP.equals(other.getSEGUNDA_VIA_TRANSFERENCIA_SP()))) &&
            ((this.DARF_COM_CODIGO_BARRAS==null && other.getDARF_COM_CODIGO_BARRAS()==null) || 
             (this.DARF_COM_CODIGO_BARRAS!=null &&
              this.DARF_COM_CODIGO_BARRAS.equals(other.getDARF_COM_CODIGO_BARRAS()))) &&
            ((this.RECEBIMENTO_ELETRONICO==null && other.getRECEBIMENTO_ELETRONICO()==null) || 
             (this.RECEBIMENTO_ELETRONICO!=null &&
              this.RECEBIMENTO_ELETRONICO.equals(other.getRECEBIMENTO_ELETRONICO()))) &&
            ((this.HABITACAO==null && other.getHABITACAO()==null) || 
             (this.HABITACAO!=null &&
              this.HABITACAO.equals(other.getHABITACAO()))) &&
            ((this.BOLETO==null && other.getBOLETO()==null) || 
             (this.BOLETO!=null &&
              this.BOLETO.equals(other.getBOLETO()))) &&
            ((this.IPVA_MG==null && other.getIPVA_MG()==null) || 
             (this.IPVA_MG!=null &&
              this.IPVA_MG.equals(other.getIPVA_MG()))) &&
            ((this.DPVAT_MG==null && other.getDPVAT_MG()==null) || 
             (this.DPVAT_MG!=null &&
              this.DPVAT_MG.equals(other.getDPVAT_MG()))) &&
            ((this.LICENCIAMENTO_MG==null && other.getLICENCIAMENTO_MG()==null) || 
             (this.LICENCIAMENTO_MG!=null &&
              this.LICENCIAMENTO_MG.equals(other.getLICENCIAMENTO_MG()))) &&
            ((this.TAXAS_VEICULOS_MG==null && other.getTAXAS_VEICULOS_MG()==null) || 
             (this.TAXAS_VEICULOS_MG!=null &&
              this.TAXAS_VEICULOS_MG.equals(other.getTAXAS_VEICULOS_MG()))) &&
            ((this.TAXAS_CONDUTOR_MG==null && other.getTAXAS_CONDUTOR_MG()==null) || 
             (this.TAXAS_CONDUTOR_MG!=null &&
              this.TAXAS_CONDUTOR_MG.equals(other.getTAXAS_CONDUTOR_MG()))) &&
            ((this.MEGASENA==null && other.getMEGASENA()==null) || 
             (this.MEGASENA!=null &&
              this.MEGASENA.equals(other.getMEGASENA()))) &&
            ((this.IPVA_RS==null && other.getIPVA_RS()==null) || 
             (this.IPVA_RS!=null &&
              this.IPVA_RS.equals(other.getIPVA_RS()))) &&
            ((this.DPVAT_RS==null && other.getDPVAT_RS()==null) || 
             (this.DPVAT_RS!=null &&
              this.DPVAT_RS.equals(other.getDPVAT_RS()))) &&
            ((this.MULTAS_RS==null && other.getMULTAS_RS()==null) || 
             (this.MULTAS_RS!=null &&
              this.MULTAS_RS.equals(other.getMULTAS_RS()))) &&
            ((this.LICENCIAMENTO_RS==null && other.getLICENCIAMENTO_RS()==null) || 
             (this.LICENCIAMENTO_RS!=null &&
              this.LICENCIAMENTO_RS.equals(other.getLICENCIAMENTO_RS()))) &&
            ((this.PAGAMENTO_UNIFICADO_RS==null && other.getPAGAMENTO_UNIFICADO_RS()==null) || 
             (this.PAGAMENTO_UNIFICADO_RS!=null &&
              this.PAGAMENTO_UNIFICADO_RS.equals(other.getPAGAMENTO_UNIFICADO_RS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCONCESSIONARIAS() != null) {
            _hashCode += getCONCESSIONARIAS().hashCode();
        }
        if (getHABITACAO_SEM_CODIGO_BARRAS() != null) {
            _hashCode += getHABITACAO_SEM_CODIGO_BARRAS().hashCode();
        }
        if (getICMS_IMPORTACAO() != null) {
            _hashCode += getICMS_IMPORTACAO().hashCode();
        }
        if (getDOC() != null) {
            _hashCode += getDOC().hashCode();
        }
        if (getTEV() != null) {
            _hashCode += getTEV().hashCode();
        }
        if (getDARF() != null) {
            _hashCode += getDARF().hashCode();
        }
        if (getFGTS() != null) {
            _hashCode += getFGTS().hashCode();
        }
        if (getGPS() != null) {
            _hashCode += getGPS().hashCode();
        }
        if (getPOUPANCA_PROGRAMADA() != null) {
            _hashCode += getPOUPANCA_PROGRAMADA().hashCode();
        }
        if (getTED() != null) {
            _hashCode += getTED().hashCode();
        }
        if (getRECARGA_CELULAR() != null) {
            _hashCode += getRECARGA_CELULAR().hashCode();
        }
        if (getCONCESSIONARIA_TELECOMUNICACAO() != null) {
            _hashCode += getCONCESSIONARIA_TELECOMUNICACAO().hashCode();
        }
        if (getIPTU() != null) {
            _hashCode += getIPTU().hashCode();
        }
        if (getISS_TAXAS() != null) {
            _hashCode += getISS_TAXAS().hashCode();
        }
        if (getCONCESSIONARIA_ENERGIA() != null) {
            _hashCode += getCONCESSIONARIA_ENERGIA().hashCode();
        }
        if (getGARE() != null) {
            _hashCode += getGARE().hashCode();
        }
        if (getDEPOSITO_JUDICIAL() != null) {
            _hashCode += getDEPOSITO_JUDICIAL().hashCode();
        }
        if (getPRIMEIRO_REGISTRO_ESTADO_SP() != null) {
            _hashCode += getPRIMEIRO_REGISTRO_ESTADO_SP().hashCode();
        }
        if (getSEGUNDA_VIA_LICENCIAMENTO_SP() != null) {
            _hashCode += getSEGUNDA_VIA_LICENCIAMENTO_SP().hashCode();
        }
        if (getBOLETO_DDA() != null) {
            _hashCode += getBOLETO_DDA().hashCode();
        }
        if (getCARTEIRA_ELETRONICA() != null) {
            _hashCode += getCARTEIRA_ELETRONICA().hashCode();
        }
        if (getDAED() != null) {
            _hashCode += getDAED().hashCode();
        }
        if (getDEBITOS_PENDENTES_SP() != null) {
            _hashCode += getDEBITOS_PENDENTES_SP().hashCode();
        }
        if (getDPVAT_SP() != null) {
            _hashCode += getDPVAT_SP().hashCode();
        }
        if (getIPVA_SP() != null) {
            _hashCode += getIPVA_SP().hashCode();
        }
        if (getTAXAS_DETRAN_SP() != null) {
            _hashCode += getTAXAS_DETRAN_SP().hashCode();
        }
        if (getLICENCIAMENTO_SP() != null) {
            _hashCode += getLICENCIAMENTO_SP().hashCode();
        }
        if (getMULTAS_SP() != null) {
            _hashCode += getMULTAS_SP().hashCode();
        }
        if (getMULTAS_RENAINFO_SP() != null) {
            _hashCode += getMULTAS_RENAINFO_SP().hashCode();
        }
        if (getPEC_GENERICO() != null) {
            _hashCode += getPEC_GENERICO().hashCode();
        }
        if (getTRANSFERENCIA_SP() != null) {
            _hashCode += getTRANSFERENCIA_SP().hashCode();
        }
        if (getSEGUNDA_VIA_TRANSFERENCIA_SP() != null) {
            _hashCode += getSEGUNDA_VIA_TRANSFERENCIA_SP().hashCode();
        }
        if (getDARF_COM_CODIGO_BARRAS() != null) {
            _hashCode += getDARF_COM_CODIGO_BARRAS().hashCode();
        }
        if (getRECEBIMENTO_ELETRONICO() != null) {
            _hashCode += getRECEBIMENTO_ELETRONICO().hashCode();
        }
        if (getHABITACAO() != null) {
            _hashCode += getHABITACAO().hashCode();
        }
        if (getBOLETO() != null) {
            _hashCode += getBOLETO().hashCode();
        }
        if (getIPVA_MG() != null) {
            _hashCode += getIPVA_MG().hashCode();
        }
        if (getDPVAT_MG() != null) {
            _hashCode += getDPVAT_MG().hashCode();
        }
        if (getLICENCIAMENTO_MG() != null) {
            _hashCode += getLICENCIAMENTO_MG().hashCode();
        }
        if (getTAXAS_VEICULOS_MG() != null) {
            _hashCode += getTAXAS_VEICULOS_MG().hashCode();
        }
        if (getTAXAS_CONDUTOR_MG() != null) {
            _hashCode += getTAXAS_CONDUTOR_MG().hashCode();
        }
        if (getMEGASENA() != null) {
            _hashCode += getMEGASENA().hashCode();
        }
        if (getIPVA_RS() != null) {
            _hashCode += getIPVA_RS().hashCode();
        }
        if (getDPVAT_RS() != null) {
            _hashCode += getDPVAT_RS().hashCode();
        }
        if (getMULTAS_RS() != null) {
            _hashCode += getMULTAS_RS().hashCode();
        }
        if (getLICENCIAMENTO_RS() != null) {
            _hashCode += getLICENCIAMENTO_RS().hashCode();
        }
        if (getPAGAMENTO_UNIFICADO_RS() != null) {
            _hashCode += getPAGAMENTO_UNIFICADO_RS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONCESSIONARIAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONCESSIONARIAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_concessionarias_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HABITACAO_SEM_CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HABITACAO_SEM_CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_habitacao_sem_codigo_barras_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ICMS_IMPORTACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ICMS_IMPORTACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_icms_importacao_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DOC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_doc_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TEV");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TEV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_tev_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DARF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DARF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_darf_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FGTS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FGTS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_fgts_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GPS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GPS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_gps_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POUPANCA_PROGRAMADA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "POUPANCA_PROGRAMADA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_poupanca_programada_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TED");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_ted_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECARGA_CELULAR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RECARGA_CELULAR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_recarga_celular_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONCESSIONARIA_TELECOMUNICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONCESSIONARIA_TELECOMUNICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_concessionaria_telecomunicacao_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IPTU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IPTU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_iptu_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISS_TAXAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ISS_TAXAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_iss_taxas_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONCESSIONARIA_ENERGIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONCESSIONARIA_ENERGIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_concessionaria_energia_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GARE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GARE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_gare_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DEPOSITO_JUDICIAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DEPOSITO_JUDICIAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_deposito_judicial_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRIMEIRO_REGISTRO_ESTADO_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRIMEIRO_REGISTRO_ESTADO_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_primeiro_registro_estado_sp_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGUNDA_VIA_LICENCIAMENTO_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGUNDA_VIA_LICENCIAMENTO_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_segunda_via_licenciamento_sp_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BOLETO_DDA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BOLETO_DDA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_boleto_dda_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARTEIRA_ELETRONICA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CARTEIRA_ELETRONICA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_carteira_eletronica_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DAED");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DAED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_daed_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DEBITOS_PENDENTES_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DEBITOS_PENDENTES_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_debitos_pendentes_sp_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DPVAT_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DPVAT_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_dpvat_sp_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IPVA_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IPVA_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_ipva_sp_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TAXAS_DETRAN_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TAXAS_DETRAN_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_taxas_detran_sp_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LICENCIAMENTO_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LICENCIAMENTO_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_licenciamento_sp_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTAS_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTAS_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_multas_sp_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTAS_RENAINFO_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTAS_RENAINFO_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_multas_renainfo_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PEC_GENERICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PEC_GENERICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_pec_generico_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSFERENCIA_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TRANSFERENCIA_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transferencia_sp_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGUNDA_VIA_TRANSFERENCIA_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGUNDA_VIA_TRANSFERENCIA_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_segunda_via_transferencia_sp_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DARF_COM_CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DARF_COM_CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_darf_com_codigo_barras_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECEBIMENTO_ELETRONICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RECEBIMENTO_ELETRONICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_recebimento_eletronico_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HABITACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HABITACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_habitacao_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BOLETO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BOLETO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_boleto_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IPVA_MG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IPVA_MG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_ipva_mg_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DPVAT_MG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DPVAT_MG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_dpvat_mg_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LICENCIAMENTO_MG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LICENCIAMENTO_MG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_licenciamento_mg_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TAXAS_VEICULOS_MG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TAXAS_VEICULOS_MG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_taxas_veiculos_mg_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TAXAS_CONDUTOR_MG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TAXAS_CONDUTOR_MG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_taxas_condutor_mg_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MEGASENA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MEGASENA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_megasena_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IPVA_RS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IPVA_RS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_ipva_rs_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DPVAT_RS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DPVAT_RS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_dpvat_rs_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTAS_RS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTAS_RS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_multas_rs_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LICENCIAMENTO_RS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LICENCIAMENTO_RS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_licenciamento_rs_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAGAMENTO_UNIFICADO_RS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PAGAMENTO_UNIFICADO_RS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_pagamento_unificado_rs_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
