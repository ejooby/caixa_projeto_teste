/**
 * Dados_saida_transacao_doc_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

public class Dados_saida_transacao_doc_Type  implements java.io.Serializable ,TransacaoInt {

	public String tipoServico;
	public String data;
	public String banco_Descricao;
	public String chaveSeguranca;

	
	
	
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_Type CONTA_ORIGEM;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_Type CONTA_DESTINO;

    private java.lang.String IDENTIFICACAO;

    private java.math.BigDecimal TARIFA_EMISSAO;

    private java.math.BigDecimal TARIFA_ADICIONAL;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_doc_Type() {
    }

    public Dados_saida_transacao_doc_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_Type CONTA_ORIGEM,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_Type CONTA_DESTINO,
           java.lang.String IDENTIFICACAO,
           java.math.BigDecimal TARIFA_EMISSAO,
           java.math.BigDecimal TARIFA_ADICIONAL,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.CONTA_ORIGEM = CONTA_ORIGEM;
           this.CONTA_DESTINO = CONTA_DESTINO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.TARIFA_EMISSAO = TARIFA_EMISSAO;
           this.TARIFA_ADICIONAL = TARIFA_ADICIONAL;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_doc_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_doc_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the CONTA_ORIGEM value for this Dados_saida_transacao_doc_Type.
     * 
     * @return CONTA_ORIGEM
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_Type getCONTA_ORIGEM() {
        return CONTA_ORIGEM;
    }


    /**
     * Sets the CONTA_ORIGEM value for this Dados_saida_transacao_doc_Type.
     * 
     * @param CONTA_ORIGEM
     */
    public void setCONTA_ORIGEM(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_Type CONTA_ORIGEM) {
        this.CONTA_ORIGEM = CONTA_ORIGEM;
    }


    /**
     * Gets the CONTA_DESTINO value for this Dados_saida_transacao_doc_Type.
     * 
     * @return CONTA_DESTINO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_Type getCONTA_DESTINO() {
        return CONTA_DESTINO;
    }


    /**
     * Sets the CONTA_DESTINO value for this Dados_saida_transacao_doc_Type.
     * 
     * @param CONTA_DESTINO
     */
    public void setCONTA_DESTINO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_Type CONTA_DESTINO) {
        this.CONTA_DESTINO = CONTA_DESTINO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_doc_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_doc_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the TARIFA_EMISSAO value for this Dados_saida_transacao_doc_Type.
     * 
     * @return TARIFA_EMISSAO
     */
    public java.math.BigDecimal getTARIFA_EMISSAO() {
        return TARIFA_EMISSAO;
    }


    /**
     * Sets the TARIFA_EMISSAO value for this Dados_saida_transacao_doc_Type.
     * 
     * @param TARIFA_EMISSAO
     */
    public void setTARIFA_EMISSAO(java.math.BigDecimal TARIFA_EMISSAO) {
        this.TARIFA_EMISSAO = TARIFA_EMISSAO;
    }


    /**
     * Gets the TARIFA_ADICIONAL value for this Dados_saida_transacao_doc_Type.
     * 
     * @return TARIFA_ADICIONAL
     */
    public java.math.BigDecimal getTARIFA_ADICIONAL() {
        return TARIFA_ADICIONAL;
    }


    /**
     * Sets the TARIFA_ADICIONAL value for this Dados_saida_transacao_doc_Type.
     * 
     * @param TARIFA_ADICIONAL
     */
    public void setTARIFA_ADICIONAL(java.math.BigDecimal TARIFA_ADICIONAL) {
        this.TARIFA_ADICIONAL = TARIFA_ADICIONAL;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_doc_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_doc_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_doc_Type)) return false;
        Dados_saida_transacao_doc_Type other = (Dados_saida_transacao_doc_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.CONTA_ORIGEM==null && other.getCONTA_ORIGEM()==null) || 
             (this.CONTA_ORIGEM!=null &&
              this.CONTA_ORIGEM.equals(other.getCONTA_ORIGEM()))) &&
            ((this.CONTA_DESTINO==null && other.getCONTA_DESTINO()==null) || 
             (this.CONTA_DESTINO!=null &&
              this.CONTA_DESTINO.equals(other.getCONTA_DESTINO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.TARIFA_EMISSAO==null && other.getTARIFA_EMISSAO()==null) || 
             (this.TARIFA_EMISSAO!=null &&
              this.TARIFA_EMISSAO.equals(other.getTARIFA_EMISSAO()))) &&
            ((this.TARIFA_ADICIONAL==null && other.getTARIFA_ADICIONAL()==null) || 
             (this.TARIFA_ADICIONAL!=null &&
              this.TARIFA_ADICIONAL.equals(other.getTARIFA_ADICIONAL()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getCONTA_ORIGEM() != null) {
            _hashCode += getCONTA_ORIGEM().hashCode();
        }
        if (getCONTA_DESTINO() != null) {
            _hashCode += getCONTA_DESTINO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getTARIFA_EMISSAO() != null) {
            _hashCode += getTARIFA_EMISSAO().hashCode();
        }
        if (getTARIFA_ADICIONAL() != null) {
            _hashCode += getTARIFA_ADICIONAL().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_doc_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_doc_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA_ORIGEM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA_ORIGEM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "conta_compe_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "conta_compe_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TARIFA_EMISSAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TARIFA_EMISSAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TARIFA_ADICIONAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TARIFA_ADICIONAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public String getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getBanco_Descricao() {
		return banco_Descricao;
	}

	public void setBanco_Descricao(String banco_Descricao) {
		this.banco_Descricao = banco_Descricao;
	}

	public String getChaveSeguranca() {
		return chaveSeguranca;
	}

	public void setChaveSeguranca(String chaveSeguranca) {
		this.chaveSeguranca = chaveSeguranca;
	}





}


