/**
 * Conta_compe_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Conta_compe_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Agencia_Type AGENCIA;

    private org.apache.axis.types.UnsignedLong CONTA;

    private org.apache.axis.types.UnsignedByte DV;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_TypeTIPO TIPO;

    private java.lang.String NOME_CLIENTE;

    private java.lang.Long CPF_CNPJ;

    public Conta_compe_Type() {
    }

    public Conta_compe_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Agencia_Type AGENCIA,
           org.apache.axis.types.UnsignedLong CONTA,
           org.apache.axis.types.UnsignedByte DV,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_TypeTIPO TIPO,
           java.lang.String NOME_CLIENTE,
           java.lang.Long CPF_CNPJ) {
           this.AGENCIA = AGENCIA;
           this.CONTA = CONTA;
           this.DV = DV;
           this.TIPO = TIPO;
           this.NOME_CLIENTE = NOME_CLIENTE;
           this.CPF_CNPJ = CPF_CNPJ;
    }


    /**
     * Gets the AGENCIA value for this Conta_compe_Type.
     * 
     * @return AGENCIA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Agencia_Type getAGENCIA() {
        return AGENCIA;
    }


    /**
     * Sets the AGENCIA value for this Conta_compe_Type.
     * 
     * @param AGENCIA
     */
    public void setAGENCIA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Agencia_Type AGENCIA) {
        this.AGENCIA = AGENCIA;
    }


    /**
     * Gets the CONTA value for this Conta_compe_Type.
     * 
     * @return CONTA
     */
    public org.apache.axis.types.UnsignedLong getCONTA() {
        return CONTA;
    }


    /**
     * Sets the CONTA value for this Conta_compe_Type.
     * 
     * @param CONTA
     */
    public void setCONTA(org.apache.axis.types.UnsignedLong CONTA) {
        this.CONTA = CONTA;
    }


    /**
     * Gets the DV value for this Conta_compe_Type.
     * 
     * @return DV
     */
    public org.apache.axis.types.UnsignedByte getDV() {
        return DV;
    }


    /**
     * Sets the DV value for this Conta_compe_Type.
     * 
     * @param DV
     */
    public void setDV(org.apache.axis.types.UnsignedByte DV) {
        this.DV = DV;
    }


    /**
     * Gets the TIPO value for this Conta_compe_Type.
     * 
     * @return TIPO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_TypeTIPO getTIPO() {
        return TIPO;
    }


    /**
     * Sets the TIPO value for this Conta_compe_Type.
     * 
     * @param TIPO
     */
    public void setTIPO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_compe_TypeTIPO TIPO) {
        this.TIPO = TIPO;
    }


    /**
     * Gets the NOME_CLIENTE value for this Conta_compe_Type.
     * 
     * @return NOME_CLIENTE
     */
    public java.lang.String getNOME_CLIENTE() {
        return NOME_CLIENTE;
    }


    /**
     * Sets the NOME_CLIENTE value for this Conta_compe_Type.
     * 
     * @param NOME_CLIENTE
     */
    public void setNOME_CLIENTE(java.lang.String NOME_CLIENTE) {
        this.NOME_CLIENTE = NOME_CLIENTE;
    }


    /**
     * Gets the CPF_CNPJ value for this Conta_compe_Type.
     * 
     * @return CPF_CNPJ
     */
    public java.lang.Long getCPF_CNPJ() {
        return CPF_CNPJ;
    }


    /**
     * Sets the CPF_CNPJ value for this Conta_compe_Type.
     * 
     * @param CPF_CNPJ
     */
    public void setCPF_CNPJ(java.lang.Long CPF_CNPJ) {
        this.CPF_CNPJ = CPF_CNPJ;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Conta_compe_Type)) return false;
        Conta_compe_Type other = (Conta_compe_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.AGENCIA==null && other.getAGENCIA()==null) || 
             (this.AGENCIA!=null &&
              this.AGENCIA.equals(other.getAGENCIA()))) &&
            ((this.CONTA==null && other.getCONTA()==null) || 
             (this.CONTA!=null &&
              this.CONTA.equals(other.getCONTA()))) &&
            ((this.DV==null && other.getDV()==null) || 
             (this.DV!=null &&
              this.DV.equals(other.getDV()))) &&
            ((this.TIPO==null && other.getTIPO()==null) || 
             (this.TIPO!=null &&
              this.TIPO.equals(other.getTIPO()))) &&
            ((this.NOME_CLIENTE==null && other.getNOME_CLIENTE()==null) || 
             (this.NOME_CLIENTE!=null &&
              this.NOME_CLIENTE.equals(other.getNOME_CLIENTE()))) &&
            ((this.CPF_CNPJ==null && other.getCPF_CNPJ()==null) || 
             (this.CPF_CNPJ!=null &&
              this.CPF_CNPJ.equals(other.getCPF_CNPJ())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAGENCIA() != null) {
            _hashCode += getAGENCIA().hashCode();
        }
        if (getCONTA() != null) {
            _hashCode += getCONTA().hashCode();
        }
        if (getDV() != null) {
            _hashCode += getDV().hashCode();
        }
        if (getTIPO() != null) {
            _hashCode += getTIPO().hashCode();
        }
        if (getNOME_CLIENTE() != null) {
            _hashCode += getNOME_CLIENTE().hashCode();
        }
        if (getCPF_CNPJ() != null) {
            _hashCode += getCPF_CNPJ().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Conta_compe_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "conta_compe_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AGENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AGENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "agencia_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DV");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">conta_compe_Type>TIPO"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_CLIENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_CLIENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF_CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF_CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
