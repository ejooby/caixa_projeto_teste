/**
 * Dados_entrada_solicitacoes_TypeSOLICITACAO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.resumo;

public class Dados_entrada_solicitacoes_TypeSOLICITACAO implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Dados_entrada_solicitacoes_TypeSOLICITACAO(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _IPVA_SP = "IPVA_SP";
    public static final java.lang.String _DPVAT_SP = "DPVAT_SP";
    public static final java.lang.String _MULTAS_SP = "MULTAS_SP";
    public static final java.lang.String _LICENCIAMENTO_SP = "LICENCIAMENTO_SP";
    public static final java.lang.String _SEGUNDA_VIA_LICENCIAMENTO_SP = "SEGUNDA_VIA_LICENCIAMENTO_SP";
    public static final java.lang.String _PRIMEIRO_REGISTRO_ESTADO_SP = "PRIMEIRO_REGISTRO_ESTADO_SP";
    public static final java.lang.String _DEBITOS_PENDENTES_SP = "DEBITOS_PENDENTES_SP";
    public static final java.lang.String _TRANSFERENCIA_SP = "TRANSFERENCIA_SP";
    public static final java.lang.String _SEGUNDA_VIA_TRANSFERENCIA_SP = "SEGUNDA_VIA_TRANSFERENCIA_SP";
    public static final java.lang.String _PEC_GENERICO = "PEC_GENERICO";
    public static final java.lang.String _MULTAS_RENAINFO_SP = "MULTAS_RENAINFO_SP";
    public static final java.lang.String _BOLETO_DDA = "BOLETO_DDA";
    public static final java.lang.String _DARF_COM_CODIGO_BARRAS = "DARF_COM_CODIGO_BARRAS";
    public static final java.lang.String _MEGASENA = "MEGASENA";
    public static final java.lang.String _RECEBIMENTO_ELETRONICO = "RECEBIMENTO_ELETRONICO";
    public static final java.lang.String _CARTEIRA_ELETRONICA = "CARTEIRA_ELETRONICA";
    public static final java.lang.String _DAED = "DAED";
    public static final java.lang.String _TAXAS_VEICULOS_MG = "TAXAS_VEICULOS_MG";
    public static final java.lang.String _IPVA_RS = "IPVA_RS";
    public static final java.lang.String _DPVAT_RS = "DPVAT_RS";
    public static final java.lang.String _MULTAS_RS = "MULTAS_RS";
    public static final java.lang.String _LICENCIAMENTO_RS = "LICENCIAMENTO_RS";
    public static final java.lang.String _PAGAMENTO_UNIFICADO_RS = "PAGAMENTO_UNIFICADO_RS";
    public static final java.lang.String _IPVA_MG = "IPVA_MG";
    public static final java.lang.String _DPVAT_MG = "DPVAT_MG";
    public static final java.lang.String _LICENCIAMENTO_MG = "LICENCIAMENTO_MG";
    public static final java.lang.String _TAXAS_DETRAN_SP = "TAXAS_DETRAN_SP";
    public static final java.lang.String _HABITACAO = "HABITACAO";
    public static final java.lang.String _BOLETO = "BOLETO";
    public static final java.lang.String _TAXAS_CONDUTOR_MG = "TAXAS_CONDUTOR_MG";
    public static final java.lang.String _CONCESSIONARIA = "CONCESSIONARIA";
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO IPVA_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_IPVA_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DPVAT_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DPVAT_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO MULTAS_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_MULTAS_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO LICENCIAMENTO_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_LICENCIAMENTO_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO SEGUNDA_VIA_LICENCIAMENTO_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_SEGUNDA_VIA_LICENCIAMENTO_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO PRIMEIRO_REGISTRO_ESTADO_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_PRIMEIRO_REGISTRO_ESTADO_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DEBITOS_PENDENTES_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DEBITOS_PENDENTES_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TRANSFERENCIA_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TRANSFERENCIA_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO SEGUNDA_VIA_TRANSFERENCIA_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_SEGUNDA_VIA_TRANSFERENCIA_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO PEC_GENERICO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_PEC_GENERICO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO MULTAS_RENAINFO_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_MULTAS_RENAINFO_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO BOLETO_DDA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_BOLETO_DDA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DARF_COM_CODIGO_BARRAS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DARF_COM_CODIGO_BARRAS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO MEGASENA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_MEGASENA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO RECEBIMENTO_ELETRONICO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_RECEBIMENTO_ELETRONICO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO CARTEIRA_ELETRONICA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_CARTEIRA_ELETRONICA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DAED = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DAED);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TAXAS_VEICULOS_MG = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TAXAS_VEICULOS_MG);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO IPVA_RS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_IPVA_RS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DPVAT_RS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DPVAT_RS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO MULTAS_RS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_MULTAS_RS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO LICENCIAMENTO_RS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_LICENCIAMENTO_RS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO PAGAMENTO_UNIFICADO_RS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_PAGAMENTO_UNIFICADO_RS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO IPVA_MG = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_IPVA_MG);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DPVAT_MG = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DPVAT_MG);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO LICENCIAMENTO_MG = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_LICENCIAMENTO_MG);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TAXAS_DETRAN_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TAXAS_DETRAN_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO HABITACAO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_HABITACAO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO BOLETO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_BOLETO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TAXAS_CONDUTOR_MG = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TAXAS_CONDUTOR_MG);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO CONCESSIONARIA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_CONCESSIONARIA);
    public java.lang.String getValue() { return _value_;}
    public static Dados_entrada_solicitacoes_TypeSOLICITACAO fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Dados_entrada_solicitacoes_TypeSOLICITACAO enumeration = (Dados_entrada_solicitacoes_TypeSOLICITACAO)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Dados_entrada_solicitacoes_TypeSOLICITACAO fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_entrada_solicitacoes_TypeSOLICITACAO.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", ">dados_entrada_solicitacoes_Type>SOLICITACAO"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
