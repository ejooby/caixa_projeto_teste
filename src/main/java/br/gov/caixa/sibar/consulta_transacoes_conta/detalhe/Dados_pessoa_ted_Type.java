/**
 * Dados_pessoa_ted_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_pessoa_ted_Type  extends br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_contas_Type TIPO_CONTA;

    public Dados_pessoa_ted_Type() {
    }

    public Dados_pessoa_ted_Type(
           java.lang.String NOME,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Cpf_cgc_Type DOCUMENTO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_contas_Type TIPO_CONTA) {
        super(
            NOME,
            DOCUMENTO);
        this.TIPO_CONTA = TIPO_CONTA;
    }


    /**
     * Gets the TIPO_CONTA value for this Dados_pessoa_ted_Type.
     * 
     * @return TIPO_CONTA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_contas_Type getTIPO_CONTA() {
        return TIPO_CONTA;
    }


    /**
     * Sets the TIPO_CONTA value for this Dados_pessoa_ted_Type.
     * 
     * @param TIPO_CONTA
     */
    public void setTIPO_CONTA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_contas_Type TIPO_CONTA) {
        this.TIPO_CONTA = TIPO_CONTA;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_pessoa_ted_Type)) return false;
        Dados_pessoa_ted_Type other = (Dados_pessoa_ted_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.TIPO_CONTA==null && other.getTIPO_CONTA()==null) || 
             (this.TIPO_CONTA!=null &&
              this.TIPO_CONTA.equals(other.getTIPO_CONTA())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTIPO_CONTA() != null) {
            _hashCode += getTIPO_CONTA().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_pessoa_ted_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_pessoa_ted_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "tipo_contas_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
