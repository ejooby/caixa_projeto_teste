/**
 * Multas_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Multas_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedByte CODIGO_RECEITA;

    private java.lang.String ORGAO;

    private java.lang.String GUIA;

    private java.util.Date VENCIMENTO;

    private java.math.BigDecimal VALOR;

    public Multas_Type() {
    }

    public Multas_Type(
           org.apache.axis.types.UnsignedByte CODIGO_RECEITA,
           java.lang.String ORGAO,
           java.lang.String GUIA,
           java.util.Date VENCIMENTO,
           java.math.BigDecimal VALOR) {
           this.CODIGO_RECEITA = CODIGO_RECEITA;
           this.ORGAO = ORGAO;
           this.GUIA = GUIA;
           this.VENCIMENTO = VENCIMENTO;
           this.VALOR = VALOR;
    }


    /**
     * Gets the CODIGO_RECEITA value for this Multas_Type.
     * 
     * @return CODIGO_RECEITA
     */
    public org.apache.axis.types.UnsignedByte getCODIGO_RECEITA() {
        return CODIGO_RECEITA;
    }


    /**
     * Sets the CODIGO_RECEITA value for this Multas_Type.
     * 
     * @param CODIGO_RECEITA
     */
    public void setCODIGO_RECEITA(org.apache.axis.types.UnsignedByte CODIGO_RECEITA) {
        this.CODIGO_RECEITA = CODIGO_RECEITA;
    }


    /**
     * Gets the ORGAO value for this Multas_Type.
     * 
     * @return ORGAO
     */
    public java.lang.String getORGAO() {
        return ORGAO;
    }


    /**
     * Sets the ORGAO value for this Multas_Type.
     * 
     * @param ORGAO
     */
    public void setORGAO(java.lang.String ORGAO) {
        this.ORGAO = ORGAO;
    }


    /**
     * Gets the GUIA value for this Multas_Type.
     * 
     * @return GUIA
     */
    public java.lang.String getGUIA() {
        return GUIA;
    }


    /**
     * Sets the GUIA value for this Multas_Type.
     * 
     * @param GUIA
     */
    public void setGUIA(java.lang.String GUIA) {
        this.GUIA = GUIA;
    }


    /**
     * Gets the VENCIMENTO value for this Multas_Type.
     * 
     * @return VENCIMENTO
     */
    public java.util.Date getVENCIMENTO() {
        return VENCIMENTO;
    }


    /**
     * Sets the VENCIMENTO value for this Multas_Type.
     * 
     * @param VENCIMENTO
     */
    public void setVENCIMENTO(java.util.Date VENCIMENTO) {
        this.VENCIMENTO = VENCIMENTO;
    }


    /**
     * Gets the VALOR value for this Multas_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Multas_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Multas_Type)) return false;
        Multas_Type other = (Multas_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CODIGO_RECEITA==null && other.getCODIGO_RECEITA()==null) || 
             (this.CODIGO_RECEITA!=null &&
              this.CODIGO_RECEITA.equals(other.getCODIGO_RECEITA()))) &&
            ((this.ORGAO==null && other.getORGAO()==null) || 
             (this.ORGAO!=null &&
              this.ORGAO.equals(other.getORGAO()))) &&
            ((this.GUIA==null && other.getGUIA()==null) || 
             (this.GUIA!=null &&
              this.GUIA.equals(other.getGUIA()))) &&
            ((this.VENCIMENTO==null && other.getVENCIMENTO()==null) || 
             (this.VENCIMENTO!=null &&
              this.VENCIMENTO.equals(other.getVENCIMENTO()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCODIGO_RECEITA() != null) {
            _hashCode += getCODIGO_RECEITA().hashCode();
        }
        if (getORGAO() != null) {
            _hashCode += getORGAO().hashCode();
        }
        if (getGUIA() != null) {
            _hashCode += getGUIA().hashCode();
        }
        if (getVENCIMENTO() != null) {
            _hashCode += getVENCIMENTO().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Multas_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "multas_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_RECEITA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_RECEITA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ORGAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ORGAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GUIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GUIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
