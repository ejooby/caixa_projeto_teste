/**
 * Consulta_Transacoes_Conta_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.ws;

public interface Consulta_Transacoes_Conta_PortType extends java.rmi.Remote {
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Servico_saida_negocial_Type LISTA(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Servico_entrada_negocial_Type parameters) throws java.rmi.RemoteException;
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_saida_negocial_Type RESUMO(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Servico_entrada_negocial_Type parameters) throws java.rmi.RemoteException;
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_saida_negocial_Type DETALHE(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_entrada_negocial_Type parameters) throws java.rmi.RemoteException;
}
