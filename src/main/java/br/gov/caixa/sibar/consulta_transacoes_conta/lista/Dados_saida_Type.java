/**
 * Dados_saida_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Dados_saida_Type  extends br.gov.caixa.sibar.DADOS_SAIDA_TYPE  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_cartao_credito_Type CARTAO_CREDITO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_pagamento_concessionaria_Type PAGAMENTO_CONCESSIONARIA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_tev_Type TEV;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_doc_Type DOC;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_ted_Type TED;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_habitacao_Type HAB;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_darf_Type DARF;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_fgts_Type FGTS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_gps_Type GPS;

    public Dados_saida_Type() {
    }

    public Dados_saida_Type(
           br.gov.caixa.sibar.CONTROLE_NEGOCIAL_TYPE[] CONTROLE_NEGOCIAL,
           java.lang.String EXCECAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_cartao_credito_Type CARTAO_CREDITO,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_pagamento_concessionaria_Type PAGAMENTO_CONCESSIONARIA,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_tev_Type TEV,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_doc_Type DOC,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_ted_Type TED,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_habitacao_Type HAB,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_darf_Type DARF,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_fgts_Type FGTS,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_gps_Type GPS) {
        super(
            CONTROLE_NEGOCIAL,
            EXCECAO);
        this.CARTAO_CREDITO = CARTAO_CREDITO;
        this.PAGAMENTO_CONCESSIONARIA = PAGAMENTO_CONCESSIONARIA;
        this.TEV = TEV;
        this.DOC = DOC;
        this.TED = TED;
        this.HAB = HAB;
        this.DARF = DARF;
        this.FGTS = FGTS;
        this.GPS = GPS;
    }


    /**
     * Gets the CARTAO_CREDITO value for this Dados_saida_Type.
     * 
     * @return CARTAO_CREDITO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_cartao_credito_Type getCARTAO_CREDITO() {
        return CARTAO_CREDITO;
    }


    /**
     * Sets the CARTAO_CREDITO value for this Dados_saida_Type.
     * 
     * @param CARTAO_CREDITO
     */
    public void setCARTAO_CREDITO(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_cartao_credito_Type CARTAO_CREDITO) {
        this.CARTAO_CREDITO = CARTAO_CREDITO;
    }


    /**
     * Gets the PAGAMENTO_CONCESSIONARIA value for this Dados_saida_Type.
     * 
     * @return PAGAMENTO_CONCESSIONARIA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_pagamento_concessionaria_Type getPAGAMENTO_CONCESSIONARIA() {
        return PAGAMENTO_CONCESSIONARIA;
    }


    /**
     * Sets the PAGAMENTO_CONCESSIONARIA value for this Dados_saida_Type.
     * 
     * @param PAGAMENTO_CONCESSIONARIA
     */
    public void setPAGAMENTO_CONCESSIONARIA(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_pagamento_concessionaria_Type PAGAMENTO_CONCESSIONARIA) {
        this.PAGAMENTO_CONCESSIONARIA = PAGAMENTO_CONCESSIONARIA;
    }


    /**
     * Gets the TEV value for this Dados_saida_Type.
     * 
     * @return TEV
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_tev_Type getTEV() {
        return TEV;
    }


    /**
     * Sets the TEV value for this Dados_saida_Type.
     * 
     * @param TEV
     */
    public void setTEV(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_tev_Type TEV) {
        this.TEV = TEV;
    }


    /**
     * Gets the DOC value for this Dados_saida_Type.
     * 
     * @return DOC
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_doc_Type getDOC() {
        return DOC;
    }


    /**
     * Sets the DOC value for this Dados_saida_Type.
     * 
     * @param DOC
     */
    public void setDOC(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_doc_Type DOC) {
        this.DOC = DOC;
    }


    /**
     * Gets the TED value for this Dados_saida_Type.
     * 
     * @return TED
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_ted_Type getTED() {
        return TED;
    }


    /**
     * Sets the TED value for this Dados_saida_Type.
     * 
     * @param TED
     */
    public void setTED(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_ted_Type TED) {
        this.TED = TED;
    }


    /**
     * Gets the HAB value for this Dados_saida_Type.
     * 
     * @return HAB
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_habitacao_Type getHAB() {
        return HAB;
    }


    /**
     * Sets the HAB value for this Dados_saida_Type.
     * 
     * @param HAB
     */
    public void setHAB(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_habitacao_Type HAB) {
        this.HAB = HAB;
    }


    /**
     * Gets the DARF value for this Dados_saida_Type.
     * 
     * @return DARF
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_darf_Type getDARF() {
        return DARF;
    }


    /**
     * Sets the DARF value for this Dados_saida_Type.
     * 
     * @param DARF
     */
    public void setDARF(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_darf_Type DARF) {
        this.DARF = DARF;
    }


    /**
     * Gets the FGTS value for this Dados_saida_Type.
     * 
     * @return FGTS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_fgts_Type getFGTS() {
        return FGTS;
    }


    /**
     * Sets the FGTS value for this Dados_saida_Type.
     * 
     * @param FGTS
     */
    public void setFGTS(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_fgts_Type FGTS) {
        this.FGTS = FGTS;
    }


    /**
     * Gets the GPS value for this Dados_saida_Type.
     * 
     * @return GPS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_gps_Type getGPS() {
        return GPS;
    }


    /**
     * Sets the GPS value for this Dados_saida_Type.
     * 
     * @param GPS
     */
    public void setGPS(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Dados_saida_gps_Type GPS) {
        this.GPS = GPS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_Type)) return false;
        Dados_saida_Type other = (Dados_saida_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.CARTAO_CREDITO==null && other.getCARTAO_CREDITO()==null) || 
             (this.CARTAO_CREDITO!=null &&
              this.CARTAO_CREDITO.equals(other.getCARTAO_CREDITO()))) &&
            ((this.PAGAMENTO_CONCESSIONARIA==null && other.getPAGAMENTO_CONCESSIONARIA()==null) || 
             (this.PAGAMENTO_CONCESSIONARIA!=null &&
              this.PAGAMENTO_CONCESSIONARIA.equals(other.getPAGAMENTO_CONCESSIONARIA()))) &&
            ((this.TEV==null && other.getTEV()==null) || 
             (this.TEV!=null &&
              this.TEV.equals(other.getTEV()))) &&
            ((this.DOC==null && other.getDOC()==null) || 
             (this.DOC!=null &&
              this.DOC.equals(other.getDOC()))) &&
            ((this.TED==null && other.getTED()==null) || 
             (this.TED!=null &&
              this.TED.equals(other.getTED()))) &&
            ((this.HAB==null && other.getHAB()==null) || 
             (this.HAB!=null &&
              this.HAB.equals(other.getHAB()))) &&
            ((this.DARF==null && other.getDARF()==null) || 
             (this.DARF!=null &&
              this.DARF.equals(other.getDARF()))) &&
            ((this.FGTS==null && other.getFGTS()==null) || 
             (this.FGTS!=null &&
              this.FGTS.equals(other.getFGTS()))) &&
            ((this.GPS==null && other.getGPS()==null) || 
             (this.GPS!=null &&
              this.GPS.equals(other.getGPS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCARTAO_CREDITO() != null) {
            _hashCode += getCARTAO_CREDITO().hashCode();
        }
        if (getPAGAMENTO_CONCESSIONARIA() != null) {
            _hashCode += getPAGAMENTO_CONCESSIONARIA().hashCode();
        }
        if (getTEV() != null) {
            _hashCode += getTEV().hashCode();
        }
        if (getDOC() != null) {
            _hashCode += getDOC().hashCode();
        }
        if (getTED() != null) {
            _hashCode += getTED().hashCode();
        }
        if (getHAB() != null) {
            _hashCode += getHAB().hashCode();
        }
        if (getDARF() != null) {
            _hashCode += getDARF().hashCode();
        }
        if (getFGTS() != null) {
            _hashCode += getFGTS().hashCode();
        }
        if (getGPS() != null) {
            _hashCode += getGPS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARTAO_CREDITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CARTAO_CREDITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_cartao_credito_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAGAMENTO_CONCESSIONARIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PAGAMENTO_CONCESSIONARIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_pagamento_concessionaria_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TEV");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TEV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_tev_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DOC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_doc_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TED");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_ted_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HAB");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HAB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_habitacao_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DARF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DARF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_darf_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FGTS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FGTS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_fgts_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GPS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GPS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_gps_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
