/**
 * Destinatario_boleto_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Destinatario_boleto_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedShort BANCO;

    private org.apache.axis.types.UnsignedInt ISPB;

    private java.lang.String NOME_BANCO;

    public Destinatario_boleto_Type() {
    }

    public Destinatario_boleto_Type(
           org.apache.axis.types.UnsignedShort BANCO,
           org.apache.axis.types.UnsignedInt ISPB,
           java.lang.String NOME_BANCO) {
           this.BANCO = BANCO;
           this.ISPB = ISPB;
           this.NOME_BANCO = NOME_BANCO;
    }


    /**
     * Gets the BANCO value for this Destinatario_boleto_Type.
     * 
     * @return BANCO
     */
    public org.apache.axis.types.UnsignedShort getBANCO() {
        return BANCO;
    }


    /**
     * Sets the BANCO value for this Destinatario_boleto_Type.
     * 
     * @param BANCO
     */
    public void setBANCO(org.apache.axis.types.UnsignedShort BANCO) {
        this.BANCO = BANCO;
    }


    /**
     * Gets the ISPB value for this Destinatario_boleto_Type.
     * 
     * @return ISPB
     */
    public org.apache.axis.types.UnsignedInt getISPB() {
        return ISPB;
    }


    /**
     * Sets the ISPB value for this Destinatario_boleto_Type.
     * 
     * @param ISPB
     */
    public void setISPB(org.apache.axis.types.UnsignedInt ISPB) {
        this.ISPB = ISPB;
    }


    /**
     * Gets the NOME_BANCO value for this Destinatario_boleto_Type.
     * 
     * @return NOME_BANCO
     */
    public java.lang.String getNOME_BANCO() {
        return NOME_BANCO;
    }


    /**
     * Sets the NOME_BANCO value for this Destinatario_boleto_Type.
     * 
     * @param NOME_BANCO
     */
    public void setNOME_BANCO(java.lang.String NOME_BANCO) {
        this.NOME_BANCO = NOME_BANCO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Destinatario_boleto_Type)) return false;
        Destinatario_boleto_Type other = (Destinatario_boleto_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.BANCO==null && other.getBANCO()==null) || 
             (this.BANCO!=null &&
              this.BANCO.equals(other.getBANCO()))) &&
            ((this.ISPB==null && other.getISPB()==null) || 
             (this.ISPB!=null &&
              this.ISPB.equals(other.getISPB()))) &&
            ((this.NOME_BANCO==null && other.getNOME_BANCO()==null) || 
             (this.NOME_BANCO!=null &&
              this.NOME_BANCO.equals(other.getNOME_BANCO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBANCO() != null) {
            _hashCode += getBANCO().hashCode();
        }
        if (getISPB() != null) {
            _hashCode += getISPB().hashCode();
        }
        if (getNOME_BANCO() != null) {
            _hashCode += getNOME_BANCO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Destinatario_boleto_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "destinatario_boleto_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BANCO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BANCO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISPB");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ISPB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_BANCO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_BANCO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
