/**
 * Multas_renainfo_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Multas_renainfo_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Orgao_atuador_Type ORGAO_ATUADOR;

    private java.lang.String NUMERO_GUIA;

    private java.util.Date VENCIMENTO;

    private java.math.BigDecimal VALOR;

    private java.lang.String UF;

    private java.lang.String AUTO_INFRACAO;

    public Multas_renainfo_Type() {
    }

    public Multas_renainfo_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Orgao_atuador_Type ORGAO_ATUADOR,
           java.lang.String NUMERO_GUIA,
           java.util.Date VENCIMENTO,
           java.math.BigDecimal VALOR,
           java.lang.String UF,
           java.lang.String AUTO_INFRACAO) {
           this.ORGAO_ATUADOR = ORGAO_ATUADOR;
           this.NUMERO_GUIA = NUMERO_GUIA;
           this.VENCIMENTO = VENCIMENTO;
           this.VALOR = VALOR;
           this.UF = UF;
           this.AUTO_INFRACAO = AUTO_INFRACAO;
    }


    /**
     * Gets the ORGAO_ATUADOR value for this Multas_renainfo_Type.
     * 
     * @return ORGAO_ATUADOR
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Orgao_atuador_Type getORGAO_ATUADOR() {
        return ORGAO_ATUADOR;
    }


    /**
     * Sets the ORGAO_ATUADOR value for this Multas_renainfo_Type.
     * 
     * @param ORGAO_ATUADOR
     */
    public void setORGAO_ATUADOR(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Orgao_atuador_Type ORGAO_ATUADOR) {
        this.ORGAO_ATUADOR = ORGAO_ATUADOR;
    }


    /**
     * Gets the NUMERO_GUIA value for this Multas_renainfo_Type.
     * 
     * @return NUMERO_GUIA
     */
    public java.lang.String getNUMERO_GUIA() {
        return NUMERO_GUIA;
    }


    /**
     * Sets the NUMERO_GUIA value for this Multas_renainfo_Type.
     * 
     * @param NUMERO_GUIA
     */
    public void setNUMERO_GUIA(java.lang.String NUMERO_GUIA) {
        this.NUMERO_GUIA = NUMERO_GUIA;
    }


    /**
     * Gets the VENCIMENTO value for this Multas_renainfo_Type.
     * 
     * @return VENCIMENTO
     */
    public java.util.Date getVENCIMENTO() {
        return VENCIMENTO;
    }


    /**
     * Sets the VENCIMENTO value for this Multas_renainfo_Type.
     * 
     * @param VENCIMENTO
     */
    public void setVENCIMENTO(java.util.Date VENCIMENTO) {
        this.VENCIMENTO = VENCIMENTO;
    }


    /**
     * Gets the VALOR value for this Multas_renainfo_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Multas_renainfo_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the UF value for this Multas_renainfo_Type.
     * 
     * @return UF
     */
    public java.lang.String getUF() {
        return UF;
    }


    /**
     * Sets the UF value for this Multas_renainfo_Type.
     * 
     * @param UF
     */
    public void setUF(java.lang.String UF) {
        this.UF = UF;
    }


    /**
     * Gets the AUTO_INFRACAO value for this Multas_renainfo_Type.
     * 
     * @return AUTO_INFRACAO
     */
    public java.lang.String getAUTO_INFRACAO() {
        return AUTO_INFRACAO;
    }


    /**
     * Sets the AUTO_INFRACAO value for this Multas_renainfo_Type.
     * 
     * @param AUTO_INFRACAO
     */
    public void setAUTO_INFRACAO(java.lang.String AUTO_INFRACAO) {
        this.AUTO_INFRACAO = AUTO_INFRACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Multas_renainfo_Type)) return false;
        Multas_renainfo_Type other = (Multas_renainfo_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ORGAO_ATUADOR==null && other.getORGAO_ATUADOR()==null) || 
             (this.ORGAO_ATUADOR!=null &&
              this.ORGAO_ATUADOR.equals(other.getORGAO_ATUADOR()))) &&
            ((this.NUMERO_GUIA==null && other.getNUMERO_GUIA()==null) || 
             (this.NUMERO_GUIA!=null &&
              this.NUMERO_GUIA.equals(other.getNUMERO_GUIA()))) &&
            ((this.VENCIMENTO==null && other.getVENCIMENTO()==null) || 
             (this.VENCIMENTO!=null &&
              this.VENCIMENTO.equals(other.getVENCIMENTO()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.UF==null && other.getUF()==null) || 
             (this.UF!=null &&
              this.UF.equals(other.getUF()))) &&
            ((this.AUTO_INFRACAO==null && other.getAUTO_INFRACAO()==null) || 
             (this.AUTO_INFRACAO!=null &&
              this.AUTO_INFRACAO.equals(other.getAUTO_INFRACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getORGAO_ATUADOR() != null) {
            _hashCode += getORGAO_ATUADOR().hashCode();
        }
        if (getNUMERO_GUIA() != null) {
            _hashCode += getNUMERO_GUIA().hashCode();
        }
        if (getVENCIMENTO() != null) {
            _hashCode += getVENCIMENTO().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getUF() != null) {
            _hashCode += getUF().hashCode();
        }
        if (getAUTO_INFRACAO() != null) {
            _hashCode += getAUTO_INFRACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Multas_renainfo_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "multas_renainfo_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ORGAO_ATUADOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ORGAO_ATUADOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "orgao_atuador_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO_GUIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO_GUIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AUTO_INFRACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AUTO_INFRACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
