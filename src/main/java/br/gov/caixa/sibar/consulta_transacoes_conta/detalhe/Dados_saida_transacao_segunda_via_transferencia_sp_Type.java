/**
 * Dados_saida_transacao_segunda_via_transferencia_sp_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_segunda_via_transferencia_sp_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private org.apache.axis.types.UnsignedByte SEGMENTO;

    private org.apache.axis.types.UnsignedInt CONVENIO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Veiculo_Type VEICULO;

    private java.lang.String UF;

    private org.apache.axis.types.UnsignedInt MUNICIPIO;

    private java.lang.String PROPRIETARIO;

    private java.lang.String AUTENTICACAO_DIGITAL;

    private java.lang.String[] CODIGO_BARRAS;

    private org.apache.axis.types.UnsignedInt NSU_CONVENIO;

    private java.math.BigDecimal TAXA_LICENCIAMENTO;

    private java.math.BigDecimal TARIFA_TRANSFERENCIA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Ipva_Type[] IPVA_PAGO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dpvat_Type[] DPVAT_PAGO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Multas_Type[] MULTA_PAGA;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_segunda_via_transferencia_sp_Type() {
    }

    public Dados_saida_transacao_segunda_via_transferencia_sp_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           org.apache.axis.types.UnsignedByte SEGMENTO,
           org.apache.axis.types.UnsignedInt CONVENIO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Veiculo_Type VEICULO,
           java.lang.String UF,
           org.apache.axis.types.UnsignedInt MUNICIPIO,
           java.lang.String PROPRIETARIO,
           java.lang.String AUTENTICACAO_DIGITAL,
           java.lang.String[] CODIGO_BARRAS,
           org.apache.axis.types.UnsignedInt NSU_CONVENIO,
           java.math.BigDecimal TAXA_LICENCIAMENTO,
           java.math.BigDecimal TARIFA_TRANSFERENCIA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Ipva_Type[] IPVA_PAGO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dpvat_Type[] DPVAT_PAGO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Multas_Type[] MULTA_PAGA,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.SEGMENTO = SEGMENTO;
           this.CONVENIO = CONVENIO;
           this.VEICULO = VEICULO;
           this.UF = UF;
           this.MUNICIPIO = MUNICIPIO;
           this.PROPRIETARIO = PROPRIETARIO;
           this.AUTENTICACAO_DIGITAL = AUTENTICACAO_DIGITAL;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.NSU_CONVENIO = NSU_CONVENIO;
           this.TAXA_LICENCIAMENTO = TAXA_LICENCIAMENTO;
           this.TARIFA_TRANSFERENCIA = TARIFA_TRANSFERENCIA;
           this.IPVA_PAGO = IPVA_PAGO;
           this.DPVAT_PAGO = DPVAT_PAGO;
           this.MULTA_PAGA = MULTA_PAGA;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the SEGMENTO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return SEGMENTO
     */
    public org.apache.axis.types.UnsignedByte getSEGMENTO() {
        return SEGMENTO;
    }


    /**
     * Sets the SEGMENTO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param SEGMENTO
     */
    public void setSEGMENTO(org.apache.axis.types.UnsignedByte SEGMENTO) {
        this.SEGMENTO = SEGMENTO;
    }


    /**
     * Gets the CONVENIO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return CONVENIO
     */
    public org.apache.axis.types.UnsignedInt getCONVENIO() {
        return CONVENIO;
    }


    /**
     * Sets the CONVENIO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param CONVENIO
     */
    public void setCONVENIO(org.apache.axis.types.UnsignedInt CONVENIO) {
        this.CONVENIO = CONVENIO;
    }


    /**
     * Gets the VEICULO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return VEICULO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Veiculo_Type getVEICULO() {
        return VEICULO;
    }


    /**
     * Sets the VEICULO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param VEICULO
     */
    public void setVEICULO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Veiculo_Type VEICULO) {
        this.VEICULO = VEICULO;
    }


    /**
     * Gets the UF value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return UF
     */
    public java.lang.String getUF() {
        return UF;
    }


    /**
     * Sets the UF value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param UF
     */
    public void setUF(java.lang.String UF) {
        this.UF = UF;
    }


    /**
     * Gets the MUNICIPIO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return MUNICIPIO
     */
    public org.apache.axis.types.UnsignedInt getMUNICIPIO() {
        return MUNICIPIO;
    }


    /**
     * Sets the MUNICIPIO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param MUNICIPIO
     */
    public void setMUNICIPIO(org.apache.axis.types.UnsignedInt MUNICIPIO) {
        this.MUNICIPIO = MUNICIPIO;
    }


    /**
     * Gets the PROPRIETARIO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return PROPRIETARIO
     */
    public java.lang.String getPROPRIETARIO() {
        return PROPRIETARIO;
    }


    /**
     * Sets the PROPRIETARIO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param PROPRIETARIO
     */
    public void setPROPRIETARIO(java.lang.String PROPRIETARIO) {
        this.PROPRIETARIO = PROPRIETARIO;
    }


    /**
     * Gets the AUTENTICACAO_DIGITAL value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return AUTENTICACAO_DIGITAL
     */
    public java.lang.String getAUTENTICACAO_DIGITAL() {
        return AUTENTICACAO_DIGITAL;
    }


    /**
     * Sets the AUTENTICACAO_DIGITAL value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param AUTENTICACAO_DIGITAL
     */
    public void setAUTENTICACAO_DIGITAL(java.lang.String AUTENTICACAO_DIGITAL) {
        this.AUTENTICACAO_DIGITAL = AUTENTICACAO_DIGITAL;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String[] getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String[] CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }

    public java.lang.String getCODIGO_BARRAS(int i) {
        return this.CODIGO_BARRAS[i];
    }

    public void setCODIGO_BARRAS(int i, java.lang.String _value) {
        this.CODIGO_BARRAS[i] = _value;
    }


    /**
     * Gets the NSU_CONVENIO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return NSU_CONVENIO
     */
    public org.apache.axis.types.UnsignedInt getNSU_CONVENIO() {
        return NSU_CONVENIO;
    }


    /**
     * Sets the NSU_CONVENIO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param NSU_CONVENIO
     */
    public void setNSU_CONVENIO(org.apache.axis.types.UnsignedInt NSU_CONVENIO) {
        this.NSU_CONVENIO = NSU_CONVENIO;
    }


    /**
     * Gets the TAXA_LICENCIAMENTO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return TAXA_LICENCIAMENTO
     */
    public java.math.BigDecimal getTAXA_LICENCIAMENTO() {
        return TAXA_LICENCIAMENTO;
    }


    /**
     * Sets the TAXA_LICENCIAMENTO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param TAXA_LICENCIAMENTO
     */
    public void setTAXA_LICENCIAMENTO(java.math.BigDecimal TAXA_LICENCIAMENTO) {
        this.TAXA_LICENCIAMENTO = TAXA_LICENCIAMENTO;
    }


    /**
     * Gets the TARIFA_TRANSFERENCIA value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return TARIFA_TRANSFERENCIA
     */
    public java.math.BigDecimal getTARIFA_TRANSFERENCIA() {
        return TARIFA_TRANSFERENCIA;
    }


    /**
     * Sets the TARIFA_TRANSFERENCIA value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param TARIFA_TRANSFERENCIA
     */
    public void setTARIFA_TRANSFERENCIA(java.math.BigDecimal TARIFA_TRANSFERENCIA) {
        this.TARIFA_TRANSFERENCIA = TARIFA_TRANSFERENCIA;
    }


    /**
     * Gets the IPVA_PAGO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return IPVA_PAGO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Ipva_Type[] getIPVA_PAGO() {
        return IPVA_PAGO;
    }


    /**
     * Sets the IPVA_PAGO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param IPVA_PAGO
     */
    public void setIPVA_PAGO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Ipva_Type[] IPVA_PAGO) {
        this.IPVA_PAGO = IPVA_PAGO;
    }

    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Ipva_Type getIPVA_PAGO(int i) {
        return this.IPVA_PAGO[i];
    }

    public void setIPVA_PAGO(int i, br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Ipva_Type _value) {
        this.IPVA_PAGO[i] = _value;
    }


    /**
     * Gets the DPVAT_PAGO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return DPVAT_PAGO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dpvat_Type[] getDPVAT_PAGO() {
        return DPVAT_PAGO;
    }


    /**
     * Sets the DPVAT_PAGO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param DPVAT_PAGO
     */
    public void setDPVAT_PAGO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dpvat_Type[] DPVAT_PAGO) {
        this.DPVAT_PAGO = DPVAT_PAGO;
    }

    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dpvat_Type getDPVAT_PAGO(int i) {
        return this.DPVAT_PAGO[i];
    }

    public void setDPVAT_PAGO(int i, br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dpvat_Type _value) {
        this.DPVAT_PAGO[i] = _value;
    }


    /**
     * Gets the MULTA_PAGA value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return MULTA_PAGA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Multas_Type[] getMULTA_PAGA() {
        return MULTA_PAGA;
    }


    /**
     * Sets the MULTA_PAGA value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param MULTA_PAGA
     */
    public void setMULTA_PAGA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Multas_Type[] MULTA_PAGA) {
        this.MULTA_PAGA = MULTA_PAGA;
    }

    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Multas_Type getMULTA_PAGA(int i) {
        return this.MULTA_PAGA[i];
    }

    public void setMULTA_PAGA(int i, br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Multas_Type _value) {
        this.MULTA_PAGA[i] = _value;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_segunda_via_transferencia_sp_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_segunda_via_transferencia_sp_Type)) return false;
        Dados_saida_transacao_segunda_via_transferencia_sp_Type other = (Dados_saida_transacao_segunda_via_transferencia_sp_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.SEGMENTO==null && other.getSEGMENTO()==null) || 
             (this.SEGMENTO!=null &&
              this.SEGMENTO.equals(other.getSEGMENTO()))) &&
            ((this.CONVENIO==null && other.getCONVENIO()==null) || 
             (this.CONVENIO!=null &&
              this.CONVENIO.equals(other.getCONVENIO()))) &&
            ((this.VEICULO==null && other.getVEICULO()==null) || 
             (this.VEICULO!=null &&
              this.VEICULO.equals(other.getVEICULO()))) &&
            ((this.UF==null && other.getUF()==null) || 
             (this.UF!=null &&
              this.UF.equals(other.getUF()))) &&
            ((this.MUNICIPIO==null && other.getMUNICIPIO()==null) || 
             (this.MUNICIPIO!=null &&
              this.MUNICIPIO.equals(other.getMUNICIPIO()))) &&
            ((this.PROPRIETARIO==null && other.getPROPRIETARIO()==null) || 
             (this.PROPRIETARIO!=null &&
              this.PROPRIETARIO.equals(other.getPROPRIETARIO()))) &&
            ((this.AUTENTICACAO_DIGITAL==null && other.getAUTENTICACAO_DIGITAL()==null) || 
             (this.AUTENTICACAO_DIGITAL!=null &&
              this.AUTENTICACAO_DIGITAL.equals(other.getAUTENTICACAO_DIGITAL()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              java.util.Arrays.equals(this.CODIGO_BARRAS, other.getCODIGO_BARRAS()))) &&
            ((this.NSU_CONVENIO==null && other.getNSU_CONVENIO()==null) || 
             (this.NSU_CONVENIO!=null &&
              this.NSU_CONVENIO.equals(other.getNSU_CONVENIO()))) &&
            ((this.TAXA_LICENCIAMENTO==null && other.getTAXA_LICENCIAMENTO()==null) || 
             (this.TAXA_LICENCIAMENTO!=null &&
              this.TAXA_LICENCIAMENTO.equals(other.getTAXA_LICENCIAMENTO()))) &&
            ((this.TARIFA_TRANSFERENCIA==null && other.getTARIFA_TRANSFERENCIA()==null) || 
             (this.TARIFA_TRANSFERENCIA!=null &&
              this.TARIFA_TRANSFERENCIA.equals(other.getTARIFA_TRANSFERENCIA()))) &&
            ((this.IPVA_PAGO==null && other.getIPVA_PAGO()==null) || 
             (this.IPVA_PAGO!=null &&
              java.util.Arrays.equals(this.IPVA_PAGO, other.getIPVA_PAGO()))) &&
            ((this.DPVAT_PAGO==null && other.getDPVAT_PAGO()==null) || 
             (this.DPVAT_PAGO!=null &&
              java.util.Arrays.equals(this.DPVAT_PAGO, other.getDPVAT_PAGO()))) &&
            ((this.MULTA_PAGA==null && other.getMULTA_PAGA()==null) || 
             (this.MULTA_PAGA!=null &&
              java.util.Arrays.equals(this.MULTA_PAGA, other.getMULTA_PAGA()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getSEGMENTO() != null) {
            _hashCode += getSEGMENTO().hashCode();
        }
        if (getCONVENIO() != null) {
            _hashCode += getCONVENIO().hashCode();
        }
        if (getVEICULO() != null) {
            _hashCode += getVEICULO().hashCode();
        }
        if (getUF() != null) {
            _hashCode += getUF().hashCode();
        }
        if (getMUNICIPIO() != null) {
            _hashCode += getMUNICIPIO().hashCode();
        }
        if (getPROPRIETARIO() != null) {
            _hashCode += getPROPRIETARIO().hashCode();
        }
        if (getAUTENTICACAO_DIGITAL() != null) {
            _hashCode += getAUTENTICACAO_DIGITAL().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCODIGO_BARRAS());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCODIGO_BARRAS(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNSU_CONVENIO() != null) {
            _hashCode += getNSU_CONVENIO().hashCode();
        }
        if (getTAXA_LICENCIAMENTO() != null) {
            _hashCode += getTAXA_LICENCIAMENTO().hashCode();
        }
        if (getTARIFA_TRANSFERENCIA() != null) {
            _hashCode += getTARIFA_TRANSFERENCIA().hashCode();
        }
        if (getIPVA_PAGO() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getIPVA_PAGO());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getIPVA_PAGO(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDPVAT_PAGO() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDPVAT_PAGO());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDPVAT_PAGO(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMULTA_PAGA() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMULTA_PAGA());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMULTA_PAGA(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_segunda_via_transferencia_sp_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_segunda_via_transferencia_sp_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONVENIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONVENIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VEICULO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VEICULO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "veiculo_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MUNICIPIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MUNICIPIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROPRIETARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PROPRIETARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AUTENTICACAO_DIGITAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AUTENTICACAO_DIGITAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_segunda_via_transferencia_sp_Type>CODIGO_BARRAS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU_CONVENIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU_CONVENIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TAXA_LICENCIAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TAXA_LICENCIAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TARIFA_TRANSFERENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TARIFA_TRANSFERENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IPVA_PAGO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IPVA_PAGO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "ipva_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DPVAT_PAGO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DPVAT_PAGO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dpvat_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTA_PAGA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTA_PAGA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "multas_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
