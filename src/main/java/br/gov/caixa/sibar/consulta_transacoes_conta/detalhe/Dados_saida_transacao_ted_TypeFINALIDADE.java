/**
 * Dados_saida_transacao_ted_TypeFINALIDADE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_ted_TypeFINALIDADE implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Dados_saida_transacao_ted_TypeFINALIDADE(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "Pagamento de Impostos, Tributos e Taxas";
    public static final java.lang.String _value2 = "Pagamento de Concessionaria de Servicos Publicos";
    public static final java.lang.String _value3 = "Pagamento de Dividendos";
    public static final java.lang.String _value4 = "Pagamentos de Salarios";
    public static final java.lang.String _value5 = "Pagamento de Fornecedores";
    public static final java.lang.String _value6 = "Pagamento de Honorarios";
    public static final java.lang.String _value7 = "Pagamento de Alugueis e Taxas de Condominio";
    public static final java.lang.String _value8 = "Pagamento de Duplicatas e Titulos";
    public static final java.lang.String _value9 = "Pagamento de Mensalidade Escolar";
    public static final java.lang.String _value10 = "Credito em Conta";
    public static final java.lang.String _value11 = "Deposito Judicial";
    public static final java.lang.String _value12 = "Pensao Alimenticia";
    public static final java.lang.String _value13 = "Transferencia entre Contas de Mesma Titulariedade";
    public static final java.lang.String _value14 = "Transferencia Internacional de Reais";
    public static final java.lang.String _value15 = "Ajuste Posicao Mercado Futuro";
    public static final java.lang.String _value16 = "Restituicao Imposto Renda";
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value1 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value1);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value2 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value2);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value3 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value3);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value4 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value4);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value5 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value5);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value6 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value6);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value7 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value7);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value8 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value8);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value9 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value9);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value10 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value10);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value11 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value11);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value12 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value12);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value13 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value13);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value14 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value14);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value15 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value15);
    public static final Dados_saida_transacao_ted_TypeFINALIDADE value16 = new Dados_saida_transacao_ted_TypeFINALIDADE(_value16);
    public java.lang.String getValue() { return _value_;}
    public static Dados_saida_transacao_ted_TypeFINALIDADE fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Dados_saida_transacao_ted_TypeFINALIDADE enumeration = (Dados_saida_transacao_ted_TypeFINALIDADE)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Dados_saida_transacao_ted_TypeFINALIDADE fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_ted_TypeFINALIDADE.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_ted_Type>FINALIDADE"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
