/**
 * Dados_saida_transacao_pec_generico_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_pec_generico_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private org.apache.axis.types.UnsignedByte SEGMENTO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_pec_Type CONVENIO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_pec_Type SERVICO;

    private java.lang.String[] IDENTIFICADOR_CONTA;

    private java.lang.String CLIENTE;

    private org.apache.axis.types.UnsignedLong CPF_CNPJ;

    private java.lang.String CODIGO_BARRAS;

    private java.lang.String REFERENCIA;

    private java.lang.Boolean PAGAMENTO_PARCIAL;

    private java.math.BigDecimal VALOR_ORIGINAL;

    private java.math.BigDecimal VALOR_PAGAMENTO;

    private java.math.BigDecimal VALOR_MULTA;

    private java.math.BigDecimal VALOR_JUROS;

    private org.apache.axis.types.UnsignedInt NSU_CONVENIO;

    private java.util.Date DATA_DEBITO;

    private java.util.Date DATA_OPERACAO;

    private java.util.Date DATA_VENCIMENTO;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_pec_generico_Type() {
    }

    public Dados_saida_transacao_pec_generico_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           org.apache.axis.types.UnsignedByte SEGMENTO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_pec_Type CONVENIO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_pec_Type SERVICO,
           java.lang.String[] IDENTIFICADOR_CONTA,
           java.lang.String CLIENTE,
           org.apache.axis.types.UnsignedLong CPF_CNPJ,
           java.lang.String CODIGO_BARRAS,
           java.lang.String REFERENCIA,
           java.lang.Boolean PAGAMENTO_PARCIAL,
           java.math.BigDecimal VALOR_ORIGINAL,
           java.math.BigDecimal VALOR_PAGAMENTO,
           java.math.BigDecimal VALOR_MULTA,
           java.math.BigDecimal VALOR_JUROS,
           org.apache.axis.types.UnsignedInt NSU_CONVENIO,
           java.util.Date DATA_DEBITO,
           java.util.Date DATA_OPERACAO,
           java.util.Date DATA_VENCIMENTO,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.SEGMENTO = SEGMENTO;
           this.CONVENIO = CONVENIO;
           this.SERVICO = SERVICO;
           this.IDENTIFICADOR_CONTA = IDENTIFICADOR_CONTA;
           this.CLIENTE = CLIENTE;
           this.CPF_CNPJ = CPF_CNPJ;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.REFERENCIA = REFERENCIA;
           this.PAGAMENTO_PARCIAL = PAGAMENTO_PARCIAL;
           this.VALOR_ORIGINAL = VALOR_ORIGINAL;
           this.VALOR_PAGAMENTO = VALOR_PAGAMENTO;
           this.VALOR_MULTA = VALOR_MULTA;
           this.VALOR_JUROS = VALOR_JUROS;
           this.NSU_CONVENIO = NSU_CONVENIO;
           this.DATA_DEBITO = DATA_DEBITO;
           this.DATA_OPERACAO = DATA_OPERACAO;
           this.DATA_VENCIMENTO = DATA_VENCIMENTO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the SEGMENTO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return SEGMENTO
     */
    public org.apache.axis.types.UnsignedByte getSEGMENTO() {
        return SEGMENTO;
    }


    /**
     * Sets the SEGMENTO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param SEGMENTO
     */
    public void setSEGMENTO(org.apache.axis.types.UnsignedByte SEGMENTO) {
        this.SEGMENTO = SEGMENTO;
    }


    /**
     * Gets the CONVENIO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return CONVENIO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_pec_Type getCONVENIO() {
        return CONVENIO;
    }


    /**
     * Sets the CONVENIO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param CONVENIO
     */
    public void setCONVENIO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_pec_Type CONVENIO) {
        this.CONVENIO = CONVENIO;
    }


    /**
     * Gets the SERVICO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return SERVICO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_pec_Type getSERVICO() {
        return SERVICO;
    }


    /**
     * Sets the SERVICO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param SERVICO
     */
    public void setSERVICO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Servico_pec_Type SERVICO) {
        this.SERVICO = SERVICO;
    }


    /**
     * Gets the IDENTIFICADOR_CONTA value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return IDENTIFICADOR_CONTA
     */
    public java.lang.String[] getIDENTIFICADOR_CONTA() {
        return IDENTIFICADOR_CONTA;
    }


    /**
     * Sets the IDENTIFICADOR_CONTA value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param IDENTIFICADOR_CONTA
     */
    public void setIDENTIFICADOR_CONTA(java.lang.String[] IDENTIFICADOR_CONTA) {
        this.IDENTIFICADOR_CONTA = IDENTIFICADOR_CONTA;
    }

    public java.lang.String getIDENTIFICADOR_CONTA(int i) {
        return this.IDENTIFICADOR_CONTA[i];
    }

    public void setIDENTIFICADOR_CONTA(int i, java.lang.String _value) {
        this.IDENTIFICADOR_CONTA[i] = _value;
    }


    /**
     * Gets the CLIENTE value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return CLIENTE
     */
    public java.lang.String getCLIENTE() {
        return CLIENTE;
    }


    /**
     * Sets the CLIENTE value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param CLIENTE
     */
    public void setCLIENTE(java.lang.String CLIENTE) {
        this.CLIENTE = CLIENTE;
    }


    /**
     * Gets the CPF_CNPJ value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return CPF_CNPJ
     */
    public org.apache.axis.types.UnsignedLong getCPF_CNPJ() {
        return CPF_CNPJ;
    }


    /**
     * Sets the CPF_CNPJ value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param CPF_CNPJ
     */
    public void setCPF_CNPJ(org.apache.axis.types.UnsignedLong CPF_CNPJ) {
        this.CPF_CNPJ = CPF_CNPJ;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }


    /**
     * Gets the REFERENCIA value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return REFERENCIA
     */
    public java.lang.String getREFERENCIA() {
        return REFERENCIA;
    }


    /**
     * Sets the REFERENCIA value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param REFERENCIA
     */
    public void setREFERENCIA(java.lang.String REFERENCIA) {
        this.REFERENCIA = REFERENCIA;
    }


    /**
     * Gets the PAGAMENTO_PARCIAL value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return PAGAMENTO_PARCIAL
     */
    public java.lang.Boolean getPAGAMENTO_PARCIAL() {
        return PAGAMENTO_PARCIAL;
    }


    /**
     * Sets the PAGAMENTO_PARCIAL value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param PAGAMENTO_PARCIAL
     */
    public void setPAGAMENTO_PARCIAL(java.lang.Boolean PAGAMENTO_PARCIAL) {
        this.PAGAMENTO_PARCIAL = PAGAMENTO_PARCIAL;
    }


    /**
     * Gets the VALOR_ORIGINAL value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return VALOR_ORIGINAL
     */
    public java.math.BigDecimal getVALOR_ORIGINAL() {
        return VALOR_ORIGINAL;
    }


    /**
     * Sets the VALOR_ORIGINAL value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param VALOR_ORIGINAL
     */
    public void setVALOR_ORIGINAL(java.math.BigDecimal VALOR_ORIGINAL) {
        this.VALOR_ORIGINAL = VALOR_ORIGINAL;
    }


    /**
     * Gets the VALOR_PAGAMENTO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return VALOR_PAGAMENTO
     */
    public java.math.BigDecimal getVALOR_PAGAMENTO() {
        return VALOR_PAGAMENTO;
    }


    /**
     * Sets the VALOR_PAGAMENTO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param VALOR_PAGAMENTO
     */
    public void setVALOR_PAGAMENTO(java.math.BigDecimal VALOR_PAGAMENTO) {
        this.VALOR_PAGAMENTO = VALOR_PAGAMENTO;
    }


    /**
     * Gets the VALOR_MULTA value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return VALOR_MULTA
     */
    public java.math.BigDecimal getVALOR_MULTA() {
        return VALOR_MULTA;
    }


    /**
     * Sets the VALOR_MULTA value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param VALOR_MULTA
     */
    public void setVALOR_MULTA(java.math.BigDecimal VALOR_MULTA) {
        this.VALOR_MULTA = VALOR_MULTA;
    }


    /**
     * Gets the VALOR_JUROS value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return VALOR_JUROS
     */
    public java.math.BigDecimal getVALOR_JUROS() {
        return VALOR_JUROS;
    }


    /**
     * Sets the VALOR_JUROS value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param VALOR_JUROS
     */
    public void setVALOR_JUROS(java.math.BigDecimal VALOR_JUROS) {
        this.VALOR_JUROS = VALOR_JUROS;
    }


    /**
     * Gets the NSU_CONVENIO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return NSU_CONVENIO
     */
    public org.apache.axis.types.UnsignedInt getNSU_CONVENIO() {
        return NSU_CONVENIO;
    }


    /**
     * Sets the NSU_CONVENIO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param NSU_CONVENIO
     */
    public void setNSU_CONVENIO(org.apache.axis.types.UnsignedInt NSU_CONVENIO) {
        this.NSU_CONVENIO = NSU_CONVENIO;
    }


    /**
     * Gets the DATA_DEBITO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return DATA_DEBITO
     */
    public java.util.Date getDATA_DEBITO() {
        return DATA_DEBITO;
    }


    /**
     * Sets the DATA_DEBITO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param DATA_DEBITO
     */
    public void setDATA_DEBITO(java.util.Date DATA_DEBITO) {
        this.DATA_DEBITO = DATA_DEBITO;
    }


    /**
     * Gets the DATA_OPERACAO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return DATA_OPERACAO
     */
    public java.util.Date getDATA_OPERACAO() {
        return DATA_OPERACAO;
    }


    /**
     * Sets the DATA_OPERACAO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param DATA_OPERACAO
     */
    public void setDATA_OPERACAO(java.util.Date DATA_OPERACAO) {
        this.DATA_OPERACAO = DATA_OPERACAO;
    }


    /**
     * Gets the DATA_VENCIMENTO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return DATA_VENCIMENTO
     */
    public java.util.Date getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }


    /**
     * Sets the DATA_VENCIMENTO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param DATA_VENCIMENTO
     */
    public void setDATA_VENCIMENTO(java.util.Date DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_pec_generico_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_pec_generico_Type)) return false;
        Dados_saida_transacao_pec_generico_Type other = (Dados_saida_transacao_pec_generico_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.SEGMENTO==null && other.getSEGMENTO()==null) || 
             (this.SEGMENTO!=null &&
              this.SEGMENTO.equals(other.getSEGMENTO()))) &&
            ((this.CONVENIO==null && other.getCONVENIO()==null) || 
             (this.CONVENIO!=null &&
              this.CONVENIO.equals(other.getCONVENIO()))) &&
            ((this.SERVICO==null && other.getSERVICO()==null) || 
             (this.SERVICO!=null &&
              this.SERVICO.equals(other.getSERVICO()))) &&
            ((this.IDENTIFICADOR_CONTA==null && other.getIDENTIFICADOR_CONTA()==null) || 
             (this.IDENTIFICADOR_CONTA!=null &&
              java.util.Arrays.equals(this.IDENTIFICADOR_CONTA, other.getIDENTIFICADOR_CONTA()))) &&
            ((this.CLIENTE==null && other.getCLIENTE()==null) || 
             (this.CLIENTE!=null &&
              this.CLIENTE.equals(other.getCLIENTE()))) &&
            ((this.CPF_CNPJ==null && other.getCPF_CNPJ()==null) || 
             (this.CPF_CNPJ!=null &&
              this.CPF_CNPJ.equals(other.getCPF_CNPJ()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              this.CODIGO_BARRAS.equals(other.getCODIGO_BARRAS()))) &&
            ((this.REFERENCIA==null && other.getREFERENCIA()==null) || 
             (this.REFERENCIA!=null &&
              this.REFERENCIA.equals(other.getREFERENCIA()))) &&
            ((this.PAGAMENTO_PARCIAL==null && other.getPAGAMENTO_PARCIAL()==null) || 
             (this.PAGAMENTO_PARCIAL!=null &&
              this.PAGAMENTO_PARCIAL.equals(other.getPAGAMENTO_PARCIAL()))) &&
            ((this.VALOR_ORIGINAL==null && other.getVALOR_ORIGINAL()==null) || 
             (this.VALOR_ORIGINAL!=null &&
              this.VALOR_ORIGINAL.equals(other.getVALOR_ORIGINAL()))) &&
            ((this.VALOR_PAGAMENTO==null && other.getVALOR_PAGAMENTO()==null) || 
             (this.VALOR_PAGAMENTO!=null &&
              this.VALOR_PAGAMENTO.equals(other.getVALOR_PAGAMENTO()))) &&
            ((this.VALOR_MULTA==null && other.getVALOR_MULTA()==null) || 
             (this.VALOR_MULTA!=null &&
              this.VALOR_MULTA.equals(other.getVALOR_MULTA()))) &&
            ((this.VALOR_JUROS==null && other.getVALOR_JUROS()==null) || 
             (this.VALOR_JUROS!=null &&
              this.VALOR_JUROS.equals(other.getVALOR_JUROS()))) &&
            ((this.NSU_CONVENIO==null && other.getNSU_CONVENIO()==null) || 
             (this.NSU_CONVENIO!=null &&
              this.NSU_CONVENIO.equals(other.getNSU_CONVENIO()))) &&
            ((this.DATA_DEBITO==null && other.getDATA_DEBITO()==null) || 
             (this.DATA_DEBITO!=null &&
              this.DATA_DEBITO.equals(other.getDATA_DEBITO()))) &&
            ((this.DATA_OPERACAO==null && other.getDATA_OPERACAO()==null) || 
             (this.DATA_OPERACAO!=null &&
              this.DATA_OPERACAO.equals(other.getDATA_OPERACAO()))) &&
            ((this.DATA_VENCIMENTO==null && other.getDATA_VENCIMENTO()==null) || 
             (this.DATA_VENCIMENTO!=null &&
              this.DATA_VENCIMENTO.equals(other.getDATA_VENCIMENTO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getSEGMENTO() != null) {
            _hashCode += getSEGMENTO().hashCode();
        }
        if (getCONVENIO() != null) {
            _hashCode += getCONVENIO().hashCode();
        }
        if (getSERVICO() != null) {
            _hashCode += getSERVICO().hashCode();
        }
        if (getIDENTIFICADOR_CONTA() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getIDENTIFICADOR_CONTA());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getIDENTIFICADOR_CONTA(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCLIENTE() != null) {
            _hashCode += getCLIENTE().hashCode();
        }
        if (getCPF_CNPJ() != null) {
            _hashCode += getCPF_CNPJ().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            _hashCode += getCODIGO_BARRAS().hashCode();
        }
        if (getREFERENCIA() != null) {
            _hashCode += getREFERENCIA().hashCode();
        }
        if (getPAGAMENTO_PARCIAL() != null) {
            _hashCode += getPAGAMENTO_PARCIAL().hashCode();
        }
        if (getVALOR_ORIGINAL() != null) {
            _hashCode += getVALOR_ORIGINAL().hashCode();
        }
        if (getVALOR_PAGAMENTO() != null) {
            _hashCode += getVALOR_PAGAMENTO().hashCode();
        }
        if (getVALOR_MULTA() != null) {
            _hashCode += getVALOR_MULTA().hashCode();
        }
        if (getVALOR_JUROS() != null) {
            _hashCode += getVALOR_JUROS().hashCode();
        }
        if (getNSU_CONVENIO() != null) {
            _hashCode += getNSU_CONVENIO().hashCode();
        }
        if (getDATA_DEBITO() != null) {
            _hashCode += getDATA_DEBITO().hashCode();
        }
        if (getDATA_OPERACAO() != null) {
            _hashCode += getDATA_OPERACAO().hashCode();
        }
        if (getDATA_VENCIMENTO() != null) {
            _hashCode += getDATA_VENCIMENTO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_pec_generico_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_pec_generico_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONVENIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONVENIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "convenio_pec_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SERVICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SERVICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "servico_pec_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICADOR_CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICADOR_CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_pec_generico_Type>IDENTIFICADOR_CONTA"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CLIENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF_CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF_CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REFERENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "REFERENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAGAMENTO_PARCIAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PAGAMENTO_PARCIAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_ORIGINAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_ORIGINAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_PAGAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_PAGAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_MULTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_MULTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_JUROS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_JUROS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU_CONVENIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU_CONVENIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_DEBITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_DEBITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_OPERACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_OPERACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
