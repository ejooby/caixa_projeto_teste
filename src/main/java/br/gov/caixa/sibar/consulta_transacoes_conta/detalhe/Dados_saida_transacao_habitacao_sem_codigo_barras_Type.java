/**
 * Dados_saida_transacao_habitacao_sem_codigo_barras_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_habitacao_sem_codigo_barras_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private org.apache.axis.types.UnsignedLong NUMERO_CONTRATO;

    private org.apache.axis.types.UnsignedInt NCPD;

    private java.util.Date DATA_VENCIMENTO;

    private java.lang.String MUTUARIO;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_habitacao_sem_codigo_barras_Type() {
    }

    public Dados_saida_transacao_habitacao_sem_codigo_barras_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           org.apache.axis.types.UnsignedLong NUMERO_CONTRATO,
           org.apache.axis.types.UnsignedInt NCPD,
           java.util.Date DATA_VENCIMENTO,
           java.lang.String MUTUARIO,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.NUMERO_CONTRATO = NUMERO_CONTRATO;
           this.NCPD = NCPD;
           this.DATA_VENCIMENTO = DATA_VENCIMENTO;
           this.MUTUARIO = MUTUARIO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the NUMERO_CONTRATO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @return NUMERO_CONTRATO
     */
    public org.apache.axis.types.UnsignedLong getNUMERO_CONTRATO() {
        return NUMERO_CONTRATO;
    }


    /**
     * Sets the NUMERO_CONTRATO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @param NUMERO_CONTRATO
     */
    public void setNUMERO_CONTRATO(org.apache.axis.types.UnsignedLong NUMERO_CONTRATO) {
        this.NUMERO_CONTRATO = NUMERO_CONTRATO;
    }


    /**
     * Gets the NCPD value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @return NCPD
     */
    public org.apache.axis.types.UnsignedInt getNCPD() {
        return NCPD;
    }


    /**
     * Sets the NCPD value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @param NCPD
     */
    public void setNCPD(org.apache.axis.types.UnsignedInt NCPD) {
        this.NCPD = NCPD;
    }


    /**
     * Gets the DATA_VENCIMENTO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @return DATA_VENCIMENTO
     */
    public java.util.Date getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }


    /**
     * Sets the DATA_VENCIMENTO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @param DATA_VENCIMENTO
     */
    public void setDATA_VENCIMENTO(java.util.Date DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }


    /**
     * Gets the MUTUARIO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @return MUTUARIO
     */
    public java.lang.String getMUTUARIO() {
        return MUTUARIO;
    }


    /**
     * Sets the MUTUARIO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @param MUTUARIO
     */
    public void setMUTUARIO(java.lang.String MUTUARIO) {
        this.MUTUARIO = MUTUARIO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_habitacao_sem_codigo_barras_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_habitacao_sem_codigo_barras_Type)) return false;
        Dados_saida_transacao_habitacao_sem_codigo_barras_Type other = (Dados_saida_transacao_habitacao_sem_codigo_barras_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.NUMERO_CONTRATO==null && other.getNUMERO_CONTRATO()==null) || 
             (this.NUMERO_CONTRATO!=null &&
              this.NUMERO_CONTRATO.equals(other.getNUMERO_CONTRATO()))) &&
            ((this.NCPD==null && other.getNCPD()==null) || 
             (this.NCPD!=null &&
              this.NCPD.equals(other.getNCPD()))) &&
            ((this.DATA_VENCIMENTO==null && other.getDATA_VENCIMENTO()==null) || 
             (this.DATA_VENCIMENTO!=null &&
              this.DATA_VENCIMENTO.equals(other.getDATA_VENCIMENTO()))) &&
            ((this.MUTUARIO==null && other.getMUTUARIO()==null) || 
             (this.MUTUARIO!=null &&
              this.MUTUARIO.equals(other.getMUTUARIO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getNUMERO_CONTRATO() != null) {
            _hashCode += getNUMERO_CONTRATO().hashCode();
        }
        if (getNCPD() != null) {
            _hashCode += getNCPD().hashCode();
        }
        if (getDATA_VENCIMENTO() != null) {
            _hashCode += getDATA_VENCIMENTO().hashCode();
        }
        if (getMUTUARIO() != null) {
            _hashCode += getMUTUARIO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_habitacao_sem_codigo_barras_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_habitacao_sem_codigo_barras_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO_CONTRATO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO_CONTRATO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NCPD");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NCPD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MUTUARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MUTUARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
