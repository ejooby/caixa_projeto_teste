/**
 * Dados_saida_transacao_darf_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_darf_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private org.apache.axis.types.UnsignedInt CODIGO_RECEITA;

    private java.lang.String CPF_CNPJ;

    private java.util.Date DATA_APURACAO;

    private java.util.Date DATA_VENCIMENTO;

    private java.math.BigDecimal VALOR_PRINCIPAL;

    private java.math.BigDecimal VALOR_MULTA;

    private java.math.BigDecimal VALOR_JUROS;

    private java.math.BigDecimal RECEITA_BRUTA;

    private java.math.BigDecimal PERCENTUAL;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contato_Type CONTATO;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_darf_Type() {
    }

    public Dados_saida_transacao_darf_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           org.apache.axis.types.UnsignedInt CODIGO_RECEITA,
           java.lang.String CPF_CNPJ,
           java.util.Date DATA_APURACAO,
           java.util.Date DATA_VENCIMENTO,
           java.math.BigDecimal VALOR_PRINCIPAL,
           java.math.BigDecimal VALOR_MULTA,
           java.math.BigDecimal VALOR_JUROS,
           java.math.BigDecimal RECEITA_BRUTA,
           java.math.BigDecimal PERCENTUAL,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contato_Type CONTATO,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.CODIGO_RECEITA = CODIGO_RECEITA;
           this.CPF_CNPJ = CPF_CNPJ;
           this.DATA_APURACAO = DATA_APURACAO;
           this.DATA_VENCIMENTO = DATA_VENCIMENTO;
           this.VALOR_PRINCIPAL = VALOR_PRINCIPAL;
           this.VALOR_MULTA = VALOR_MULTA;
           this.VALOR_JUROS = VALOR_JUROS;
           this.RECEITA_BRUTA = RECEITA_BRUTA;
           this.PERCENTUAL = PERCENTUAL;
           this.CONTATO = CONTATO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the CODIGO_RECEITA value for this Dados_saida_transacao_darf_Type.
     * 
     * @return CODIGO_RECEITA
     */
    public org.apache.axis.types.UnsignedInt getCODIGO_RECEITA() {
        return CODIGO_RECEITA;
    }


    /**
     * Sets the CODIGO_RECEITA value for this Dados_saida_transacao_darf_Type.
     * 
     * @param CODIGO_RECEITA
     */
    public void setCODIGO_RECEITA(org.apache.axis.types.UnsignedInt CODIGO_RECEITA) {
        this.CODIGO_RECEITA = CODIGO_RECEITA;
    }


    /**
     * Gets the CPF_CNPJ value for this Dados_saida_transacao_darf_Type.
     * 
     * @return CPF_CNPJ
     */
    public java.lang.String getCPF_CNPJ() {
        return CPF_CNPJ;
    }


    /**
     * Sets the CPF_CNPJ value for this Dados_saida_transacao_darf_Type.
     * 
     * @param CPF_CNPJ
     */
    public void setCPF_CNPJ(java.lang.String CPF_CNPJ) {
        this.CPF_CNPJ = CPF_CNPJ;
    }


    /**
     * Gets the DATA_APURACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return DATA_APURACAO
     */
    public java.util.Date getDATA_APURACAO() {
        return DATA_APURACAO;
    }


    /**
     * Sets the DATA_APURACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param DATA_APURACAO
     */
    public void setDATA_APURACAO(java.util.Date DATA_APURACAO) {
        this.DATA_APURACAO = DATA_APURACAO;
    }


    /**
     * Gets the DATA_VENCIMENTO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return DATA_VENCIMENTO
     */
    public java.util.Date getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }


    /**
     * Sets the DATA_VENCIMENTO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param DATA_VENCIMENTO
     */
    public void setDATA_VENCIMENTO(java.util.Date DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }


    /**
     * Gets the VALOR_PRINCIPAL value for this Dados_saida_transacao_darf_Type.
     * 
     * @return VALOR_PRINCIPAL
     */
    public java.math.BigDecimal getVALOR_PRINCIPAL() {
        return VALOR_PRINCIPAL;
    }


    /**
     * Sets the VALOR_PRINCIPAL value for this Dados_saida_transacao_darf_Type.
     * 
     * @param VALOR_PRINCIPAL
     */
    public void setVALOR_PRINCIPAL(java.math.BigDecimal VALOR_PRINCIPAL) {
        this.VALOR_PRINCIPAL = VALOR_PRINCIPAL;
    }


    /**
     * Gets the VALOR_MULTA value for this Dados_saida_transacao_darf_Type.
     * 
     * @return VALOR_MULTA
     */
    public java.math.BigDecimal getVALOR_MULTA() {
        return VALOR_MULTA;
    }


    /**
     * Sets the VALOR_MULTA value for this Dados_saida_transacao_darf_Type.
     * 
     * @param VALOR_MULTA
     */
    public void setVALOR_MULTA(java.math.BigDecimal VALOR_MULTA) {
        this.VALOR_MULTA = VALOR_MULTA;
    }


    /**
     * Gets the VALOR_JUROS value for this Dados_saida_transacao_darf_Type.
     * 
     * @return VALOR_JUROS
     */
    public java.math.BigDecimal getVALOR_JUROS() {
        return VALOR_JUROS;
    }


    /**
     * Sets the VALOR_JUROS value for this Dados_saida_transacao_darf_Type.
     * 
     * @param VALOR_JUROS
     */
    public void setVALOR_JUROS(java.math.BigDecimal VALOR_JUROS) {
        this.VALOR_JUROS = VALOR_JUROS;
    }


    /**
     * Gets the RECEITA_BRUTA value for this Dados_saida_transacao_darf_Type.
     * 
     * @return RECEITA_BRUTA
     */
    public java.math.BigDecimal getRECEITA_BRUTA() {
        return RECEITA_BRUTA;
    }


    /**
     * Sets the RECEITA_BRUTA value for this Dados_saida_transacao_darf_Type.
     * 
     * @param RECEITA_BRUTA
     */
    public void setRECEITA_BRUTA(java.math.BigDecimal RECEITA_BRUTA) {
        this.RECEITA_BRUTA = RECEITA_BRUTA;
    }


    /**
     * Gets the PERCENTUAL value for this Dados_saida_transacao_darf_Type.
     * 
     * @return PERCENTUAL
     */
    public java.math.BigDecimal getPERCENTUAL() {
        return PERCENTUAL;
    }


    /**
     * Sets the PERCENTUAL value for this Dados_saida_transacao_darf_Type.
     * 
     * @param PERCENTUAL
     */
    public void setPERCENTUAL(java.math.BigDecimal PERCENTUAL) {
        this.PERCENTUAL = PERCENTUAL;
    }


    /**
     * Gets the CONTATO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return CONTATO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contato_Type getCONTATO() {
        return CONTATO;
    }


    /**
     * Sets the CONTATO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param CONTATO
     */
    public void setCONTATO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contato_Type CONTATO) {
        this.CONTATO = CONTATO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_darf_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_darf_Type)) return false;
        Dados_saida_transacao_darf_Type other = (Dados_saida_transacao_darf_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.CODIGO_RECEITA==null && other.getCODIGO_RECEITA()==null) || 
             (this.CODIGO_RECEITA!=null &&
              this.CODIGO_RECEITA.equals(other.getCODIGO_RECEITA()))) &&
            ((this.CPF_CNPJ==null && other.getCPF_CNPJ()==null) || 
             (this.CPF_CNPJ!=null &&
              this.CPF_CNPJ.equals(other.getCPF_CNPJ()))) &&
            ((this.DATA_APURACAO==null && other.getDATA_APURACAO()==null) || 
             (this.DATA_APURACAO!=null &&
              this.DATA_APURACAO.equals(other.getDATA_APURACAO()))) &&
            ((this.DATA_VENCIMENTO==null && other.getDATA_VENCIMENTO()==null) || 
             (this.DATA_VENCIMENTO!=null &&
              this.DATA_VENCIMENTO.equals(other.getDATA_VENCIMENTO()))) &&
            ((this.VALOR_PRINCIPAL==null && other.getVALOR_PRINCIPAL()==null) || 
             (this.VALOR_PRINCIPAL!=null &&
              this.VALOR_PRINCIPAL.equals(other.getVALOR_PRINCIPAL()))) &&
            ((this.VALOR_MULTA==null && other.getVALOR_MULTA()==null) || 
             (this.VALOR_MULTA!=null &&
              this.VALOR_MULTA.equals(other.getVALOR_MULTA()))) &&
            ((this.VALOR_JUROS==null && other.getVALOR_JUROS()==null) || 
             (this.VALOR_JUROS!=null &&
              this.VALOR_JUROS.equals(other.getVALOR_JUROS()))) &&
            ((this.RECEITA_BRUTA==null && other.getRECEITA_BRUTA()==null) || 
             (this.RECEITA_BRUTA!=null &&
              this.RECEITA_BRUTA.equals(other.getRECEITA_BRUTA()))) &&
            ((this.PERCENTUAL==null && other.getPERCENTUAL()==null) || 
             (this.PERCENTUAL!=null &&
              this.PERCENTUAL.equals(other.getPERCENTUAL()))) &&
            ((this.CONTATO==null && other.getCONTATO()==null) || 
             (this.CONTATO!=null &&
              this.CONTATO.equals(other.getCONTATO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getCODIGO_RECEITA() != null) {
            _hashCode += getCODIGO_RECEITA().hashCode();
        }
        if (getCPF_CNPJ() != null) {
            _hashCode += getCPF_CNPJ().hashCode();
        }
        if (getDATA_APURACAO() != null) {
            _hashCode += getDATA_APURACAO().hashCode();
        }
        if (getDATA_VENCIMENTO() != null) {
            _hashCode += getDATA_VENCIMENTO().hashCode();
        }
        if (getVALOR_PRINCIPAL() != null) {
            _hashCode += getVALOR_PRINCIPAL().hashCode();
        }
        if (getVALOR_MULTA() != null) {
            _hashCode += getVALOR_MULTA().hashCode();
        }
        if (getVALOR_JUROS() != null) {
            _hashCode += getVALOR_JUROS().hashCode();
        }
        if (getRECEITA_BRUTA() != null) {
            _hashCode += getRECEITA_BRUTA().hashCode();
        }
        if (getPERCENTUAL() != null) {
            _hashCode += getPERCENTUAL().hashCode();
        }
        if (getCONTATO() != null) {
            _hashCode += getCONTATO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_darf_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_darf_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_RECEITA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_RECEITA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF_CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF_CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_APURACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_APURACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_PRINCIPAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_PRINCIPAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_MULTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_MULTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_JUROS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_JUROS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECEITA_BRUTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RECEITA_BRUTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PERCENTUAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PERCENTUAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTATO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTATO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "contato_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
