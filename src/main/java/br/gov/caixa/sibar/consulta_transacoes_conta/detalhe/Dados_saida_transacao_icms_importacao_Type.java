/**
 * Dados_saida_transacao_icms_importacao_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_icms_importacao_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private java.lang.String CODIGO_BARRAS;

    private java.util.Date DATA_EFETIVACAO;

    private java.lang.String AUTENTICACAO_DIGITAL;

    private org.apache.axis.types.UnsignedByte VIAS;

    private byte GRUPO_RECEITA;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    private java.lang.String IDENTIFICACAO_RECEITA;

    private java.lang.String[] MENSAGENS;

    public Dados_saida_transacao_icms_importacao_Type() {
    }

    public Dados_saida_transacao_icms_importacao_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           java.lang.String CODIGO_BARRAS,
           java.util.Date DATA_EFETIVACAO,
           java.lang.String AUTENTICACAO_DIGITAL,
           org.apache.axis.types.UnsignedByte VIAS,
           byte GRUPO_RECEITA,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO,
           java.lang.String IDENTIFICACAO_RECEITA,
           java.lang.String[] MENSAGENS) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.DATA_EFETIVACAO = DATA_EFETIVACAO;
           this.AUTENTICACAO_DIGITAL = AUTENTICACAO_DIGITAL;
           this.VIAS = VIAS;
           this.GRUPO_RECEITA = GRUPO_RECEITA;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
           this.IDENTIFICACAO_RECEITA = IDENTIFICACAO_RECEITA;
           this.MENSAGENS = MENSAGENS;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }


    /**
     * Gets the DATA_EFETIVACAO value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @return DATA_EFETIVACAO
     */
    public java.util.Date getDATA_EFETIVACAO() {
        return DATA_EFETIVACAO;
    }


    /**
     * Sets the DATA_EFETIVACAO value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @param DATA_EFETIVACAO
     */
    public void setDATA_EFETIVACAO(java.util.Date DATA_EFETIVACAO) {
        this.DATA_EFETIVACAO = DATA_EFETIVACAO;
    }


    /**
     * Gets the AUTENTICACAO_DIGITAL value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @return AUTENTICACAO_DIGITAL
     */
    public java.lang.String getAUTENTICACAO_DIGITAL() {
        return AUTENTICACAO_DIGITAL;
    }


    /**
     * Sets the AUTENTICACAO_DIGITAL value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @param AUTENTICACAO_DIGITAL
     */
    public void setAUTENTICACAO_DIGITAL(java.lang.String AUTENTICACAO_DIGITAL) {
        this.AUTENTICACAO_DIGITAL = AUTENTICACAO_DIGITAL;
    }


    /**
     * Gets the VIAS value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @return VIAS
     */
    public org.apache.axis.types.UnsignedByte getVIAS() {
        return VIAS;
    }


    /**
     * Sets the VIAS value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @param VIAS
     */
    public void setVIAS(org.apache.axis.types.UnsignedByte VIAS) {
        this.VIAS = VIAS;
    }


    /**
     * Gets the GRUPO_RECEITA value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @return GRUPO_RECEITA
     */
    public byte getGRUPO_RECEITA() {
        return GRUPO_RECEITA;
    }


    /**
     * Sets the GRUPO_RECEITA value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @param GRUPO_RECEITA
     */
    public void setGRUPO_RECEITA(byte GRUPO_RECEITA) {
        this.GRUPO_RECEITA = GRUPO_RECEITA;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the IDENTIFICACAO_RECEITA value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @return IDENTIFICACAO_RECEITA
     */
    public java.lang.String getIDENTIFICACAO_RECEITA() {
        return IDENTIFICACAO_RECEITA;
    }


    /**
     * Sets the IDENTIFICACAO_RECEITA value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @param IDENTIFICACAO_RECEITA
     */
    public void setIDENTIFICACAO_RECEITA(java.lang.String IDENTIFICACAO_RECEITA) {
        this.IDENTIFICACAO_RECEITA = IDENTIFICACAO_RECEITA;
    }


    /**
     * Gets the MENSAGENS value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @return MENSAGENS
     */
    public java.lang.String[] getMENSAGENS() {
        return MENSAGENS;
    }


    /**
     * Sets the MENSAGENS value for this Dados_saida_transacao_icms_importacao_Type.
     * 
     * @param MENSAGENS
     */
    public void setMENSAGENS(java.lang.String[] MENSAGENS) {
        this.MENSAGENS = MENSAGENS;
    }

    public java.lang.String getMENSAGENS(int i) {
        return this.MENSAGENS[i];
    }

    public void setMENSAGENS(int i, java.lang.String _value) {
        this.MENSAGENS[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_icms_importacao_Type)) return false;
        Dados_saida_transacao_icms_importacao_Type other = (Dados_saida_transacao_icms_importacao_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              this.CODIGO_BARRAS.equals(other.getCODIGO_BARRAS()))) &&
            ((this.DATA_EFETIVACAO==null && other.getDATA_EFETIVACAO()==null) || 
             (this.DATA_EFETIVACAO!=null &&
              this.DATA_EFETIVACAO.equals(other.getDATA_EFETIVACAO()))) &&
            ((this.AUTENTICACAO_DIGITAL==null && other.getAUTENTICACAO_DIGITAL()==null) || 
             (this.AUTENTICACAO_DIGITAL!=null &&
              this.AUTENTICACAO_DIGITAL.equals(other.getAUTENTICACAO_DIGITAL()))) &&
            ((this.VIAS==null && other.getVIAS()==null) || 
             (this.VIAS!=null &&
              this.VIAS.equals(other.getVIAS()))) &&
            this.GRUPO_RECEITA == other.getGRUPO_RECEITA() &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO()))) &&
            ((this.IDENTIFICACAO_RECEITA==null && other.getIDENTIFICACAO_RECEITA()==null) || 
             (this.IDENTIFICACAO_RECEITA!=null &&
              this.IDENTIFICACAO_RECEITA.equals(other.getIDENTIFICACAO_RECEITA()))) &&
            ((this.MENSAGENS==null && other.getMENSAGENS()==null) || 
             (this.MENSAGENS!=null &&
              java.util.Arrays.equals(this.MENSAGENS, other.getMENSAGENS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            _hashCode += getCODIGO_BARRAS().hashCode();
        }
        if (getDATA_EFETIVACAO() != null) {
            _hashCode += getDATA_EFETIVACAO().hashCode();
        }
        if (getAUTENTICACAO_DIGITAL() != null) {
            _hashCode += getAUTENTICACAO_DIGITAL().hashCode();
        }
        if (getVIAS() != null) {
            _hashCode += getVIAS().hashCode();
        }
        _hashCode += getGRUPO_RECEITA();
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        if (getIDENTIFICACAO_RECEITA() != null) {
            _hashCode += getIDENTIFICACAO_RECEITA().hashCode();
        }
        if (getMENSAGENS() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMENSAGENS());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMENSAGENS(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_icms_importacao_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_icms_importacao_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_EFETIVACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_EFETIVACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AUTENTICACAO_DIGITAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AUTENTICACAO_DIGITAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VIAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VIAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GRUPO_RECEITA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GRUPO_RECEITA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO_RECEITA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO_RECEITA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MENSAGENS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MENSAGENS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_icms_importacao_Type>MENSAGENS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
