/**
 * Valores_calculados_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Valores_calculados_Type  implements java.io.Serializable {
    private java.math.BigDecimal VALOR;

    private java.math.BigDecimal JUROS;

    private java.math.BigDecimal MULTA;

    private java.math.BigDecimal DESCONTO;

    private java.math.BigDecimal ABATIMENTO;

    private java.math.BigDecimal TOTAL;

    public Valores_calculados_Type() {
    }

    public Valores_calculados_Type(
           java.math.BigDecimal VALOR,
           java.math.BigDecimal JUROS,
           java.math.BigDecimal MULTA,
           java.math.BigDecimal DESCONTO,
           java.math.BigDecimal ABATIMENTO,
           java.math.BigDecimal TOTAL) {
           this.VALOR = VALOR;
           this.JUROS = JUROS;
           this.MULTA = MULTA;
           this.DESCONTO = DESCONTO;
           this.ABATIMENTO = ABATIMENTO;
           this.TOTAL = TOTAL;
    }


    /**
     * Gets the VALOR value for this Valores_calculados_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Valores_calculados_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the JUROS value for this Valores_calculados_Type.
     * 
     * @return JUROS
     */
    public java.math.BigDecimal getJUROS() {
        return JUROS;
    }


    /**
     * Sets the JUROS value for this Valores_calculados_Type.
     * 
     * @param JUROS
     */
    public void setJUROS(java.math.BigDecimal JUROS) {
        this.JUROS = JUROS;
    }


    /**
     * Gets the MULTA value for this Valores_calculados_Type.
     * 
     * @return MULTA
     */
    public java.math.BigDecimal getMULTA() {
        return MULTA;
    }


    /**
     * Sets the MULTA value for this Valores_calculados_Type.
     * 
     * @param MULTA
     */
    public void setMULTA(java.math.BigDecimal MULTA) {
        this.MULTA = MULTA;
    }


    /**
     * Gets the DESCONTO value for this Valores_calculados_Type.
     * 
     * @return DESCONTO
     */
    public java.math.BigDecimal getDESCONTO() {
        return DESCONTO;
    }


    /**
     * Sets the DESCONTO value for this Valores_calculados_Type.
     * 
     * @param DESCONTO
     */
    public void setDESCONTO(java.math.BigDecimal DESCONTO) {
        this.DESCONTO = DESCONTO;
    }


    /**
     * Gets the ABATIMENTO value for this Valores_calculados_Type.
     * 
     * @return ABATIMENTO
     */
    public java.math.BigDecimal getABATIMENTO() {
        return ABATIMENTO;
    }


    /**
     * Sets the ABATIMENTO value for this Valores_calculados_Type.
     * 
     * @param ABATIMENTO
     */
    public void setABATIMENTO(java.math.BigDecimal ABATIMENTO) {
        this.ABATIMENTO = ABATIMENTO;
    }


    /**
     * Gets the TOTAL value for this Valores_calculados_Type.
     * 
     * @return TOTAL
     */
    public java.math.BigDecimal getTOTAL() {
        return TOTAL;
    }


    /**
     * Sets the TOTAL value for this Valores_calculados_Type.
     * 
     * @param TOTAL
     */
    public void setTOTAL(java.math.BigDecimal TOTAL) {
        this.TOTAL = TOTAL;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Valores_calculados_Type)) return false;
        Valores_calculados_Type other = (Valores_calculados_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.JUROS==null && other.getJUROS()==null) || 
             (this.JUROS!=null &&
              this.JUROS.equals(other.getJUROS()))) &&
            ((this.MULTA==null && other.getMULTA()==null) || 
             (this.MULTA!=null &&
              this.MULTA.equals(other.getMULTA()))) &&
            ((this.DESCONTO==null && other.getDESCONTO()==null) || 
             (this.DESCONTO!=null &&
              this.DESCONTO.equals(other.getDESCONTO()))) &&
            ((this.ABATIMENTO==null && other.getABATIMENTO()==null) || 
             (this.ABATIMENTO!=null &&
              this.ABATIMENTO.equals(other.getABATIMENTO()))) &&
            ((this.TOTAL==null && other.getTOTAL()==null) || 
             (this.TOTAL!=null &&
              this.TOTAL.equals(other.getTOTAL())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getJUROS() != null) {
            _hashCode += getJUROS().hashCode();
        }
        if (getMULTA() != null) {
            _hashCode += getMULTA().hashCode();
        }
        if (getDESCONTO() != null) {
            _hashCode += getDESCONTO().hashCode();
        }
        if (getABATIMENTO() != null) {
            _hashCode += getABATIMENTO().hashCode();
        }
        if (getTOTAL() != null) {
            _hashCode += getTOTAL().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Valores_calculados_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "valores_calculados_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("JUROS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "JUROS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DESCONTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DESCONTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ABATIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ABATIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOTAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TOTAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
