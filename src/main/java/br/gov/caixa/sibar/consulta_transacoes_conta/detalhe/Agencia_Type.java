/**
 * Agencia_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Agencia_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedShort CODIGO_BANCO;

    private org.apache.axis.types.UnsignedInt NUMERO;

    private org.apache.axis.types.UnsignedByte DV;

    private java.lang.String NOME;

    public Agencia_Type() {
    }

    public Agencia_Type(
           org.apache.axis.types.UnsignedShort CODIGO_BANCO,
           org.apache.axis.types.UnsignedInt NUMERO,
           org.apache.axis.types.UnsignedByte DV,
           java.lang.String NOME) {
           this.CODIGO_BANCO = CODIGO_BANCO;
           this.NUMERO = NUMERO;
           this.DV = DV;
           this.NOME = NOME;
    }


    /**
     * Gets the CODIGO_BANCO value for this Agencia_Type.
     * 
     * @return CODIGO_BANCO
     */
    public org.apache.axis.types.UnsignedShort getCODIGO_BANCO() {
        return CODIGO_BANCO;
    }


    /**
     * Sets the CODIGO_BANCO value for this Agencia_Type.
     * 
     * @param CODIGO_BANCO
     */
    public void setCODIGO_BANCO(org.apache.axis.types.UnsignedShort CODIGO_BANCO) {
        this.CODIGO_BANCO = CODIGO_BANCO;
    }


    /**
     * Gets the NUMERO value for this Agencia_Type.
     * 
     * @return NUMERO
     */
    public org.apache.axis.types.UnsignedInt getNUMERO() {
        return NUMERO;
    }


    /**
     * Sets the NUMERO value for this Agencia_Type.
     * 
     * @param NUMERO
     */
    public void setNUMERO(org.apache.axis.types.UnsignedInt NUMERO) {
        this.NUMERO = NUMERO;
    }


    /**
     * Gets the DV value for this Agencia_Type.
     * 
     * @return DV
     */
    public org.apache.axis.types.UnsignedByte getDV() {
        return DV;
    }


    /**
     * Sets the DV value for this Agencia_Type.
     * 
     * @param DV
     */
    public void setDV(org.apache.axis.types.UnsignedByte DV) {
        this.DV = DV;
    }


    /**
     * Gets the NOME value for this Agencia_Type.
     * 
     * @return NOME
     */
    public java.lang.String getNOME() {
        return NOME;
    }


    /**
     * Sets the NOME value for this Agencia_Type.
     * 
     * @param NOME
     */
    public void setNOME(java.lang.String NOME) {
        this.NOME = NOME;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Agencia_Type)) return false;
        Agencia_Type other = (Agencia_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CODIGO_BANCO==null && other.getCODIGO_BANCO()==null) || 
             (this.CODIGO_BANCO!=null &&
              this.CODIGO_BANCO.equals(other.getCODIGO_BANCO()))) &&
            ((this.NUMERO==null && other.getNUMERO()==null) || 
             (this.NUMERO!=null &&
              this.NUMERO.equals(other.getNUMERO()))) &&
            ((this.DV==null && other.getDV()==null) || 
             (this.DV!=null &&
              this.DV.equals(other.getDV()))) &&
            ((this.NOME==null && other.getNOME()==null) || 
             (this.NOME!=null &&
              this.NOME.equals(other.getNOME())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCODIGO_BANCO() != null) {
            _hashCode += getCODIGO_BANCO().hashCode();
        }
        if (getNUMERO() != null) {
            _hashCode += getNUMERO().hashCode();
        }
        if (getDV() != null) {
            _hashCode += getDV().hashCode();
        }
        if (getNOME() != null) {
            _hashCode += getNOME().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Agencia_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "agencia_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BANCO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BANCO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DV");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
