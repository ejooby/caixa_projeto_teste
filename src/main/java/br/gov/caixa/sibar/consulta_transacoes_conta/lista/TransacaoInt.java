package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public interface TransacaoInt {

	public String getTipoServico();
	public void setTipoServico(java.lang.String tipo);
	
	
	public String getData();
	public void setData(String data);
	public void setBanco_Descricao(String bancoDescricao);
	public String getBanco_Descricao();
	public String getChaveSeguranca();
	public void setChaveSeguranca(String chave);
	

}
