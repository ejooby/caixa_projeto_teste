/**
 * Consulta_Transacoes_Conta_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.ws;

public interface Consulta_Transacoes_Conta_Service extends javax.xml.rpc.Service {
    public java.lang.String getConsulta_Transacoes_ContaSOAPAddress();

    public br.gov.caixa.sibar.consulta_transacoes_conta.ws.Consulta_Transacoes_Conta_PortType getConsulta_Transacoes_ContaSOAP() throws javax.xml.rpc.ServiceException;

    public br.gov.caixa.sibar.consulta_transacoes_conta.ws.Consulta_Transacoes_Conta_PortType getConsulta_Transacoes_ContaSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
