/**
 * Dpvat_TypeTIPO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dpvat_TypeTIPO implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Dpvat_TypeTIPO(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "Cota Unica sem Desconto";
    public static final java.lang.String _value2 = "CotaUnica com Desconto";
    public static final java.lang.String _value3 = "Primeira Cota";
    public static final java.lang.String _value4 = "Segunda Cota";
    public static final java.lang.String _value5 = "Terceira Cota";
    public static final java.lang.String _value6 = "Quarta Cota";
    public static final java.lang.String _value7 = "Quinta Cota";
    public static final java.lang.String _value8 = "Sexta Cota";
    public static final java.lang.String _value9 = "Exercicios Anteriores";
    public static final java.lang.String _value10 = "Cota Unica Complementar";
    public static final java.lang.String _value11 = "Primeira Cota Complementar";
    public static final java.lang.String _value12 = "Segunda Cota Complementar";
    public static final java.lang.String _value13 = "Terceira Cota Complementar";
    public static final java.lang.String _value14 = "Quarta Cota Complementar";
    public static final java.lang.String _value15 = "Quinta Cota Complementar";
    public static final java.lang.String _value16 = "Sexta Cota Complementar";
    public static final Dpvat_TypeTIPO value1 = new Dpvat_TypeTIPO(_value1);
    public static final Dpvat_TypeTIPO value2 = new Dpvat_TypeTIPO(_value2);
    public static final Dpvat_TypeTIPO value3 = new Dpvat_TypeTIPO(_value3);
    public static final Dpvat_TypeTIPO value4 = new Dpvat_TypeTIPO(_value4);
    public static final Dpvat_TypeTIPO value5 = new Dpvat_TypeTIPO(_value5);
    public static final Dpvat_TypeTIPO value6 = new Dpvat_TypeTIPO(_value6);
    public static final Dpvat_TypeTIPO value7 = new Dpvat_TypeTIPO(_value7);
    public static final Dpvat_TypeTIPO value8 = new Dpvat_TypeTIPO(_value8);
    public static final Dpvat_TypeTIPO value9 = new Dpvat_TypeTIPO(_value9);
    public static final Dpvat_TypeTIPO value10 = new Dpvat_TypeTIPO(_value10);
    public static final Dpvat_TypeTIPO value11 = new Dpvat_TypeTIPO(_value11);
    public static final Dpvat_TypeTIPO value12 = new Dpvat_TypeTIPO(_value12);
    public static final Dpvat_TypeTIPO value13 = new Dpvat_TypeTIPO(_value13);
    public static final Dpvat_TypeTIPO value14 = new Dpvat_TypeTIPO(_value14);
    public static final Dpvat_TypeTIPO value15 = new Dpvat_TypeTIPO(_value15);
    public static final Dpvat_TypeTIPO value16 = new Dpvat_TypeTIPO(_value16);
    public java.lang.String getValue() { return _value_;}
    public static Dpvat_TypeTIPO fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Dpvat_TypeTIPO enumeration = (Dpvat_TypeTIPO)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Dpvat_TypeTIPO fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dpvat_TypeTIPO.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dpvat_Type>TIPO"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
