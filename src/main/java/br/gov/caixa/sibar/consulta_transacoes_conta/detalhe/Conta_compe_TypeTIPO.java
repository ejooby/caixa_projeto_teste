/**
 * Conta_compe_TypeTIPO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Conta_compe_TypeTIPO implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Conta_compe_TypeTIPO(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "Conta Corrente Individual";
    public static final java.lang.String _value2 = "Conta Poupanca Individual";
    public static final java.lang.String _value3 = "Conta Corrente Conjunta";
    public static final java.lang.String _value4 = "Conta Poupanca Conjunta";
    public static final Conta_compe_TypeTIPO value1 = new Conta_compe_TypeTIPO(_value1);
    public static final Conta_compe_TypeTIPO value2 = new Conta_compe_TypeTIPO(_value2);
    public static final Conta_compe_TypeTIPO value3 = new Conta_compe_TypeTIPO(_value3);
    public static final Conta_compe_TypeTIPO value4 = new Conta_compe_TypeTIPO(_value4);
    public java.lang.String getValue() { return _value_;}
    public static Conta_compe_TypeTIPO fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Conta_compe_TypeTIPO enumeration = (Conta_compe_TypeTIPO)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Conta_compe_TypeTIPO fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Conta_compe_TypeTIPO.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">conta_compe_Type>TIPO"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
