/**
 * Competencia_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Competencia_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedByte MES;

    private org.apache.axis.types.UnsignedShort ANO;

    public Competencia_Type() {
    }

    public Competencia_Type(
           org.apache.axis.types.UnsignedByte MES,
           org.apache.axis.types.UnsignedShort ANO) {
           this.MES = MES;
           this.ANO = ANO;
    }


    /**
     * Gets the MES value for this Competencia_Type.
     * 
     * @return MES
     */
    public org.apache.axis.types.UnsignedByte getMES() {
        return MES;
    }


    /**
     * Sets the MES value for this Competencia_Type.
     * 
     * @param MES
     */
    public void setMES(org.apache.axis.types.UnsignedByte MES) {
        this.MES = MES;
    }


    /**
     * Gets the ANO value for this Competencia_Type.
     * 
     * @return ANO
     */
    public org.apache.axis.types.UnsignedShort getANO() {
        return ANO;
    }


    /**
     * Sets the ANO value for this Competencia_Type.
     * 
     * @param ANO
     */
    public void setANO(org.apache.axis.types.UnsignedShort ANO) {
        this.ANO = ANO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Competencia_Type)) return false;
        Competencia_Type other = (Competencia_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MES==null && other.getMES()==null) || 
             (this.MES!=null &&
              this.MES.equals(other.getMES()))) &&
            ((this.ANO==null && other.getANO()==null) || 
             (this.ANO!=null &&
              this.ANO.equals(other.getANO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMES() != null) {
            _hashCode += getMES().hashCode();
        }
        if (getANO() != null) {
            _hashCode += getANO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Competencia_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "competencia_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MES");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MES"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ANO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
