/**
 * Dados_saida_geral_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_geral_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedInt NSU;

    private java.lang.String HISTORICO;

    private java.util.Date DATA_MOVIMENTO;

    private java.util.Date DATA_VALIDACAO;

    private java.math.BigDecimal VALOR_TOTAL;

    public Dados_saida_geral_Type() {
    }

    public Dados_saida_geral_Type(
           org.apache.axis.types.UnsignedInt NSU,
           java.lang.String HISTORICO,
           java.util.Date DATA_MOVIMENTO,
           java.util.Date DATA_VALIDACAO,
           java.math.BigDecimal VALOR_TOTAL) {
           this.NSU = NSU;
           this.HISTORICO = HISTORICO;
           this.DATA_MOVIMENTO = DATA_MOVIMENTO;
           this.DATA_VALIDACAO = DATA_VALIDACAO;
           this.VALOR_TOTAL = VALOR_TOTAL;
    }


    /**
     * Gets the NSU value for this Dados_saida_geral_Type.
     * 
     * @return NSU
     */
    public org.apache.axis.types.UnsignedInt getNSU() {
        return NSU;
    }


    /**
     * Sets the NSU value for this Dados_saida_geral_Type.
     * 
     * @param NSU
     */
    public void setNSU(org.apache.axis.types.UnsignedInt NSU) {
        this.NSU = NSU;
    }


    /**
     * Gets the HISTORICO value for this Dados_saida_geral_Type.
     * 
     * @return HISTORICO
     */
    public java.lang.String getHISTORICO() {
        return HISTORICO;
    }


    /**
     * Sets the HISTORICO value for this Dados_saida_geral_Type.
     * 
     * @param HISTORICO
     */
    public void setHISTORICO(java.lang.String HISTORICO) {
        this.HISTORICO = HISTORICO;
    }


    /**
     * Gets the DATA_MOVIMENTO value for this Dados_saida_geral_Type.
     * 
     * @return DATA_MOVIMENTO
     */
    public java.util.Date getDATA_MOVIMENTO() {
        return DATA_MOVIMENTO;
    }


    /**
     * Sets the DATA_MOVIMENTO value for this Dados_saida_geral_Type.
     * 
     * @param DATA_MOVIMENTO
     */
    public void setDATA_MOVIMENTO(java.util.Date DATA_MOVIMENTO) {
        this.DATA_MOVIMENTO = DATA_MOVIMENTO;
    }


    /**
     * Gets the DATA_VALIDACAO value for this Dados_saida_geral_Type.
     * 
     * @return DATA_VALIDACAO
     */
    public java.util.Date getDATA_VALIDACAO() {
        return DATA_VALIDACAO;
    }


    /**
     * Sets the DATA_VALIDACAO value for this Dados_saida_geral_Type.
     * 
     * @param DATA_VALIDACAO
     */
    public void setDATA_VALIDACAO(java.util.Date DATA_VALIDACAO) {
        this.DATA_VALIDACAO = DATA_VALIDACAO;
    }


    /**
     * Gets the VALOR_TOTAL value for this Dados_saida_geral_Type.
     * 
     * @return VALOR_TOTAL
     */
    public java.math.BigDecimal getVALOR_TOTAL() {
        return VALOR_TOTAL;
    }


    /**
     * Sets the VALOR_TOTAL value for this Dados_saida_geral_Type.
     * 
     * @param VALOR_TOTAL
     */
    public void setVALOR_TOTAL(java.math.BigDecimal VALOR_TOTAL) {
        this.VALOR_TOTAL = VALOR_TOTAL;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_geral_Type)) return false;
        Dados_saida_geral_Type other = (Dados_saida_geral_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NSU==null && other.getNSU()==null) || 
             (this.NSU!=null &&
              this.NSU.equals(other.getNSU()))) &&
            ((this.HISTORICO==null && other.getHISTORICO()==null) || 
             (this.HISTORICO!=null &&
              this.HISTORICO.equals(other.getHISTORICO()))) &&
            ((this.DATA_MOVIMENTO==null && other.getDATA_MOVIMENTO()==null) || 
             (this.DATA_MOVIMENTO!=null &&
              this.DATA_MOVIMENTO.equals(other.getDATA_MOVIMENTO()))) &&
            ((this.DATA_VALIDACAO==null && other.getDATA_VALIDACAO()==null) || 
             (this.DATA_VALIDACAO!=null &&
              this.DATA_VALIDACAO.equals(other.getDATA_VALIDACAO()))) &&
            ((this.VALOR_TOTAL==null && other.getVALOR_TOTAL()==null) || 
             (this.VALOR_TOTAL!=null &&
              this.VALOR_TOTAL.equals(other.getVALOR_TOTAL())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNSU() != null) {
            _hashCode += getNSU().hashCode();
        }
        if (getHISTORICO() != null) {
            _hashCode += getHISTORICO().hashCode();
        }
        if (getDATA_MOVIMENTO() != null) {
            _hashCode += getDATA_MOVIMENTO().hashCode();
        }
        if (getDATA_VALIDACAO() != null) {
            _hashCode += getDATA_VALIDACAO().hashCode();
        }
        if (getVALOR_TOTAL() != null) {
            _hashCode += getVALOR_TOTAL().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_geral_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HISTORICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HISTORICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_MOVIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_MOVIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VALIDACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VALIDACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_TOTAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_TOTAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
