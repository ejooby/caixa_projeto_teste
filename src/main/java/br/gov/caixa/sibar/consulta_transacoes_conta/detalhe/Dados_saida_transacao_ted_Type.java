/**
 * Dados_saida_transacao_ted_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

import br.gov.caixa.sibar.consulta_transacoes_conta.lista.TransacaoInt;

public class Dados_saida_transacao_ted_Type  implements java.io.Serializable ,TransacaoInt {

	public String tipoServico;
	public String data;
	public String banco_Descricao;
	public String chaveSeguranca;

	
	
	
	
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_pessoa_ted_Type[] DADOS_REMETENTE;

    private org.apache.axis.types.UnsignedInt NSU_CONFIRMACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_transferencia_Type TIPO_TRANSFERENCIA;

    private java.lang.String MODALIDADE;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_ted_Type CONTA_DESTINATARIO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_pessoa_ted_Type DADOS_DESTINATARIO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ted_TypeFINALIDADE FINALIDADE;

    private java.math.BigDecimal VALOR;

    private java.math.BigDecimal VALOR_TARIFA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Corretora_Type CORRETORA;

    private org.apache.axis.types.UnsignedInt ISPB_DESTINO;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_ted_Type() {
    }

    public Dados_saida_transacao_ted_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_pessoa_ted_Type[] DADOS_REMETENTE,
           org.apache.axis.types.UnsignedInt NSU_CONFIRMACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_transferencia_Type TIPO_TRANSFERENCIA,
           java.lang.String MODALIDADE,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_ted_Type CONTA_DESTINATARIO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_pessoa_ted_Type DADOS_DESTINATARIO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ted_TypeFINALIDADE FINALIDADE,
           java.math.BigDecimal VALOR,
           java.math.BigDecimal VALOR_TARIFA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Corretora_Type CORRETORA,
           org.apache.axis.types.UnsignedInt ISPB_DESTINO,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.DADOS_REMETENTE = DADOS_REMETENTE;
           this.NSU_CONFIRMACAO = NSU_CONFIRMACAO;
           this.TIPO_TRANSFERENCIA = TIPO_TRANSFERENCIA;
           this.MODALIDADE = MODALIDADE;
           this.CONTA_DESTINATARIO = CONTA_DESTINATARIO;
           this.DADOS_DESTINATARIO = DADOS_DESTINATARIO;
           this.FINALIDADE = FINALIDADE;
           this.VALOR = VALOR;
           this.VALOR_TARIFA = VALOR_TARIFA;
           this.CORRETORA = CORRETORA;
           this.ISPB_DESTINO = ISPB_DESTINO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the DADOS_REMETENTE value for this Dados_saida_transacao_ted_Type.
     * 
     * @return DADOS_REMETENTE
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_pessoa_ted_Type[] getDADOS_REMETENTE() {
        return DADOS_REMETENTE;
    }


    /**
     * Sets the DADOS_REMETENTE value for this Dados_saida_transacao_ted_Type.
     * 
     * @param DADOS_REMETENTE
     */
    public void setDADOS_REMETENTE(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_pessoa_ted_Type[] DADOS_REMETENTE) {
        this.DADOS_REMETENTE = DADOS_REMETENTE;
    }

    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_pessoa_ted_Type getDADOS_REMETENTE(int i) {
        return this.DADOS_REMETENTE[i];
    }

    public void setDADOS_REMETENTE(int i, br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_pessoa_ted_Type _value) {
        this.DADOS_REMETENTE[i] = _value;
    }


    /**
     * Gets the NSU_CONFIRMACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return NSU_CONFIRMACAO
     */
    public org.apache.axis.types.UnsignedInt getNSU_CONFIRMACAO() {
        return NSU_CONFIRMACAO;
    }


    /**
     * Sets the NSU_CONFIRMACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param NSU_CONFIRMACAO
     */
    public void setNSU_CONFIRMACAO(org.apache.axis.types.UnsignedInt NSU_CONFIRMACAO) {
        this.NSU_CONFIRMACAO = NSU_CONFIRMACAO;
    }


    /**
     * Gets the TIPO_TRANSFERENCIA value for this Dados_saida_transacao_ted_Type.
     * 
     * @return TIPO_TRANSFERENCIA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_transferencia_Type getTIPO_TRANSFERENCIA() {
        return TIPO_TRANSFERENCIA;
    }


    /**
     * Sets the TIPO_TRANSFERENCIA value for this Dados_saida_transacao_ted_Type.
     * 
     * @param TIPO_TRANSFERENCIA
     */
    public void setTIPO_TRANSFERENCIA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_transferencia_Type TIPO_TRANSFERENCIA) {
        this.TIPO_TRANSFERENCIA = TIPO_TRANSFERENCIA;
    }


    /**
     * Gets the MODALIDADE value for this Dados_saida_transacao_ted_Type.
     * 
     * @return MODALIDADE
     */
    public java.lang.String getMODALIDADE() {
        return MODALIDADE;
    }


    /**
     * Sets the MODALIDADE value for this Dados_saida_transacao_ted_Type.
     * 
     * @param MODALIDADE
     */
    public void setMODALIDADE(java.lang.String MODALIDADE) {
        this.MODALIDADE = MODALIDADE;
    }


    /**
     * Gets the CONTA_DESTINATARIO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return CONTA_DESTINATARIO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_ted_Type getCONTA_DESTINATARIO() {
        return CONTA_DESTINATARIO;
    }


    /**
     * Sets the CONTA_DESTINATARIO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param CONTA_DESTINATARIO
     */
    public void setCONTA_DESTINATARIO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_ted_Type CONTA_DESTINATARIO) {
        this.CONTA_DESTINATARIO = CONTA_DESTINATARIO;
    }


    /**
     * Gets the DADOS_DESTINATARIO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return DADOS_DESTINATARIO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_pessoa_ted_Type getDADOS_DESTINATARIO() {
        return DADOS_DESTINATARIO;
    }


    /**
     * Sets the DADOS_DESTINATARIO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param DADOS_DESTINATARIO
     */
    public void setDADOS_DESTINATARIO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_pessoa_ted_Type DADOS_DESTINATARIO) {
        this.DADOS_DESTINATARIO = DADOS_DESTINATARIO;
    }


    /**
     * Gets the FINALIDADE value for this Dados_saida_transacao_ted_Type.
     * 
     * @return FINALIDADE
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ted_TypeFINALIDADE getFINALIDADE() {
        return FINALIDADE;
    }


    /**
     * Sets the FINALIDADE value for this Dados_saida_transacao_ted_Type.
     * 
     * @param FINALIDADE
     */
    public void setFINALIDADE(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_transacao_ted_TypeFINALIDADE FINALIDADE) {
        this.FINALIDADE = FINALIDADE;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_ted_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_ted_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the VALOR_TARIFA value for this Dados_saida_transacao_ted_Type.
     * 
     * @return VALOR_TARIFA
     */
    public java.math.BigDecimal getVALOR_TARIFA() {
        return VALOR_TARIFA;
    }


    /**
     * Sets the VALOR_TARIFA value for this Dados_saida_transacao_ted_Type.
     * 
     * @param VALOR_TARIFA
     */
    public void setVALOR_TARIFA(java.math.BigDecimal VALOR_TARIFA) {
        this.VALOR_TARIFA = VALOR_TARIFA;
    }


    /**
     * Gets the CORRETORA value for this Dados_saida_transacao_ted_Type.
     * 
     * @return CORRETORA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Corretora_Type getCORRETORA() {
        return CORRETORA;
    }


    /**
     * Sets the CORRETORA value for this Dados_saida_transacao_ted_Type.
     * 
     * @param CORRETORA
     */
    public void setCORRETORA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Corretora_Type CORRETORA) {
        this.CORRETORA = CORRETORA;
    }


    /**
     * Gets the ISPB_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return ISPB_DESTINO
     */
    public org.apache.axis.types.UnsignedInt getISPB_DESTINO() {
        return ISPB_DESTINO;
    }


    /**
     * Sets the ISPB_DESTINO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param ISPB_DESTINO
     */
    public void setISPB_DESTINO(org.apache.axis.types.UnsignedInt ISPB_DESTINO) {
        this.ISPB_DESTINO = ISPB_DESTINO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_ted_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_ted_Type)) return false;
        Dados_saida_transacao_ted_Type other = (Dados_saida_transacao_ted_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.DADOS_REMETENTE==null && other.getDADOS_REMETENTE()==null) || 
             (this.DADOS_REMETENTE!=null &&
              java.util.Arrays.equals(this.DADOS_REMETENTE, other.getDADOS_REMETENTE()))) &&
            ((this.NSU_CONFIRMACAO==null && other.getNSU_CONFIRMACAO()==null) || 
             (this.NSU_CONFIRMACAO!=null &&
              this.NSU_CONFIRMACAO.equals(other.getNSU_CONFIRMACAO()))) &&
            ((this.TIPO_TRANSFERENCIA==null && other.getTIPO_TRANSFERENCIA()==null) || 
             (this.TIPO_TRANSFERENCIA!=null &&
              this.TIPO_TRANSFERENCIA.equals(other.getTIPO_TRANSFERENCIA()))) &&
            ((this.MODALIDADE==null && other.getMODALIDADE()==null) || 
             (this.MODALIDADE!=null &&
              this.MODALIDADE.equals(other.getMODALIDADE()))) &&
            ((this.CONTA_DESTINATARIO==null && other.getCONTA_DESTINATARIO()==null) || 
             (this.CONTA_DESTINATARIO!=null &&
              this.CONTA_DESTINATARIO.equals(other.getCONTA_DESTINATARIO()))) &&
            ((this.DADOS_DESTINATARIO==null && other.getDADOS_DESTINATARIO()==null) || 
             (this.DADOS_DESTINATARIO!=null &&
              this.DADOS_DESTINATARIO.equals(other.getDADOS_DESTINATARIO()))) &&
            ((this.FINALIDADE==null && other.getFINALIDADE()==null) || 
             (this.FINALIDADE!=null &&
              this.FINALIDADE.equals(other.getFINALIDADE()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.VALOR_TARIFA==null && other.getVALOR_TARIFA()==null) || 
             (this.VALOR_TARIFA!=null &&
              this.VALOR_TARIFA.equals(other.getVALOR_TARIFA()))) &&
            ((this.CORRETORA==null && other.getCORRETORA()==null) || 
             (this.CORRETORA!=null &&
              this.CORRETORA.equals(other.getCORRETORA()))) &&
            ((this.ISPB_DESTINO==null && other.getISPB_DESTINO()==null) || 
             (this.ISPB_DESTINO!=null &&
              this.ISPB_DESTINO.equals(other.getISPB_DESTINO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getDADOS_REMETENTE() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDADOS_REMETENTE());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDADOS_REMETENTE(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNSU_CONFIRMACAO() != null) {
            _hashCode += getNSU_CONFIRMACAO().hashCode();
        }
        if (getTIPO_TRANSFERENCIA() != null) {
            _hashCode += getTIPO_TRANSFERENCIA().hashCode();
        }
        if (getMODALIDADE() != null) {
            _hashCode += getMODALIDADE().hashCode();
        }
        if (getCONTA_DESTINATARIO() != null) {
            _hashCode += getCONTA_DESTINATARIO().hashCode();
        }
        if (getDADOS_DESTINATARIO() != null) {
            _hashCode += getDADOS_DESTINATARIO().hashCode();
        }
        if (getFINALIDADE() != null) {
            _hashCode += getFINALIDADE().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getVALOR_TARIFA() != null) {
            _hashCode += getVALOR_TARIFA().hashCode();
        }
        if (getCORRETORA() != null) {
            _hashCode += getCORRETORA().hashCode();
        }
        if (getISPB_DESTINO() != null) {
            _hashCode += getISPB_DESTINO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_ted_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_ted_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_REMETENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_REMETENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_pessoa_ted_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU_CONFIRMACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU_CONFIRMACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_TRANSFERENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_TRANSFERENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "tipo_transferencia_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MODALIDADE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MODALIDADE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA_DESTINATARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA_DESTINATARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "conta_ted_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_DESTINATARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_DESTINATARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_pessoa_ted_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FINALIDADE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FINALIDADE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_ted_Type>FINALIDADE"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_TARIFA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_TARIFA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CORRETORA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CORRETORA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "corretora_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISPB_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ISPB_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public String getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getBanco_Descricao() {
		return banco_Descricao;
	}

	public void setBanco_Descricao(String banco_Descricao) {
		this.banco_Descricao = banco_Descricao;
	}

	public String getChaveSeguranca() {
		return chaveSeguranca;
	}

	public void setChaveSeguranca(String chaveSeguranca) {
		this.chaveSeguranca = chaveSeguranca;
	}

    
    
    
    
}
