/**
 * Dados_saida_transacao_iptu_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_iptu_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type PREFEITURA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contribuinte_Type CONTRIBUINTE;

    private org.apache.axis.types.UnsignedShort EXERCICIO;

    private org.apache.axis.types.UnsignedByte NOTIFICACAO;

    private java.util.Date VENCIMENTO;

    private org.apache.axis.types.UnsignedByte PARCELA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Prestacoes_em_aberto_Type PRESTACOES_EM_ABERTO;

    private java.lang.String CODIGO_BARRAS;

    private org.apache.axis.types.UnsignedInt NSU_PREFEITURA;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_iptu_Type() {
    }

    public Dados_saida_transacao_iptu_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type PREFEITURA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contribuinte_Type CONTRIBUINTE,
           org.apache.axis.types.UnsignedShort EXERCICIO,
           org.apache.axis.types.UnsignedByte NOTIFICACAO,
           java.util.Date VENCIMENTO,
           org.apache.axis.types.UnsignedByte PARCELA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Prestacoes_em_aberto_Type PRESTACOES_EM_ABERTO,
           java.lang.String CODIGO_BARRAS,
           org.apache.axis.types.UnsignedInt NSU_PREFEITURA,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.PREFEITURA = PREFEITURA;
           this.CONTRIBUINTE = CONTRIBUINTE;
           this.EXERCICIO = EXERCICIO;
           this.NOTIFICACAO = NOTIFICACAO;
           this.VENCIMENTO = VENCIMENTO;
           this.PARCELA = PARCELA;
           this.PRESTACOES_EM_ABERTO = PRESTACOES_EM_ABERTO;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.NSU_PREFEITURA = NSU_PREFEITURA;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the PREFEITURA value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return PREFEITURA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type getPREFEITURA() {
        return PREFEITURA;
    }


    /**
     * Sets the PREFEITURA value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param PREFEITURA
     */
    public void setPREFEITURA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type PREFEITURA) {
        this.PREFEITURA = PREFEITURA;
    }


    /**
     * Gets the CONTRIBUINTE value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return CONTRIBUINTE
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contribuinte_Type getCONTRIBUINTE() {
        return CONTRIBUINTE;
    }


    /**
     * Sets the CONTRIBUINTE value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param CONTRIBUINTE
     */
    public void setCONTRIBUINTE(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contribuinte_Type CONTRIBUINTE) {
        this.CONTRIBUINTE = CONTRIBUINTE;
    }


    /**
     * Gets the EXERCICIO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return EXERCICIO
     */
    public org.apache.axis.types.UnsignedShort getEXERCICIO() {
        return EXERCICIO;
    }


    /**
     * Sets the EXERCICIO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param EXERCICIO
     */
    public void setEXERCICIO(org.apache.axis.types.UnsignedShort EXERCICIO) {
        this.EXERCICIO = EXERCICIO;
    }


    /**
     * Gets the NOTIFICACAO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return NOTIFICACAO
     */
    public org.apache.axis.types.UnsignedByte getNOTIFICACAO() {
        return NOTIFICACAO;
    }


    /**
     * Sets the NOTIFICACAO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param NOTIFICACAO
     */
    public void setNOTIFICACAO(org.apache.axis.types.UnsignedByte NOTIFICACAO) {
        this.NOTIFICACAO = NOTIFICACAO;
    }


    /**
     * Gets the VENCIMENTO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return VENCIMENTO
     */
    public java.util.Date getVENCIMENTO() {
        return VENCIMENTO;
    }


    /**
     * Sets the VENCIMENTO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param VENCIMENTO
     */
    public void setVENCIMENTO(java.util.Date VENCIMENTO) {
        this.VENCIMENTO = VENCIMENTO;
    }


    /**
     * Gets the PARCELA value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return PARCELA
     */
    public org.apache.axis.types.UnsignedByte getPARCELA() {
        return PARCELA;
    }


    /**
     * Sets the PARCELA value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param PARCELA
     */
    public void setPARCELA(org.apache.axis.types.UnsignedByte PARCELA) {
        this.PARCELA = PARCELA;
    }


    /**
     * Gets the PRESTACOES_EM_ABERTO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return PRESTACOES_EM_ABERTO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Prestacoes_em_aberto_Type getPRESTACOES_EM_ABERTO() {
        return PRESTACOES_EM_ABERTO;
    }


    /**
     * Sets the PRESTACOES_EM_ABERTO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param PRESTACOES_EM_ABERTO
     */
    public void setPRESTACOES_EM_ABERTO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Prestacoes_em_aberto_Type PRESTACOES_EM_ABERTO) {
        this.PRESTACOES_EM_ABERTO = PRESTACOES_EM_ABERTO;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }


    /**
     * Gets the NSU_PREFEITURA value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return NSU_PREFEITURA
     */
    public org.apache.axis.types.UnsignedInt getNSU_PREFEITURA() {
        return NSU_PREFEITURA;
    }


    /**
     * Sets the NSU_PREFEITURA value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param NSU_PREFEITURA
     */
    public void setNSU_PREFEITURA(org.apache.axis.types.UnsignedInt NSU_PREFEITURA) {
        this.NSU_PREFEITURA = NSU_PREFEITURA;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_iptu_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_iptu_Type)) return false;
        Dados_saida_transacao_iptu_Type other = (Dados_saida_transacao_iptu_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.PREFEITURA==null && other.getPREFEITURA()==null) || 
             (this.PREFEITURA!=null &&
              this.PREFEITURA.equals(other.getPREFEITURA()))) &&
            ((this.CONTRIBUINTE==null && other.getCONTRIBUINTE()==null) || 
             (this.CONTRIBUINTE!=null &&
              this.CONTRIBUINTE.equals(other.getCONTRIBUINTE()))) &&
            ((this.EXERCICIO==null && other.getEXERCICIO()==null) || 
             (this.EXERCICIO!=null &&
              this.EXERCICIO.equals(other.getEXERCICIO()))) &&
            ((this.NOTIFICACAO==null && other.getNOTIFICACAO()==null) || 
             (this.NOTIFICACAO!=null &&
              this.NOTIFICACAO.equals(other.getNOTIFICACAO()))) &&
            ((this.VENCIMENTO==null && other.getVENCIMENTO()==null) || 
             (this.VENCIMENTO!=null &&
              this.VENCIMENTO.equals(other.getVENCIMENTO()))) &&
            ((this.PARCELA==null && other.getPARCELA()==null) || 
             (this.PARCELA!=null &&
              this.PARCELA.equals(other.getPARCELA()))) &&
            ((this.PRESTACOES_EM_ABERTO==null && other.getPRESTACOES_EM_ABERTO()==null) || 
             (this.PRESTACOES_EM_ABERTO!=null &&
              this.PRESTACOES_EM_ABERTO.equals(other.getPRESTACOES_EM_ABERTO()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              this.CODIGO_BARRAS.equals(other.getCODIGO_BARRAS()))) &&
            ((this.NSU_PREFEITURA==null && other.getNSU_PREFEITURA()==null) || 
             (this.NSU_PREFEITURA!=null &&
              this.NSU_PREFEITURA.equals(other.getNSU_PREFEITURA()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getPREFEITURA() != null) {
            _hashCode += getPREFEITURA().hashCode();
        }
        if (getCONTRIBUINTE() != null) {
            _hashCode += getCONTRIBUINTE().hashCode();
        }
        if (getEXERCICIO() != null) {
            _hashCode += getEXERCICIO().hashCode();
        }
        if (getNOTIFICACAO() != null) {
            _hashCode += getNOTIFICACAO().hashCode();
        }
        if (getVENCIMENTO() != null) {
            _hashCode += getVENCIMENTO().hashCode();
        }
        if (getPARCELA() != null) {
            _hashCode += getPARCELA().hashCode();
        }
        if (getPRESTACOES_EM_ABERTO() != null) {
            _hashCode += getPRESTACOES_EM_ABERTO().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            _hashCode += getCODIGO_BARRAS().hashCode();
        }
        if (getNSU_PREFEITURA() != null) {
            _hashCode += getNSU_PREFEITURA().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_iptu_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_iptu_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PREFEITURA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PREFEITURA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "convenio_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTRIBUINTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTRIBUINTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "contribuinte_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EXERCICIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EXERCICIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARCELA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PARCELA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRESTACOES_EM_ABERTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRESTACOES_EM_ABERTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "prestacoes_em_aberto_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU_PREFEITURA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU_PREFEITURA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
