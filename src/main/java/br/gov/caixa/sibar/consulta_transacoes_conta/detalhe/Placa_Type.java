/**
 * Placa_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Placa_Type  implements java.io.Serializable {
    private java.lang.String LETRAS;

    private org.apache.axis.types.UnsignedShort NUMEROS;

    public Placa_Type() {
    }

    public Placa_Type(
           java.lang.String LETRAS,
           org.apache.axis.types.UnsignedShort NUMEROS) {
           this.LETRAS = LETRAS;
           this.NUMEROS = NUMEROS;
    }


    /**
     * Gets the LETRAS value for this Placa_Type.
     * 
     * @return LETRAS
     */
    public java.lang.String getLETRAS() {
        return LETRAS;
    }


    /**
     * Sets the LETRAS value for this Placa_Type.
     * 
     * @param LETRAS
     */
    public void setLETRAS(java.lang.String LETRAS) {
        this.LETRAS = LETRAS;
    }


    /**
     * Gets the NUMEROS value for this Placa_Type.
     * 
     * @return NUMEROS
     */
    public org.apache.axis.types.UnsignedShort getNUMEROS() {
        return NUMEROS;
    }


    /**
     * Sets the NUMEROS value for this Placa_Type.
     * 
     * @param NUMEROS
     */
    public void setNUMEROS(org.apache.axis.types.UnsignedShort NUMEROS) {
        this.NUMEROS = NUMEROS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Placa_Type)) return false;
        Placa_Type other = (Placa_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.LETRAS==null && other.getLETRAS()==null) || 
             (this.LETRAS!=null &&
              this.LETRAS.equals(other.getLETRAS()))) &&
            ((this.NUMEROS==null && other.getNUMEROS()==null) || 
             (this.NUMEROS!=null &&
              this.NUMEROS.equals(other.getNUMEROS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLETRAS() != null) {
            _hashCode += getLETRAS().hashCode();
        }
        if (getNUMEROS() != null) {
            _hashCode += getNUMEROS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Placa_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "placa_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LETRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LETRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMEROS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMEROS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
