/**
 * Dados_entrada_solicitacoes_TypeSOLICITACAO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_entrada_solicitacoes_TypeSOLICITACAO implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Dados_entrada_solicitacoes_TypeSOLICITACAO(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _CONCESSIONARIA = "CONCESSIONARIA";
    public static final java.lang.String _HABITACAO_SEM_BARRAS = "HABITACAO_SEM_BARRAS";
    public static final java.lang.String _ICMS_IMPORTACAO = "ICMS_IMPORTACAO";
    public static final java.lang.String _DOC = "DOC";
    public static final java.lang.String _TEV = "TEV";
    public static final java.lang.String _DARF = "DARF";
    public static final java.lang.String _FGTS = "FGTS";
    public static final java.lang.String _GPS = "GPS";
    public static final java.lang.String _POUPANCA_PROGRAMADA = "POUPANCA_PROGRAMADA";
    public static final java.lang.String _TED = "TED";
    public static final java.lang.String _RECARGA_CELULAR = "RECARGA_CELULAR";
    public static final java.lang.String _CONCESSIONARIA_TELECOMUNICACAO = "CONCESSIONARIA_TELECOMUNICACAO";
    public static final java.lang.String _IPTU = "IPTU";
    public static final java.lang.String _ISS_TAXAS = "ISS_TAXAS";
    public static final java.lang.String _CONCESSIONARIA_ENERGIA = "CONCESSIONARIA_ENERGIA";
    public static final java.lang.String _GARE = "GARE";
    public static final java.lang.String _DEPOSITO_JUDICIAL = "DEPOSITO_JUDICIAL";
    public static final java.lang.String _PRIMEIRO_REGISTRO_ESTADO_SP = "PRIMEIRO_REGISTRO_ESTADO_SP";
    public static final java.lang.String _SEGUNDA_VIA_LICENCIAMENTO_SP = "SEGUNDA_VIA_LICENCIAMENTO_SP";
    public static final java.lang.String _BOLETO_DDA = "BOLETO_DDA";
    public static final java.lang.String _CARTEIRA_ELETRONICA = "CARTEIRA_ELETRONICA";
    public static final java.lang.String _DAED = "DAED";
    public static final java.lang.String _DEBITOS_PENDENTES_SP = "DEBITOS_PENDENTES_SP";
    public static final java.lang.String _DPVAT_SP = "DPVAT_SP";
    public static final java.lang.String _IPVA_SP = "IPVA_SP";
    public static final java.lang.String _TAXAS_DETRAN_SP = "TAXAS_DETRAN_SP";
    public static final java.lang.String _LICENCIAMENTO_SP = "LICENCIAMENTO_SP";
    public static final java.lang.String _MULTAS_SP = "MULTAS_SP";
    public static final java.lang.String _MULTAS_RENAINF = "MULTAS_RENAINF";
    public static final java.lang.String _PEC_GENERICO = "PEC_GENERICO";
    public static final java.lang.String _TRANSFERENCIA_SP = "TRANSFERENCIA_SP";
    public static final java.lang.String _SEGUNDA_VIA_TRANSFERENCIA_SP = "SEGUNDA_VIA_TRANSFERENCIA_SP";
    public static final java.lang.String _DARF_COM_CODIGO_BARRAS = "DARF_COM_CODIGO_BARRAS";
    public static final java.lang.String _RECEBIMENTO_ELETRONICO = "RECEBIMENTO_ELETRONICO";
    public static final java.lang.String _HABITACAO = "HABITACAO";
    public static final java.lang.String _BOLETO = "BOLETO";
    public static final java.lang.String _IPVA_MG = "IPVA_MG";
    public static final java.lang.String _DPVAT_MG = "DPVAT_MG";
    public static final java.lang.String _LICENCIAMENTO_MG = "LICENCIAMENTO_MG";
    public static final java.lang.String _TAXAS_VEICULOS_MG = "TAXAS_VEICULOS_MG";
    public static final java.lang.String _TAXAS_CONDUTOR_MG = "TAXAS_CONDUTOR_MG";
    public static final java.lang.String _MEGASENA = "MEGASENA";
    public static final java.lang.String _IPVA_RS = "IPVA_RS";
    public static final java.lang.String _DPVAT_RS = "DPVAT_RS";
    public static final java.lang.String _MULTAS_RS = "MULTAS_RS";
    public static final java.lang.String _LICENCIAMENTO_RS = "LICENCIAMENTO_RS";
    public static final java.lang.String _PAGAMENTO_UNIFICADO_RS = "PAGAMENTO_UNIFICADO_RS";
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO CONCESSIONARIA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_CONCESSIONARIA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO HABITACAO_SEM_BARRAS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_HABITACAO_SEM_BARRAS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO ICMS_IMPORTACAO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_ICMS_IMPORTACAO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DOC = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DOC);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TEV = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TEV);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DARF = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DARF);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO FGTS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_FGTS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO GPS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_GPS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO POUPANCA_PROGRAMADA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_POUPANCA_PROGRAMADA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TED = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TED);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO RECARGA_CELULAR = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_RECARGA_CELULAR);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO CONCESSIONARIA_TELECOMUNICACAO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_CONCESSIONARIA_TELECOMUNICACAO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO IPTU = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_IPTU);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO ISS_TAXAS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_ISS_TAXAS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO CONCESSIONARIA_ENERGIA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_CONCESSIONARIA_ENERGIA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO GARE = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_GARE);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DEPOSITO_JUDICIAL = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DEPOSITO_JUDICIAL);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO PRIMEIRO_REGISTRO_ESTADO_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_PRIMEIRO_REGISTRO_ESTADO_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO SEGUNDA_VIA_LICENCIAMENTO_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_SEGUNDA_VIA_LICENCIAMENTO_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO BOLETO_DDA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_BOLETO_DDA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO CARTEIRA_ELETRONICA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_CARTEIRA_ELETRONICA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DAED = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DAED);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DEBITOS_PENDENTES_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DEBITOS_PENDENTES_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DPVAT_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DPVAT_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO IPVA_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_IPVA_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TAXAS_DETRAN_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TAXAS_DETRAN_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO LICENCIAMENTO_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_LICENCIAMENTO_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO MULTAS_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_MULTAS_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO MULTAS_RENAINF = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_MULTAS_RENAINF);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO PEC_GENERICO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_PEC_GENERICO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TRANSFERENCIA_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TRANSFERENCIA_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO SEGUNDA_VIA_TRANSFERENCIA_SP = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_SEGUNDA_VIA_TRANSFERENCIA_SP);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DARF_COM_CODIGO_BARRAS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DARF_COM_CODIGO_BARRAS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO RECEBIMENTO_ELETRONICO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_RECEBIMENTO_ELETRONICO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO HABITACAO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_HABITACAO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO BOLETO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_BOLETO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO IPVA_MG = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_IPVA_MG);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DPVAT_MG = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DPVAT_MG);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO LICENCIAMENTO_MG = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_LICENCIAMENTO_MG);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TAXAS_VEICULOS_MG = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TAXAS_VEICULOS_MG);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TAXAS_CONDUTOR_MG = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TAXAS_CONDUTOR_MG);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO MEGASENA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_MEGASENA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO IPVA_RS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_IPVA_RS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DPVAT_RS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DPVAT_RS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO MULTAS_RS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_MULTAS_RS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO LICENCIAMENTO_RS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_LICENCIAMENTO_RS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO PAGAMENTO_UNIFICADO_RS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_PAGAMENTO_UNIFICADO_RS);
    public java.lang.String getValue() { return _value_;}
    public static Dados_entrada_solicitacoes_TypeSOLICITACAO fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Dados_entrada_solicitacoes_TypeSOLICITACAO enumeration = (Dados_entrada_solicitacoes_TypeSOLICITACAO)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Dados_entrada_solicitacoes_TypeSOLICITACAO fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_entrada_solicitacoes_TypeSOLICITACAO.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_entrada_solicitacoes_Type>SOLICITACAO"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
