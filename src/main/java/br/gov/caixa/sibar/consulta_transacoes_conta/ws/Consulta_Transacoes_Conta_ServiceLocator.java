/**
 * Consulta_Transacoes_Conta_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.ws;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consulta_Transacoes_Conta_ServiceLocator extends org.apache.axis.client.Service implements br.gov.caixa.sibar.consulta_transacoes_conta.ws.Consulta_Transacoes_Conta_Service {

	protected static final Logger logger = LoggerFactory.getLogger(Consulta_Transacoes_Conta_ServiceLocator.class);
	
	
    public Consulta_Transacoes_Conta_ServiceLocator() {
    }


    public Consulta_Transacoes_Conta_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Consulta_Transacoes_Conta_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for Consulta_Transacoes_ContaSOAP
    //private java.lang.String Consulta_Transacoes_ContaSOAP_address = "https://des.barramento.caixa/sibar/ConsultaTransacoesConta";

    private String ExtratoSOAP_ServiceName = "ConsultaTransacoesConta";
    private java.lang.String Consulta_Transacoes_ContaSOAP_address = getJndiAccessor("apnbm.sibar.wsurl");
    
    
    
    public java.lang.String getConsulta_Transacoes_ContaSOAPAddress() {
        return Consulta_Transacoes_ContaSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String Consulta_Transacoes_ContaSOAPWSDDServiceName = "Consulta_Transacoes_ContaSOAP";

    public java.lang.String getConsulta_Transacoes_ContaSOAPWSDDServiceName() {
        return Consulta_Transacoes_ContaSOAPWSDDServiceName;
    }

    public void setConsulta_Transacoes_ContaSOAPWSDDServiceName(java.lang.String name) {
        Consulta_Transacoes_ContaSOAPWSDDServiceName = name;
    }

    public br.gov.caixa.sibar.consulta_transacoes_conta.ws.Consulta_Transacoes_Conta_PortType getConsulta_Transacoes_ContaSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(Consulta_Transacoes_ContaSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getConsulta_Transacoes_ContaSOAP(endpoint);
    }

    public br.gov.caixa.sibar.consulta_transacoes_conta.ws.Consulta_Transacoes_Conta_PortType getConsulta_Transacoes_ContaSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.gov.caixa.sibar.consulta_transacoes_conta.ws.ConsultaTransacoesContaSOAPStub _stub = new br.gov.caixa.sibar.consulta_transacoes_conta.ws.ConsultaTransacoesContaSOAPStub(portAddress, this);
            _stub.setPortName(getConsulta_Transacoes_ContaSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setConsulta_Transacoes_ContaSOAPEndpointAddress(java.lang.String address) {
        Consulta_Transacoes_ContaSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.gov.caixa.sibar.consulta_transacoes_conta.ws.Consulta_Transacoes_Conta_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                br.gov.caixa.sibar.consulta_transacoes_conta.ws.ConsultaTransacoesContaSOAPStub _stub = new br.gov.caixa.sibar.consulta_transacoes_conta.ws.ConsultaTransacoesContaSOAPStub(new java.net.URL(Consulta_Transacoes_ContaSOAP_address), this);
                _stub.setPortName(getConsulta_Transacoes_ContaSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("Consulta_Transacoes_ContaSOAP".equals(inputPortName)) {
            return getConsulta_Transacoes_ContaSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/ws", "Consulta_Transacoes_Conta");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/ws", "Consulta_Transacoes_ContaSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("Consulta_Transacoes_ContaSOAP".equals(portName)) {
            setConsulta_Transacoes_ContaSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

    
    public String getJndiAccessor(String jndiKeytokenFile) {
    	logger.info("  getJndiAccessor INI  " );
		InitialContext ctx = null;
		String name = null;
		try {
			
			ctx = new InitialContext();
			name = (String)ctx.lookup(jndiKeytokenFile);
			
		} catch (NamingException e1) {
			e1.printStackTrace();
			
			//name = "http://des.barramento.caixa/sibar";
			
		}
		

		
    	if (name != null && name.length() > 0) {
         char x =   name.charAt(name.length() - 1);
         if(!(x == '/')){
        	 name = name+"/";
         }
        }
    	name = name+ExtratoSOAP_ServiceName;
    	logger.info("  url =   "+name );
    	logger.info("  getJndiAccessor FIM  " );
		
		
		return name;
		
	} 

    

    
    
}
