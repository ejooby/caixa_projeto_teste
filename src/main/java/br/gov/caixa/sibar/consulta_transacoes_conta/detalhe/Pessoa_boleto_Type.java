/**
 * Pessoa_boleto_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Pessoa_boleto_Type  implements java.io.Serializable {
    private java.lang.String NOME_FANTASIA;

    private java.lang.String RAZAO_SOCIAL;

    private org.apache.axis.types.UnsignedLong CPF;

    private org.apache.axis.types.UnsignedLong CNPJ;

    public Pessoa_boleto_Type() {
    }

    public Pessoa_boleto_Type(
           java.lang.String NOME_FANTASIA,
           java.lang.String RAZAO_SOCIAL,
           org.apache.axis.types.UnsignedLong CPF,
           org.apache.axis.types.UnsignedLong CNPJ) {
           this.NOME_FANTASIA = NOME_FANTASIA;
           this.RAZAO_SOCIAL = RAZAO_SOCIAL;
           this.CPF = CPF;
           this.CNPJ = CNPJ;
    }


    /**
     * Gets the NOME_FANTASIA value for this Pessoa_boleto_Type.
     * 
     * @return NOME_FANTASIA
     */
    public java.lang.String getNOME_FANTASIA() {
        return NOME_FANTASIA;
    }


    /**
     * Sets the NOME_FANTASIA value for this Pessoa_boleto_Type.
     * 
     * @param NOME_FANTASIA
     */
    public void setNOME_FANTASIA(java.lang.String NOME_FANTASIA) {
        this.NOME_FANTASIA = NOME_FANTASIA;
    }


    /**
     * Gets the RAZAO_SOCIAL value for this Pessoa_boleto_Type.
     * 
     * @return RAZAO_SOCIAL
     */
    public java.lang.String getRAZAO_SOCIAL() {
        return RAZAO_SOCIAL;
    }


    /**
     * Sets the RAZAO_SOCIAL value for this Pessoa_boleto_Type.
     * 
     * @param RAZAO_SOCIAL
     */
    public void setRAZAO_SOCIAL(java.lang.String RAZAO_SOCIAL) {
        this.RAZAO_SOCIAL = RAZAO_SOCIAL;
    }


    /**
     * Gets the CPF value for this Pessoa_boleto_Type.
     * 
     * @return CPF
     */
    public org.apache.axis.types.UnsignedLong getCPF() {
        return CPF;
    }


    /**
     * Sets the CPF value for this Pessoa_boleto_Type.
     * 
     * @param CPF
     */
    public void setCPF(org.apache.axis.types.UnsignedLong CPF) {
        this.CPF = CPF;
    }


    /**
     * Gets the CNPJ value for this Pessoa_boleto_Type.
     * 
     * @return CNPJ
     */
    public org.apache.axis.types.UnsignedLong getCNPJ() {
        return CNPJ;
    }


    /**
     * Sets the CNPJ value for this Pessoa_boleto_Type.
     * 
     * @param CNPJ
     */
    public void setCNPJ(org.apache.axis.types.UnsignedLong CNPJ) {
        this.CNPJ = CNPJ;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Pessoa_boleto_Type)) return false;
        Pessoa_boleto_Type other = (Pessoa_boleto_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NOME_FANTASIA==null && other.getNOME_FANTASIA()==null) || 
             (this.NOME_FANTASIA!=null &&
              this.NOME_FANTASIA.equals(other.getNOME_FANTASIA()))) &&
            ((this.RAZAO_SOCIAL==null && other.getRAZAO_SOCIAL()==null) || 
             (this.RAZAO_SOCIAL!=null &&
              this.RAZAO_SOCIAL.equals(other.getRAZAO_SOCIAL()))) &&
            ((this.CPF==null && other.getCPF()==null) || 
             (this.CPF!=null &&
              this.CPF.equals(other.getCPF()))) &&
            ((this.CNPJ==null && other.getCNPJ()==null) || 
             (this.CNPJ!=null &&
              this.CNPJ.equals(other.getCNPJ())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNOME_FANTASIA() != null) {
            _hashCode += getNOME_FANTASIA().hashCode();
        }
        if (getRAZAO_SOCIAL() != null) {
            _hashCode += getRAZAO_SOCIAL().hashCode();
        }
        if (getCPF() != null) {
            _hashCode += getCPF().hashCode();
        }
        if (getCNPJ() != null) {
            _hashCode += getCNPJ().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Pessoa_boleto_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "pessoa_boleto_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_FANTASIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_FANTASIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RAZAO_SOCIAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RAZAO_SOCIAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
