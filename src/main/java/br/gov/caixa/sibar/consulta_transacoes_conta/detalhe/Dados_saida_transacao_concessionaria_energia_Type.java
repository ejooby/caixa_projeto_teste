/**
 * Dados_saida_transacao_concessionaria_energia_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_concessionaria_energia_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type CONCESSIONARIA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Unidade_consumidora_Type UNIDADE_CONSUMIDORA;

    private java.util.Date VENCIMENTO;

    private java.math.BigDecimal TARIFA_SERVICO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type PERIODO_CONSUMO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_concessionaria_energia_Type() {
    }

    public Dados_saida_transacao_concessionaria_energia_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type CONCESSIONARIA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Unidade_consumidora_Type UNIDADE_CONSUMIDORA,
           java.util.Date VENCIMENTO,
           java.math.BigDecimal TARIFA_SERVICO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type PERIODO_CONSUMO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.CONCESSIONARIA = CONCESSIONARIA;
           this.UNIDADE_CONSUMIDORA = UNIDADE_CONSUMIDORA;
           this.VENCIMENTO = VENCIMENTO;
           this.TARIFA_SERVICO = TARIFA_SERVICO;
           this.PERIODO_CONSUMO = PERIODO_CONSUMO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the CONCESSIONARIA value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @return CONCESSIONARIA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type getCONCESSIONARIA() {
        return CONCESSIONARIA;
    }


    /**
     * Sets the CONCESSIONARIA value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @param CONCESSIONARIA
     */
    public void setCONCESSIONARIA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type CONCESSIONARIA) {
        this.CONCESSIONARIA = CONCESSIONARIA;
    }


    /**
     * Gets the UNIDADE_CONSUMIDORA value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @return UNIDADE_CONSUMIDORA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Unidade_consumidora_Type getUNIDADE_CONSUMIDORA() {
        return UNIDADE_CONSUMIDORA;
    }


    /**
     * Sets the UNIDADE_CONSUMIDORA value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @param UNIDADE_CONSUMIDORA
     */
    public void setUNIDADE_CONSUMIDORA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Unidade_consumidora_Type UNIDADE_CONSUMIDORA) {
        this.UNIDADE_CONSUMIDORA = UNIDADE_CONSUMIDORA;
    }


    /**
     * Gets the VENCIMENTO value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @return VENCIMENTO
     */
    public java.util.Date getVENCIMENTO() {
        return VENCIMENTO;
    }


    /**
     * Sets the VENCIMENTO value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @param VENCIMENTO
     */
    public void setVENCIMENTO(java.util.Date VENCIMENTO) {
        this.VENCIMENTO = VENCIMENTO;
    }


    /**
     * Gets the TARIFA_SERVICO value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @return TARIFA_SERVICO
     */
    public java.math.BigDecimal getTARIFA_SERVICO() {
        return TARIFA_SERVICO;
    }


    /**
     * Sets the TARIFA_SERVICO value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @param TARIFA_SERVICO
     */
    public void setTARIFA_SERVICO(java.math.BigDecimal TARIFA_SERVICO) {
        this.TARIFA_SERVICO = TARIFA_SERVICO;
    }


    /**
     * Gets the PERIODO_CONSUMO value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @return PERIODO_CONSUMO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type getPERIODO_CONSUMO() {
        return PERIODO_CONSUMO;
    }


    /**
     * Sets the PERIODO_CONSUMO value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @param PERIODO_CONSUMO
     */
    public void setPERIODO_CONSUMO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Periodo_Type PERIODO_CONSUMO) {
        this.PERIODO_CONSUMO = PERIODO_CONSUMO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_concessionaria_energia_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_concessionaria_energia_Type)) return false;
        Dados_saida_transacao_concessionaria_energia_Type other = (Dados_saida_transacao_concessionaria_energia_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.CONCESSIONARIA==null && other.getCONCESSIONARIA()==null) || 
             (this.CONCESSIONARIA!=null &&
              this.CONCESSIONARIA.equals(other.getCONCESSIONARIA()))) &&
            ((this.UNIDADE_CONSUMIDORA==null && other.getUNIDADE_CONSUMIDORA()==null) || 
             (this.UNIDADE_CONSUMIDORA!=null &&
              this.UNIDADE_CONSUMIDORA.equals(other.getUNIDADE_CONSUMIDORA()))) &&
            ((this.VENCIMENTO==null && other.getVENCIMENTO()==null) || 
             (this.VENCIMENTO!=null &&
              this.VENCIMENTO.equals(other.getVENCIMENTO()))) &&
            ((this.TARIFA_SERVICO==null && other.getTARIFA_SERVICO()==null) || 
             (this.TARIFA_SERVICO!=null &&
              this.TARIFA_SERVICO.equals(other.getTARIFA_SERVICO()))) &&
            ((this.PERIODO_CONSUMO==null && other.getPERIODO_CONSUMO()==null) || 
             (this.PERIODO_CONSUMO!=null &&
              this.PERIODO_CONSUMO.equals(other.getPERIODO_CONSUMO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getCONCESSIONARIA() != null) {
            _hashCode += getCONCESSIONARIA().hashCode();
        }
        if (getUNIDADE_CONSUMIDORA() != null) {
            _hashCode += getUNIDADE_CONSUMIDORA().hashCode();
        }
        if (getVENCIMENTO() != null) {
            _hashCode += getVENCIMENTO().hashCode();
        }
        if (getTARIFA_SERVICO() != null) {
            _hashCode += getTARIFA_SERVICO().hashCode();
        }
        if (getPERIODO_CONSUMO() != null) {
            _hashCode += getPERIODO_CONSUMO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_concessionaria_energia_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_concessionaria_energia_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONCESSIONARIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONCESSIONARIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "convenio_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UNIDADE_CONSUMIDORA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UNIDADE_CONSUMIDORA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "unidade_consumidora_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TARIFA_SERVICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TARIFA_SERVICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PERIODO_CONSUMO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PERIODO_CONSUMO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "periodo_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
