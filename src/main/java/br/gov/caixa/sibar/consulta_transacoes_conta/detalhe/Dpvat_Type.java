/**
 * Dpvat_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dpvat_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedShort EXERCICIO;

    private java.math.BigDecimal VALOR;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dpvat_TypeTIPO TIPO;

    public Dpvat_Type() {
    }

    public Dpvat_Type(
           org.apache.axis.types.UnsignedShort EXERCICIO,
           java.math.BigDecimal VALOR,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dpvat_TypeTIPO TIPO) {
           this.EXERCICIO = EXERCICIO;
           this.VALOR = VALOR;
           this.TIPO = TIPO;
    }


    /**
     * Gets the EXERCICIO value for this Dpvat_Type.
     * 
     * @return EXERCICIO
     */
    public org.apache.axis.types.UnsignedShort getEXERCICIO() {
        return EXERCICIO;
    }


    /**
     * Sets the EXERCICIO value for this Dpvat_Type.
     * 
     * @param EXERCICIO
     */
    public void setEXERCICIO(org.apache.axis.types.UnsignedShort EXERCICIO) {
        this.EXERCICIO = EXERCICIO;
    }


    /**
     * Gets the VALOR value for this Dpvat_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dpvat_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the TIPO value for this Dpvat_Type.
     * 
     * @return TIPO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dpvat_TypeTIPO getTIPO() {
        return TIPO;
    }


    /**
     * Sets the TIPO value for this Dpvat_Type.
     * 
     * @param TIPO
     */
    public void setTIPO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dpvat_TypeTIPO TIPO) {
        this.TIPO = TIPO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dpvat_Type)) return false;
        Dpvat_Type other = (Dpvat_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.EXERCICIO==null && other.getEXERCICIO()==null) || 
             (this.EXERCICIO!=null &&
              this.EXERCICIO.equals(other.getEXERCICIO()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.TIPO==null && other.getTIPO()==null) || 
             (this.TIPO!=null &&
              this.TIPO.equals(other.getTIPO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEXERCICIO() != null) {
            _hashCode += getEXERCICIO().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getTIPO() != null) {
            _hashCode += getTIPO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dpvat_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dpvat_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EXERCICIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EXERCICIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dpvat_Type>TIPO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
