/**
 * Processo_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Processo_Type  implements java.io.Serializable {
    private java.lang.String NUMERO;

    private java.lang.String RECLAMANTE;

    private java.lang.String RECLAMADO;

    public Processo_Type() {
    }

    public Processo_Type(
           java.lang.String NUMERO,
           java.lang.String RECLAMANTE,
           java.lang.String RECLAMADO) {
           this.NUMERO = NUMERO;
           this.RECLAMANTE = RECLAMANTE;
           this.RECLAMADO = RECLAMADO;
    }


    /**
     * Gets the NUMERO value for this Processo_Type.
     * 
     * @return NUMERO
     */
    public java.lang.String getNUMERO() {
        return NUMERO;
    }


    /**
     * Sets the NUMERO value for this Processo_Type.
     * 
     * @param NUMERO
     */
    public void setNUMERO(java.lang.String NUMERO) {
        this.NUMERO = NUMERO;
    }


    /**
     * Gets the RECLAMANTE value for this Processo_Type.
     * 
     * @return RECLAMANTE
     */
    public java.lang.String getRECLAMANTE() {
        return RECLAMANTE;
    }


    /**
     * Sets the RECLAMANTE value for this Processo_Type.
     * 
     * @param RECLAMANTE
     */
    public void setRECLAMANTE(java.lang.String RECLAMANTE) {
        this.RECLAMANTE = RECLAMANTE;
    }


    /**
     * Gets the RECLAMADO value for this Processo_Type.
     * 
     * @return RECLAMADO
     */
    public java.lang.String getRECLAMADO() {
        return RECLAMADO;
    }


    /**
     * Sets the RECLAMADO value for this Processo_Type.
     * 
     * @param RECLAMADO
     */
    public void setRECLAMADO(java.lang.String RECLAMADO) {
        this.RECLAMADO = RECLAMADO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Processo_Type)) return false;
        Processo_Type other = (Processo_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NUMERO==null && other.getNUMERO()==null) || 
             (this.NUMERO!=null &&
              this.NUMERO.equals(other.getNUMERO()))) &&
            ((this.RECLAMANTE==null && other.getRECLAMANTE()==null) || 
             (this.RECLAMANTE!=null &&
              this.RECLAMANTE.equals(other.getRECLAMANTE()))) &&
            ((this.RECLAMADO==null && other.getRECLAMADO()==null) || 
             (this.RECLAMADO!=null &&
              this.RECLAMADO.equals(other.getRECLAMADO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNUMERO() != null) {
            _hashCode += getNUMERO().hashCode();
        }
        if (getRECLAMANTE() != null) {
            _hashCode += getRECLAMANTE().hashCode();
        }
        if (getRECLAMADO() != null) {
            _hashCode += getRECLAMADO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Processo_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "processo_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECLAMANTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RECLAMANTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECLAMADO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RECLAMADO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
