/**
 * Tipo_transferencia_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Tipo_transferencia_Type implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Tipo_transferencia_Type(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "Para Terceiros";
    public static final java.lang.String _value2 = "Para Mesmo Titular";
    public static final java.lang.String _value3 = "IF para Cliente";
    public static final java.lang.String _value4 = "Cliente para IF";
    public static final java.lang.String _value5 = "Deposito Judicial";
    public static final java.lang.String _value6 = "Entre Bancos";
    public static final java.lang.String _value7 = "Repasse Tesouro";
    public static final Tipo_transferencia_Type value1 = new Tipo_transferencia_Type(_value1);
    public static final Tipo_transferencia_Type value2 = new Tipo_transferencia_Type(_value2);
    public static final Tipo_transferencia_Type value3 = new Tipo_transferencia_Type(_value3);
    public static final Tipo_transferencia_Type value4 = new Tipo_transferencia_Type(_value4);
    public static final Tipo_transferencia_Type value5 = new Tipo_transferencia_Type(_value5);
    public static final Tipo_transferencia_Type value6 = new Tipo_transferencia_Type(_value6);
    public static final Tipo_transferencia_Type value7 = new Tipo_transferencia_Type(_value7);
    public java.lang.String getValue() { return _value_;}
    public static Tipo_transferencia_Type fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Tipo_transferencia_Type enumeration = (Tipo_transferencia_Type)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Tipo_transferencia_Type fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_transferencia_Type.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "tipo_transferencia_Type"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
