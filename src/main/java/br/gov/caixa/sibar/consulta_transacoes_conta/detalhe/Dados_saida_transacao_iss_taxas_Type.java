/**
 * Dados_saida_transacao_iss_taxas_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_iss_taxas_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type PREFEITURA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contribuinte_Type CONTRIBUINTE;

    private org.apache.axis.types.UnsignedInt CODIGO_TRIBUTO;

    private org.apache.axis.types.UnsignedByte PORCENTAGEM_MULTA;

    private java.util.Date DATA_VENCIMENTO;

    private java.math.BigDecimal VALOR_TRIBUTO;

    private java.math.BigDecimal VALOR_MULTA;

    private java.math.BigDecimal VALOR_MORA;

    private java.math.BigDecimal VALOR_CORRECAO;

    private java.lang.String CODIGO_BARRAS;

    private org.apache.axis.types.UnsignedInt NSU_PREFEITURA;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_iss_taxas_Type() {
    }

    public Dados_saida_transacao_iss_taxas_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type PREFEITURA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contribuinte_Type CONTRIBUINTE,
           org.apache.axis.types.UnsignedInt CODIGO_TRIBUTO,
           org.apache.axis.types.UnsignedByte PORCENTAGEM_MULTA,
           java.util.Date DATA_VENCIMENTO,
           java.math.BigDecimal VALOR_TRIBUTO,
           java.math.BigDecimal VALOR_MULTA,
           java.math.BigDecimal VALOR_MORA,
           java.math.BigDecimal VALOR_CORRECAO,
           java.lang.String CODIGO_BARRAS,
           org.apache.axis.types.UnsignedInt NSU_PREFEITURA,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.PREFEITURA = PREFEITURA;
           this.CONTRIBUINTE = CONTRIBUINTE;
           this.CODIGO_TRIBUTO = CODIGO_TRIBUTO;
           this.PORCENTAGEM_MULTA = PORCENTAGEM_MULTA;
           this.DATA_VENCIMENTO = DATA_VENCIMENTO;
           this.VALOR_TRIBUTO = VALOR_TRIBUTO;
           this.VALOR_MULTA = VALOR_MULTA;
           this.VALOR_MORA = VALOR_MORA;
           this.VALOR_CORRECAO = VALOR_CORRECAO;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.NSU_PREFEITURA = NSU_PREFEITURA;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the PREFEITURA value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return PREFEITURA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type getPREFEITURA() {
        return PREFEITURA;
    }


    /**
     * Sets the PREFEITURA value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param PREFEITURA
     */
    public void setPREFEITURA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Convenio_Type PREFEITURA) {
        this.PREFEITURA = PREFEITURA;
    }


    /**
     * Gets the CONTRIBUINTE value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return CONTRIBUINTE
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contribuinte_Type getCONTRIBUINTE() {
        return CONTRIBUINTE;
    }


    /**
     * Sets the CONTRIBUINTE value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param CONTRIBUINTE
     */
    public void setCONTRIBUINTE(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Contribuinte_Type CONTRIBUINTE) {
        this.CONTRIBUINTE = CONTRIBUINTE;
    }


    /**
     * Gets the CODIGO_TRIBUTO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return CODIGO_TRIBUTO
     */
    public org.apache.axis.types.UnsignedInt getCODIGO_TRIBUTO() {
        return CODIGO_TRIBUTO;
    }


    /**
     * Sets the CODIGO_TRIBUTO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param CODIGO_TRIBUTO
     */
    public void setCODIGO_TRIBUTO(org.apache.axis.types.UnsignedInt CODIGO_TRIBUTO) {
        this.CODIGO_TRIBUTO = CODIGO_TRIBUTO;
    }


    /**
     * Gets the PORCENTAGEM_MULTA value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return PORCENTAGEM_MULTA
     */
    public org.apache.axis.types.UnsignedByte getPORCENTAGEM_MULTA() {
        return PORCENTAGEM_MULTA;
    }


    /**
     * Sets the PORCENTAGEM_MULTA value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param PORCENTAGEM_MULTA
     */
    public void setPORCENTAGEM_MULTA(org.apache.axis.types.UnsignedByte PORCENTAGEM_MULTA) {
        this.PORCENTAGEM_MULTA = PORCENTAGEM_MULTA;
    }


    /**
     * Gets the DATA_VENCIMENTO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return DATA_VENCIMENTO
     */
    public java.util.Date getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }


    /**
     * Sets the DATA_VENCIMENTO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param DATA_VENCIMENTO
     */
    public void setDATA_VENCIMENTO(java.util.Date DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }


    /**
     * Gets the VALOR_TRIBUTO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return VALOR_TRIBUTO
     */
    public java.math.BigDecimal getVALOR_TRIBUTO() {
        return VALOR_TRIBUTO;
    }


    /**
     * Sets the VALOR_TRIBUTO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param VALOR_TRIBUTO
     */
    public void setVALOR_TRIBUTO(java.math.BigDecimal VALOR_TRIBUTO) {
        this.VALOR_TRIBUTO = VALOR_TRIBUTO;
    }


    /**
     * Gets the VALOR_MULTA value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return VALOR_MULTA
     */
    public java.math.BigDecimal getVALOR_MULTA() {
        return VALOR_MULTA;
    }


    /**
     * Sets the VALOR_MULTA value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param VALOR_MULTA
     */
    public void setVALOR_MULTA(java.math.BigDecimal VALOR_MULTA) {
        this.VALOR_MULTA = VALOR_MULTA;
    }


    /**
     * Gets the VALOR_MORA value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return VALOR_MORA
     */
    public java.math.BigDecimal getVALOR_MORA() {
        return VALOR_MORA;
    }


    /**
     * Sets the VALOR_MORA value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param VALOR_MORA
     */
    public void setVALOR_MORA(java.math.BigDecimal VALOR_MORA) {
        this.VALOR_MORA = VALOR_MORA;
    }


    /**
     * Gets the VALOR_CORRECAO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return VALOR_CORRECAO
     */
    public java.math.BigDecimal getVALOR_CORRECAO() {
        return VALOR_CORRECAO;
    }


    /**
     * Sets the VALOR_CORRECAO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param VALOR_CORRECAO
     */
    public void setVALOR_CORRECAO(java.math.BigDecimal VALOR_CORRECAO) {
        this.VALOR_CORRECAO = VALOR_CORRECAO;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }


    /**
     * Gets the NSU_PREFEITURA value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return NSU_PREFEITURA
     */
    public org.apache.axis.types.UnsignedInt getNSU_PREFEITURA() {
        return NSU_PREFEITURA;
    }


    /**
     * Sets the NSU_PREFEITURA value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param NSU_PREFEITURA
     */
    public void setNSU_PREFEITURA(org.apache.axis.types.UnsignedInt NSU_PREFEITURA) {
        this.NSU_PREFEITURA = NSU_PREFEITURA;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_iss_taxas_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_iss_taxas_Type)) return false;
        Dados_saida_transacao_iss_taxas_Type other = (Dados_saida_transacao_iss_taxas_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.PREFEITURA==null && other.getPREFEITURA()==null) || 
             (this.PREFEITURA!=null &&
              this.PREFEITURA.equals(other.getPREFEITURA()))) &&
            ((this.CONTRIBUINTE==null && other.getCONTRIBUINTE()==null) || 
             (this.CONTRIBUINTE!=null &&
              this.CONTRIBUINTE.equals(other.getCONTRIBUINTE()))) &&
            ((this.CODIGO_TRIBUTO==null && other.getCODIGO_TRIBUTO()==null) || 
             (this.CODIGO_TRIBUTO!=null &&
              this.CODIGO_TRIBUTO.equals(other.getCODIGO_TRIBUTO()))) &&
            ((this.PORCENTAGEM_MULTA==null && other.getPORCENTAGEM_MULTA()==null) || 
             (this.PORCENTAGEM_MULTA!=null &&
              this.PORCENTAGEM_MULTA.equals(other.getPORCENTAGEM_MULTA()))) &&
            ((this.DATA_VENCIMENTO==null && other.getDATA_VENCIMENTO()==null) || 
             (this.DATA_VENCIMENTO!=null &&
              this.DATA_VENCIMENTO.equals(other.getDATA_VENCIMENTO()))) &&
            ((this.VALOR_TRIBUTO==null && other.getVALOR_TRIBUTO()==null) || 
             (this.VALOR_TRIBUTO!=null &&
              this.VALOR_TRIBUTO.equals(other.getVALOR_TRIBUTO()))) &&
            ((this.VALOR_MULTA==null && other.getVALOR_MULTA()==null) || 
             (this.VALOR_MULTA!=null &&
              this.VALOR_MULTA.equals(other.getVALOR_MULTA()))) &&
            ((this.VALOR_MORA==null && other.getVALOR_MORA()==null) || 
             (this.VALOR_MORA!=null &&
              this.VALOR_MORA.equals(other.getVALOR_MORA()))) &&
            ((this.VALOR_CORRECAO==null && other.getVALOR_CORRECAO()==null) || 
             (this.VALOR_CORRECAO!=null &&
              this.VALOR_CORRECAO.equals(other.getVALOR_CORRECAO()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              this.CODIGO_BARRAS.equals(other.getCODIGO_BARRAS()))) &&
            ((this.NSU_PREFEITURA==null && other.getNSU_PREFEITURA()==null) || 
             (this.NSU_PREFEITURA!=null &&
              this.NSU_PREFEITURA.equals(other.getNSU_PREFEITURA()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getPREFEITURA() != null) {
            _hashCode += getPREFEITURA().hashCode();
        }
        if (getCONTRIBUINTE() != null) {
            _hashCode += getCONTRIBUINTE().hashCode();
        }
        if (getCODIGO_TRIBUTO() != null) {
            _hashCode += getCODIGO_TRIBUTO().hashCode();
        }
        if (getPORCENTAGEM_MULTA() != null) {
            _hashCode += getPORCENTAGEM_MULTA().hashCode();
        }
        if (getDATA_VENCIMENTO() != null) {
            _hashCode += getDATA_VENCIMENTO().hashCode();
        }
        if (getVALOR_TRIBUTO() != null) {
            _hashCode += getVALOR_TRIBUTO().hashCode();
        }
        if (getVALOR_MULTA() != null) {
            _hashCode += getVALOR_MULTA().hashCode();
        }
        if (getVALOR_MORA() != null) {
            _hashCode += getVALOR_MORA().hashCode();
        }
        if (getVALOR_CORRECAO() != null) {
            _hashCode += getVALOR_CORRECAO().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            _hashCode += getCODIGO_BARRAS().hashCode();
        }
        if (getNSU_PREFEITURA() != null) {
            _hashCode += getNSU_PREFEITURA().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_iss_taxas_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_iss_taxas_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PREFEITURA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PREFEITURA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "convenio_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTRIBUINTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTRIBUINTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "contribuinte_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_TRIBUTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_TRIBUTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PORCENTAGEM_MULTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PORCENTAGEM_MULTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_TRIBUTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_TRIBUTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_MULTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_MULTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_MORA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_MORA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_CORRECAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_CORRECAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU_PREFEITURA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU_PREFEITURA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
