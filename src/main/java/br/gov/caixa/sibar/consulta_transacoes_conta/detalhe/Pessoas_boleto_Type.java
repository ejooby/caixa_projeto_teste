/**
 * Pessoas_boleto_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Pessoas_boleto_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type ORIGINAL;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type FINAL;

    public Pessoas_boleto_Type() {
    }

    public Pessoas_boleto_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type ORIGINAL,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type FINAL) {
           this.ORIGINAL = ORIGINAL;
           this.FINAL = FINAL;
    }


    /**
     * Gets the ORIGINAL value for this Pessoas_boleto_Type.
     * 
     * @return ORIGINAL
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type getORIGINAL() {
        return ORIGINAL;
    }


    /**
     * Sets the ORIGINAL value for this Pessoas_boleto_Type.
     * 
     * @param ORIGINAL
     */
    public void setORIGINAL(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type ORIGINAL) {
        this.ORIGINAL = ORIGINAL;
    }


    /**
     * Gets the FINAL value for this Pessoas_boleto_Type.
     * 
     * @return FINAL
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type getFINAL() {
        return FINAL;
    }


    /**
     * Sets the FINAL value for this Pessoas_boleto_Type.
     * 
     * @param FINAL
     */
    public void setFINAL(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Pessoa_boleto_Type FINAL) {
        this.FINAL = FINAL;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Pessoas_boleto_Type)) return false;
        Pessoas_boleto_Type other = (Pessoas_boleto_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ORIGINAL==null && other.getORIGINAL()==null) || 
             (this.ORIGINAL!=null &&
              this.ORIGINAL.equals(other.getORIGINAL()))) &&
            ((this.FINAL==null && other.getFINAL()==null) || 
             (this.FINAL!=null &&
              this.FINAL.equals(other.getFINAL())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getORIGINAL() != null) {
            _hashCode += getORIGINAL().hashCode();
        }
        if (getFINAL() != null) {
            _hashCode += getFINAL().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Pessoas_boleto_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "pessoas_boleto_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ORIGINAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ORIGINAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "pessoa_boleto_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FINAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FINAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "pessoa_boleto_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
