/**
 * Dados_saida_transacao_mega_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.resumo;

public class Dados_saida_transacao_mega_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedLong NSU;

    private java.util.Date DATA_APOSTA;

    private java.util.Date DATA_CONCURSO;

    private java.math.BigDecimal VALOR;

    private org.apache.axis.types.UnsignedShort CONCURSO;

    public Dados_saida_transacao_mega_Type() {
    }

    public Dados_saida_transacao_mega_Type(
           org.apache.axis.types.UnsignedLong NSU,
           java.util.Date DATA_APOSTA,
           java.util.Date DATA_CONCURSO,
           java.math.BigDecimal VALOR,
           org.apache.axis.types.UnsignedShort CONCURSO) {
           this.NSU = NSU;
           this.DATA_APOSTA = DATA_APOSTA;
           this.DATA_CONCURSO = DATA_CONCURSO;
           this.VALOR = VALOR;
           this.CONCURSO = CONCURSO;
    }


    /**
     * Gets the NSU value for this Dados_saida_transacao_mega_Type.
     * 
     * @return NSU
     */
    public org.apache.axis.types.UnsignedLong getNSU() {
        return NSU;
    }


    /**
     * Sets the NSU value for this Dados_saida_transacao_mega_Type.
     * 
     * @param NSU
     */
    public void setNSU(org.apache.axis.types.UnsignedLong NSU) {
        this.NSU = NSU;
    }


    /**
     * Gets the DATA_APOSTA value for this Dados_saida_transacao_mega_Type.
     * 
     * @return DATA_APOSTA
     */
    public java.util.Date getDATA_APOSTA() {
        return DATA_APOSTA;
    }


    /**
     * Sets the DATA_APOSTA value for this Dados_saida_transacao_mega_Type.
     * 
     * @param DATA_APOSTA
     */
    public void setDATA_APOSTA(java.util.Date DATA_APOSTA) {
        this.DATA_APOSTA = DATA_APOSTA;
    }


    /**
     * Gets the DATA_CONCURSO value for this Dados_saida_transacao_mega_Type.
     * 
     * @return DATA_CONCURSO
     */
    public java.util.Date getDATA_CONCURSO() {
        return DATA_CONCURSO;
    }


    /**
     * Sets the DATA_CONCURSO value for this Dados_saida_transacao_mega_Type.
     * 
     * @param DATA_CONCURSO
     */
    public void setDATA_CONCURSO(java.util.Date DATA_CONCURSO) {
        this.DATA_CONCURSO = DATA_CONCURSO;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_mega_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_mega_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the CONCURSO value for this Dados_saida_transacao_mega_Type.
     * 
     * @return CONCURSO
     */
    public org.apache.axis.types.UnsignedShort getCONCURSO() {
        return CONCURSO;
    }


    /**
     * Sets the CONCURSO value for this Dados_saida_transacao_mega_Type.
     * 
     * @param CONCURSO
     */
    public void setCONCURSO(org.apache.axis.types.UnsignedShort CONCURSO) {
        this.CONCURSO = CONCURSO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_mega_Type)) return false;
        Dados_saida_transacao_mega_Type other = (Dados_saida_transacao_mega_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NSU==null && other.getNSU()==null) || 
             (this.NSU!=null &&
              this.NSU.equals(other.getNSU()))) &&
            ((this.DATA_APOSTA==null && other.getDATA_APOSTA()==null) || 
             (this.DATA_APOSTA!=null &&
              this.DATA_APOSTA.equals(other.getDATA_APOSTA()))) &&
            ((this.DATA_CONCURSO==null && other.getDATA_CONCURSO()==null) || 
             (this.DATA_CONCURSO!=null &&
              this.DATA_CONCURSO.equals(other.getDATA_CONCURSO()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.CONCURSO==null && other.getCONCURSO()==null) || 
             (this.CONCURSO!=null &&
              this.CONCURSO.equals(other.getCONCURSO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNSU() != null) {
            _hashCode += getNSU().hashCode();
        }
        if (getDATA_APOSTA() != null) {
            _hashCode += getDATA_APOSTA().hashCode();
        }
        if (getDATA_CONCURSO() != null) {
            _hashCode += getDATA_CONCURSO().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getCONCURSO() != null) {
            _hashCode += getCONCURSO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_mega_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_transacao_mega_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_APOSTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_APOSTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_CONCURSO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_CONCURSO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONCURSO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONCURSO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
