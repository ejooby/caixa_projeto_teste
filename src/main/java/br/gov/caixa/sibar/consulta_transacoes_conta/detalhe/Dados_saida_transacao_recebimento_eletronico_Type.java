/**
 * Dados_saida_transacao_recebimento_eletronico_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_recebimento_eletronico_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedByte SEGMENTO;

    private org.apache.axis.types.UnsignedByte ORIGEM;

    private org.apache.axis.types.UnsignedShort CONVENIO;

    private org.apache.axis.types.UnsignedLong CPF;

    private org.apache.axis.types.UnsignedLong CNPJ;

    private java.lang.String NOME_CONVENENTE;

    private org.apache.axis.types.UnsignedInt IDENTIFICADOR_CONVENIO;

    private org.apache.axis.types.UnsignedLong IDENTIFICADOR_CLIENTE;

    private java.math.BigDecimal SALDO_INICIAL;

    private java.math.BigDecimal VALOR_RECEBIDO;

    private java.math.BigDecimal SALDO_DISPONIVEL;

    private org.apache.axis.types.UnsignedLong NSU_CONVENENTE;

    private java.lang.String REFERENCIA;

    private java.lang.String INFORMACOES;

    private java.util.Date DATA_VALIDACAO;

    private java.lang.String IDENTIFICACAO;

    public Dados_saida_transacao_recebimento_eletronico_Type() {
    }

    public Dados_saida_transacao_recebimento_eletronico_Type(
           org.apache.axis.types.UnsignedByte SEGMENTO,
           org.apache.axis.types.UnsignedByte ORIGEM,
           org.apache.axis.types.UnsignedShort CONVENIO,
           org.apache.axis.types.UnsignedLong CPF,
           org.apache.axis.types.UnsignedLong CNPJ,
           java.lang.String NOME_CONVENENTE,
           org.apache.axis.types.UnsignedInt IDENTIFICADOR_CONVENIO,
           org.apache.axis.types.UnsignedLong IDENTIFICADOR_CLIENTE,
           java.math.BigDecimal SALDO_INICIAL,
           java.math.BigDecimal VALOR_RECEBIDO,
           java.math.BigDecimal SALDO_DISPONIVEL,
           org.apache.axis.types.UnsignedLong NSU_CONVENENTE,
           java.lang.String REFERENCIA,
           java.lang.String INFORMACOES,
           java.util.Date DATA_VALIDACAO,
           java.lang.String IDENTIFICACAO) {
           this.SEGMENTO = SEGMENTO;
           this.ORIGEM = ORIGEM;
           this.CONVENIO = CONVENIO;
           this.CPF = CPF;
           this.CNPJ = CNPJ;
           this.NOME_CONVENENTE = NOME_CONVENENTE;
           this.IDENTIFICADOR_CONVENIO = IDENTIFICADOR_CONVENIO;
           this.IDENTIFICADOR_CLIENTE = IDENTIFICADOR_CLIENTE;
           this.SALDO_INICIAL = SALDO_INICIAL;
           this.VALOR_RECEBIDO = VALOR_RECEBIDO;
           this.SALDO_DISPONIVEL = SALDO_DISPONIVEL;
           this.NSU_CONVENENTE = NSU_CONVENENTE;
           this.REFERENCIA = REFERENCIA;
           this.INFORMACOES = INFORMACOES;
           this.DATA_VALIDACAO = DATA_VALIDACAO;
           this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the SEGMENTO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return SEGMENTO
     */
    public org.apache.axis.types.UnsignedByte getSEGMENTO() {
        return SEGMENTO;
    }


    /**
     * Sets the SEGMENTO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param SEGMENTO
     */
    public void setSEGMENTO(org.apache.axis.types.UnsignedByte SEGMENTO) {
        this.SEGMENTO = SEGMENTO;
    }


    /**
     * Gets the ORIGEM value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return ORIGEM
     */
    public org.apache.axis.types.UnsignedByte getORIGEM() {
        return ORIGEM;
    }


    /**
     * Sets the ORIGEM value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param ORIGEM
     */
    public void setORIGEM(org.apache.axis.types.UnsignedByte ORIGEM) {
        this.ORIGEM = ORIGEM;
    }


    /**
     * Gets the CONVENIO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return CONVENIO
     */
    public org.apache.axis.types.UnsignedShort getCONVENIO() {
        return CONVENIO;
    }


    /**
     * Sets the CONVENIO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param CONVENIO
     */
    public void setCONVENIO(org.apache.axis.types.UnsignedShort CONVENIO) {
        this.CONVENIO = CONVENIO;
    }


    /**
     * Gets the CPF value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return CPF
     */
    public org.apache.axis.types.UnsignedLong getCPF() {
        return CPF;
    }


    /**
     * Sets the CPF value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param CPF
     */
    public void setCPF(org.apache.axis.types.UnsignedLong CPF) {
        this.CPF = CPF;
    }


    /**
     * Gets the CNPJ value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return CNPJ
     */
    public org.apache.axis.types.UnsignedLong getCNPJ() {
        return CNPJ;
    }


    /**
     * Sets the CNPJ value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param CNPJ
     */
    public void setCNPJ(org.apache.axis.types.UnsignedLong CNPJ) {
        this.CNPJ = CNPJ;
    }


    /**
     * Gets the NOME_CONVENENTE value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return NOME_CONVENENTE
     */
    public java.lang.String getNOME_CONVENENTE() {
        return NOME_CONVENENTE;
    }


    /**
     * Sets the NOME_CONVENENTE value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param NOME_CONVENENTE
     */
    public void setNOME_CONVENENTE(java.lang.String NOME_CONVENENTE) {
        this.NOME_CONVENENTE = NOME_CONVENENTE;
    }


    /**
     * Gets the IDENTIFICADOR_CONVENIO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return IDENTIFICADOR_CONVENIO
     */
    public org.apache.axis.types.UnsignedInt getIDENTIFICADOR_CONVENIO() {
        return IDENTIFICADOR_CONVENIO;
    }


    /**
     * Sets the IDENTIFICADOR_CONVENIO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param IDENTIFICADOR_CONVENIO
     */
    public void setIDENTIFICADOR_CONVENIO(org.apache.axis.types.UnsignedInt IDENTIFICADOR_CONVENIO) {
        this.IDENTIFICADOR_CONVENIO = IDENTIFICADOR_CONVENIO;
    }


    /**
     * Gets the IDENTIFICADOR_CLIENTE value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return IDENTIFICADOR_CLIENTE
     */
    public org.apache.axis.types.UnsignedLong getIDENTIFICADOR_CLIENTE() {
        return IDENTIFICADOR_CLIENTE;
    }


    /**
     * Sets the IDENTIFICADOR_CLIENTE value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param IDENTIFICADOR_CLIENTE
     */
    public void setIDENTIFICADOR_CLIENTE(org.apache.axis.types.UnsignedLong IDENTIFICADOR_CLIENTE) {
        this.IDENTIFICADOR_CLIENTE = IDENTIFICADOR_CLIENTE;
    }


    /**
     * Gets the SALDO_INICIAL value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return SALDO_INICIAL
     */
    public java.math.BigDecimal getSALDO_INICIAL() {
        return SALDO_INICIAL;
    }


    /**
     * Sets the SALDO_INICIAL value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param SALDO_INICIAL
     */
    public void setSALDO_INICIAL(java.math.BigDecimal SALDO_INICIAL) {
        this.SALDO_INICIAL = SALDO_INICIAL;
    }


    /**
     * Gets the VALOR_RECEBIDO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return VALOR_RECEBIDO
     */
    public java.math.BigDecimal getVALOR_RECEBIDO() {
        return VALOR_RECEBIDO;
    }


    /**
     * Sets the VALOR_RECEBIDO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param VALOR_RECEBIDO
     */
    public void setVALOR_RECEBIDO(java.math.BigDecimal VALOR_RECEBIDO) {
        this.VALOR_RECEBIDO = VALOR_RECEBIDO;
    }


    /**
     * Gets the SALDO_DISPONIVEL value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return SALDO_DISPONIVEL
     */
    public java.math.BigDecimal getSALDO_DISPONIVEL() {
        return SALDO_DISPONIVEL;
    }


    /**
     * Sets the SALDO_DISPONIVEL value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param SALDO_DISPONIVEL
     */
    public void setSALDO_DISPONIVEL(java.math.BigDecimal SALDO_DISPONIVEL) {
        this.SALDO_DISPONIVEL = SALDO_DISPONIVEL;
    }


    /**
     * Gets the NSU_CONVENENTE value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return NSU_CONVENENTE
     */
    public org.apache.axis.types.UnsignedLong getNSU_CONVENENTE() {
        return NSU_CONVENENTE;
    }


    /**
     * Sets the NSU_CONVENENTE value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param NSU_CONVENENTE
     */
    public void setNSU_CONVENENTE(org.apache.axis.types.UnsignedLong NSU_CONVENENTE) {
        this.NSU_CONVENENTE = NSU_CONVENENTE;
    }


    /**
     * Gets the REFERENCIA value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return REFERENCIA
     */
    public java.lang.String getREFERENCIA() {
        return REFERENCIA;
    }


    /**
     * Sets the REFERENCIA value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param REFERENCIA
     */
    public void setREFERENCIA(java.lang.String REFERENCIA) {
        this.REFERENCIA = REFERENCIA;
    }


    /**
     * Gets the INFORMACOES value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return INFORMACOES
     */
    public java.lang.String getINFORMACOES() {
        return INFORMACOES;
    }


    /**
     * Sets the INFORMACOES value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param INFORMACOES
     */
    public void setINFORMACOES(java.lang.String INFORMACOES) {
        this.INFORMACOES = INFORMACOES;
    }


    /**
     * Gets the DATA_VALIDACAO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return DATA_VALIDACAO
     */
    public java.util.Date getDATA_VALIDACAO() {
        return DATA_VALIDACAO;
    }


    /**
     * Sets the DATA_VALIDACAO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param DATA_VALIDACAO
     */
    public void setDATA_VALIDACAO(java.util.Date DATA_VALIDACAO) {
        this.DATA_VALIDACAO = DATA_VALIDACAO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_recebimento_eletronico_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_recebimento_eletronico_Type)) return false;
        Dados_saida_transacao_recebimento_eletronico_Type other = (Dados_saida_transacao_recebimento_eletronico_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.SEGMENTO==null && other.getSEGMENTO()==null) || 
             (this.SEGMENTO!=null &&
              this.SEGMENTO.equals(other.getSEGMENTO()))) &&
            ((this.ORIGEM==null && other.getORIGEM()==null) || 
             (this.ORIGEM!=null &&
              this.ORIGEM.equals(other.getORIGEM()))) &&
            ((this.CONVENIO==null && other.getCONVENIO()==null) || 
             (this.CONVENIO!=null &&
              this.CONVENIO.equals(other.getCONVENIO()))) &&
            ((this.CPF==null && other.getCPF()==null) || 
             (this.CPF!=null &&
              this.CPF.equals(other.getCPF()))) &&
            ((this.CNPJ==null && other.getCNPJ()==null) || 
             (this.CNPJ!=null &&
              this.CNPJ.equals(other.getCNPJ()))) &&
            ((this.NOME_CONVENENTE==null && other.getNOME_CONVENENTE()==null) || 
             (this.NOME_CONVENENTE!=null &&
              this.NOME_CONVENENTE.equals(other.getNOME_CONVENENTE()))) &&
            ((this.IDENTIFICADOR_CONVENIO==null && other.getIDENTIFICADOR_CONVENIO()==null) || 
             (this.IDENTIFICADOR_CONVENIO!=null &&
              this.IDENTIFICADOR_CONVENIO.equals(other.getIDENTIFICADOR_CONVENIO()))) &&
            ((this.IDENTIFICADOR_CLIENTE==null && other.getIDENTIFICADOR_CLIENTE()==null) || 
             (this.IDENTIFICADOR_CLIENTE!=null &&
              this.IDENTIFICADOR_CLIENTE.equals(other.getIDENTIFICADOR_CLIENTE()))) &&
            ((this.SALDO_INICIAL==null && other.getSALDO_INICIAL()==null) || 
             (this.SALDO_INICIAL!=null &&
              this.SALDO_INICIAL.equals(other.getSALDO_INICIAL()))) &&
            ((this.VALOR_RECEBIDO==null && other.getVALOR_RECEBIDO()==null) || 
             (this.VALOR_RECEBIDO!=null &&
              this.VALOR_RECEBIDO.equals(other.getVALOR_RECEBIDO()))) &&
            ((this.SALDO_DISPONIVEL==null && other.getSALDO_DISPONIVEL()==null) || 
             (this.SALDO_DISPONIVEL!=null &&
              this.SALDO_DISPONIVEL.equals(other.getSALDO_DISPONIVEL()))) &&
            ((this.NSU_CONVENENTE==null && other.getNSU_CONVENENTE()==null) || 
             (this.NSU_CONVENENTE!=null &&
              this.NSU_CONVENENTE.equals(other.getNSU_CONVENENTE()))) &&
            ((this.REFERENCIA==null && other.getREFERENCIA()==null) || 
             (this.REFERENCIA!=null &&
              this.REFERENCIA.equals(other.getREFERENCIA()))) &&
            ((this.INFORMACOES==null && other.getINFORMACOES()==null) || 
             (this.INFORMACOES!=null &&
              this.INFORMACOES.equals(other.getINFORMACOES()))) &&
            ((this.DATA_VALIDACAO==null && other.getDATA_VALIDACAO()==null) || 
             (this.DATA_VALIDACAO!=null &&
              this.DATA_VALIDACAO.equals(other.getDATA_VALIDACAO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSEGMENTO() != null) {
            _hashCode += getSEGMENTO().hashCode();
        }
        if (getORIGEM() != null) {
            _hashCode += getORIGEM().hashCode();
        }
        if (getCONVENIO() != null) {
            _hashCode += getCONVENIO().hashCode();
        }
        if (getCPF() != null) {
            _hashCode += getCPF().hashCode();
        }
        if (getCNPJ() != null) {
            _hashCode += getCNPJ().hashCode();
        }
        if (getNOME_CONVENENTE() != null) {
            _hashCode += getNOME_CONVENENTE().hashCode();
        }
        if (getIDENTIFICADOR_CONVENIO() != null) {
            _hashCode += getIDENTIFICADOR_CONVENIO().hashCode();
        }
        if (getIDENTIFICADOR_CLIENTE() != null) {
            _hashCode += getIDENTIFICADOR_CLIENTE().hashCode();
        }
        if (getSALDO_INICIAL() != null) {
            _hashCode += getSALDO_INICIAL().hashCode();
        }
        if (getVALOR_RECEBIDO() != null) {
            _hashCode += getVALOR_RECEBIDO().hashCode();
        }
        if (getSALDO_DISPONIVEL() != null) {
            _hashCode += getSALDO_DISPONIVEL().hashCode();
        }
        if (getNSU_CONVENENTE() != null) {
            _hashCode += getNSU_CONVENENTE().hashCode();
        }
        if (getREFERENCIA() != null) {
            _hashCode += getREFERENCIA().hashCode();
        }
        if (getINFORMACOES() != null) {
            _hashCode += getINFORMACOES().hashCode();
        }
        if (getDATA_VALIDACAO() != null) {
            _hashCode += getDATA_VALIDACAO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_recebimento_eletronico_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_recebimento_eletronico_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ORIGEM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ORIGEM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONVENIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONVENIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_CONVENENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_CONVENENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICADOR_CONVENIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICADOR_CONVENIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICADOR_CLIENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICADOR_CLIENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SALDO_INICIAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SALDO_INICIAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_RECEBIDO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_RECEBIDO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SALDO_DISPONIVEL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SALDO_DISPONIVEL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU_CONVENENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU_CONVENENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REFERENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "REFERENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("INFORMACOES");
        elemField.setXmlName(new javax.xml.namespace.QName("", "INFORMACOES"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VALIDACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VALIDACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
