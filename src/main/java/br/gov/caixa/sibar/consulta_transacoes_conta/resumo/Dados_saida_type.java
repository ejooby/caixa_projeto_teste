/**
 * Dados_saida_type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.resumo;

public class Dados_saida_type  extends br.gov.caixa.sibar.DADOS_SAIDA_TYPE  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Conta_Type CONTA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type IPVA_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DPVAT_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type MULTAS_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type LICENCIAMENTO_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type SEGUNDA_VIA_LICENCIAMENTO_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type PRIMEIRO_REGISTRO_ESTADO_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DEBITOS_PENDENTES_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TRANSFERENCIAS_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type SEGUNDA_VIA_TRANSFERENCIA_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type PEC_GENERICO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type MULTAS_RENAINF_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type BOLETO_DDA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DARF_CODIGO_BARRAS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_mega_Type MEGASENA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type RECEBIMENTO_ELETRONICO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type CARTEIRA_ELETRONICA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DAED;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TAXAS_VEICULOS_MG;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type IPVA_RS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DPVAT_RS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type MULTAS_RS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type LICENCIAMENTO_RS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type PAGAMENTO_UNIFICADO_RS;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type IPVA_MG;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DPVAT_MG;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type LICENCIAMENTO_MG;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TAXAS_DETRAN_SP;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type HABITACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type BOLETO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TAXAS_CONDUTOR_MG;
    
    private br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type CONCESSIONARIA;
    

    public Dados_saida_type() {
    }

    public Dados_saida_type(
           br.gov.caixa.sibar.CONTROLE_NEGOCIAL_TYPE[] CONTROLE_NEGOCIAL,
           java.lang.String EXCECAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Conta_Type CONTA,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type IPVA_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DPVAT_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type MULTAS_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type LICENCIAMENTO_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type SEGUNDA_VIA_LICENCIAMENTO_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type PRIMEIRO_REGISTRO_ESTADO_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DEBITOS_PENDENTES_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TRANSFERENCIAS_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type SEGUNDA_VIA_TRANSFERENCIA_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type PEC_GENERICO,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type MULTAS_RENAINF_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type BOLETO_DDA,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DARF_CODIGO_BARRAS,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_mega_Type MEGASENA,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type RECEBIMENTO_ELETRONICO,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type CARTEIRA_ELETRONICA,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DAED,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TAXAS_VEICULOS_MG,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type IPVA_RS,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DPVAT_RS,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type MULTAS_RS,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type LICENCIAMENTO_RS,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type PAGAMENTO_UNIFICADO_RS,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type IPVA_MG,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DPVAT_MG,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type LICENCIAMENTO_MG,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TAXAS_DETRAN_SP,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type HABITACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type BOLETO,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TAXAS_CONDUTOR_MG,
           br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type CONCESSIONARIA) {
        super(
            CONTROLE_NEGOCIAL,
            EXCECAO);
        this.CONTA = CONTA;
        this.IPVA_SP = IPVA_SP;
        this.DPVAT_SP = DPVAT_SP;
        this.MULTAS_SP = MULTAS_SP;
        this.LICENCIAMENTO_SP = LICENCIAMENTO_SP;
        this.SEGUNDA_VIA_LICENCIAMENTO_SP = SEGUNDA_VIA_LICENCIAMENTO_SP;
        this.PRIMEIRO_REGISTRO_ESTADO_SP = PRIMEIRO_REGISTRO_ESTADO_SP;
        this.DEBITOS_PENDENTES_SP = DEBITOS_PENDENTES_SP;
        this.TRANSFERENCIAS_SP = TRANSFERENCIAS_SP;
        this.SEGUNDA_VIA_TRANSFERENCIA_SP = SEGUNDA_VIA_TRANSFERENCIA_SP;
        this.PEC_GENERICO = PEC_GENERICO;
        this.MULTAS_RENAINF_SP = MULTAS_RENAINF_SP;
        this.BOLETO_DDA = BOLETO_DDA;
        this.DARF_CODIGO_BARRAS = DARF_CODIGO_BARRAS;
        this.MEGASENA = MEGASENA;
        this.RECEBIMENTO_ELETRONICO = RECEBIMENTO_ELETRONICO;
        this.CARTEIRA_ELETRONICA = CARTEIRA_ELETRONICA;
        this.DAED = DAED;
        this.TAXAS_VEICULOS_MG = TAXAS_VEICULOS_MG;
        this.IPVA_RS = IPVA_RS;
        this.DPVAT_RS = DPVAT_RS;
        this.MULTAS_RS = MULTAS_RS;
        this.LICENCIAMENTO_RS = LICENCIAMENTO_RS;
        this.PAGAMENTO_UNIFICADO_RS = PAGAMENTO_UNIFICADO_RS;
        this.IPVA_MG = IPVA_MG;
        this.DPVAT_MG = DPVAT_MG;
        this.LICENCIAMENTO_MG = LICENCIAMENTO_MG;
        this.TAXAS_DETRAN_SP = TAXAS_DETRAN_SP;
        this.HABITACAO = HABITACAO;
        this.BOLETO = BOLETO;
        this.TAXAS_CONDUTOR_MG = TAXAS_CONDUTOR_MG;
        this.CONCESSIONARIA = CONCESSIONARIA;
    }


    /**
     * Gets the CONTA value for this Dados_saida_type.
     * 
     * @return CONTA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Conta_Type getCONTA() {
        return CONTA;
    }


    /**
     * Sets the CONTA value for this Dados_saida_type.
     * 
     * @param CONTA
     */
    public void setCONTA(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Conta_Type CONTA) {
        this.CONTA = CONTA;
    }


    /**
     * Gets the IPVA_SP value for this Dados_saida_type.
     * 
     * @return IPVA_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getIPVA_SP() {
        return IPVA_SP;
    }


    /**
     * Sets the IPVA_SP value for this Dados_saida_type.
     * 
     * @param IPVA_SP
     */
    public void setIPVA_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type IPVA_SP) {
        this.IPVA_SP = IPVA_SP;
    }


    /**
     * Gets the DPVAT_SP value for this Dados_saida_type.
     * 
     * @return DPVAT_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getDPVAT_SP() {
        return DPVAT_SP;
    }


    /**
     * Sets the DPVAT_SP value for this Dados_saida_type.
     * 
     * @param DPVAT_SP
     */
    public void setDPVAT_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DPVAT_SP) {
        this.DPVAT_SP = DPVAT_SP;
    }


    /**
     * Gets the MULTAS_SP value for this Dados_saida_type.
     * 
     * @return MULTAS_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getMULTAS_SP() {
        return MULTAS_SP;
    }


    /**
     * Sets the MULTAS_SP value for this Dados_saida_type.
     * 
     * @param MULTAS_SP
     */
    public void setMULTAS_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type MULTAS_SP) {
        this.MULTAS_SP = MULTAS_SP;
    }


    /**
     * Gets the LICENCIAMENTO_SP value for this Dados_saida_type.
     * 
     * @return LICENCIAMENTO_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getLICENCIAMENTO_SP() {
        return LICENCIAMENTO_SP;
    }


    /**
     * Sets the LICENCIAMENTO_SP value for this Dados_saida_type.
     * 
     * @param LICENCIAMENTO_SP
     */
    public void setLICENCIAMENTO_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type LICENCIAMENTO_SP) {
        this.LICENCIAMENTO_SP = LICENCIAMENTO_SP;
    }


    /**
     * Gets the SEGUNDA_VIA_LICENCIAMENTO_SP value for this Dados_saida_type.
     * 
     * @return SEGUNDA_VIA_LICENCIAMENTO_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getSEGUNDA_VIA_LICENCIAMENTO_SP() {
        return SEGUNDA_VIA_LICENCIAMENTO_SP;
    }


    /**
     * Sets the SEGUNDA_VIA_LICENCIAMENTO_SP value for this Dados_saida_type.
     * 
     * @param SEGUNDA_VIA_LICENCIAMENTO_SP
     */
    public void setSEGUNDA_VIA_LICENCIAMENTO_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type SEGUNDA_VIA_LICENCIAMENTO_SP) {
        this.SEGUNDA_VIA_LICENCIAMENTO_SP = SEGUNDA_VIA_LICENCIAMENTO_SP;
    }


    /**
     * Gets the PRIMEIRO_REGISTRO_ESTADO_SP value for this Dados_saida_type.
     * 
     * @return PRIMEIRO_REGISTRO_ESTADO_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getPRIMEIRO_REGISTRO_ESTADO_SP() {
        return PRIMEIRO_REGISTRO_ESTADO_SP;
    }


    /**
     * Sets the PRIMEIRO_REGISTRO_ESTADO_SP value for this Dados_saida_type.
     * 
     * @param PRIMEIRO_REGISTRO_ESTADO_SP
     */
    public void setPRIMEIRO_REGISTRO_ESTADO_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type PRIMEIRO_REGISTRO_ESTADO_SP) {
        this.PRIMEIRO_REGISTRO_ESTADO_SP = PRIMEIRO_REGISTRO_ESTADO_SP;
    }


    /**
     * Gets the DEBITOS_PENDENTES_SP value for this Dados_saida_type.
     * 
     * @return DEBITOS_PENDENTES_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getDEBITOS_PENDENTES_SP() {
        return DEBITOS_PENDENTES_SP;
    }


    /**
     * Sets the DEBITOS_PENDENTES_SP value for this Dados_saida_type.
     * 
     * @param DEBITOS_PENDENTES_SP
     */
    public void setDEBITOS_PENDENTES_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DEBITOS_PENDENTES_SP) {
        this.DEBITOS_PENDENTES_SP = DEBITOS_PENDENTES_SP;
    }


    /**
     * Gets the TRANSFERENCIAS_SP value for this Dados_saida_type.
     * 
     * @return TRANSFERENCIAS_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getTRANSFERENCIAS_SP() {
        return TRANSFERENCIAS_SP;
    }


    /**
     * Sets the TRANSFERENCIAS_SP value for this Dados_saida_type.
     * 
     * @param TRANSFERENCIAS_SP
     */
    public void setTRANSFERENCIAS_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TRANSFERENCIAS_SP) {
        this.TRANSFERENCIAS_SP = TRANSFERENCIAS_SP;
    }


    /**
     * Gets the SEGUNDA_VIA_TRANSFERENCIA_SP value for this Dados_saida_type.
     * 
     * @return SEGUNDA_VIA_TRANSFERENCIA_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getSEGUNDA_VIA_TRANSFERENCIA_SP() {
        return SEGUNDA_VIA_TRANSFERENCIA_SP;
    }


    /**
     * Sets the SEGUNDA_VIA_TRANSFERENCIA_SP value for this Dados_saida_type.
     * 
     * @param SEGUNDA_VIA_TRANSFERENCIA_SP
     */
    public void setSEGUNDA_VIA_TRANSFERENCIA_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type SEGUNDA_VIA_TRANSFERENCIA_SP) {
        this.SEGUNDA_VIA_TRANSFERENCIA_SP = SEGUNDA_VIA_TRANSFERENCIA_SP;
    }


    /**
     * Gets the PEC_GENERICO value for this Dados_saida_type.
     * 
     * @return PEC_GENERICO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getPEC_GENERICO() {
        return PEC_GENERICO;
    }


    /**
     * Sets the PEC_GENERICO value for this Dados_saida_type.
     * 
     * @param PEC_GENERICO
     */
    public void setPEC_GENERICO(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type PEC_GENERICO) {
        this.PEC_GENERICO = PEC_GENERICO;
    }


    /**
     * Gets the MULTAS_RENAINF_SP value for this Dados_saida_type.
     * 
     * @return MULTAS_RENAINF_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getMULTAS_RENAINF_SP() {
        return MULTAS_RENAINF_SP;
    }


    /**
     * Sets the MULTAS_RENAINF_SP value for this Dados_saida_type.
     * 
     * @param MULTAS_RENAINF_SP
     */
    public void setMULTAS_RENAINF_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type MULTAS_RENAINF_SP) {
        this.MULTAS_RENAINF_SP = MULTAS_RENAINF_SP;
    }


    /**
     * Gets the BOLETO_DDA value for this Dados_saida_type.
     * 
     * @return BOLETO_DDA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getBOLETO_DDA() {
        return BOLETO_DDA;
    }


    /**
     * Sets the BOLETO_DDA value for this Dados_saida_type.
     * 
     * @param BOLETO_DDA
     */
    public void setBOLETO_DDA(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type BOLETO_DDA) {
        this.BOLETO_DDA = BOLETO_DDA;
    }


    /**
     * Gets the DARF_CODIGO_BARRAS value for this Dados_saida_type.
     * 
     * @return DARF_CODIGO_BARRAS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getDARF_CODIGO_BARRAS() {
        return DARF_CODIGO_BARRAS;
    }


    /**
     * Sets the DARF_CODIGO_BARRAS value for this Dados_saida_type.
     * 
     * @param DARF_CODIGO_BARRAS
     */
    public void setDARF_CODIGO_BARRAS(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DARF_CODIGO_BARRAS) {
        this.DARF_CODIGO_BARRAS = DARF_CODIGO_BARRAS;
    }


    /**
     * Gets the MEGASENA value for this Dados_saida_type.
     * 
     * @return MEGASENA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_mega_Type getMEGASENA() {
        return MEGASENA;
    }


    /**
     * Sets the MEGASENA value for this Dados_saida_type.
     * 
     * @param MEGASENA
     */
    public void setMEGASENA(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_mega_Type MEGASENA) {
        this.MEGASENA = MEGASENA;
    }


    /**
     * Gets the RECEBIMENTO_ELETRONICO value for this Dados_saida_type.
     * 
     * @return RECEBIMENTO_ELETRONICO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getRECEBIMENTO_ELETRONICO() {
        return RECEBIMENTO_ELETRONICO;
    }


    /**
     * Sets the RECEBIMENTO_ELETRONICO value for this Dados_saida_type.
     * 
     * @param RECEBIMENTO_ELETRONICO
     */
    public void setRECEBIMENTO_ELETRONICO(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type RECEBIMENTO_ELETRONICO) {
        this.RECEBIMENTO_ELETRONICO = RECEBIMENTO_ELETRONICO;
    }


    /**
     * Gets the CARTEIRA_ELETRONICA value for this Dados_saida_type.
     * 
     * @return CARTEIRA_ELETRONICA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getCARTEIRA_ELETRONICA() {
        return CARTEIRA_ELETRONICA;
    }


    /**
     * Sets the CARTEIRA_ELETRONICA value for this Dados_saida_type.
     * 
     * @param CARTEIRA_ELETRONICA
     */
    public void setCARTEIRA_ELETRONICA(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type CARTEIRA_ELETRONICA) {
        this.CARTEIRA_ELETRONICA = CARTEIRA_ELETRONICA;
    }


    /**
     * Gets the DAED value for this Dados_saida_type.
     * 
     * @return DAED
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getDAED() {
        return DAED;
    }


    /**
     * Sets the DAED value for this Dados_saida_type.
     * 
     * @param DAED
     */
    public void setDAED(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DAED) {
        this.DAED = DAED;
    }


    /**
     * Gets the TAXAS_VEICULOS_MG value for this Dados_saida_type.
     * 
     * @return TAXAS_VEICULOS_MG
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getTAXAS_VEICULOS_MG() {
        return TAXAS_VEICULOS_MG;
    }


    /**
     * Sets the TAXAS_VEICULOS_MG value for this Dados_saida_type.
     * 
     * @param TAXAS_VEICULOS_MG
     */
    public void setTAXAS_VEICULOS_MG(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TAXAS_VEICULOS_MG) {
        this.TAXAS_VEICULOS_MG = TAXAS_VEICULOS_MG;
    }


    /**
     * Gets the IPVA_RS value for this Dados_saida_type.
     * 
     * @return IPVA_RS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getIPVA_RS() {
        return IPVA_RS;
    }


    /**
     * Sets the IPVA_RS value for this Dados_saida_type.
     * 
     * @param IPVA_RS
     */
    public void setIPVA_RS(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type IPVA_RS) {
        this.IPVA_RS = IPVA_RS;
    }


    /**
     * Gets the DPVAT_RS value for this Dados_saida_type.
     * 
     * @return DPVAT_RS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getDPVAT_RS() {
        return DPVAT_RS;
    }


    /**
     * Sets the DPVAT_RS value for this Dados_saida_type.
     * 
     * @param DPVAT_RS
     */
    public void setDPVAT_RS(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DPVAT_RS) {
        this.DPVAT_RS = DPVAT_RS;
    }


    /**
     * Gets the MULTAS_RS value for this Dados_saida_type.
     * 
     * @return MULTAS_RS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getMULTAS_RS() {
        return MULTAS_RS;
    }


    /**
     * Sets the MULTAS_RS value for this Dados_saida_type.
     * 
     * @param MULTAS_RS
     */
    public void setMULTAS_RS(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type MULTAS_RS) {
        this.MULTAS_RS = MULTAS_RS;
    }


    /**
     * Gets the LICENCIAMENTO_RS value for this Dados_saida_type.
     * 
     * @return LICENCIAMENTO_RS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getLICENCIAMENTO_RS() {
        return LICENCIAMENTO_RS;
    }


    /**
     * Sets the LICENCIAMENTO_RS value for this Dados_saida_type.
     * 
     * @param LICENCIAMENTO_RS
     */
    public void setLICENCIAMENTO_RS(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type LICENCIAMENTO_RS) {
        this.LICENCIAMENTO_RS = LICENCIAMENTO_RS;
    }


    /**
     * Gets the PAGAMENTO_UNIFICADO_RS value for this Dados_saida_type.
     * 
     * @return PAGAMENTO_UNIFICADO_RS
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getPAGAMENTO_UNIFICADO_RS() {
        return PAGAMENTO_UNIFICADO_RS;
    }


    /**
     * Sets the PAGAMENTO_UNIFICADO_RS value for this Dados_saida_type.
     * 
     * @param PAGAMENTO_UNIFICADO_RS
     */
    public void setPAGAMENTO_UNIFICADO_RS(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type PAGAMENTO_UNIFICADO_RS) {
        this.PAGAMENTO_UNIFICADO_RS = PAGAMENTO_UNIFICADO_RS;
    }


    /**
     * Gets the IPVA_MG value for this Dados_saida_type.
     * 
     * @return IPVA_MG
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getIPVA_MG() {
        return IPVA_MG;
    }


    /**
     * Sets the IPVA_MG value for this Dados_saida_type.
     * 
     * @param IPVA_MG
     */
    public void setIPVA_MG(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type IPVA_MG) {
        this.IPVA_MG = IPVA_MG;
    }


    /**
     * Gets the DPVAT_MG value for this Dados_saida_type.
     * 
     * @return DPVAT_MG
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getDPVAT_MG() {
        return DPVAT_MG;
    }


    /**
     * Sets the DPVAT_MG value for this Dados_saida_type.
     * 
     * @param DPVAT_MG
     */
    public void setDPVAT_MG(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type DPVAT_MG) {
        this.DPVAT_MG = DPVAT_MG;
    }


    /**
     * Gets the LICENCIAMENTO_MG value for this Dados_saida_type.
     * 
     * @return LICENCIAMENTO_MG
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getLICENCIAMENTO_MG() {
        return LICENCIAMENTO_MG;
    }


    /**
     * Sets the LICENCIAMENTO_MG value for this Dados_saida_type.
     * 
     * @param LICENCIAMENTO_MG
     */
    public void setLICENCIAMENTO_MG(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type LICENCIAMENTO_MG) {
        this.LICENCIAMENTO_MG = LICENCIAMENTO_MG;
    }


    /**
     * Gets the TAXAS_DETRAN_SP value for this Dados_saida_type.
     * 
     * @return TAXAS_DETRAN_SP
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getTAXAS_DETRAN_SP() {
        return TAXAS_DETRAN_SP;
    }


    /**
     * Sets the TAXAS_DETRAN_SP value for this Dados_saida_type.
     * 
     * @param TAXAS_DETRAN_SP
     */
    public void setTAXAS_DETRAN_SP(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TAXAS_DETRAN_SP) {
        this.TAXAS_DETRAN_SP = TAXAS_DETRAN_SP;
    }


    /**
     * Gets the HABITACAO value for this Dados_saida_type.
     * 
     * @return HABITACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getHABITACAO() {
        return HABITACAO;
    }


    /**
     * Sets the HABITACAO value for this Dados_saida_type.
     * 
     * @param HABITACAO
     */
    public void setHABITACAO(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type HABITACAO) {
        this.HABITACAO = HABITACAO;
    }


    /**
     * Gets the BOLETO value for this Dados_saida_type.
     * 
     * @return BOLETO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getBOLETO() {
        return BOLETO;
    }


    /**
     * Sets the BOLETO value for this Dados_saida_type.
     * 
     * @param BOLETO
     */
    public void setBOLETO(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type BOLETO) {
        this.BOLETO = BOLETO;
    }


    /**
     * Gets the TAXAS_CONDUTOR_MG value for this Dados_saida_type.
     * 
     * @return TAXAS_CONDUTOR_MG
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getTAXAS_CONDUTOR_MG() {
        return TAXAS_CONDUTOR_MG;
    }


    /**
     * Sets the TAXAS_CONDUTOR_MG value for this Dados_saida_type.
     * 
     * @param TAXAS_CONDUTOR_MG
     */
    public void setTAXAS_CONDUTOR_MG(br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type TAXAS_CONDUTOR_MG) {
        this.TAXAS_CONDUTOR_MG = TAXAS_CONDUTOR_MG;
    }
    

    public br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type getCONCESSIONARIA() {
		return CONCESSIONARIA;
	}

	public void setCONCESSIONARIA(
			br.gov.caixa.sibar.consulta_transacoes_conta.resumo.Dados_saida_geral_Type cONCESSIONARIA) {
		CONCESSIONARIA = cONCESSIONARIA;
	}


	private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_type)) return false;
        Dados_saida_type other = (Dados_saida_type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.CONTA==null && other.getCONTA()==null) || 
             (this.CONTA!=null &&
              this.CONTA.equals(other.getCONTA()))) &&
            ((this.IPVA_SP==null && other.getIPVA_SP()==null) || 
             (this.IPVA_SP!=null &&
              this.IPVA_SP.equals(other.getIPVA_SP()))) &&
            ((this.DPVAT_SP==null && other.getDPVAT_SP()==null) || 
             (this.DPVAT_SP!=null &&
              this.DPVAT_SP.equals(other.getDPVAT_SP()))) &&
            ((this.MULTAS_SP==null && other.getMULTAS_SP()==null) || 
             (this.MULTAS_SP!=null &&
              this.MULTAS_SP.equals(other.getMULTAS_SP()))) &&
            ((this.LICENCIAMENTO_SP==null && other.getLICENCIAMENTO_SP()==null) || 
             (this.LICENCIAMENTO_SP!=null &&
              this.LICENCIAMENTO_SP.equals(other.getLICENCIAMENTO_SP()))) &&
            ((this.SEGUNDA_VIA_LICENCIAMENTO_SP==null && other.getSEGUNDA_VIA_LICENCIAMENTO_SP()==null) || 
             (this.SEGUNDA_VIA_LICENCIAMENTO_SP!=null &&
              this.SEGUNDA_VIA_LICENCIAMENTO_SP.equals(other.getSEGUNDA_VIA_LICENCIAMENTO_SP()))) &&
            ((this.PRIMEIRO_REGISTRO_ESTADO_SP==null && other.getPRIMEIRO_REGISTRO_ESTADO_SP()==null) || 
             (this.PRIMEIRO_REGISTRO_ESTADO_SP!=null &&
              this.PRIMEIRO_REGISTRO_ESTADO_SP.equals(other.getPRIMEIRO_REGISTRO_ESTADO_SP()))) &&
            ((this.DEBITOS_PENDENTES_SP==null && other.getDEBITOS_PENDENTES_SP()==null) || 
             (this.DEBITOS_PENDENTES_SP!=null &&
              this.DEBITOS_PENDENTES_SP.equals(other.getDEBITOS_PENDENTES_SP()))) &&
            ((this.TRANSFERENCIAS_SP==null && other.getTRANSFERENCIAS_SP()==null) || 
             (this.TRANSFERENCIAS_SP!=null &&
              this.TRANSFERENCIAS_SP.equals(other.getTRANSFERENCIAS_SP()))) &&
            ((this.SEGUNDA_VIA_TRANSFERENCIA_SP==null && other.getSEGUNDA_VIA_TRANSFERENCIA_SP()==null) || 
             (this.SEGUNDA_VIA_TRANSFERENCIA_SP!=null &&
              this.SEGUNDA_VIA_TRANSFERENCIA_SP.equals(other.getSEGUNDA_VIA_TRANSFERENCIA_SP()))) &&
            ((this.PEC_GENERICO==null && other.getPEC_GENERICO()==null) || 
             (this.PEC_GENERICO!=null &&
              this.PEC_GENERICO.equals(other.getPEC_GENERICO()))) &&
            ((this.MULTAS_RENAINF_SP==null && other.getMULTAS_RENAINF_SP()==null) || 
             (this.MULTAS_RENAINF_SP!=null &&
              this.MULTAS_RENAINF_SP.equals(other.getMULTAS_RENAINF_SP()))) &&
            ((this.BOLETO_DDA==null && other.getBOLETO_DDA()==null) || 
             (this.BOLETO_DDA!=null &&
              this.BOLETO_DDA.equals(other.getBOLETO_DDA()))) &&
            ((this.DARF_CODIGO_BARRAS==null && other.getDARF_CODIGO_BARRAS()==null) || 
             (this.DARF_CODIGO_BARRAS!=null &&
              this.DARF_CODIGO_BARRAS.equals(other.getDARF_CODIGO_BARRAS()))) &&
            ((this.MEGASENA==null && other.getMEGASENA()==null) || 
             (this.MEGASENA!=null &&
              this.MEGASENA.equals(other.getMEGASENA()))) &&
            ((this.RECEBIMENTO_ELETRONICO==null && other.getRECEBIMENTO_ELETRONICO()==null) || 
             (this.RECEBIMENTO_ELETRONICO!=null &&
              this.RECEBIMENTO_ELETRONICO.equals(other.getRECEBIMENTO_ELETRONICO()))) &&
            ((this.CARTEIRA_ELETRONICA==null && other.getCARTEIRA_ELETRONICA()==null) || 
             (this.CARTEIRA_ELETRONICA!=null &&
              this.CARTEIRA_ELETRONICA.equals(other.getCARTEIRA_ELETRONICA()))) &&
            ((this.DAED==null && other.getDAED()==null) || 
             (this.DAED!=null &&
              this.DAED.equals(other.getDAED()))) &&
            ((this.TAXAS_VEICULOS_MG==null && other.getTAXAS_VEICULOS_MG()==null) || 
             (this.TAXAS_VEICULOS_MG!=null &&
              this.TAXAS_VEICULOS_MG.equals(other.getTAXAS_VEICULOS_MG()))) &&
            ((this.IPVA_RS==null && other.getIPVA_RS()==null) || 
             (this.IPVA_RS!=null &&
              this.IPVA_RS.equals(other.getIPVA_RS()))) &&
            ((this.DPVAT_RS==null && other.getDPVAT_RS()==null) || 
             (this.DPVAT_RS!=null &&
              this.DPVAT_RS.equals(other.getDPVAT_RS()))) &&
            ((this.MULTAS_RS==null && other.getMULTAS_RS()==null) || 
             (this.MULTAS_RS!=null &&
              this.MULTAS_RS.equals(other.getMULTAS_RS()))) &&
            ((this.LICENCIAMENTO_RS==null && other.getLICENCIAMENTO_RS()==null) || 
             (this.LICENCIAMENTO_RS!=null &&
              this.LICENCIAMENTO_RS.equals(other.getLICENCIAMENTO_RS()))) &&
            ((this.PAGAMENTO_UNIFICADO_RS==null && other.getPAGAMENTO_UNIFICADO_RS()==null) || 
             (this.PAGAMENTO_UNIFICADO_RS!=null &&
              this.PAGAMENTO_UNIFICADO_RS.equals(other.getPAGAMENTO_UNIFICADO_RS()))) &&
            ((this.IPVA_MG==null && other.getIPVA_MG()==null) || 
             (this.IPVA_MG!=null &&
              this.IPVA_MG.equals(other.getIPVA_MG()))) &&
            ((this.DPVAT_MG==null && other.getDPVAT_MG()==null) || 
             (this.DPVAT_MG!=null &&
              this.DPVAT_MG.equals(other.getDPVAT_MG()))) &&
            ((this.LICENCIAMENTO_MG==null && other.getLICENCIAMENTO_MG()==null) || 
             (this.LICENCIAMENTO_MG!=null &&
              this.LICENCIAMENTO_MG.equals(other.getLICENCIAMENTO_MG()))) &&
            ((this.TAXAS_DETRAN_SP==null && other.getTAXAS_DETRAN_SP()==null) || 
             (this.TAXAS_DETRAN_SP!=null &&
              this.TAXAS_DETRAN_SP.equals(other.getTAXAS_DETRAN_SP()))) &&
            ((this.HABITACAO==null && other.getHABITACAO()==null) || 
             (this.HABITACAO!=null &&
              this.HABITACAO.equals(other.getHABITACAO()))) &&
            ((this.BOLETO==null && other.getBOLETO()==null) || 
             (this.BOLETO!=null &&
              this.BOLETO.equals(other.getBOLETO()))) &&
            ((this.TAXAS_CONDUTOR_MG==null && other.getTAXAS_CONDUTOR_MG()==null) || 
             (this.TAXAS_CONDUTOR_MG!=null &&
              this.CONCESSIONARIA.equals(other.getCONCESSIONARIA()))) &&
            ((this.CONCESSIONARIA==null && other.getCONCESSIONARIA()==null) || 
                    (this.CONCESSIONARIA!=null &&
                     this.CONCESSIONARIA.equals(other.getCONCESSIONARIA())));
        
        
        
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCONTA() != null) {
            _hashCode += getCONTA().hashCode();
        }
        if (getIPVA_SP() != null) {
            _hashCode += getIPVA_SP().hashCode();
        }
        if (getDPVAT_SP() != null) {
            _hashCode += getDPVAT_SP().hashCode();
        }
        if (getMULTAS_SP() != null) {
            _hashCode += getMULTAS_SP().hashCode();
        }
        if (getLICENCIAMENTO_SP() != null) {
            _hashCode += getLICENCIAMENTO_SP().hashCode();
        }
        if (getSEGUNDA_VIA_LICENCIAMENTO_SP() != null) {
            _hashCode += getSEGUNDA_VIA_LICENCIAMENTO_SP().hashCode();
        }
        if (getPRIMEIRO_REGISTRO_ESTADO_SP() != null) {
            _hashCode += getPRIMEIRO_REGISTRO_ESTADO_SP().hashCode();
        }
        if (getDEBITOS_PENDENTES_SP() != null) {
            _hashCode += getDEBITOS_PENDENTES_SP().hashCode();
        }
        if (getTRANSFERENCIAS_SP() != null) {
            _hashCode += getTRANSFERENCIAS_SP().hashCode();
        }
        if (getSEGUNDA_VIA_TRANSFERENCIA_SP() != null) {
            _hashCode += getSEGUNDA_VIA_TRANSFERENCIA_SP().hashCode();
        }
        if (getPEC_GENERICO() != null) {
            _hashCode += getPEC_GENERICO().hashCode();
        }
        if (getMULTAS_RENAINF_SP() != null) {
            _hashCode += getMULTAS_RENAINF_SP().hashCode();
        }
        if (getBOLETO_DDA() != null) {
            _hashCode += getBOLETO_DDA().hashCode();
        }
        if (getDARF_CODIGO_BARRAS() != null) {
            _hashCode += getDARF_CODIGO_BARRAS().hashCode();
        }
        if (getMEGASENA() != null) {
            _hashCode += getMEGASENA().hashCode();
        }
        if (getRECEBIMENTO_ELETRONICO() != null) {
            _hashCode += getRECEBIMENTO_ELETRONICO().hashCode();
        }
        if (getCARTEIRA_ELETRONICA() != null) {
            _hashCode += getCARTEIRA_ELETRONICA().hashCode();
        }
        if (getDAED() != null) {
            _hashCode += getDAED().hashCode();
        }
        if (getTAXAS_VEICULOS_MG() != null) {
            _hashCode += getTAXAS_VEICULOS_MG().hashCode();
        }
        if (getIPVA_RS() != null) {
            _hashCode += getIPVA_RS().hashCode();
        }
        if (getDPVAT_RS() != null) {
            _hashCode += getDPVAT_RS().hashCode();
        }
        if (getMULTAS_RS() != null) {
            _hashCode += getMULTAS_RS().hashCode();
        }
        if (getLICENCIAMENTO_RS() != null) {
            _hashCode += getLICENCIAMENTO_RS().hashCode();
        }
        if (getPAGAMENTO_UNIFICADO_RS() != null) {
            _hashCode += getPAGAMENTO_UNIFICADO_RS().hashCode();
        }
        if (getIPVA_MG() != null) {
            _hashCode += getIPVA_MG().hashCode();
        }
        if (getDPVAT_MG() != null) {
            _hashCode += getDPVAT_MG().hashCode();
        }
        if (getLICENCIAMENTO_MG() != null) {
            _hashCode += getLICENCIAMENTO_MG().hashCode();
        }
        if (getTAXAS_DETRAN_SP() != null) {
            _hashCode += getTAXAS_DETRAN_SP().hashCode();
        }
        if (getHABITACAO() != null) {
            _hashCode += getHABITACAO().hashCode();
        }
        if (getBOLETO() != null) {
            _hashCode += getBOLETO().hashCode();
        }
        if (getTAXAS_CONDUTOR_MG() != null) {
            _hashCode += getTAXAS_CONDUTOR_MG().hashCode();
        }
        if (getCONCESSIONARIA() != null) {
            _hashCode += getCONCESSIONARIA().hashCode();
        }
        
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "conta_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IPVA_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IPVA_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DPVAT_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DPVAT_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTAS_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTAS_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LICENCIAMENTO_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LICENCIAMENTO_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGUNDA_VIA_LICENCIAMENTO_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGUNDA_VIA_LICENCIAMENTO_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRIMEIRO_REGISTRO_ESTADO_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRIMEIRO_REGISTRO_ESTADO_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DEBITOS_PENDENTES_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DEBITOS_PENDENTES_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSFERENCIAS_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TRANSFERENCIAS_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEGUNDA_VIA_TRANSFERENCIA_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEGUNDA_VIA_TRANSFERENCIA_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PEC_GENERICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PEC_GENERICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTAS_RENAINF_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTAS_RENAINF_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BOLETO_DDA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BOLETO_DDA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DARF_CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DARF_CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MEGASENA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MEGASENA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_mega_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECEBIMENTO_ELETRONICO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RECEBIMENTO_ELETRONICO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARTEIRA_ELETRONICA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CARTEIRA_ELETRONICA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DAED");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DAED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TAXAS_VEICULOS_MG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TAXAS_VEICULOS_MG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IPVA_RS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IPVA_RS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DPVAT_RS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DPVAT_RS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTAS_RS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTAS_RS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LICENCIAMENTO_RS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LICENCIAMENTO_RS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAGAMENTO_UNIFICADO_RS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PAGAMENTO_UNIFICADO_RS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IPVA_MG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IPVA_MG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DPVAT_MG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DPVAT_MG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LICENCIAMENTO_MG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LICENCIAMENTO_MG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TAXAS_DETRAN_SP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TAXAS_DETRAN_SP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HABITACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HABITACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BOLETO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BOLETO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TAXAS_CONDUTOR_MG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TAXAS_CONDUTOR_MG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField.setFieldName("CONCESSIONARIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONCESSIONARIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/resumo", "dados_saida_geral_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
