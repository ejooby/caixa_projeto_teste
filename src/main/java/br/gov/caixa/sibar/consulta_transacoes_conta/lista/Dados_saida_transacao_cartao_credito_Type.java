/**
 * Dados_saida_transacao_cartao_credito_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Dados_saida_transacao_cartao_credito_Type  implements java.io.Serializable {
    private java.lang.Integer NSU;

    private java.lang.String NOME_AGENCIA;

    private java.lang.Short TERMINAL;

    private java.lang.String NOME_CLIENTE;

    private java.lang.Long NUMERO;

    private java.util.Date DATA_PAGAMENTO;

    private java.math.BigDecimal VALOR;

    private java.lang.String IDENTIFICACAO;

    private java.util.Calendar DATA_TRANSACAO;

    private java.lang.String CANAL_ORIGEM;

    private java.lang.Integer NUMERO_DOCUMENTO;

    public Dados_saida_transacao_cartao_credito_Type() {
    }

    public Dados_saida_transacao_cartao_credito_Type(
           java.lang.Integer NSU,
           java.lang.String NOME_AGENCIA,
           java.lang.Short TERMINAL,
           java.lang.String NOME_CLIENTE,
           java.lang.Long NUMERO,
           java.util.Date DATA_PAGAMENTO,
           java.math.BigDecimal VALOR,
           java.lang.String IDENTIFICACAO,
           java.util.Calendar DATA_TRANSACAO,
           java.lang.String CANAL_ORIGEM,
           java.lang.Integer NUMERO_DOCUMENTO) {
           this.NSU = NSU;
           this.NOME_AGENCIA = NOME_AGENCIA;
           this.TERMINAL = TERMINAL;
           this.NOME_CLIENTE = NOME_CLIENTE;
           this.NUMERO = NUMERO;
           this.DATA_PAGAMENTO = DATA_PAGAMENTO;
           this.VALOR = VALOR;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.DATA_TRANSACAO = DATA_TRANSACAO;
           this.CANAL_ORIGEM = CANAL_ORIGEM;
           this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
    }


    /**
     * Gets the NSU value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return NSU
     */
    public java.lang.Integer getNSU() {
        return NSU;
    }


    /**
     * Sets the NSU value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param NSU
     */
    public void setNSU(java.lang.Integer NSU) {
        this.NSU = NSU;
    }


    /**
     * Gets the NOME_AGENCIA value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return NOME_AGENCIA
     */
    public java.lang.String getNOME_AGENCIA() {
        return NOME_AGENCIA;
    }


    /**
     * Sets the NOME_AGENCIA value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param NOME_AGENCIA
     */
    public void setNOME_AGENCIA(java.lang.String NOME_AGENCIA) {
        this.NOME_AGENCIA = NOME_AGENCIA;
    }


    /**
     * Gets the TERMINAL value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return TERMINAL
     */
    public java.lang.Short getTERMINAL() {
        return TERMINAL;
    }


    /**
     * Sets the TERMINAL value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param TERMINAL
     */
    public void setTERMINAL(java.lang.Short TERMINAL) {
        this.TERMINAL = TERMINAL;
    }


    /**
     * Gets the NOME_CLIENTE value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return NOME_CLIENTE
     */
    public java.lang.String getNOME_CLIENTE() {
        return NOME_CLIENTE;
    }


    /**
     * Sets the NOME_CLIENTE value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param NOME_CLIENTE
     */
    public void setNOME_CLIENTE(java.lang.String NOME_CLIENTE) {
        this.NOME_CLIENTE = NOME_CLIENTE;
    }


    /**
     * Gets the NUMERO value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return NUMERO
     */
    public java.lang.Long getNUMERO() {
        return NUMERO;
    }


    /**
     * Sets the NUMERO value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param NUMERO
     */
    public void setNUMERO(java.lang.Long NUMERO) {
        this.NUMERO = NUMERO;
    }


    /**
     * Gets the DATA_PAGAMENTO value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return DATA_PAGAMENTO
     */
    public java.util.Date getDATA_PAGAMENTO() {
        return DATA_PAGAMENTO;
    }


    /**
     * Sets the DATA_PAGAMENTO value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param DATA_PAGAMENTO
     */
    public void setDATA_PAGAMENTO(java.util.Date DATA_PAGAMENTO) {
        this.DATA_PAGAMENTO = DATA_PAGAMENTO;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the DATA_TRANSACAO value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return DATA_TRANSACAO
     */
    public java.util.Calendar getDATA_TRANSACAO() {
        return DATA_TRANSACAO;
    }


    /**
     * Sets the DATA_TRANSACAO value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param DATA_TRANSACAO
     */
    public void setDATA_TRANSACAO(java.util.Calendar DATA_TRANSACAO) {
        this.DATA_TRANSACAO = DATA_TRANSACAO;
    }


    /**
     * Gets the CANAL_ORIGEM value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return CANAL_ORIGEM
     */
    public java.lang.String getCANAL_ORIGEM() {
        return CANAL_ORIGEM;
    }


    /**
     * Sets the CANAL_ORIGEM value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param CANAL_ORIGEM
     */
    public void setCANAL_ORIGEM(java.lang.String CANAL_ORIGEM) {
        this.CANAL_ORIGEM = CANAL_ORIGEM;
    }


    /**
     * Gets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @return NUMERO_DOCUMENTO
     */
    public java.lang.Integer getNUMERO_DOCUMENTO() {
        return NUMERO_DOCUMENTO;
    }


    /**
     * Sets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_cartao_credito_Type.
     * 
     * @param NUMERO_DOCUMENTO
     */
    public void setNUMERO_DOCUMENTO(java.lang.Integer NUMERO_DOCUMENTO) {
        this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_cartao_credito_Type)) return false;
        Dados_saida_transacao_cartao_credito_Type other = (Dados_saida_transacao_cartao_credito_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NSU==null && other.getNSU()==null) || 
             (this.NSU!=null &&
              this.NSU.equals(other.getNSU()))) &&
            ((this.NOME_AGENCIA==null && other.getNOME_AGENCIA()==null) || 
             (this.NOME_AGENCIA!=null &&
              this.NOME_AGENCIA.equals(other.getNOME_AGENCIA()))) &&
            ((this.TERMINAL==null && other.getTERMINAL()==null) || 
             (this.TERMINAL!=null &&
              this.TERMINAL.equals(other.getTERMINAL()))) &&
            ((this.NOME_CLIENTE==null && other.getNOME_CLIENTE()==null) || 
             (this.NOME_CLIENTE!=null &&
              this.NOME_CLIENTE.equals(other.getNOME_CLIENTE()))) &&
            ((this.NUMERO==null && other.getNUMERO()==null) || 
             (this.NUMERO!=null &&
              this.NUMERO.equals(other.getNUMERO()))) &&
            ((this.DATA_PAGAMENTO==null && other.getDATA_PAGAMENTO()==null) || 
             (this.DATA_PAGAMENTO!=null &&
              this.DATA_PAGAMENTO.equals(other.getDATA_PAGAMENTO()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.DATA_TRANSACAO==null && other.getDATA_TRANSACAO()==null) || 
             (this.DATA_TRANSACAO!=null &&
              this.DATA_TRANSACAO.equals(other.getDATA_TRANSACAO()))) &&
            ((this.CANAL_ORIGEM==null && other.getCANAL_ORIGEM()==null) || 
             (this.CANAL_ORIGEM!=null &&
              this.CANAL_ORIGEM.equals(other.getCANAL_ORIGEM()))) &&
            ((this.NUMERO_DOCUMENTO==null && other.getNUMERO_DOCUMENTO()==null) || 
             (this.NUMERO_DOCUMENTO!=null &&
              this.NUMERO_DOCUMENTO.equals(other.getNUMERO_DOCUMENTO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNSU() != null) {
            _hashCode += getNSU().hashCode();
        }
        if (getNOME_AGENCIA() != null) {
            _hashCode += getNOME_AGENCIA().hashCode();
        }
        if (getTERMINAL() != null) {
            _hashCode += getTERMINAL().hashCode();
        }
        if (getNOME_CLIENTE() != null) {
            _hashCode += getNOME_CLIENTE().hashCode();
        }
        if (getNUMERO() != null) {
            _hashCode += getNUMERO().hashCode();
        }
        if (getDATA_PAGAMENTO() != null) {
            _hashCode += getDATA_PAGAMENTO().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getDATA_TRANSACAO() != null) {
            _hashCode += getDATA_TRANSACAO().hashCode();
        }
        if (getCANAL_ORIGEM() != null) {
            _hashCode += getCANAL_ORIGEM().hashCode();
        }
        if (getNUMERO_DOCUMENTO() != null) {
            _hashCode += getNUMERO_DOCUMENTO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_cartao_credito_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_transacao_cartao_credito_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_AGENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_AGENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TERMINAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TERMINAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_CLIENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_CLIENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_PAGAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_PAGAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CANAL_ORIGEM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CANAL_ORIGEM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO_DOCUMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO_DOCUMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
