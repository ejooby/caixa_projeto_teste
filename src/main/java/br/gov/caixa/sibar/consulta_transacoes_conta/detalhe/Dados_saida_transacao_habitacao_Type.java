/**
 * Dados_saida_transacao_habitacao_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_habitacao_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedLong NUMERO_CONTRATO;

    private org.apache.axis.types.UnsignedInt NCPD;

    private java.lang.String CODIGO_BARRAS;

    private java.util.Date DATA_VENCIMENTO;

    private java.lang.String MUTUARIO;

    private java.lang.String CLIENTE;

    private org.apache.axis.types.UnsignedLong CPF;

    private org.apache.axis.types.UnsignedLong CNPJ;

    private java.util.Date DATA_MOVIMENTO;

    private java.util.Date DATA_EFETIVACAO;

    private org.apache.axis.types.UnsignedShort PRESTACAO;

    private java.lang.String IDENTIFICACAO;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_habitacao_Type() {
    }

    public Dados_saida_transacao_habitacao_Type(
           org.apache.axis.types.UnsignedLong NUMERO_CONTRATO,
           org.apache.axis.types.UnsignedInt NCPD,
           java.lang.String CODIGO_BARRAS,
           java.util.Date DATA_VENCIMENTO,
           java.lang.String MUTUARIO,
           java.lang.String CLIENTE,
           org.apache.axis.types.UnsignedLong CPF,
           org.apache.axis.types.UnsignedLong CNPJ,
           java.util.Date DATA_MOVIMENTO,
           java.util.Date DATA_EFETIVACAO,
           org.apache.axis.types.UnsignedShort PRESTACAO,
           java.lang.String IDENTIFICACAO,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.NUMERO_CONTRATO = NUMERO_CONTRATO;
           this.NCPD = NCPD;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.DATA_VENCIMENTO = DATA_VENCIMENTO;
           this.MUTUARIO = MUTUARIO;
           this.CLIENTE = CLIENTE;
           this.CPF = CPF;
           this.CNPJ = CNPJ;
           this.DATA_MOVIMENTO = DATA_MOVIMENTO;
           this.DATA_EFETIVACAO = DATA_EFETIVACAO;
           this.PRESTACAO = PRESTACAO;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the NUMERO_CONTRATO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return NUMERO_CONTRATO
     */
    public org.apache.axis.types.UnsignedLong getNUMERO_CONTRATO() {
        return NUMERO_CONTRATO;
    }


    /**
     * Sets the NUMERO_CONTRATO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param NUMERO_CONTRATO
     */
    public void setNUMERO_CONTRATO(org.apache.axis.types.UnsignedLong NUMERO_CONTRATO) {
        this.NUMERO_CONTRATO = NUMERO_CONTRATO;
    }


    /**
     * Gets the NCPD value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return NCPD
     */
    public org.apache.axis.types.UnsignedInt getNCPD() {
        return NCPD;
    }


    /**
     * Sets the NCPD value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param NCPD
     */
    public void setNCPD(org.apache.axis.types.UnsignedInt NCPD) {
        this.NCPD = NCPD;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }


    /**
     * Gets the DATA_VENCIMENTO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return DATA_VENCIMENTO
     */
    public java.util.Date getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }


    /**
     * Sets the DATA_VENCIMENTO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param DATA_VENCIMENTO
     */
    public void setDATA_VENCIMENTO(java.util.Date DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }


    /**
     * Gets the MUTUARIO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return MUTUARIO
     */
    public java.lang.String getMUTUARIO() {
        return MUTUARIO;
    }


    /**
     * Sets the MUTUARIO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param MUTUARIO
     */
    public void setMUTUARIO(java.lang.String MUTUARIO) {
        this.MUTUARIO = MUTUARIO;
    }


    /**
     * Gets the CLIENTE value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return CLIENTE
     */
    public java.lang.String getCLIENTE() {
        return CLIENTE;
    }


    /**
     * Sets the CLIENTE value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param CLIENTE
     */
    public void setCLIENTE(java.lang.String CLIENTE) {
        this.CLIENTE = CLIENTE;
    }


    /**
     * Gets the CPF value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return CPF
     */
    public org.apache.axis.types.UnsignedLong getCPF() {
        return CPF;
    }


    /**
     * Sets the CPF value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param CPF
     */
    public void setCPF(org.apache.axis.types.UnsignedLong CPF) {
        this.CPF = CPF;
    }


    /**
     * Gets the CNPJ value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return CNPJ
     */
    public org.apache.axis.types.UnsignedLong getCNPJ() {
        return CNPJ;
    }


    /**
     * Sets the CNPJ value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param CNPJ
     */
    public void setCNPJ(org.apache.axis.types.UnsignedLong CNPJ) {
        this.CNPJ = CNPJ;
    }


    /**
     * Gets the DATA_MOVIMENTO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return DATA_MOVIMENTO
     */
    public java.util.Date getDATA_MOVIMENTO() {
        return DATA_MOVIMENTO;
    }


    /**
     * Sets the DATA_MOVIMENTO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param DATA_MOVIMENTO
     */
    public void setDATA_MOVIMENTO(java.util.Date DATA_MOVIMENTO) {
        this.DATA_MOVIMENTO = DATA_MOVIMENTO;
    }


    /**
     * Gets the DATA_EFETIVACAO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return DATA_EFETIVACAO
     */
    public java.util.Date getDATA_EFETIVACAO() {
        return DATA_EFETIVACAO;
    }


    /**
     * Sets the DATA_EFETIVACAO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param DATA_EFETIVACAO
     */
    public void setDATA_EFETIVACAO(java.util.Date DATA_EFETIVACAO) {
        this.DATA_EFETIVACAO = DATA_EFETIVACAO;
    }


    /**
     * Gets the PRESTACAO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return PRESTACAO
     */
    public org.apache.axis.types.UnsignedShort getPRESTACAO() {
        return PRESTACAO;
    }


    /**
     * Sets the PRESTACAO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param PRESTACAO
     */
    public void setPRESTACAO(org.apache.axis.types.UnsignedShort PRESTACAO) {
        this.PRESTACAO = PRESTACAO;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_habitacao_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_habitacao_Type)) return false;
        Dados_saida_transacao_habitacao_Type other = (Dados_saida_transacao_habitacao_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NUMERO_CONTRATO==null && other.getNUMERO_CONTRATO()==null) || 
             (this.NUMERO_CONTRATO!=null &&
              this.NUMERO_CONTRATO.equals(other.getNUMERO_CONTRATO()))) &&
            ((this.NCPD==null && other.getNCPD()==null) || 
             (this.NCPD!=null &&
              this.NCPD.equals(other.getNCPD()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              this.CODIGO_BARRAS.equals(other.getCODIGO_BARRAS()))) &&
            ((this.DATA_VENCIMENTO==null && other.getDATA_VENCIMENTO()==null) || 
             (this.DATA_VENCIMENTO!=null &&
              this.DATA_VENCIMENTO.equals(other.getDATA_VENCIMENTO()))) &&
            ((this.MUTUARIO==null && other.getMUTUARIO()==null) || 
             (this.MUTUARIO!=null &&
              this.MUTUARIO.equals(other.getMUTUARIO()))) &&
            ((this.CLIENTE==null && other.getCLIENTE()==null) || 
             (this.CLIENTE!=null &&
              this.CLIENTE.equals(other.getCLIENTE()))) &&
            ((this.CPF==null && other.getCPF()==null) || 
             (this.CPF!=null &&
              this.CPF.equals(other.getCPF()))) &&
            ((this.CNPJ==null && other.getCNPJ()==null) || 
             (this.CNPJ!=null &&
              this.CNPJ.equals(other.getCNPJ()))) &&
            ((this.DATA_MOVIMENTO==null && other.getDATA_MOVIMENTO()==null) || 
             (this.DATA_MOVIMENTO!=null &&
              this.DATA_MOVIMENTO.equals(other.getDATA_MOVIMENTO()))) &&
            ((this.DATA_EFETIVACAO==null && other.getDATA_EFETIVACAO()==null) || 
             (this.DATA_EFETIVACAO!=null &&
              this.DATA_EFETIVACAO.equals(other.getDATA_EFETIVACAO()))) &&
            ((this.PRESTACAO==null && other.getPRESTACAO()==null) || 
             (this.PRESTACAO!=null &&
              this.PRESTACAO.equals(other.getPRESTACAO()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNUMERO_CONTRATO() != null) {
            _hashCode += getNUMERO_CONTRATO().hashCode();
        }
        if (getNCPD() != null) {
            _hashCode += getNCPD().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            _hashCode += getCODIGO_BARRAS().hashCode();
        }
        if (getDATA_VENCIMENTO() != null) {
            _hashCode += getDATA_VENCIMENTO().hashCode();
        }
        if (getMUTUARIO() != null) {
            _hashCode += getMUTUARIO().hashCode();
        }
        if (getCLIENTE() != null) {
            _hashCode += getCLIENTE().hashCode();
        }
        if (getCPF() != null) {
            _hashCode += getCPF().hashCode();
        }
        if (getCNPJ() != null) {
            _hashCode += getCNPJ().hashCode();
        }
        if (getDATA_MOVIMENTO() != null) {
            _hashCode += getDATA_MOVIMENTO().hashCode();
        }
        if (getDATA_EFETIVACAO() != null) {
            _hashCode += getDATA_EFETIVACAO().hashCode();
        }
        if (getPRESTACAO() != null) {
            _hashCode += getPRESTACAO().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_habitacao_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_habitacao_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO_CONTRATO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO_CONTRATO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NCPD");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NCPD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_VENCIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_VENCIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MUTUARIO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MUTUARIO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CLIENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_MOVIMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_MOVIMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_EFETIVACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_EFETIVACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRESTACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRESTACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
