/**
 * Prestacoes_em_aberto_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Prestacoes_em_aberto_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedByte PRIMEIRA;

    private org.apache.axis.types.UnsignedByte ULTIMA;

    public Prestacoes_em_aberto_Type() {
    }

    public Prestacoes_em_aberto_Type(
           org.apache.axis.types.UnsignedByte PRIMEIRA,
           org.apache.axis.types.UnsignedByte ULTIMA) {
           this.PRIMEIRA = PRIMEIRA;
           this.ULTIMA = ULTIMA;
    }


    /**
     * Gets the PRIMEIRA value for this Prestacoes_em_aberto_Type.
     * 
     * @return PRIMEIRA
     */
    public org.apache.axis.types.UnsignedByte getPRIMEIRA() {
        return PRIMEIRA;
    }


    /**
     * Sets the PRIMEIRA value for this Prestacoes_em_aberto_Type.
     * 
     * @param PRIMEIRA
     */
    public void setPRIMEIRA(org.apache.axis.types.UnsignedByte PRIMEIRA) {
        this.PRIMEIRA = PRIMEIRA;
    }


    /**
     * Gets the ULTIMA value for this Prestacoes_em_aberto_Type.
     * 
     * @return ULTIMA
     */
    public org.apache.axis.types.UnsignedByte getULTIMA() {
        return ULTIMA;
    }


    /**
     * Sets the ULTIMA value for this Prestacoes_em_aberto_Type.
     * 
     * @param ULTIMA
     */
    public void setULTIMA(org.apache.axis.types.UnsignedByte ULTIMA) {
        this.ULTIMA = ULTIMA;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Prestacoes_em_aberto_Type)) return false;
        Prestacoes_em_aberto_Type other = (Prestacoes_em_aberto_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PRIMEIRA==null && other.getPRIMEIRA()==null) || 
             (this.PRIMEIRA!=null &&
              this.PRIMEIRA.equals(other.getPRIMEIRA()))) &&
            ((this.ULTIMA==null && other.getULTIMA()==null) || 
             (this.ULTIMA!=null &&
              this.ULTIMA.equals(other.getULTIMA())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPRIMEIRA() != null) {
            _hashCode += getPRIMEIRA().hashCode();
        }
        if (getULTIMA() != null) {
            _hashCode += getULTIMA().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Prestacoes_em_aberto_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "prestacoes_em_aberto_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRIMEIRA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRIMEIRA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ULTIMA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ULTIMA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
