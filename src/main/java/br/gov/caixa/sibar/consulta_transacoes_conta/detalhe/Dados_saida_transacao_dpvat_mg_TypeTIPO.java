/**
 * Dados_saida_transacao_dpvat_mg_TypeTIPO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_dpvat_mg_TypeTIPO implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Dados_saida_transacao_dpvat_mg_TypeTIPO(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "Primeira Parcela";
    public static final java.lang.String _value2 = "Segunda Parcela";
    public static final java.lang.String _value3 = "Terceira Parcela";
    public static final java.lang.String _value4 = "Cota Unica sem Desconto";
    public static final java.lang.String _value5 = "Cota Unica com Desconto";
    public static final Dados_saida_transacao_dpvat_mg_TypeTIPO value1 = new Dados_saida_transacao_dpvat_mg_TypeTIPO(_value1);
    public static final Dados_saida_transacao_dpvat_mg_TypeTIPO value2 = new Dados_saida_transacao_dpvat_mg_TypeTIPO(_value2);
    public static final Dados_saida_transacao_dpvat_mg_TypeTIPO value3 = new Dados_saida_transacao_dpvat_mg_TypeTIPO(_value3);
    public static final Dados_saida_transacao_dpvat_mg_TypeTIPO value4 = new Dados_saida_transacao_dpvat_mg_TypeTIPO(_value4);
    public static final Dados_saida_transacao_dpvat_mg_TypeTIPO value5 = new Dados_saida_transacao_dpvat_mg_TypeTIPO(_value5);
    public java.lang.String getValue() { return _value_;}
    public static Dados_saida_transacao_dpvat_mg_TypeTIPO fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Dados_saida_transacao_dpvat_mg_TypeTIPO enumeration = (Dados_saida_transacao_dpvat_mg_TypeTIPO)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Dados_saida_transacao_dpvat_mg_TypeTIPO fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_dpvat_mg_TypeTIPO.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_saida_transacao_dpvat_mg_Type>TIPO"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
