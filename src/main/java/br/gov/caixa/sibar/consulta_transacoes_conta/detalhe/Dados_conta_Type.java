/**
 * Dados_conta_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_conta_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type CONTA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_contas_Type TIPO_CONTA;

    private java.lang.String NOME_CLIENTE;

    private java.lang.String NOME_AGENCIA;

    public Dados_conta_Type() {
    }

    public Dados_conta_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type CONTA,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_contas_Type TIPO_CONTA,
           java.lang.String NOME_CLIENTE,
           java.lang.String NOME_AGENCIA) {
           this.CONTA = CONTA;
           this.TIPO_CONTA = TIPO_CONTA;
           this.NOME_CLIENTE = NOME_CLIENTE;
           this.NOME_AGENCIA = NOME_AGENCIA;
    }


    /**
     * Gets the CONTA value for this Dados_conta_Type.
     * 
     * @return CONTA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type getCONTA() {
        return CONTA;
    }


    /**
     * Sets the CONTA value for this Dados_conta_Type.
     * 
     * @param CONTA
     */
    public void setCONTA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Conta_Type CONTA) {
        this.CONTA = CONTA;
    }


    /**
     * Gets the TIPO_CONTA value for this Dados_conta_Type.
     * 
     * @return TIPO_CONTA
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_contas_Type getTIPO_CONTA() {
        return TIPO_CONTA;
    }


    /**
     * Sets the TIPO_CONTA value for this Dados_conta_Type.
     * 
     * @param TIPO_CONTA
     */
    public void setTIPO_CONTA(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Tipo_contas_Type TIPO_CONTA) {
        this.TIPO_CONTA = TIPO_CONTA;
    }


    /**
     * Gets the NOME_CLIENTE value for this Dados_conta_Type.
     * 
     * @return NOME_CLIENTE
     */
    public java.lang.String getNOME_CLIENTE() {
        return NOME_CLIENTE;
    }


    /**
     * Sets the NOME_CLIENTE value for this Dados_conta_Type.
     * 
     * @param NOME_CLIENTE
     */
    public void setNOME_CLIENTE(java.lang.String NOME_CLIENTE) {
        this.NOME_CLIENTE = NOME_CLIENTE;
    }


    /**
     * Gets the NOME_AGENCIA value for this Dados_conta_Type.
     * 
     * @return NOME_AGENCIA
     */
    public java.lang.String getNOME_AGENCIA() {
        return NOME_AGENCIA;
    }


    /**
     * Sets the NOME_AGENCIA value for this Dados_conta_Type.
     * 
     * @param NOME_AGENCIA
     */
    public void setNOME_AGENCIA(java.lang.String NOME_AGENCIA) {
        this.NOME_AGENCIA = NOME_AGENCIA;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_conta_Type)) return false;
        Dados_conta_Type other = (Dados_conta_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CONTA==null && other.getCONTA()==null) || 
             (this.CONTA!=null &&
              this.CONTA.equals(other.getCONTA()))) &&
            ((this.TIPO_CONTA==null && other.getTIPO_CONTA()==null) || 
             (this.TIPO_CONTA!=null &&
              this.TIPO_CONTA.equals(other.getTIPO_CONTA()))) &&
            ((this.NOME_CLIENTE==null && other.getNOME_CLIENTE()==null) || 
             (this.NOME_CLIENTE!=null &&
              this.NOME_CLIENTE.equals(other.getNOME_CLIENTE()))) &&
            ((this.NOME_AGENCIA==null && other.getNOME_AGENCIA()==null) || 
             (this.NOME_AGENCIA!=null &&
              this.NOME_AGENCIA.equals(other.getNOME_AGENCIA())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCONTA() != null) {
            _hashCode += getCONTA().hashCode();
        }
        if (getTIPO_CONTA() != null) {
            _hashCode += getTIPO_CONTA().hashCode();
        }
        if (getNOME_CLIENTE() != null) {
            _hashCode += getNOME_CLIENTE().hashCode();
        }
        if (getNOME_AGENCIA() != null) {
            _hashCode += getNOME_AGENCIA().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_conta_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_conta_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "conta_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "tipo_contas_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_CLIENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_CLIENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME_AGENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME_AGENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
