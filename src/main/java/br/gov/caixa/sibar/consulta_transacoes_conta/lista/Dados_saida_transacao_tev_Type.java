/**
 * Dados_saida_transacao_tev_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Dados_saida_transacao_tev_Type  implements java.io.Serializable,TransacaoInt {
	
	
	public String tipoServico;
	public String data;
	public String banco_Descricao;
	public String chaveSeguranca;

	
	
	
    private java.lang.Integer NSU;

    private java.lang.Long CPF_DESTINO;

    private java.lang.String TIPO_CONTA_DEBITO;

    private java.lang.String TIPO_CONTA_CREDITO;

    private java.lang.String AGENCIA_DEBITO;

    private java.lang.String AGENCIA_CREDITO;

    private java.lang.String CLIENTE_CREDITO;

    private java.lang.String ID_CONTA;

    private br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type CONTA_CREDITO;

    private java.lang.String DATA_TRANSFERENCIA;

    private java.math.BigDecimal VALOR;

    private java.lang.String IDENTIFICACAO;

    private java.util.Calendar DATA_TRANSACAO;

    private java.lang.String CANAL_ORIGEM;

    private java.lang.Integer NUMERO_DOCUMENTO;

    private java.lang.Integer NUM_DOC_CREDITO;

    public Dados_saida_transacao_tev_Type() {
    }

    public Dados_saida_transacao_tev_Type(
           java.lang.Integer NSU,
           java.lang.Long CPF_DESTINO,
           java.lang.String TIPO_CONTA_DEBITO,
           java.lang.String TIPO_CONTA_CREDITO,
           java.lang.String AGENCIA_DEBITO,
           java.lang.String AGENCIA_CREDITO,
           java.lang.String CLIENTE_CREDITO,
           java.lang.String ID_CONTA,
           br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type CONTA_CREDITO,
           java.lang.String DATA_TRANSFERENCIA,
           java.math.BigDecimal VALOR,
           java.lang.String IDENTIFICACAO,
           java.util.Calendar DATA_TRANSACAO,
           java.lang.String CANAL_ORIGEM,
           java.lang.Integer NUMERO_DOCUMENTO,
           java.lang.Integer NUM_DOC_CREDITO) {
           this.NSU = NSU;
           this.CPF_DESTINO = CPF_DESTINO;
           this.TIPO_CONTA_DEBITO = TIPO_CONTA_DEBITO;
           this.TIPO_CONTA_CREDITO = TIPO_CONTA_CREDITO;
           this.AGENCIA_DEBITO = AGENCIA_DEBITO;
           this.AGENCIA_CREDITO = AGENCIA_CREDITO;
           this.CLIENTE_CREDITO = CLIENTE_CREDITO;
           this.ID_CONTA = ID_CONTA;
           this.CONTA_CREDITO = CONTA_CREDITO;
           this.DATA_TRANSFERENCIA = DATA_TRANSFERENCIA;
           this.VALOR = VALOR;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.DATA_TRANSACAO = DATA_TRANSACAO;
           this.CANAL_ORIGEM = CANAL_ORIGEM;
           this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
           this.NUM_DOC_CREDITO = NUM_DOC_CREDITO;
    }


    /**
     * Gets the NSU value for this Dados_saida_transacao_tev_Type.
     * 
     * @return NSU
     */
    public java.lang.Integer getNSU() {
        return NSU;
    }


    /**
     * Sets the NSU value for this Dados_saida_transacao_tev_Type.
     * 
     * @param NSU
     */
    public void setNSU(java.lang.Integer NSU) {
        this.NSU = NSU;
    }


    /**
     * Gets the CPF_DESTINO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return CPF_DESTINO
     */
    public java.lang.Long getCPF_DESTINO() {
        return CPF_DESTINO;
    }


    /**
     * Sets the CPF_DESTINO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param CPF_DESTINO
     */
    public void setCPF_DESTINO(java.lang.Long CPF_DESTINO) {
        this.CPF_DESTINO = CPF_DESTINO;
    }


    /**
     * Gets the TIPO_CONTA_DEBITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return TIPO_CONTA_DEBITO
     */
    public java.lang.String getTIPO_CONTA_DEBITO() {
        return TIPO_CONTA_DEBITO;
    }


    /**
     * Sets the TIPO_CONTA_DEBITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param TIPO_CONTA_DEBITO
     */
    public void setTIPO_CONTA_DEBITO(java.lang.String TIPO_CONTA_DEBITO) {
        this.TIPO_CONTA_DEBITO = TIPO_CONTA_DEBITO;
    }


    /**
     * Gets the TIPO_CONTA_CREDITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return TIPO_CONTA_CREDITO
     */
    public java.lang.String getTIPO_CONTA_CREDITO() {
        return TIPO_CONTA_CREDITO;
    }


    /**
     * Sets the TIPO_CONTA_CREDITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param TIPO_CONTA_CREDITO
     */
    public void setTIPO_CONTA_CREDITO(java.lang.String TIPO_CONTA_CREDITO) {
        this.TIPO_CONTA_CREDITO = TIPO_CONTA_CREDITO;
    }


    /**
     * Gets the AGENCIA_DEBITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return AGENCIA_DEBITO
     */
    public java.lang.String getAGENCIA_DEBITO() {
        return AGENCIA_DEBITO;
    }


    /**
     * Sets the AGENCIA_DEBITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param AGENCIA_DEBITO
     */
    public void setAGENCIA_DEBITO(java.lang.String AGENCIA_DEBITO) {
        this.AGENCIA_DEBITO = AGENCIA_DEBITO;
    }


    /**
     * Gets the AGENCIA_CREDITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return AGENCIA_CREDITO
     */
    public java.lang.String getAGENCIA_CREDITO() {
        return AGENCIA_CREDITO;
    }


    /**
     * Sets the AGENCIA_CREDITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param AGENCIA_CREDITO
     */
    public void setAGENCIA_CREDITO(java.lang.String AGENCIA_CREDITO) {
        this.AGENCIA_CREDITO = AGENCIA_CREDITO;
    }


    /**
     * Gets the CLIENTE_CREDITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return CLIENTE_CREDITO
     */
    public java.lang.String getCLIENTE_CREDITO() {
        return CLIENTE_CREDITO;
    }


    /**
     * Sets the CLIENTE_CREDITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param CLIENTE_CREDITO
     */
    public void setCLIENTE_CREDITO(java.lang.String CLIENTE_CREDITO) {
        this.CLIENTE_CREDITO = CLIENTE_CREDITO;
    }


    /**
     * Gets the ID_CONTA value for this Dados_saida_transacao_tev_Type.
     * 
     * @return ID_CONTA
     */
    public java.lang.String getID_CONTA() {
        return ID_CONTA;
    }


    /**
     * Sets the ID_CONTA value for this Dados_saida_transacao_tev_Type.
     * 
     * @param ID_CONTA
     */
    public void setID_CONTA(java.lang.String ID_CONTA) {
        this.ID_CONTA = ID_CONTA;
    }


    /**
     * Gets the CONTA_CREDITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return CONTA_CREDITO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type getCONTA_CREDITO() {
        return CONTA_CREDITO;
    }


    /**
     * Sets the CONTA_CREDITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param CONTA_CREDITO
     */
    public void setCONTA_CREDITO(br.gov.caixa.sibar.consulta_transacoes_conta.lista.Conta_Type CONTA_CREDITO) {
        this.CONTA_CREDITO = CONTA_CREDITO;
    }


    /**
     * Gets the DATA_TRANSFERENCIA value for this Dados_saida_transacao_tev_Type.
     * 
     * @return DATA_TRANSFERENCIA
     */
    public java.lang.String getDATA_TRANSFERENCIA() {
        return DATA_TRANSFERENCIA;
    }


    /**
     * Sets the DATA_TRANSFERENCIA value for this Dados_saida_transacao_tev_Type.
     * 
     * @param DATA_TRANSFERENCIA
     */
    public void setDATA_TRANSFERENCIA(java.lang.String DATA_TRANSFERENCIA) {
        this.DATA_TRANSFERENCIA = DATA_TRANSFERENCIA;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_tev_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_tev_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the DATA_TRANSACAO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return DATA_TRANSACAO
     */
    public java.util.Calendar getDATA_TRANSACAO() {
        return DATA_TRANSACAO;
    }


    /**
     * Sets the DATA_TRANSACAO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param DATA_TRANSACAO
     */
    public void setDATA_TRANSACAO(java.util.Calendar DATA_TRANSACAO) {
        this.DATA_TRANSACAO = DATA_TRANSACAO;
    }


    /**
     * Gets the CANAL_ORIGEM value for this Dados_saida_transacao_tev_Type.
     * 
     * @return CANAL_ORIGEM
     */
    public java.lang.String getCANAL_ORIGEM() {
        return CANAL_ORIGEM;
    }


    /**
     * Sets the CANAL_ORIGEM value for this Dados_saida_transacao_tev_Type.
     * 
     * @param CANAL_ORIGEM
     */
    public void setCANAL_ORIGEM(java.lang.String CANAL_ORIGEM) {
        this.CANAL_ORIGEM = CANAL_ORIGEM;
    }


    /**
     * Gets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return NUMERO_DOCUMENTO
     */
    public java.lang.Integer getNUMERO_DOCUMENTO() {
        return NUMERO_DOCUMENTO;
    }


    /**
     * Sets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param NUMERO_DOCUMENTO
     */
    public void setNUMERO_DOCUMENTO(java.lang.Integer NUMERO_DOCUMENTO) {
        this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
    }


    /**
     * Gets the NUM_DOC_CREDITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @return NUM_DOC_CREDITO
     */
    public java.lang.Integer getNUM_DOC_CREDITO() {
        return NUM_DOC_CREDITO;
    }


    /**
     * Sets the NUM_DOC_CREDITO value for this Dados_saida_transacao_tev_Type.
     * 
     * @param NUM_DOC_CREDITO
     */
    public void setNUM_DOC_CREDITO(java.lang.Integer NUM_DOC_CREDITO) {
        this.NUM_DOC_CREDITO = NUM_DOC_CREDITO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_tev_Type)) return false;
        Dados_saida_transacao_tev_Type other = (Dados_saida_transacao_tev_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NSU==null && other.getNSU()==null) || 
             (this.NSU!=null &&
              this.NSU.equals(other.getNSU()))) &&
            ((this.CPF_DESTINO==null && other.getCPF_DESTINO()==null) || 
             (this.CPF_DESTINO!=null &&
              this.CPF_DESTINO.equals(other.getCPF_DESTINO()))) &&
            ((this.TIPO_CONTA_DEBITO==null && other.getTIPO_CONTA_DEBITO()==null) || 
             (this.TIPO_CONTA_DEBITO!=null &&
              this.TIPO_CONTA_DEBITO.equals(other.getTIPO_CONTA_DEBITO()))) &&
            ((this.TIPO_CONTA_CREDITO==null && other.getTIPO_CONTA_CREDITO()==null) || 
             (this.TIPO_CONTA_CREDITO!=null &&
              this.TIPO_CONTA_CREDITO.equals(other.getTIPO_CONTA_CREDITO()))) &&
            ((this.AGENCIA_DEBITO==null && other.getAGENCIA_DEBITO()==null) || 
             (this.AGENCIA_DEBITO!=null &&
              this.AGENCIA_DEBITO.equals(other.getAGENCIA_DEBITO()))) &&
            ((this.AGENCIA_CREDITO==null && other.getAGENCIA_CREDITO()==null) || 
             (this.AGENCIA_CREDITO!=null &&
              this.AGENCIA_CREDITO.equals(other.getAGENCIA_CREDITO()))) &&
            ((this.CLIENTE_CREDITO==null && other.getCLIENTE_CREDITO()==null) || 
             (this.CLIENTE_CREDITO!=null &&
              this.CLIENTE_CREDITO.equals(other.getCLIENTE_CREDITO()))) &&
            ((this.ID_CONTA==null && other.getID_CONTA()==null) || 
             (this.ID_CONTA!=null &&
              this.ID_CONTA.equals(other.getID_CONTA()))) &&
            ((this.CONTA_CREDITO==null && other.getCONTA_CREDITO()==null) || 
             (this.CONTA_CREDITO!=null &&
              this.CONTA_CREDITO.equals(other.getCONTA_CREDITO()))) &&
            ((this.DATA_TRANSFERENCIA==null && other.getDATA_TRANSFERENCIA()==null) || 
             (this.DATA_TRANSFERENCIA!=null &&
              this.DATA_TRANSFERENCIA.equals(other.getDATA_TRANSFERENCIA()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.DATA_TRANSACAO==null && other.getDATA_TRANSACAO()==null) || 
             (this.DATA_TRANSACAO!=null &&
              this.DATA_TRANSACAO.equals(other.getDATA_TRANSACAO()))) &&
            ((this.CANAL_ORIGEM==null && other.getCANAL_ORIGEM()==null) || 
             (this.CANAL_ORIGEM!=null &&
              this.CANAL_ORIGEM.equals(other.getCANAL_ORIGEM()))) &&
            ((this.NUMERO_DOCUMENTO==null && other.getNUMERO_DOCUMENTO()==null) || 
             (this.NUMERO_DOCUMENTO!=null &&
              this.NUMERO_DOCUMENTO.equals(other.getNUMERO_DOCUMENTO()))) &&
            ((this.NUM_DOC_CREDITO==null && other.getNUM_DOC_CREDITO()==null) || 
             (this.NUM_DOC_CREDITO!=null &&
              this.NUM_DOC_CREDITO.equals(other.getNUM_DOC_CREDITO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNSU() != null) {
            _hashCode += getNSU().hashCode();
        }
        if (getCPF_DESTINO() != null) {
            _hashCode += getCPF_DESTINO().hashCode();
        }
        if (getTIPO_CONTA_DEBITO() != null) {
            _hashCode += getTIPO_CONTA_DEBITO().hashCode();
        }
        if (getTIPO_CONTA_CREDITO() != null) {
            _hashCode += getTIPO_CONTA_CREDITO().hashCode();
        }
        if (getAGENCIA_DEBITO() != null) {
            _hashCode += getAGENCIA_DEBITO().hashCode();
        }
        if (getAGENCIA_CREDITO() != null) {
            _hashCode += getAGENCIA_CREDITO().hashCode();
        }
        if (getCLIENTE_CREDITO() != null) {
            _hashCode += getCLIENTE_CREDITO().hashCode();
        }
        if (getID_CONTA() != null) {
            _hashCode += getID_CONTA().hashCode();
        }
        if (getCONTA_CREDITO() != null) {
            _hashCode += getCONTA_CREDITO().hashCode();
        }
        if (getDATA_TRANSFERENCIA() != null) {
            _hashCode += getDATA_TRANSFERENCIA().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getDATA_TRANSACAO() != null) {
            _hashCode += getDATA_TRANSACAO().hashCode();
        }
        if (getCANAL_ORIGEM() != null) {
            _hashCode += getCANAL_ORIGEM().hashCode();
        }
        if (getNUMERO_DOCUMENTO() != null) {
            _hashCode += getNUMERO_DOCUMENTO().hashCode();
        }
        if (getNUM_DOC_CREDITO() != null) {
            _hashCode += getNUM_DOC_CREDITO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_tev_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_transacao_tev_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF_DESTINO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CPF_DESTINO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_CONTA_DEBITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_CONTA_DEBITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIPO_CONTA_CREDITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIPO_CONTA_CREDITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AGENCIA_DEBITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AGENCIA_DEBITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AGENCIA_CREDITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AGENCIA_CREDITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENTE_CREDITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CLIENTE_CREDITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_CONTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ID_CONTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTA_CREDITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTA_CREDITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "conta_Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_TRANSFERENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_TRANSFERENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CANAL_ORIGEM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CANAL_ORIGEM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO_DOCUMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO_DOCUMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUM_DOC_CREDITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUM_DOC_CREDITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public String getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getBanco_Descricao() {
		return banco_Descricao;
	}

	public void setBanco_Descricao(String banco_Descricao) {
		this.banco_Descricao = banco_Descricao;
	}

	public String getChaveSeguranca() {
		return chaveSeguranca;
	}

	public void setChaveSeguranca(String chaveSeguranca) {
		this.chaveSeguranca = chaveSeguranca;
	}

    
    
    
}
