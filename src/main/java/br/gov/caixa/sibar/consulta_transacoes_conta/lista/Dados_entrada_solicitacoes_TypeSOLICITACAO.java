/**
 * Dados_entrada_solicitacoes_TypeSOLICITACAO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Dados_entrada_solicitacoes_TypeSOLICITACAO implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Dados_entrada_solicitacoes_TypeSOLICITACAO(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _CARTAO_CREDITO = "CARTAO_CREDITO";
    public static final java.lang.String _PAGAMENTO_CONCESSIONARIA = "PAGAMENTO_CONCESSIONARIA";
    public static final java.lang.String _TEV = "TEV";
    public static final java.lang.String _DOC = "DOC";
    public static final java.lang.String _TED = "TED";
    public static final java.lang.String _HABITACAO_SEM_BARRA = "HABITACAO_SEM_BARRA";
    public static final java.lang.String _DARF = "DARF";
    public static final java.lang.String _FGTS = "FGTS";
    public static final java.lang.String _GPS = "GPS";
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO CARTAO_CREDITO = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_CARTAO_CREDITO);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO PAGAMENTO_CONCESSIONARIA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_PAGAMENTO_CONCESSIONARIA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TEV = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TEV);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DOC = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DOC);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO TED = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_TED);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO HABITACAO_SEM_BARRA = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_HABITACAO_SEM_BARRA);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO DARF = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_DARF);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO FGTS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_FGTS);
    public static final Dados_entrada_solicitacoes_TypeSOLICITACAO GPS = new Dados_entrada_solicitacoes_TypeSOLICITACAO(_GPS);
    public java.lang.String getValue() { return _value_;}
    public static Dados_entrada_solicitacoes_TypeSOLICITACAO fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Dados_entrada_solicitacoes_TypeSOLICITACAO enumeration = (Dados_entrada_solicitacoes_TypeSOLICITACAO)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Dados_entrada_solicitacoes_TypeSOLICITACAO fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_entrada_solicitacoes_TypeSOLICITACAO.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", ">dados_entrada_solicitacoes_Type>SOLICITACAO"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
