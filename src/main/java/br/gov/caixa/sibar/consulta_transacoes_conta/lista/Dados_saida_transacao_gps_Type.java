/**
 * Dados_saida_transacao_gps_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Dados_saida_transacao_gps_Type  implements java.io.Serializable {
    private java.lang.Integer NSU;

    private java.lang.String CODIGO;

    private java.lang.Short MES_COMPETENCIA;

    private java.lang.Short ANO_COMPETENCIA;

    private java.lang.String ID_CONTRIBUINTE;

    private java.math.BigDecimal PAGAMENTO;

    private java.math.BigDecimal ENTIDADE;

    private java.math.BigDecimal MULTA;

    private java.util.Date DATA_PAGAMENTO;

    private java.lang.String NOME;

    private java.lang.String IDENTIFICACAO;

    private java.util.Calendar DATA_TRANSACAO;

    private java.lang.String CANAL_ORIGEM;

    private java.lang.Integer NUMERO_DOCUMENTO;

    public Dados_saida_transacao_gps_Type() {
    }

    public Dados_saida_transacao_gps_Type(
           java.lang.Integer NSU,
           java.lang.String CODIGO,
           java.lang.Short MES_COMPETENCIA,
           java.lang.Short ANO_COMPETENCIA,
           java.lang.String ID_CONTRIBUINTE,
           java.math.BigDecimal PAGAMENTO,
           java.math.BigDecimal ENTIDADE,
           java.math.BigDecimal MULTA,
           java.util.Date DATA_PAGAMENTO,
           java.lang.String NOME,
           java.lang.String IDENTIFICACAO,
           java.util.Calendar DATA_TRANSACAO,
           java.lang.String CANAL_ORIGEM,
           java.lang.Integer NUMERO_DOCUMENTO) {
           this.NSU = NSU;
           this.CODIGO = CODIGO;
           this.MES_COMPETENCIA = MES_COMPETENCIA;
           this.ANO_COMPETENCIA = ANO_COMPETENCIA;
           this.ID_CONTRIBUINTE = ID_CONTRIBUINTE;
           this.PAGAMENTO = PAGAMENTO;
           this.ENTIDADE = ENTIDADE;
           this.MULTA = MULTA;
           this.DATA_PAGAMENTO = DATA_PAGAMENTO;
           this.NOME = NOME;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.DATA_TRANSACAO = DATA_TRANSACAO;
           this.CANAL_ORIGEM = CANAL_ORIGEM;
           this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
    }


    /**
     * Gets the NSU value for this Dados_saida_transacao_gps_Type.
     * 
     * @return NSU
     */
    public java.lang.Integer getNSU() {
        return NSU;
    }


    /**
     * Sets the NSU value for this Dados_saida_transacao_gps_Type.
     * 
     * @param NSU
     */
    public void setNSU(java.lang.Integer NSU) {
        this.NSU = NSU;
    }


    /**
     * Gets the CODIGO value for this Dados_saida_transacao_gps_Type.
     * 
     * @return CODIGO
     */
    public java.lang.String getCODIGO() {
        return CODIGO;
    }


    /**
     * Sets the CODIGO value for this Dados_saida_transacao_gps_Type.
     * 
     * @param CODIGO
     */
    public void setCODIGO(java.lang.String CODIGO) {
        this.CODIGO = CODIGO;
    }


    /**
     * Gets the MES_COMPETENCIA value for this Dados_saida_transacao_gps_Type.
     * 
     * @return MES_COMPETENCIA
     */
    public java.lang.Short getMES_COMPETENCIA() {
        return MES_COMPETENCIA;
    }


    /**
     * Sets the MES_COMPETENCIA value for this Dados_saida_transacao_gps_Type.
     * 
     * @param MES_COMPETENCIA
     */
    public void setMES_COMPETENCIA(java.lang.Short MES_COMPETENCIA) {
        this.MES_COMPETENCIA = MES_COMPETENCIA;
    }


    /**
     * Gets the ANO_COMPETENCIA value for this Dados_saida_transacao_gps_Type.
     * 
     * @return ANO_COMPETENCIA
     */
    public java.lang.Short getANO_COMPETENCIA() {
        return ANO_COMPETENCIA;
    }


    /**
     * Sets the ANO_COMPETENCIA value for this Dados_saida_transacao_gps_Type.
     * 
     * @param ANO_COMPETENCIA
     */
    public void setANO_COMPETENCIA(java.lang.Short ANO_COMPETENCIA) {
        this.ANO_COMPETENCIA = ANO_COMPETENCIA;
    }


    /**
     * Gets the ID_CONTRIBUINTE value for this Dados_saida_transacao_gps_Type.
     * 
     * @return ID_CONTRIBUINTE
     */
    public java.lang.String getID_CONTRIBUINTE() {
        return ID_CONTRIBUINTE;
    }


    /**
     * Sets the ID_CONTRIBUINTE value for this Dados_saida_transacao_gps_Type.
     * 
     * @param ID_CONTRIBUINTE
     */
    public void setID_CONTRIBUINTE(java.lang.String ID_CONTRIBUINTE) {
        this.ID_CONTRIBUINTE = ID_CONTRIBUINTE;
    }


    /**
     * Gets the PAGAMENTO value for this Dados_saida_transacao_gps_Type.
     * 
     * @return PAGAMENTO
     */
    public java.math.BigDecimal getPAGAMENTO() {
        return PAGAMENTO;
    }


    /**
     * Sets the PAGAMENTO value for this Dados_saida_transacao_gps_Type.
     * 
     * @param PAGAMENTO
     */
    public void setPAGAMENTO(java.math.BigDecimal PAGAMENTO) {
        this.PAGAMENTO = PAGAMENTO;
    }


    /**
     * Gets the ENTIDADE value for this Dados_saida_transacao_gps_Type.
     * 
     * @return ENTIDADE
     */
    public java.math.BigDecimal getENTIDADE() {
        return ENTIDADE;
    }


    /**
     * Sets the ENTIDADE value for this Dados_saida_transacao_gps_Type.
     * 
     * @param ENTIDADE
     */
    public void setENTIDADE(java.math.BigDecimal ENTIDADE) {
        this.ENTIDADE = ENTIDADE;
    }


    /**
     * Gets the MULTA value for this Dados_saida_transacao_gps_Type.
     * 
     * @return MULTA
     */
    public java.math.BigDecimal getMULTA() {
        return MULTA;
    }


    /**
     * Sets the MULTA value for this Dados_saida_transacao_gps_Type.
     * 
     * @param MULTA
     */
    public void setMULTA(java.math.BigDecimal MULTA) {
        this.MULTA = MULTA;
    }


    /**
     * Gets the DATA_PAGAMENTO value for this Dados_saida_transacao_gps_Type.
     * 
     * @return DATA_PAGAMENTO
     */
    public java.util.Date getDATA_PAGAMENTO() {
        return DATA_PAGAMENTO;
    }


    /**
     * Sets the DATA_PAGAMENTO value for this Dados_saida_transacao_gps_Type.
     * 
     * @param DATA_PAGAMENTO
     */
    public void setDATA_PAGAMENTO(java.util.Date DATA_PAGAMENTO) {
        this.DATA_PAGAMENTO = DATA_PAGAMENTO;
    }


    /**
     * Gets the NOME value for this Dados_saida_transacao_gps_Type.
     * 
     * @return NOME
     */
    public java.lang.String getNOME() {
        return NOME;
    }


    /**
     * Sets the NOME value for this Dados_saida_transacao_gps_Type.
     * 
     * @param NOME
     */
    public void setNOME(java.lang.String NOME) {
        this.NOME = NOME;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_gps_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_gps_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the DATA_TRANSACAO value for this Dados_saida_transacao_gps_Type.
     * 
     * @return DATA_TRANSACAO
     */
    public java.util.Calendar getDATA_TRANSACAO() {
        return DATA_TRANSACAO;
    }


    /**
     * Sets the DATA_TRANSACAO value for this Dados_saida_transacao_gps_Type.
     * 
     * @param DATA_TRANSACAO
     */
    public void setDATA_TRANSACAO(java.util.Calendar DATA_TRANSACAO) {
        this.DATA_TRANSACAO = DATA_TRANSACAO;
    }


    /**
     * Gets the CANAL_ORIGEM value for this Dados_saida_transacao_gps_Type.
     * 
     * @return CANAL_ORIGEM
     */
    public java.lang.String getCANAL_ORIGEM() {
        return CANAL_ORIGEM;
    }


    /**
     * Sets the CANAL_ORIGEM value for this Dados_saida_transacao_gps_Type.
     * 
     * @param CANAL_ORIGEM
     */
    public void setCANAL_ORIGEM(java.lang.String CANAL_ORIGEM) {
        this.CANAL_ORIGEM = CANAL_ORIGEM;
    }


    /**
     * Gets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_gps_Type.
     * 
     * @return NUMERO_DOCUMENTO
     */
    public java.lang.Integer getNUMERO_DOCUMENTO() {
        return NUMERO_DOCUMENTO;
    }


    /**
     * Sets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_gps_Type.
     * 
     * @param NUMERO_DOCUMENTO
     */
    public void setNUMERO_DOCUMENTO(java.lang.Integer NUMERO_DOCUMENTO) {
        this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_gps_Type)) return false;
        Dados_saida_transacao_gps_Type other = (Dados_saida_transacao_gps_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NSU==null && other.getNSU()==null) || 
             (this.NSU!=null &&
              this.NSU.equals(other.getNSU()))) &&
            ((this.CODIGO==null && other.getCODIGO()==null) || 
             (this.CODIGO!=null &&
              this.CODIGO.equals(other.getCODIGO()))) &&
            ((this.MES_COMPETENCIA==null && other.getMES_COMPETENCIA()==null) || 
             (this.MES_COMPETENCIA!=null &&
              this.MES_COMPETENCIA.equals(other.getMES_COMPETENCIA()))) &&
            ((this.ANO_COMPETENCIA==null && other.getANO_COMPETENCIA()==null) || 
             (this.ANO_COMPETENCIA!=null &&
              this.ANO_COMPETENCIA.equals(other.getANO_COMPETENCIA()))) &&
            ((this.ID_CONTRIBUINTE==null && other.getID_CONTRIBUINTE()==null) || 
             (this.ID_CONTRIBUINTE!=null &&
              this.ID_CONTRIBUINTE.equals(other.getID_CONTRIBUINTE()))) &&
            ((this.PAGAMENTO==null && other.getPAGAMENTO()==null) || 
             (this.PAGAMENTO!=null &&
              this.PAGAMENTO.equals(other.getPAGAMENTO()))) &&
            ((this.ENTIDADE==null && other.getENTIDADE()==null) || 
             (this.ENTIDADE!=null &&
              this.ENTIDADE.equals(other.getENTIDADE()))) &&
            ((this.MULTA==null && other.getMULTA()==null) || 
             (this.MULTA!=null &&
              this.MULTA.equals(other.getMULTA()))) &&
            ((this.DATA_PAGAMENTO==null && other.getDATA_PAGAMENTO()==null) || 
             (this.DATA_PAGAMENTO!=null &&
              this.DATA_PAGAMENTO.equals(other.getDATA_PAGAMENTO()))) &&
            ((this.NOME==null && other.getNOME()==null) || 
             (this.NOME!=null &&
              this.NOME.equals(other.getNOME()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.DATA_TRANSACAO==null && other.getDATA_TRANSACAO()==null) || 
             (this.DATA_TRANSACAO!=null &&
              this.DATA_TRANSACAO.equals(other.getDATA_TRANSACAO()))) &&
            ((this.CANAL_ORIGEM==null && other.getCANAL_ORIGEM()==null) || 
             (this.CANAL_ORIGEM!=null &&
              this.CANAL_ORIGEM.equals(other.getCANAL_ORIGEM()))) &&
            ((this.NUMERO_DOCUMENTO==null && other.getNUMERO_DOCUMENTO()==null) || 
             (this.NUMERO_DOCUMENTO!=null &&
              this.NUMERO_DOCUMENTO.equals(other.getNUMERO_DOCUMENTO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNSU() != null) {
            _hashCode += getNSU().hashCode();
        }
        if (getCODIGO() != null) {
            _hashCode += getCODIGO().hashCode();
        }
        if (getMES_COMPETENCIA() != null) {
            _hashCode += getMES_COMPETENCIA().hashCode();
        }
        if (getANO_COMPETENCIA() != null) {
            _hashCode += getANO_COMPETENCIA().hashCode();
        }
        if (getID_CONTRIBUINTE() != null) {
            _hashCode += getID_CONTRIBUINTE().hashCode();
        }
        if (getPAGAMENTO() != null) {
            _hashCode += getPAGAMENTO().hashCode();
        }
        if (getENTIDADE() != null) {
            _hashCode += getENTIDADE().hashCode();
        }
        if (getMULTA() != null) {
            _hashCode += getMULTA().hashCode();
        }
        if (getDATA_PAGAMENTO() != null) {
            _hashCode += getDATA_PAGAMENTO().hashCode();
        }
        if (getNOME() != null) {
            _hashCode += getNOME().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getDATA_TRANSACAO() != null) {
            _hashCode += getDATA_TRANSACAO().hashCode();
        }
        if (getCANAL_ORIGEM() != null) {
            _hashCode += getCANAL_ORIGEM().hashCode();
        }
        if (getNUMERO_DOCUMENTO() != null) {
            _hashCode += getNUMERO_DOCUMENTO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_gps_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_transacao_gps_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MES_COMPETENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MES_COMPETENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANO_COMPETENCIA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ANO_COMPETENCIA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_CONTRIBUINTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ID_CONTRIBUINTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAGAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PAGAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ENTIDADE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ENTIDADE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MULTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MULTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_PAGAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_PAGAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NOME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CANAL_ORIGEM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CANAL_ORIGEM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO_DOCUMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO_DOCUMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
