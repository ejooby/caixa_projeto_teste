/**
 * Dados_entrada_TypeACAO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_entrada_TypeACAO implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Dados_entrada_TypeACAO(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _CONSULTAR = "CONSULTAR";
    public static final java.lang.String _AGENDAR = "AGENDAR";
    public static final java.lang.String _CANCELAR_AGENDAMENTO = "CANCELAR_AGENDAMENTO";
    public static final Dados_entrada_TypeACAO CONSULTAR = new Dados_entrada_TypeACAO(_CONSULTAR);
    public static final Dados_entrada_TypeACAO AGENDAR = new Dados_entrada_TypeACAO(_AGENDAR);
    public static final Dados_entrada_TypeACAO CANCELAR_AGENDAMENTO = new Dados_entrada_TypeACAO(_CANCELAR_AGENDAMENTO);
    public java.lang.String getValue() { return _value_;}
    public static Dados_entrada_TypeACAO fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Dados_entrada_TypeACAO enumeration = (Dados_entrada_TypeACAO)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Dados_entrada_TypeACAO fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_entrada_TypeACAO.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", ">dados_entrada_Type>ACAO"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
