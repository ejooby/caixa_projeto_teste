/**
 * Conta_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Conta_Type  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedInt UNIDADE;

    private org.apache.axis.types.UnsignedShort PRODUTO;

    private org.apache.axis.types.UnsignedLong NUMERO;

    private byte DV;

    public Conta_Type() {
    }

    public Conta_Type(
           org.apache.axis.types.UnsignedInt UNIDADE,
           org.apache.axis.types.UnsignedShort PRODUTO,
           org.apache.axis.types.UnsignedLong NUMERO,
           byte DV) {
           this.UNIDADE = UNIDADE;
           this.PRODUTO = PRODUTO;
           this.NUMERO = NUMERO;
           this.DV = DV;
    }


    /**
     * Gets the UNIDADE value for this Conta_Type.
     * 
     * @return UNIDADE
     */
    public org.apache.axis.types.UnsignedInt getUNIDADE() {
        return UNIDADE;
    }


    /**
     * Sets the UNIDADE value for this Conta_Type.
     * 
     * @param UNIDADE
     */
    public void setUNIDADE(org.apache.axis.types.UnsignedInt UNIDADE) {
        this.UNIDADE = UNIDADE;
    }


    /**
     * Gets the PRODUTO value for this Conta_Type.
     * 
     * @return PRODUTO
     */
    public org.apache.axis.types.UnsignedShort getPRODUTO() {
        return PRODUTO;
    }


    /**
     * Sets the PRODUTO value for this Conta_Type.
     * 
     * @param PRODUTO
     */
    public void setPRODUTO(org.apache.axis.types.UnsignedShort PRODUTO) {
        this.PRODUTO = PRODUTO;
    }


    /**
     * Gets the NUMERO value for this Conta_Type.
     * 
     * @return NUMERO
     */
    public org.apache.axis.types.UnsignedLong getNUMERO() {
        return NUMERO;
    }


    /**
     * Sets the NUMERO value for this Conta_Type.
     * 
     * @param NUMERO
     */
    public void setNUMERO(org.apache.axis.types.UnsignedLong NUMERO) {
        this.NUMERO = NUMERO;
    }


    /**
     * Gets the DV value for this Conta_Type.
     * 
     * @return DV
     */
    public byte getDV() {
        return DV;
    }


    /**
     * Sets the DV value for this Conta_Type.
     * 
     * @param DV
     */
    public void setDV(byte DV) {
        this.DV = DV;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Conta_Type)) return false;
        Conta_Type other = (Conta_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.UNIDADE==null && other.getUNIDADE()==null) || 
             (this.UNIDADE!=null &&
              this.UNIDADE.equals(other.getUNIDADE()))) &&
            ((this.PRODUTO==null && other.getPRODUTO()==null) || 
             (this.PRODUTO!=null &&
              this.PRODUTO.equals(other.getPRODUTO()))) &&
            ((this.NUMERO==null && other.getNUMERO()==null) || 
             (this.NUMERO!=null &&
              this.NUMERO.equals(other.getNUMERO()))) &&
            this.DV == other.getDV();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUNIDADE() != null) {
            _hashCode += getUNIDADE().hashCode();
        }
        if (getPRODUTO() != null) {
            _hashCode += getPRODUTO().hashCode();
        }
        if (getNUMERO() != null) {
            _hashCode += getNUMERO().hashCode();
        }
        _hashCode += getDV();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Conta_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "conta_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UNIDADE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UNIDADE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRODUTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRODUTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DV");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
