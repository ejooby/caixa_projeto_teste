/**
 * Dados_saida_transacao_pagamento_concessionaria_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.lista;

public class Dados_saida_transacao_pagamento_concessionaria_Type  implements java.io.Serializable {
    private java.lang.Integer NSU;

    private java.lang.Long BARRA1;

    private java.lang.Long BARRA2;

    private java.lang.Long BARRA3;

    private java.lang.Long BARRA4;

    private java.lang.String EMPRESA;

    private java.lang.String IDENTIFICACAO;

    private java.lang.String DATA_PAGAMENTO;

    private java.math.BigDecimal VALOR;

    private java.util.Calendar DATA_TRANSACAO;

    private java.lang.String NUM_IDENTIFICACAO;

    private java.lang.String MSG1;

    private java.lang.String MSG2;

    private java.lang.String MSG3;

    private java.lang.String MSG4;

    private java.lang.String CANAL_ORIGEM;

    private java.lang.Integer NUMERO_DOCUMENTO;

    public Dados_saida_transacao_pagamento_concessionaria_Type() {
    }

    public Dados_saida_transacao_pagamento_concessionaria_Type(
           java.lang.Integer NSU,
           java.lang.Long BARRA1,
           java.lang.Long BARRA2,
           java.lang.Long BARRA3,
           java.lang.Long BARRA4,
           java.lang.String EMPRESA,
           java.lang.String IDENTIFICACAO,
           java.lang.String DATA_PAGAMENTO,
           java.math.BigDecimal VALOR,
           java.util.Calendar DATA_TRANSACAO,
           java.lang.String NUM_IDENTIFICACAO,
           java.lang.String MSG1,
           java.lang.String MSG2,
           java.lang.String MSG3,
           java.lang.String MSG4,
           java.lang.String CANAL_ORIGEM,
           java.lang.Integer NUMERO_DOCUMENTO) {
           this.NSU = NSU;
           this.BARRA1 = BARRA1;
           this.BARRA2 = BARRA2;
           this.BARRA3 = BARRA3;
           this.BARRA4 = BARRA4;
           this.EMPRESA = EMPRESA;
           this.IDENTIFICACAO = IDENTIFICACAO;
           this.DATA_PAGAMENTO = DATA_PAGAMENTO;
           this.VALOR = VALOR;
           this.DATA_TRANSACAO = DATA_TRANSACAO;
           this.NUM_IDENTIFICACAO = NUM_IDENTIFICACAO;
           this.MSG1 = MSG1;
           this.MSG2 = MSG2;
           this.MSG3 = MSG3;
           this.MSG4 = MSG4;
           this.CANAL_ORIGEM = CANAL_ORIGEM;
           this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
    }


    /**
     * Gets the NSU value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return NSU
     */
    public java.lang.Integer getNSU() {
        return NSU;
    }


    /**
     * Sets the NSU value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param NSU
     */
    public void setNSU(java.lang.Integer NSU) {
        this.NSU = NSU;
    }


    /**
     * Gets the BARRA1 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return BARRA1
     */
    public java.lang.Long getBARRA1() {
        return BARRA1;
    }


    /**
     * Sets the BARRA1 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param BARRA1
     */
    public void setBARRA1(java.lang.Long BARRA1) {
        this.BARRA1 = BARRA1;
    }


    /**
     * Gets the BARRA2 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return BARRA2
     */
    public java.lang.Long getBARRA2() {
        return BARRA2;
    }


    /**
     * Sets the BARRA2 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param BARRA2
     */
    public void setBARRA2(java.lang.Long BARRA2) {
        this.BARRA2 = BARRA2;
    }


    /**
     * Gets the BARRA3 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return BARRA3
     */
    public java.lang.Long getBARRA3() {
        return BARRA3;
    }


    /**
     * Sets the BARRA3 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param BARRA3
     */
    public void setBARRA3(java.lang.Long BARRA3) {
        this.BARRA3 = BARRA3;
    }


    /**
     * Gets the BARRA4 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return BARRA4
     */
    public java.lang.Long getBARRA4() {
        return BARRA4;
    }


    /**
     * Sets the BARRA4 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param BARRA4
     */
    public void setBARRA4(java.lang.Long BARRA4) {
        this.BARRA4 = BARRA4;
    }


    /**
     * Gets the EMPRESA value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return EMPRESA
     */
    public java.lang.String getEMPRESA() {
        return EMPRESA;
    }


    /**
     * Sets the EMPRESA value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param EMPRESA
     */
    public void setEMPRESA(java.lang.String EMPRESA) {
        this.EMPRESA = EMPRESA;
    }


    /**
     * Gets the IDENTIFICACAO value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return IDENTIFICACAO
     */
    public java.lang.String getIDENTIFICACAO() {
        return IDENTIFICACAO;
    }


    /**
     * Sets the IDENTIFICACAO value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param IDENTIFICACAO
     */
    public void setIDENTIFICACAO(java.lang.String IDENTIFICACAO) {
        this.IDENTIFICACAO = IDENTIFICACAO;
    }


    /**
     * Gets the DATA_PAGAMENTO value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return DATA_PAGAMENTO
     */
    public java.lang.String getDATA_PAGAMENTO() {
        return DATA_PAGAMENTO;
    }


    /**
     * Sets the DATA_PAGAMENTO value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param DATA_PAGAMENTO
     */
    public void setDATA_PAGAMENTO(java.lang.String DATA_PAGAMENTO) {
        this.DATA_PAGAMENTO = DATA_PAGAMENTO;
    }


    /**
     * Gets the VALOR value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return VALOR
     */
    public java.math.BigDecimal getVALOR() {
        return VALOR;
    }


    /**
     * Sets the VALOR value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param VALOR
     */
    public void setVALOR(java.math.BigDecimal VALOR) {
        this.VALOR = VALOR;
    }


    /**
     * Gets the DATA_TRANSACAO value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return DATA_TRANSACAO
     */
    public java.util.Calendar getDATA_TRANSACAO() {
        return DATA_TRANSACAO;
    }


    /**
     * Sets the DATA_TRANSACAO value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param DATA_TRANSACAO
     */
    public void setDATA_TRANSACAO(java.util.Calendar DATA_TRANSACAO) {
        this.DATA_TRANSACAO = DATA_TRANSACAO;
    }


    /**
     * Gets the NUM_IDENTIFICACAO value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return NUM_IDENTIFICACAO
     */
    public java.lang.String getNUM_IDENTIFICACAO() {
        return NUM_IDENTIFICACAO;
    }


    /**
     * Sets the NUM_IDENTIFICACAO value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param NUM_IDENTIFICACAO
     */
    public void setNUM_IDENTIFICACAO(java.lang.String NUM_IDENTIFICACAO) {
        this.NUM_IDENTIFICACAO = NUM_IDENTIFICACAO;
    }


    /**
     * Gets the MSG1 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return MSG1
     */
    public java.lang.String getMSG1() {
        return MSG1;
    }


    /**
     * Sets the MSG1 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param MSG1
     */
    public void setMSG1(java.lang.String MSG1) {
        this.MSG1 = MSG1;
    }


    /**
     * Gets the MSG2 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return MSG2
     */
    public java.lang.String getMSG2() {
        return MSG2;
    }


    /**
     * Sets the MSG2 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param MSG2
     */
    public void setMSG2(java.lang.String MSG2) {
        this.MSG2 = MSG2;
    }


    /**
     * Gets the MSG3 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return MSG3
     */
    public java.lang.String getMSG3() {
        return MSG3;
    }


    /**
     * Sets the MSG3 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param MSG3
     */
    public void setMSG3(java.lang.String MSG3) {
        this.MSG3 = MSG3;
    }


    /**
     * Gets the MSG4 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return MSG4
     */
    public java.lang.String getMSG4() {
        return MSG4;
    }


    /**
     * Sets the MSG4 value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param MSG4
     */
    public void setMSG4(java.lang.String MSG4) {
        this.MSG4 = MSG4;
    }


    /**
     * Gets the CANAL_ORIGEM value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return CANAL_ORIGEM
     */
    public java.lang.String getCANAL_ORIGEM() {
        return CANAL_ORIGEM;
    }


    /**
     * Sets the CANAL_ORIGEM value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param CANAL_ORIGEM
     */
    public void setCANAL_ORIGEM(java.lang.String CANAL_ORIGEM) {
        this.CANAL_ORIGEM = CANAL_ORIGEM;
    }


    /**
     * Gets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @return NUMERO_DOCUMENTO
     */
    public java.lang.Integer getNUMERO_DOCUMENTO() {
        return NUMERO_DOCUMENTO;
    }


    /**
     * Sets the NUMERO_DOCUMENTO value for this Dados_saida_transacao_pagamento_concessionaria_Type.
     * 
     * @param NUMERO_DOCUMENTO
     */
    public void setNUMERO_DOCUMENTO(java.lang.Integer NUMERO_DOCUMENTO) {
        this.NUMERO_DOCUMENTO = NUMERO_DOCUMENTO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_pagamento_concessionaria_Type)) return false;
        Dados_saida_transacao_pagamento_concessionaria_Type other = (Dados_saida_transacao_pagamento_concessionaria_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NSU==null && other.getNSU()==null) || 
             (this.NSU!=null &&
              this.NSU.equals(other.getNSU()))) &&
            ((this.BARRA1==null && other.getBARRA1()==null) || 
             (this.BARRA1!=null &&
              this.BARRA1.equals(other.getBARRA1()))) &&
            ((this.BARRA2==null && other.getBARRA2()==null) || 
             (this.BARRA2!=null &&
              this.BARRA2.equals(other.getBARRA2()))) &&
            ((this.BARRA3==null && other.getBARRA3()==null) || 
             (this.BARRA3!=null &&
              this.BARRA3.equals(other.getBARRA3()))) &&
            ((this.BARRA4==null && other.getBARRA4()==null) || 
             (this.BARRA4!=null &&
              this.BARRA4.equals(other.getBARRA4()))) &&
            ((this.EMPRESA==null && other.getEMPRESA()==null) || 
             (this.EMPRESA!=null &&
              this.EMPRESA.equals(other.getEMPRESA()))) &&
            ((this.IDENTIFICACAO==null && other.getIDENTIFICACAO()==null) || 
             (this.IDENTIFICACAO!=null &&
              this.IDENTIFICACAO.equals(other.getIDENTIFICACAO()))) &&
            ((this.DATA_PAGAMENTO==null && other.getDATA_PAGAMENTO()==null) || 
             (this.DATA_PAGAMENTO!=null &&
              this.DATA_PAGAMENTO.equals(other.getDATA_PAGAMENTO()))) &&
            ((this.VALOR==null && other.getVALOR()==null) || 
             (this.VALOR!=null &&
              this.VALOR.equals(other.getVALOR()))) &&
            ((this.DATA_TRANSACAO==null && other.getDATA_TRANSACAO()==null) || 
             (this.DATA_TRANSACAO!=null &&
              this.DATA_TRANSACAO.equals(other.getDATA_TRANSACAO()))) &&
            ((this.NUM_IDENTIFICACAO==null && other.getNUM_IDENTIFICACAO()==null) || 
             (this.NUM_IDENTIFICACAO!=null &&
              this.NUM_IDENTIFICACAO.equals(other.getNUM_IDENTIFICACAO()))) &&
            ((this.MSG1==null && other.getMSG1()==null) || 
             (this.MSG1!=null &&
              this.MSG1.equals(other.getMSG1()))) &&
            ((this.MSG2==null && other.getMSG2()==null) || 
             (this.MSG2!=null &&
              this.MSG2.equals(other.getMSG2()))) &&
            ((this.MSG3==null && other.getMSG3()==null) || 
             (this.MSG3!=null &&
              this.MSG3.equals(other.getMSG3()))) &&
            ((this.MSG4==null && other.getMSG4()==null) || 
             (this.MSG4!=null &&
              this.MSG4.equals(other.getMSG4()))) &&
            ((this.CANAL_ORIGEM==null && other.getCANAL_ORIGEM()==null) || 
             (this.CANAL_ORIGEM!=null &&
              this.CANAL_ORIGEM.equals(other.getCANAL_ORIGEM()))) &&
            ((this.NUMERO_DOCUMENTO==null && other.getNUMERO_DOCUMENTO()==null) || 
             (this.NUMERO_DOCUMENTO!=null &&
              this.NUMERO_DOCUMENTO.equals(other.getNUMERO_DOCUMENTO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNSU() != null) {
            _hashCode += getNSU().hashCode();
        }
        if (getBARRA1() != null) {
            _hashCode += getBARRA1().hashCode();
        }
        if (getBARRA2() != null) {
            _hashCode += getBARRA2().hashCode();
        }
        if (getBARRA3() != null) {
            _hashCode += getBARRA3().hashCode();
        }
        if (getBARRA4() != null) {
            _hashCode += getBARRA4().hashCode();
        }
        if (getEMPRESA() != null) {
            _hashCode += getEMPRESA().hashCode();
        }
        if (getIDENTIFICACAO() != null) {
            _hashCode += getIDENTIFICACAO().hashCode();
        }
        if (getDATA_PAGAMENTO() != null) {
            _hashCode += getDATA_PAGAMENTO().hashCode();
        }
        if (getVALOR() != null) {
            _hashCode += getVALOR().hashCode();
        }
        if (getDATA_TRANSACAO() != null) {
            _hashCode += getDATA_TRANSACAO().hashCode();
        }
        if (getNUM_IDENTIFICACAO() != null) {
            _hashCode += getNUM_IDENTIFICACAO().hashCode();
        }
        if (getMSG1() != null) {
            _hashCode += getMSG1().hashCode();
        }
        if (getMSG2() != null) {
            _hashCode += getMSG2().hashCode();
        }
        if (getMSG3() != null) {
            _hashCode += getMSG3().hashCode();
        }
        if (getMSG4() != null) {
            _hashCode += getMSG4().hashCode();
        }
        if (getCANAL_ORIGEM() != null) {
            _hashCode += getCANAL_ORIGEM().hashCode();
        }
        if (getNUMERO_DOCUMENTO() != null) {
            _hashCode += getNUMERO_DOCUMENTO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_pagamento_concessionaria_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/lista", "dados_saida_transacao_pagamento_concessionaria_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NSU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BARRA1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BARRA1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BARRA2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BARRA2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BARRA3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BARRA3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BARRA4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BARRA4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMPRESA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EMPRESA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_PAGAMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_PAGAMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUM_IDENTIFICACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUM_IDENTIFICACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MSG1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MSG2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MSG3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MSG4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CANAL_ORIGEM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CANAL_ORIGEM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMERO_DOCUMENTO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NUMERO_DOCUMENTO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
