/**
 * Dados_saida_transacao_fgts_Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.gov.caixa.sibar.consulta_transacoes_conta.detalhe;

public class Dados_saida_transacao_fgts_Type  implements java.io.Serializable {
    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO;

    private br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Cpf_cgc_Type DOCUMENTO_TOMADOR;

    private java.math.BigDecimal VALOR_DEPOSITO;

    private java.math.BigDecimal VALOR_MULTA;

    private java.math.BigDecimal JUROS_CORRECAO;

    private java.lang.String LACRE_CONECTIVIDADE;

    private java.lang.String CODIGO_BARRAS;

    private java.lang.String IDENTIFICADOR;

    private java.lang.String IDENTIFICACAO_CLIENTE;

    private org.apache.axis.types.Time HORA_TRANSACAO;

    public Dados_saida_transacao_fgts_Type() {
    }

    public Dados_saida_transacao_fgts_Type(
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO,
           br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Cpf_cgc_Type DOCUMENTO_TOMADOR,
           java.math.BigDecimal VALOR_DEPOSITO,
           java.math.BigDecimal VALOR_MULTA,
           java.math.BigDecimal JUROS_CORRECAO,
           java.lang.String LACRE_CONECTIVIDADE,
           java.lang.String CODIGO_BARRAS,
           java.lang.String IDENTIFICADOR,
           java.lang.String IDENTIFICACAO_CLIENTE,
           org.apache.axis.types.Time HORA_TRANSACAO) {
           this.DADOS_TRANSACAO = DADOS_TRANSACAO;
           this.DOCUMENTO_TOMADOR = DOCUMENTO_TOMADOR;
           this.VALOR_DEPOSITO = VALOR_DEPOSITO;
           this.VALOR_MULTA = VALOR_MULTA;
           this.JUROS_CORRECAO = JUROS_CORRECAO;
           this.LACRE_CONECTIVIDADE = LACRE_CONECTIVIDADE;
           this.CODIGO_BARRAS = CODIGO_BARRAS;
           this.IDENTIFICADOR = IDENTIFICADOR;
           this.IDENTIFICACAO_CLIENTE = IDENTIFICACAO_CLIENTE;
           this.HORA_TRANSACAO = HORA_TRANSACAO;
    }


    /**
     * Gets the DADOS_TRANSACAO value for this Dados_saida_transacao_fgts_Type.
     * 
     * @return DADOS_TRANSACAO
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type getDADOS_TRANSACAO() {
        return DADOS_TRANSACAO;
    }


    /**
     * Sets the DADOS_TRANSACAO value for this Dados_saida_transacao_fgts_Type.
     * 
     * @param DADOS_TRANSACAO
     */
    public void setDADOS_TRANSACAO(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Dados_saida_geral_Type DADOS_TRANSACAO) {
        this.DADOS_TRANSACAO = DADOS_TRANSACAO;
    }


    /**
     * Gets the DOCUMENTO_TOMADOR value for this Dados_saida_transacao_fgts_Type.
     * 
     * @return DOCUMENTO_TOMADOR
     */
    public br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Cpf_cgc_Type getDOCUMENTO_TOMADOR() {
        return DOCUMENTO_TOMADOR;
    }


    /**
     * Sets the DOCUMENTO_TOMADOR value for this Dados_saida_transacao_fgts_Type.
     * 
     * @param DOCUMENTO_TOMADOR
     */
    public void setDOCUMENTO_TOMADOR(br.gov.caixa.sibar.consulta_transacoes_conta.detalhe.Cpf_cgc_Type DOCUMENTO_TOMADOR) {
        this.DOCUMENTO_TOMADOR = DOCUMENTO_TOMADOR;
    }


    /**
     * Gets the VALOR_DEPOSITO value for this Dados_saida_transacao_fgts_Type.
     * 
     * @return VALOR_DEPOSITO
     */
    public java.math.BigDecimal getVALOR_DEPOSITO() {
        return VALOR_DEPOSITO;
    }


    /**
     * Sets the VALOR_DEPOSITO value for this Dados_saida_transacao_fgts_Type.
     * 
     * @param VALOR_DEPOSITO
     */
    public void setVALOR_DEPOSITO(java.math.BigDecimal VALOR_DEPOSITO) {
        this.VALOR_DEPOSITO = VALOR_DEPOSITO;
    }


    /**
     * Gets the VALOR_MULTA value for this Dados_saida_transacao_fgts_Type.
     * 
     * @return VALOR_MULTA
     */
    public java.math.BigDecimal getVALOR_MULTA() {
        return VALOR_MULTA;
    }


    /**
     * Sets the VALOR_MULTA value for this Dados_saida_transacao_fgts_Type.
     * 
     * @param VALOR_MULTA
     */
    public void setVALOR_MULTA(java.math.BigDecimal VALOR_MULTA) {
        this.VALOR_MULTA = VALOR_MULTA;
    }


    /**
     * Gets the JUROS_CORRECAO value for this Dados_saida_transacao_fgts_Type.
     * 
     * @return JUROS_CORRECAO
     */
    public java.math.BigDecimal getJUROS_CORRECAO() {
        return JUROS_CORRECAO;
    }


    /**
     * Sets the JUROS_CORRECAO value for this Dados_saida_transacao_fgts_Type.
     * 
     * @param JUROS_CORRECAO
     */
    public void setJUROS_CORRECAO(java.math.BigDecimal JUROS_CORRECAO) {
        this.JUROS_CORRECAO = JUROS_CORRECAO;
    }


    /**
     * Gets the LACRE_CONECTIVIDADE value for this Dados_saida_transacao_fgts_Type.
     * 
     * @return LACRE_CONECTIVIDADE
     */
    public java.lang.String getLACRE_CONECTIVIDADE() {
        return LACRE_CONECTIVIDADE;
    }


    /**
     * Sets the LACRE_CONECTIVIDADE value for this Dados_saida_transacao_fgts_Type.
     * 
     * @param LACRE_CONECTIVIDADE
     */
    public void setLACRE_CONECTIVIDADE(java.lang.String LACRE_CONECTIVIDADE) {
        this.LACRE_CONECTIVIDADE = LACRE_CONECTIVIDADE;
    }


    /**
     * Gets the CODIGO_BARRAS value for this Dados_saida_transacao_fgts_Type.
     * 
     * @return CODIGO_BARRAS
     */
    public java.lang.String getCODIGO_BARRAS() {
        return CODIGO_BARRAS;
    }


    /**
     * Sets the CODIGO_BARRAS value for this Dados_saida_transacao_fgts_Type.
     * 
     * @param CODIGO_BARRAS
     */
    public void setCODIGO_BARRAS(java.lang.String CODIGO_BARRAS) {
        this.CODIGO_BARRAS = CODIGO_BARRAS;
    }


    /**
     * Gets the IDENTIFICADOR value for this Dados_saida_transacao_fgts_Type.
     * 
     * @return IDENTIFICADOR
     */
    public java.lang.String getIDENTIFICADOR() {
        return IDENTIFICADOR;
    }


    /**
     * Sets the IDENTIFICADOR value for this Dados_saida_transacao_fgts_Type.
     * 
     * @param IDENTIFICADOR
     */
    public void setIDENTIFICADOR(java.lang.String IDENTIFICADOR) {
        this.IDENTIFICADOR = IDENTIFICADOR;
    }


    /**
     * Gets the IDENTIFICACAO_CLIENTE value for this Dados_saida_transacao_fgts_Type.
     * 
     * @return IDENTIFICACAO_CLIENTE
     */
    public java.lang.String getIDENTIFICACAO_CLIENTE() {
        return IDENTIFICACAO_CLIENTE;
    }


    /**
     * Sets the IDENTIFICACAO_CLIENTE value for this Dados_saida_transacao_fgts_Type.
     * 
     * @param IDENTIFICACAO_CLIENTE
     */
    public void setIDENTIFICACAO_CLIENTE(java.lang.String IDENTIFICACAO_CLIENTE) {
        this.IDENTIFICACAO_CLIENTE = IDENTIFICACAO_CLIENTE;
    }


    /**
     * Gets the HORA_TRANSACAO value for this Dados_saida_transacao_fgts_Type.
     * 
     * @return HORA_TRANSACAO
     */
    public org.apache.axis.types.Time getHORA_TRANSACAO() {
        return HORA_TRANSACAO;
    }


    /**
     * Sets the HORA_TRANSACAO value for this Dados_saida_transacao_fgts_Type.
     * 
     * @param HORA_TRANSACAO
     */
    public void setHORA_TRANSACAO(org.apache.axis.types.Time HORA_TRANSACAO) {
        this.HORA_TRANSACAO = HORA_TRANSACAO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_saida_transacao_fgts_Type)) return false;
        Dados_saida_transacao_fgts_Type other = (Dados_saida_transacao_fgts_Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DADOS_TRANSACAO==null && other.getDADOS_TRANSACAO()==null) || 
             (this.DADOS_TRANSACAO!=null &&
              this.DADOS_TRANSACAO.equals(other.getDADOS_TRANSACAO()))) &&
            ((this.DOCUMENTO_TOMADOR==null && other.getDOCUMENTO_TOMADOR()==null) || 
             (this.DOCUMENTO_TOMADOR!=null &&
              this.DOCUMENTO_TOMADOR.equals(other.getDOCUMENTO_TOMADOR()))) &&
            ((this.VALOR_DEPOSITO==null && other.getVALOR_DEPOSITO()==null) || 
             (this.VALOR_DEPOSITO!=null &&
              this.VALOR_DEPOSITO.equals(other.getVALOR_DEPOSITO()))) &&
            ((this.VALOR_MULTA==null && other.getVALOR_MULTA()==null) || 
             (this.VALOR_MULTA!=null &&
              this.VALOR_MULTA.equals(other.getVALOR_MULTA()))) &&
            ((this.JUROS_CORRECAO==null && other.getJUROS_CORRECAO()==null) || 
             (this.JUROS_CORRECAO!=null &&
              this.JUROS_CORRECAO.equals(other.getJUROS_CORRECAO()))) &&
            ((this.LACRE_CONECTIVIDADE==null && other.getLACRE_CONECTIVIDADE()==null) || 
             (this.LACRE_CONECTIVIDADE!=null &&
              this.LACRE_CONECTIVIDADE.equals(other.getLACRE_CONECTIVIDADE()))) &&
            ((this.CODIGO_BARRAS==null && other.getCODIGO_BARRAS()==null) || 
             (this.CODIGO_BARRAS!=null &&
              this.CODIGO_BARRAS.equals(other.getCODIGO_BARRAS()))) &&
            ((this.IDENTIFICADOR==null && other.getIDENTIFICADOR()==null) || 
             (this.IDENTIFICADOR!=null &&
              this.IDENTIFICADOR.equals(other.getIDENTIFICADOR()))) &&
            ((this.IDENTIFICACAO_CLIENTE==null && other.getIDENTIFICACAO_CLIENTE()==null) || 
             (this.IDENTIFICACAO_CLIENTE!=null &&
              this.IDENTIFICACAO_CLIENTE.equals(other.getIDENTIFICACAO_CLIENTE()))) &&
            ((this.HORA_TRANSACAO==null && other.getHORA_TRANSACAO()==null) || 
             (this.HORA_TRANSACAO!=null &&
              this.HORA_TRANSACAO.equals(other.getHORA_TRANSACAO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDADOS_TRANSACAO() != null) {
            _hashCode += getDADOS_TRANSACAO().hashCode();
        }
        if (getDOCUMENTO_TOMADOR() != null) {
            _hashCode += getDOCUMENTO_TOMADOR().hashCode();
        }
        if (getVALOR_DEPOSITO() != null) {
            _hashCode += getVALOR_DEPOSITO().hashCode();
        }
        if (getVALOR_MULTA() != null) {
            _hashCode += getVALOR_MULTA().hashCode();
        }
        if (getJUROS_CORRECAO() != null) {
            _hashCode += getJUROS_CORRECAO().hashCode();
        }
        if (getLACRE_CONECTIVIDADE() != null) {
            _hashCode += getLACRE_CONECTIVIDADE().hashCode();
        }
        if (getCODIGO_BARRAS() != null) {
            _hashCode += getCODIGO_BARRAS().hashCode();
        }
        if (getIDENTIFICADOR() != null) {
            _hashCode += getIDENTIFICADOR().hashCode();
        }
        if (getIDENTIFICACAO_CLIENTE() != null) {
            _hashCode += getIDENTIFICACAO_CLIENTE().hashCode();
        }
        if (getHORA_TRANSACAO() != null) {
            _hashCode += getHORA_TRANSACAO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_saida_transacao_fgts_Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_transacao_fgts_Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DADOS_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DADOS_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "dados_saida_geral_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOCUMENTO_TOMADOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DOCUMENTO_TOMADOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://caixa.gov.br/sibar/consulta_transacoes_conta/detalhe", "cpf_cgc_Type"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_DEPOSITO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_DEPOSITO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VALOR_MULTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VALOR_MULTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("JUROS_CORRECAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "JUROS_CORRECAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LACRE_CONECTIVIDADE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LACRE_CONECTIVIDADE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CODIGO_BARRAS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CODIGO_BARRAS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICADOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICADOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDENTIFICACAO_CLIENTE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IDENTIFICACAO_CLIENTE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HORA_TRANSACAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HORA_TRANSACAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
